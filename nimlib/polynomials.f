!-------------------------------------------------------------------------------
!< Defines the classes for evaluation of different types of polynomials. The
!  base class [[lagr_1d]] provides nodal storage and some utility functions. The
!  class [[gauleg]] extends [[lagr_1d]] providing storage for quadrature
!  weights.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the classes for evaluation of different types of polynomials. The
!  base class [[lagr_1d]] provides nodal storage and some utility functions. The
!  class [[gauleg]] extends [[lagr_1d]] providing storage for quadrature
!  weights.
!-------------------------------------------------------------------------------
MODULE poly_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: lagr_1d, gauleg, lobleg

  INTERFACE legendre_poly
    MODULE PROCEDURE legendre_poly_val, legendre_poly_many
  END INTERFACE legendre_poly

  INTERFACE legendre_polyd
    MODULE PROCEDURE legendre_polyd_val, legendre_polyd_many
  END INTERFACE legendre_polyd

!-------------------------------------------------------------------------------
!> Base type that provides commonly needed storage and a unified evaluation
!  scheme.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: lagr_1d
    PRIVATE

    INTEGER(i4) :: pd = 1 !! polynomial degree of the basis set
    !* abscissae/nodal locations
    REAL(r8), DIMENSION(:), ALLOCATABLE :: nodes
    !* coefficients to transform from Legendre polynomials to the basis set
    REAL(r8), DIMENSION(:, :), ALLOCATABLE :: cardcoefs
  CONTAINS

    PROCEDURE, PASS(basis) :: getpd => poly_degree
    PROCEDURE, PASS(basis) :: getnodes => poly_nodes
    PROCEDURE, PASS(basis), PRIVATE :: legendre_eval_val
    PROCEDURE, PASS(basis), PRIVATE :: legendre_deriv_val
    PROCEDURE, PASS(basis), PRIVATE :: legendre_eval_many
    PROCEDURE, PASS(basis), PRIVATE :: legendre_deriv_many
    GENERIC :: eval => legendre_eval_val, legendre_eval_many
    GENERIC :: eval_d => legendre_deriv_val, legendre_deriv_many
  END TYPE lagr_1d

!-------------------------------------------------------------------------------
!> This type is responsible for generating the abscissae and weights for
!  Gauss-Legendre integration. The abscissae are also the nodal positions for
!  the associated Lagrange cardinal functions, which can also be evaluated.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(lagr_1d) :: gauleg
    PRIVATE

    !* weights for Gauss-Legendre integration
    REAL(r8), DIMENSION(:), ALLOCATABLE :: weights
  CONTAINS

    PROCEDURE, PASS(basis) :: init => initialize_gauleg
    PROCEDURE, PASS(basis) :: dealloc => deallocate_gauleg
    PROCEDURE, PASS(basis) :: getweights => getweights_gauleg
  END TYPE gauleg

!-------------------------------------------------------------------------------
!> This type is responsible for generating the abscissae and weights for
!  Lobatto-Legendre integration. The abscissae are also the nodal positions for
!  the associated Lagrange cardinal functions, which can also be evaluated.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(lagr_1d) :: lobleg
    PRIVATE

    !* weights for Lobatto-Legendre integration
    REAL(r8), DIMENSION(:), ALLOCATABLE :: weights
  CONTAINS

    PROCEDURE, PASS(basis) :: init => initialize_lobleg
    PROCEDURE, PASS(basis) :: dealloc => deallocate_lobleg
    PROCEDURE, PASS(basis) :: getweights => getweights_lobleg
  END TYPE lobleg

CONTAINS

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the sequence of Legendre polynomial
!  evaluations at position x in [-1, 1] up to order n.
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_poly_val(x, n, p)
    IMPLICIT NONE

    REAL(r8), INTENT(IN) :: x !! abscissa expected in [-1, 1]
    INTEGER(i4), INTENT(IN) :: n !! highest polynomial degree
    !* array of Legendre evaluations
    REAL(r8), DIMENSION(0:n), INTENT(OUT) :: p

    INTEGER(i4) :: i

    p(0) = 1._r8
    IF (n == 0) RETURN
    p(1) = x
    DO i = 2, n
      p(i) = (REAL(2*i - 1, r8)*x*p(i-1) - REAL(i - 1, r8)*p(i-2))/REAL(i, r8)
    ENDDO
  END SUBROUTINE legendre_poly_val

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the sequence of Legendre polynomial
!  derivative evaluations at position x in [-1, 1] up to order n.
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_polyd_val(x, n, dp)
    IMPLICIT NONE

    REAL(r8), INTENT(IN) :: x !! abscissa expected in [-1, 1]
    INTEGER(i4), INTENT(IN) :: n !! highest polynomial degree
    !* array of Legendre derivative evaluations
    REAL(r8), DIMENSION(0:n), INTENT(OUT) :: dp

    REAL(r8), DIMENSION(0:n-1) :: p
    INTEGER(i4) :: i

    dp(0) = 0._r8
    IF (n == 0) RETURN
    dp(1) = 1._r8
    CALL legendre_poly_val(x, n-1, p)
    DO i = 2, n
      dp(i) = REAL(2*i - 1, r8)*p(i-1) + dp(i-2)
    ENDDO
  END SUBROUTINE legendre_polyd_val

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the sequence of Legendre polynomial
!  evaluations at position x in [-1, 1] up to order n.
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_poly_many(x, n, p)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:), INTENT(IN) :: x !! abscissa expected in [-1, 1]
    INTEGER(i4), INTENT(IN) :: n !! highest polynomial degree
    !* array of Legendre evaluations
    REAL(r8), DIMENSION(SIZE(x), 0:n), INTENT(OUT) :: p

    INTEGER(i4) :: i

    p(:, 0) = 1._r8
    IF (n == 0) RETURN
    p(:, 1) = x
    DO i = 2, n
      p(:, i) = (REAL(2*i-1,r8)*x*p(:, i-1) - REAL(i-1,r8)*p(:, i-2))/REAL(i,r8)
    ENDDO
  END SUBROUTINE legendre_poly_many

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the sequence of Legendre polynomial
!  derivative evaluations at position x in [-1, 1] up to order n.
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_polyd_many(x, n, dp)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:), INTENT(IN) :: x !! abscissa expected in [-1, 1]
    INTEGER(i4), INTENT(IN) :: n !! highest polynomial degree
    !* array of Legendre derivative evaluations
    REAL(r8), DIMENSION(SIZE(x), 0:n), INTENT(OUT) :: dp

    REAL(r8), DIMENSION(SIZE(x), 0:n) :: p
    INTEGER(i4) :: i

    dp(:, 0) = 0._r8
    IF (n == 0) RETURN
    dp(:, 1) = 1._r8
    CALL legendre_poly_many(x, n, p)
    DO i = 2, n
      dp(:, i) = REAL(2*i - 1, r8)*p(:, i-1) + dp(:, i-2)
    ENDDO
  END SUBROUTINE legendre_polyd_many

!-------------------------------------------------------------------------------
!* Queries the polynomial degree of the basis.
!-------------------------------------------------------------------------------
  PURE INTEGER(i4) FUNCTION poly_degree(basis)
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(IN) :: basis

    poly_degree = basis%pd
  END FUNCTION poly_degree

!-------------------------------------------------------------------------------
!> This subroutine retreives the scaled nodal locations for any set of Lagrange
!  cardinal functions.
!-------------------------------------------------------------------------------
  SUBROUTINE poly_nodes(basis, x, x1, x2)
    IMPLICIT NONE

    !* Minimum abscissa for scaling. Defaults to 0.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x1
    !* Maximum abscissa for scaling. Defaults to 1.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x2
    CLASS(lagr_1d), INTENT(IN) :: basis
    !* Scaled nodal locations. Index from 1 to poly_degree + 1.
    REAL(r8), DIMENSION(0:basis%pd), INTENT(OUT) :: x

    REAL(r8) :: xmin, xmax

    IF (PRESENT(x1)) THEN
      xmin = x1
    ELSE
      xmin = 0._r8
    ENDIF
    IF (PRESENT(x1)) THEN
      xmax = x2
    ELSE
      xmax = 1._r8
    ENDIF
    x = 0.5_r8*((xmax - xmin)*basis%nodes + xmax + xmin)
  END SUBROUTINE poly_nodes

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the Gauss-Legendre cardinal subroutine
!  evaluations for the position x in [0, 1].
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_eval_val(basis, x, lagr)
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(IN) :: basis
    REAL(r8), INTENT(IN) :: x !! abscissa expected in [0, 1]
    !* array for value of cardinal functions
    REAL(r8), DIMENSION(0:basis%pd), INTENT(OUT) :: lagr

    REAL(r8), DIMENSION(0:basis%pd) :: p

    CALL legendre_poly(2._r8*x - 1._r8, basis%pd, p)
#ifdef HAVE_BLAS
    CALL dgemv('t', basis%pd+1, basis%pd+1, 1._r8, basis%cardcoefs, basis%pd+1, &
               p, 1, 0._r8, lagr, 1)
#else
    lagr = MATMUL(p, basis%cardcoefs)
#endif
  END SUBROUTINE legendre_eval_val

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the Gauss-Legendre cardinal subroutine
!  derivative evaluations for the position x in [0, 1].
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_deriv_val(basis, x, dlagr)
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(IN) :: basis
    REAL(r8), INTENT(IN) :: x !! abscissa expected in [0, 1]
    !* array for derivative of cardinal functions
    REAL(r8), DIMENSION(0:basis%pd), INTENT(OUT) :: dlagr

    REAL(r8), DIMENSION(0:basis%pd) :: dp

    CALL legendre_polyd(2._r8*x - 1._r8, basis%pd, dp)
#ifdef HAVE_BLAS
    CALL dgemv('t', basis%pd+1, basis%pd+1, 2._r8, basis%cardcoefs, basis%pd+1, &
               dp, 1, 0._r8, dlagr, 1)
#else
    dlagr = MATMUL(2._r8*dp, basis%cardcoefs)
#endif
  END SUBROUTINE legendre_deriv_val

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the Gauss-Legendre cardinal subroutine
!  evaluations for the position x in [0, 1].
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_eval_many(basis, x, lagr)
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(IN) :: basis
    REAL(r8), DIMENSION(:), INTENT(IN) :: x !! abscissa expected in [0, 1]
    !* array for value of cardinal functions
    REAL(r8), DIMENSION(SIZE(x),0:basis%pd), INTENT(OUT) :: lagr

    REAL(r8), DIMENSION(SIZE(x),0:basis%pd) :: p
    INTEGER(i4) :: nx

    nx = SIZE(x)
    CALL legendre_poly(2._r8*x - 1._r8, basis%pd, p)
#ifdef HAVE_BLAS
    CALL dgemm('n', 'n', nx, basis%pd+1, basis%pd+1, 1._r8, p, nx,              &
               basis%cardcoefs, basis%pd+1, 0._r8, lagr, nx)
#else
    lagr = MATMUL(p, basis%cardcoefs)
#endif
  END SUBROUTINE legendre_eval_many

!-------------------------------------------------------------------------------
!> This subroutine returns an array of the Gauss-Legendre cardinal subroutine
!  derivative evaluations for the position x in [0, 1].
!-------------------------------------------------------------------------------
  SUBROUTINE legendre_deriv_many(basis, x, dlagr)
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(IN) :: basis
    REAL(r8), DIMENSION(:), INTENT(IN) :: x !! abscissa expected in [0, 1]
    !* array for derivative of cardinal functions
    REAL(r8), DIMENSION(SIZE(x),0:basis%pd), INTENT(OUT) :: dlagr

    REAL(r8), DIMENSION(SIZE(x),0:basis%pd) :: dp
    INTEGER(i4) :: nx

    nx = SIZE(x)
    CALL legendre_polyd(2._r8*x - 1._r8, basis%pd, dp)
#ifdef HAVE_BLAS
    CALL dgemm('n', 'n', nx, basis%pd+1, basis%pd+1, 2._r8, dp, nx,             &
           basis%cardcoefs, basis%pd+1, 0._r8, dlagr, nx)
#else
    dlagr = MATMUL(2._r8*dp, basis%cardcoefs)
#endif
  END SUBROUTINE legendre_deriv_many

!-------------------------------------------------------------------------------
!> This subroutine initializes the Gauss-Legendre abscissae and weights as well
!  as the transformation matrix from Legendre to cardinal functions.
!-------------------------------------------------------------------------------
  SUBROUTINE initialize_gauleg(basis, poly_deg)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: poly_deg !! polynomial degree
    CLASS(gauleg), INTENT(OUT) :: basis

    REAL(r8), DIMENSION(0:poly_deg+1) :: p, dp
    REAL(r8), DIMENSION(0:poly_deg) :: phi, w
    REAL(r8), PARAMETER :: eps = EPSILON(1._r8)/2._r8
    REAL(r8) :: x, x_old, err
    INTEGER(i4) :: i

    basis%pd = poly_deg
    IF (ALLOCATED(basis%cardcoefs)) THEN
      IF (basis%getpd()==poly_deg) THEN
        RETURN
      ELSE
        CALL par%nim_stop("initialize_gauleg tried to reallocate")
      ENDIF
    ENDIF
    ALLOCATE(basis%nodes(0:poly_deg))
    ALLOCATE(basis%cardcoefs(0:poly_deg,0:poly_deg))
    ALLOCATE(basis%weights(0:poly_deg))
!-------------------------------------------------------------------------------
!   Use Newton's method to find half the nodes
!-------------------------------------------------------------------------------
    DO i = 0, (poly_deg-1)/2
      x = COS(pi*REAL(2*i + 1, r8)/(2._r8*REAL(poly_deg + 1, r8)))
      err = 1._r8
      DO WHILE (err > eps)
        x_old = x
        CALL legendre_poly(x, poly_deg + 1, p)
        CALL legendre_polyd(x, poly_deg + 1, dp)
        x = x_old - p(poly_deg+1)/dp(poly_deg+1)
        err = ABS(x_old - x)
      ENDDO
      basis%nodes(i) = -x
      basis%nodes(poly_deg-i) = x
      CALL legendre_polyd(x, poly_deg + 1, dp)
      basis%weights(i) = 2._r8/((1._r8 - x**2)*dp(poly_deg+1)**2)
      basis%weights(poly_deg-i) = basis%weights(i)
    ENDDO
    IF (MOD(poly_deg,2) == 0) THEN
      basis%nodes(poly_deg/2) = 0._r8
      CALL legendre_polyd(0._r8, poly_deg + 1, dp)
      basis%weights(poly_deg/2) = 2._r8/(dp(poly_deg+1)**2)
    ENDIF
!-------------------------------------------------------------------------------
!   Find the elements of the transformation matrix from Legendre polynomials to
!   cardinal functions
!-------------------------------------------------------------------------------
    DO i = 0, poly_deg
      w(i) = 0.5_r8*REAL(2*i + 1, r8)
    ENDDO
    DO i = 0, poly_deg
      CALL legendre_poly(basis%nodes(i), poly_deg, phi)
      basis%cardcoefs(0:poly_deg, i) = phi*basis%weights(i)*w
    ENDDO
  END SUBROUTINE initialize_gauleg

!-------------------------------------------------------------------------------
!* Deallocate storage for [[gauleg]].
!-------------------------------------------------------------------------------
  SUBROUTINE deallocate_gauleg(basis)
    IMPLICIT NONE

    CLASS(gauleg), INTENT(INOUT) :: basis

    IF (ALLOCATED(basis%weights)) DEALLOCATE(basis%weights)
    IF (ALLOCATED(basis%cardcoefs)) DEALLOCATE(basis%cardcoefs)
    IF (ALLOCATED(basis%nodes)) DEALLOCATE(basis%nodes)
  END SUBROUTINE deallocate_gauleg

!-------------------------------------------------------------------------------
!* This subroutine retreives the scaled weights for Gauss-Legendre integration.
!-------------------------------------------------------------------------------
  SUBROUTINE getweights_gauleg(basis, w, x1, x2)
    IMPLICIT NONE

    !* Minimum abscissa for scaling. Defaults to 0.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x1
    !* Maximum abscissa for scaling. Defaults to 1.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x2
    CLASS(gauleg), INTENT(IN) :: basis
    !* Scaled weights. Index from 0 to poly_degree.
    REAL(r8), DIMENSION(0:basis%pd), INTENT(OUT) :: w

    REAL(r8) :: xmin, xmax

    IF (PRESENT(x1)) THEN
      xmin = x1
    ELSE
      xmin = 0._r8
    ENDIF
    IF (PRESENT(x1)) THEN
      xmax = x2
    ELSE
      xmax = 1._r8
    ENDIF
    w = 0.5_r8*(xmax - xmin)*basis%weights
  END SUBROUTINE getweights_gauleg

!-------------------------------------------------------------------------------
!> This subroutine initializes the Lobatto-Legendre abscissae and weights as
!  well as the transformation matrix from Legendre to cardinal functions.
!-------------------------------------------------------------------------------
  SUBROUTINE initialize_lobleg(basis, poly_deg)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: poly_deg !! polynomial degree
    CLASS(lobleg), INTENT(OUT) :: basis

    TYPE(gauleg) :: gl
    REAL(r8), DIMENSION(0:poly_deg) :: p, phi, w
    REAL(r8), DIMENSION(1:poly_deg) :: x_gl
    REAL(r8), PARAMETER :: eps = EPSILON(1._r8)/2._r8
    REAL(r8) :: x, x_old, err
    INTEGER(i4) :: i

    IF (poly_deg < 1) CALL par%nim_stop('Cannot make lobleg with pd = 0.')
    CALL gl%init(poly_deg - 1)
    basis%pd = poly_deg
    IF (ALLOCATED(basis%cardcoefs)) THEN
      IF (basis%getpd()==poly_deg) THEN
        RETURN
      ELSE
        CALL par%nim_stop("initialize_lobleg tried to reallocate")
      ENDIF
    ENDIF
    ALLOCATE(basis%nodes(0:poly_deg))
    ALLOCATE(basis%cardcoefs(0:poly_deg,0:poly_deg))
    ALLOCATE(basis%weights(0:poly_deg))
!-------------------------------------------------------------------------------
!   Use Newton's method to find half the nodes
!-------------------------------------------------------------------------------
    basis%nodes(0) = -1._r8
    basis%nodes(poly_deg) = 1._r8
    basis%weights(0) = 2._r8/REAL(poly_deg*(poly_deg + 1), r8)
    basis%weights(poly_deg) = basis%weights(0)
    CALL gl%getnodes(x_gl, -1._r8, 1._r8)
    DO i = 1, (poly_deg-1)/2
      x = 0.5_r8*(x_gl(i) + x_gl(i + 1))
      err = 1._r8
      DO WHILE (err > eps)
        x_old = x
        CALL legendre_poly(x, poly_deg, p)
        x = x_old - (x*p(poly_deg) - p(poly_deg-1))                             &
                    /(REAL(poly_deg + 2, r8)*p(poly_deg))
        err = ABS(x_old - x)
      ENDDO
      basis%nodes(i) = x
      basis%nodes(poly_deg-i) = -x
      CALL legendre_poly(x, poly_deg, p)
      basis%weights(i) = basis%weights(0)/(p(poly_deg)**2)
      basis%weights(poly_deg-i) = basis%weights(i)
    ENDDO
    IF (MOD(poly_deg,2) == 0) THEN
      basis%nodes(poly_deg/2) = 0._r8
      CALL legendre_poly(0._r8, poly_deg, p)
      basis%weights(poly_deg/2) = basis%weights(0)/(p(poly_deg)**2)
    ENDIF
!-------------------------------------------------------------------------------
!   Find the elements of the transformation matrix from Legendre polynomials to
!   cardinal functions
!-------------------------------------------------------------------------------
    DO i = 0, poly_deg - 1
      w(i) = 0.5_r8*REAL(2*i + 1, r8)
    ENDDO
    ! This magic was copied from the original poly_nodes. I don't understand it.
    w(poly_deg) = 0.5_r8*REAL(poly_deg, r8)
    DO i = 0, poly_deg
      CALL legendre_poly(basis%nodes(i), poly_deg, phi)
      basis%cardcoefs(0:poly_deg, i) = phi*basis%weights(i)*w
    ENDDO
  END SUBROUTINE initialize_lobleg

!-------------------------------------------------------------------------------
!* Deallocate storage for [[lobleg]].
!-------------------------------------------------------------------------------
  SUBROUTINE deallocate_lobleg(basis)
    IMPLICIT NONE

    CLASS(lobleg), INTENT(INOUT) :: basis

    IF (ALLOCATED(basis%weights)) DEALLOCATE(basis%weights)
    IF (ALLOCATED(basis%cardcoefs)) DEALLOCATE(basis%cardcoefs)
    IF (ALLOCATED(basis%nodes)) DEALLOCATE(basis%nodes)
  END SUBROUTINE deallocate_lobleg

!-------------------------------------------------------------------------------
!> This subroutine retreives the scaled weights for Lobatto-Legendre
!  integration.
!-------------------------------------------------------------------------------
  SUBROUTINE getweights_lobleg(basis, w, x1, x2)
    IMPLICIT NONE

    !* Minimum abscissa for scaling. Defaults to 0.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x1
    !* Maximum abscissa for scaling. Defaults to 1.0 if not present.
    REAL(r8), INTENT(IN), OPTIONAL :: x2
    CLASS(lobleg), INTENT(IN) :: basis
    !* Scaled weights. Index from 0 to poly_degree.
    REAL(r8), DIMENSION(0:basis%pd), INTENT(OUT) :: w

    REAL(r8) :: xmin, xmax

    IF (PRESENT(x1)) THEN
      xmin = x1
    ELSE
      xmin = 0._r8
    ENDIF
    IF (PRESENT(x1)) THEN
      xmax = x2
    ELSE
      xmax = 1._r8
    ENDIF
    w = 0.5_r8*(xmax - xmin)*basis%weights
  END SUBROUTINE getweights_lobleg

END MODULE poly_mod
