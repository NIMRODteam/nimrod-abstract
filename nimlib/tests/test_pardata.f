!-------------------------------------------------------------------------------
!! pardata test driver
!-------------------------------------------------------------------------------
PROGRAM test_pardata
  USE local
  USE pardata_mod
  USE nimtest_utils
  IMPLICIT NONE

  INTEGER(i4) :: nargs,it,nlayers
  CHARACTER(64) :: test_type
  INTEGER(i4), PARAMETER :: ntests=7
  INTEGER(i4), DIMENSION(ntests) :: nmodes_total,nbl_total,decompflag

  nmodes_total=[1,6,8,15,7,18,36]
  nbl_total=[1,2,4,6,10,11,15]
  decompflag=[0,1,0,1,0,1,0]
!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 1) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
!-------------------------------------------------------------------------------
! call different test cases.
!-------------------------------------------------------------------------------
  SELECT CASE(TRIM(test_type))
  CASE ("alloc_dealloc")
    CALL par%dealloc
  CASE ("init")
    CALL par%init
    CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
  CASE ("set_decomp")
    CALL par%init
!-------------------------------------------------------------------------------
!   loop over test parameters
!-------------------------------------------------------------------------------
    DO it=1,ntests
      nlayers=par%nprocs
      IF (MOD(nlayers,2)==0.AND.nlayers>2) nlayers=nlayers/2
      IF (nlayers>nmodes_total(it)) nlayers=nmodes_total(it)
      IF (nbl_total(it)<par%nprocs/nlayers) nbl_total(it)=par%nprocs/nlayers
      par%nlayers=nlayers
      par%decompflag=decompflag(it)
      CALL par%set_decomp(nmodes_total(it),nbl_total(it))
      WRITE(*,*) "nmodes_total=",nmodes_total(it)
      WRITE(*,*) "nlayer=",nlayers
      WRITE(*,*) "nbl_total=",nbl_total(it)
      WRITE(*,*) "decompflag=",decompflag(it)
      WRITE(*,*) "par%mode2layer=",par%mode2layer
      WRITE(*,*) "par%block2proc=",par%block2proc
      WRITE(*,*) "par%global2local=",par%global2local
      WRITE(*,*) "par%loc2glob=",par%loc2glob
      WRITE(*,*) "par%layer2proc=",par%layer2proc
      CALL par%dealloc
    ENDDO
    CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
  CASE DEFAULT
    WRITE(*,*) 'No test named ',TRIM(test_type)
    CALL print_usage
  END SELECT
CONTAINS

!-------------------------------------------------------------------------------
!* helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    IMPLICIT NONE

    WRITE(*,*) 'Usage: <test program> <test name>'
    WRITE(*,*) '  where <test name> is one of'
    WRITE(*,*) '    alloc_dealloc'
    WRITE(*,*) '    init'
    WRITE(*,*) '    set_decomp'
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

END PROGRAM test_pardata
