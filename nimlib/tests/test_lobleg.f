!-------------------------------------------------------------------------------
!! Test for lobleg.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Test for lobleg.
!-------------------------------------------------------------------------------
PROGRAM test_lobleg
  USE local
  USE poly_mod
  USE test_poly_mod
  USE nimtest_utils
  IMPLICIT NONE

  TYPE(lobleg) :: test_basis
  INTEGER(i4), PARAMETER :: n_pd=9
  INTEGER(i4) :: i

  DO i = 1, n_pd
    CALL test_basis%init(i)
    CALL run_test_lobleg(test_basis)
  END DO
END PROGRAM test_lobleg
