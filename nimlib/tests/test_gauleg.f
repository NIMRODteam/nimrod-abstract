!-------------------------------------------------------------------------------
!! Test for gauleg.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Test for gauleg.
!-------------------------------------------------------------------------------
PROGRAM test_gauleg
  USE local
  USE poly_mod
  USE test_poly_mod
  USE nimtest_utils
  IMPLICIT NONE

  TYPE(gauleg) :: test_basis
  INTEGER(i4), PARAMETER :: n_pd=10
  INTEGER(i4) :: i

  DO i = 0, n_pd - 1
    CALL test_basis%init(i)
    CALL run_test_gauleg(test_basis)
  END DO
END PROGRAM test_gauleg
