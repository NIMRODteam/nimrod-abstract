!-------------------------------------------------------------------------------
!! define parallel input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module that defines parallel input
!-------------------------------------------------------------------------------
MODULE parallel_input_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: parallel_in

!-------------------------------------------------------------------------------
!: namelist parallel_input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> The distribution of blocks on parallel machines is controlled by `decompflag`
! (0 = clumped, 1 = strided).
!-------------------------------------------------------------------------------
  INTEGER(i4) :: decompflag=0
!-------------------------------------------------------------------------------
!> For parallel computation, `nlayers` sets the number of processor-layers into
! which the Fourier components are distributed.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nlayers=1
!-------------------------------------------------------------------------------
!* If false, use multiple GPU streams for additional block/kernel parallelism
!-------------------------------------------------------------------------------
  LOGICAL :: single_stream=.FALSE.
!-------------------------------------------------------------------------------
!: computed
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* type for parallel input parameter namespacing
!-------------------------------------------------------------------------------
  TYPE :: parallel_input
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
    INTEGER(i4) :: decompflag
    INTEGER(i4) :: nlayers
    LOGICAL :: single_stream
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
  CONTAINS

    PROCEDURE, PASS(parin) :: compute => parallel_compute
    PROCEDURE, PASS(parin) :: read_namelist => parallel_read_namelist
    PROCEDURE, PASS(parin) :: copy_namelist_to_type                             &
                               => parallel_copy_namelist_to_type
    PROCEDURE, PASS(parin) :: h5read => parallel_h5read
    PROCEDURE, PASS(parin) :: h5write => parallel_h5write
    PROCEDURE, PASS(parin) :: bcast_input => parallel_bcast_input
  END TYPE parallel_input

  !* parallel input singleton type
  TYPE(parallel_input) :: parallel_in
CONTAINS

!-------------------------------------------------------------------------------
!> compute secondary parameters and check input
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_compute(parin)
    USE pardata_mod
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), INTENT(INOUT) :: parin

!-------------------------------------------------------------------------------
!   copy to pardata -- this is a unique operation to the parallel_input in order
!   to avoid a circular dependency with pardata.
!-------------------------------------------------------------------------------
    par%decompflag=parin%decompflag
    par%nlayers=parin%nlayers
    par%single_stream=parin%single_stream
  END SUBROUTINE parallel_compute

!-------------------------------------------------------------------------------
!> open and read the namelist input.
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_read_namelist(parin,infile)
    USE local
    USE read_namelist_mod
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), TARGET, INTENT(INOUT) :: parin
    !> input file name
    CHARACTER(*), INTENT(IN) :: infile

    INTEGER(i4) :: read_stat
    CHARACTER(64) :: tempfile
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
    NAMELIST/parallel_input/decompflag,nlayers,single_stream
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   Open input file. Remove comments from input file and put into temporary
!   file.
!-------------------------------------------------------------------------------
    tempfile='temp'//ADJUSTL(infile)
    CALL rmcomment(infile,tempfile)
    OPEN(UNIT=in_unit,FILE=tempfile,STATUS='OLD',POSITION='REWIND')
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
    CALL position_at_namelist('parallel_input', read_stat)
    IF (read_stat==0) THEN
      READ(UNIT=in_unit,NML=parallel_input,IOSTAT=read_stat)
      IF (read_stat/=0) THEN
        CALL print_namelist_error('parallel_input')
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   close and delete temporary input file.
!-------------------------------------------------------------------------------
    CLOSE(in_unit,STATUS='DELETE')
!-------------------------------------------------------------------------------
!   compute secondary parameters.
!-------------------------------------------------------------------------------
    CALL parin%copy_namelist_to_type
    CALL parin%compute
  END SUBROUTINE parallel_read_namelist

!-------------------------------------------------------------------------------
!> copy the namelist values into the namespaced type
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_copy_namelist_to_type(parin)
    USE io
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), INTENT(INOUT) :: parin
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
    parin%decompflag=decompflag
    parin%nlayers=nlayers
    parin%single_stream=single_stream
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
  END SUBROUTINE parallel_copy_namelist_to_type

!-------------------------------------------------------------------------------
!> read h5 namelist from dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_h5read(parin)
    USE io
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), INTENT(INOUT) :: parin

    INTEGER(HID_T) :: gid

    CALL open_group(rootgid,"parallel",gid,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL write_attribute(gid,"parin%decompflag",parin%decompflag,h5in,h5err)
    CALL write_attribute(gid,"parin%nlayers",parin%nlayers,h5in,h5err)
    CALL write_attribute(gid,"parin%single_stream",                             &
                         parin%single_stream,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL close_group("parallel",gid,h5err)
    CALL parin%compute
  END SUBROUTINE parallel_h5read

!-------------------------------------------------------------------------------
!> write h5 namelist to dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_h5write(parin)
    USE io
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), INTENT(INOUT) :: parin

    INTEGER(HID_T) :: gid

    CALL make_group(rootgid,"parallel",gid,h5in,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL read_attribute(gid,"parin%decompflag",parin%decompflag,h5in,h5err)
    CALL read_attribute(gid,"parin%nlayers",parin%nlayers,h5in,h5err)
    CALL read_attribute(gid,"parin%single_stream",                              &
                        parin%single_stream,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL close_group("parallel",gid,h5err)
  END SUBROUTINE parallel_h5write

!-------------------------------------------------------------------------------
!> broadcast info read by proc 0 out of nimrod.in (namelist reads) to all
! processors. every quantity in input module is broadcast in case it was read.
!-------------------------------------------------------------------------------
  SUBROUTINE parallel_bcast_input(parin)
    USE pardata_mod
    IMPLICIT NONE

    !> parallel_input type
    CLASS(parallel_input), INTENT(INOUT) :: parin

!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   parallel_input
!-------------------------------------------------------------------------------
    CALL par%all_bcast(parin%decompflag,0)
    CALL par%all_bcast(parin%nlayers,0)
    CALL par%all_bcast(parin%single_stream,0)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
    CALL parin%compute
  END SUBROUTINE parallel_bcast_input

END MODULE parallel_input_mod
