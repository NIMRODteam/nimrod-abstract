#!/bin/bash
#
# This is a script to rename variables in pardata so that it compiles with nvfortran
#

# if the original version is default

if false
then
  cat pardata.f | sed 's/par%/prd%/g ; s/(par)/(prd)/g ; s/(IN) :: par/(IN) :: prd/g ; s/(INOUT) :: par/(INOUT) :: prd/g ; s/(par,/(prd,/g' > pardata.nv.f

  cat pardata.nv.f | sed 's/prd/par/g' > pardata.orig.f
fi

# if the nvfortran version is default

if true
then
  cat pardata.f | sed 's/prd/par/g' > pardata.orig.f

  cat pardata.orig.f | sed 's/par%/prd%/g ; s/(par)/(prd)/g ; s/(IN) :: par/(IN) :: prd/g ; s/(INOUT) :: par/(INOUT) :: prd/g ; s/(par,/(prd,/g' > pardata.nv.f
fi

