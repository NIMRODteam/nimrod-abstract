!-------------------------------------------------------------------------------
!< mathematical operations on quadrature data. Keeping this file with these
!  functions as they may be integrated into quadrature.f but should be tested
!  and needed.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing mathematical operations on quadrature data
!-------------------------------------------------------------------------------
MODULE quad_math
  USE local
  IMPLICIT NONE

  INTERFACE cart_cross
    MODULE PROCEDURE cart_cross_rr,cart_cross_rc,cart_cross_cr,cart_cross_cc
  END INTERFACE cart_cross

  INTERFACE cadd_cross
    MODULE PROCEDURE cadd_cross_rr,cadd_cross_rc,cadd_cross_cr,cadd_cross_cc
  END INTERFACE cadd_cross

CONTAINS

!-------------------------------------------------------------------------------
!> evaluates the Cartesian cross product multiplied by a scalar
!  over 2D arrays (both reals).
!-------------------------------------------------------------------------------
  SUBROUTINE cart_cross_rr(vprod,v1,v2,scal)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:,:), INTENT(OUT) :: vprod
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v1,v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cart_cross_rr

!-------------------------------------------------------------------------------
!> evaluates the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (one real and one complex).
!-------------------------------------------------------------------------------
  SUBROUTINE cart_cross_rc(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: vprod
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v1
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cart_cross_rc

!-------------------------------------------------------------------------------
!> evaluates the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (one real and one complex).
!-------------------------------------------------------------------------------
  SUBROUTINE cart_cross_cr(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: vprod
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v1
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cart_cross_cr

!-------------------------------------------------------------------------------
!> evaluates the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (both complex).
!-------------------------------------------------------------------------------
  SUBROUTINE cart_cross_cc(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: vprod
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v1,v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cart_cross_cc

!-------------------------------------------------------------------------------
!> adds the Cartesian cross product multiplied by a scalar
!  over 2D arrays (both reals) to an array.
!-------------------------------------------------------------------------------
  SUBROUTINE cadd_cross_rr(vprod,v1,v2,scal)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:,:), INTENT(INOUT) :: vprod
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v1,v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=vprod(1,:,:)+scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=vprod(2,:,:)+scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=vprod(3,:,:)+scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cadd_cross_rr

!-------------------------------------------------------------------------------
!> adds the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (one real and one complex) to an array.
!-------------------------------------------------------------------------------
  SUBROUTINE cadd_cross_rc(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(INOUT) :: vprod
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v1
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=vprod(1,:,:)+scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=vprod(2,:,:)+scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=vprod(3,:,:)+scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cadd_cross_rc

!-------------------------------------------------------------------------------
!> adds the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (one real and one complex) to an array.
!-------------------------------------------------------------------------------
  SUBROUTINE cadd_cross_cr(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(INOUT) :: vprod
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v1
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=vprod(1,:,:)+scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=vprod(2,:,:)+scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=vprod(3,:,:)+scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cadd_cross_cr

!-------------------------------------------------------------------------------
!> adds the Cartesian cross product multiplied by a real scalar
!  over 2D arrays (both complex) to an array.
!-------------------------------------------------------------------------------
  SUBROUTINE cadd_cross_cc(vprod,v1,v2,scal)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(INOUT) :: vprod
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: v1,v2
    REAL(r8), INTENT(IN) :: scal
!-------------------------------------------------------------------------------
!   _x,_y, and _z or _r,_z, and _phi
!-------------------------------------------------------------------------------
    vprod(1,:,:)=vprod(1,:,:)+scal*(v1(2,:,:)*v2(3,:,:)-v1(3,:,:)*v2(2,:,:))
    vprod(2,:,:)=vprod(2,:,:)+scal*(v1(3,:,:)*v2(1,:,:)-v1(1,:,:)*v2(3,:,:))
    vprod(3,:,:)=vprod(3,:,:)+scal*(v1(1,:,:)*v2(2,:,:)-v1(2,:,:)*v2(1,:,:))
    RETURN
  END SUBROUTINE cadd_cross_cc

!-------------------------------------------------------------------------------
!> find metric-related derivatives of r and z.
!-------------------------------------------------------------------------------
  SUBROUTINE grid(option,drzdx,drzdy,jac,dxdr,dxdz,dydr,dydz)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: drzdx,drzdy
    CHARACTER(*), INTENT(IN) :: option
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: jac,dxdr,dxdz,dydr,dydz

!-------------------------------------------------------------------------------
!   find the jacobian, then perform a matrix inversion.
!-------------------------------------------------------------------------------
    jac = drzdx(1,:,:)*drzdy(2,:,:)-drzdx(2,:,:)*drzdy(1,:,:)
    IF (option=='jaco') RETURN
    dxdr= drzdy(2,:,:)/jac
    dydr=-drzdx(2,:,:)/jac
    dxdz=-drzdy(1,:,:)/jac
    dydz= drzdx(1,:,:)/jac
    RETURN
  END SUBROUTINE grid

!-------------------------------------------------------------------------------
!> find the cylindrical curl of the incoming vector (with cylindrical
!  phi component) where derivatives with respect to r and z are
!  supplied.  this version uses complex arrays.
!-------------------------------------------------------------------------------
  SUBROUTINE curl(nmodes,keff,geom,bigr,vec,dvecr,dvecz,curl,scal)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: nmodes
    REAL(r8), INTENT(IN) :: scal
    REAL(r8), DIMENSION(:), INTENT(IN) :: keff
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: bigr
    CHARACTER(*), INTENT(IN) :: geom
    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(IN) :: vec,dvecr,dvecz
    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(OUT) :: curl

    INTEGER(i4) :: im
!-------------------------------------------------------------------------------
!   note:  vector storage is (comp1,comp2,comp3)
!   where (1,2,3)=(x,y,z) or (r,z,phi).
!
!   separate coding is now required for toroidal and linear
!   geometries since phi component is cylindrical not covariant.
!-------------------------------------------------------------------------------
    IF (geom=='tor') THEN
      DO im=1,nmodes
        curl(1,:,:,im)= scal*(                                                  &
           dvecz(3,:,:,im)-(0,1)*keff(im)*vec(2,:,:,im) /bigr )
        curl(2,:,:,im)= scal*(-dvecr(3,:,:,im)                                  &
          +(-vec(3,:,:,im)+(0,1)*keff(im)*vec(1,:,:,im))/bigr )
        curl(3,:,:,im)= scal*( dvecr(2,:,:,im)-dvecz(1,:,:,im) )
      ENDDO
    ELSE
      DO im=1,nmodes
        curl(1,:,:,im)= scal*(                                                  &
           dvecz(3,:,:,im)-(0,1)*keff(im)*vec(2,:,:,im) )
        curl(2,:,:,im)= scal*(                                                  &
          -dvecr(3,:,:,im)+(0,1)*keff(im)*vec(1,:,:,im) )
        curl(3,:,:,im)= scal*( dvecr(2,:,:,im)-dvecz(1,:,:,im) )
      ENDDO
    ENDIF
    RETURN
  END SUBROUTINE curl

!-------------------------------------------------------------------------------
!> assemble a field and its r and z derivatives into storage for a
!  gradient where the index cycles over the partial-derivative
!  directions fastest.
!
!  additional terms for 3-vectors in cylindrical/toroidal geometry
!  are added, and an n=0 component is optional.
!-------------------------------------------------------------------------------
  SUBROUTINE grad(nmodes,keff,nq,geom,vec,dvecr,dvecz,grad,bigr,                &
                  vec0,dvec0r,dvec0z)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: nmodes,nq
    REAL(r8), DIMENSION(:), INTENT(IN) :: keff
    REAL(r8), DIMENSION(:,:), INTENT(IN), OPTIONAL :: bigr
    REAL(r8), DIMENSION(:,:,:), INTENT(IN), OPTIONAL :: vec0,dvec0r,dvec0z
    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(IN) :: vec,dvecr,dvecz
    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(OUT) :: grad
    CHARACTER(*), INTENT(IN) :: geom

    INTEGER(i4) :: im,iq,igr
    LOGICAL :: add_n0
!-------------------------------------------------------------------------------
!   note:  incoming vector storage is (comp1,comp2,comp3)
!   where (1,2,3)=(x,y,z) or (r,z,phi).  the outgoing gradient
!   storage is (dcomp1/dr,dcomp1/dz,...)
!-------------------------------------------------------------------------------
    IF (PRESENT(vec0)) THEN
      add_n0=.true.
    ELSE
      add_n0=.false.
    ENDIF
!-------------------------------------------------------------------------------
!   nq-specific cases are intended for optimization.
!-------------------------------------------------------------------------------
    IF (geom=='tor') THEN
      SELECT CASE(nq)
      CASE(1)
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            grad(1,:,:,im)=dvecr(1,:,:,im)+dvec0r(1,:,:)
            grad(2,:,:,im)=dvecz(1,:,:,im)+dvec0z(1,:,:)
            grad(3,:,:,im)=0
          ELSE
            grad(1,:,:,im)=dvecr(1,:,:,im)
            grad(2,:,:,im)=dvecz(1,:,:,im)
            grad(3,:,:,im)=  vec(1,:,:,im)*(0,1)*keff(im)/bigr
          ENDIF
        ENDDO
      CASE(3) !  Including curvature terms.
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            grad(1,:,:,im)=dvecr(1,:,:,im)+dvec0r(1,:,:)
            grad(2,:,:,im)=dvecz(1,:,:,im)+dvec0z(1,:,:)
            grad(3,:,:,im)=-(vec(3,:,:,im)+  vec0(3,:,:))/bigr
            grad(4,:,:,im)=dvecr(2,:,:,im)+dvec0r(2,:,:)
            grad(5,:,:,im)=dvecz(2,:,:,im)+dvec0z(2,:,:)
            grad(6,:,:,im)=0
            grad(7,:,:,im)=dvecr(3,:,:,im)+dvec0r(3,:,:)
            grad(8,:,:,im)=dvecz(3,:,:,im)+dvec0z(3,:,:)
            grad(9,:,:,im)= (vec(1,:,:,im)+  vec0(1,:,:))/bigr
          ELSE
            grad(1,:,:,im)=dvecr(1,:,:,im)
            grad(2,:,:,im)=dvecz(1,:,:,im)
            grad(3,:,:,im)= (vec(1,:,:,im)*(0,1)*keff(im)-vec(3,:,:,im))/bigr
            grad(4,:,:,im)=dvecr(2,:,:,im)
            grad(5,:,:,im)=dvecz(2,:,:,im)
            grad(6,:,:,im)=  vec(2,:,:,im)*(0,1)*keff(im)/bigr
            grad(7,:,:,im)=dvecr(3,:,:,im)
            grad(8,:,:,im)=dvecz(3,:,:,im)
            grad(9,:,:,im)= (vec(3,:,:,im)*(0,1)*keff(im)+vec(1,:,:,im))/bigr
          ENDIF
        ENDDO
      CASE DEFAULT
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            igr=1
            DO iq=1,nq
              grad(igr  ,:,:,im)=dvecr(iq,:,:,im)+dvec0r(iq,:,:)
              grad(igr+1,:,:,im)=dvecz(iq,:,:,im)+dvec0z(iq,:,:)
              grad(igr+2,:,:,im)=0
              igr=igr+3
            ENDDO
          ELSE
            igr=1
            DO iq=1,nq
              grad(igr  ,:,:,im)=dvecr(iq,:,:,im)
              grad(igr+1,:,:,im)=dvecz(iq,:,:,im)
              grad(igr+2,:,:,im)=  vec(iq,:,:,im)*(0,1)*keff(im)/bigr
              igr=igr+3
            ENDDO
          ENDIF
        ENDDO
      END SELECT
    ELSE !  geom='lin'
!-------------------------------------------------------------------------------
!     same for linear geometry.
!-------------------------------------------------------------------------------
      SELECT CASE(nq)
      CASE(1)
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            grad(1,:,:,im)=dvecr(1,:,:,im)+dvec0r(1,:,:)
            grad(2,:,:,im)=dvecz(1,:,:,im)+dvec0z(1,:,:)
            grad(3,:,:,im)=0
          ELSE
            grad(1,:,:,im)=dvecr(1,:,:,im)
            grad(2,:,:,im)=dvecz(1,:,:,im)
            grad(3,:,:,im)=  vec(1,:,:,im)*(0,1)*keff(im)
          ENDIF
        ENDDO
      CASE(3)
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            grad(1,:,:,im)=dvecr(1,:,:,im)+dvec0r(1,:,:)
            grad(2,:,:,im)=dvecz(1,:,:,im)+dvec0z(1,:,:)
            grad(3,:,:,im)=0
            grad(4,:,:,im)=dvecr(2,:,:,im)+dvec0r(2,:,:)
            grad(5,:,:,im)=dvecz(2,:,:,im)+dvec0z(2,:,:)
            grad(6,:,:,im)=0
            grad(7,:,:,im)=dvecr(3,:,:,im)+dvec0r(3,:,:)
            grad(8,:,:,im)=dvecz(3,:,:,im)+dvec0z(3,:,:)
            grad(9,:,:,im)=0
          ELSE
            grad(1,:,:,im)=dvecr(1,:,:,im)
            grad(2,:,:,im)=dvecz(1,:,:,im)
            grad(3,:,:,im)=  vec(1,:,:,im)*(0,1)*keff(im)
            grad(4,:,:,im)=dvecr(2,:,:,im)
            grad(5,:,:,im)=dvecz(2,:,:,im)
            grad(6,:,:,im)=  vec(2,:,:,im)*(0,1)*keff(im)
            grad(7,:,:,im)=dvecr(3,:,:,im)
            grad(8,:,:,im)=dvecz(3,:,:,im)
            grad(9,:,:,im)=  vec(3,:,:,im)*(0,1)*keff(im)
          ENDIF
        ENDDO
      CASE DEFAULT
        DO im=1,nmodes
          IF (keff(im)==0.AND.add_n0) THEN
            igr=1
            DO iq=1,nq
              grad(igr  ,:,:,im)=dvecr(iq,:,:,im)+dvec0r(iq,:,:)
              grad(igr+1,:,:,im)=dvecz(iq,:,:,im)+dvec0z(iq,:,:)
              grad(igr+2,:,:,im)=0
              igr=igr+3
            ENDDO
          ELSE
            igr=1
            DO iq=1,nq
              grad(igr  ,:,:,im)=dvecr(iq,:,:,im)
              grad(igr+1,:,:,im)=dvecz(iq,:,:,im)
              grad(igr+2,:,:,im)=  vec(iq,:,:,im)*(0,1)*keff(im)
              igr=igr+3
            ENDDO
          ENDIF
        ENDDO
      END SELECT
    ENDIF
    RETURN
  END SUBROUTINE grad

END MODULE quad_math
