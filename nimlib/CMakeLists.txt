######################################################################
#
# CMakeLists.txt for nimlib
#
######################################################################

set(NIMLIB_SOURCES
  function.f
  io.f
  math_tran.f
  pardata_input.f
  polynomials.f
  read_nml.f
  timer.f
)

if (ENABLE_MPI AND NOT HAVE_MPI_F08)
  set(NIMLIB_SOURCES ${NIMLIB_SOURCES} pardata.old_mpi.f)
else ()
  set(NIMLIB_SOURCES ${NIMLIB_SOURCES} pardata.f)
endif ()

if (HAVE_CUBLAS)
  set(NIMLIB_SOURCES ${NIMLIB_SOURCES} acc_cublas.f)
endif ()

if (OBJ_MEM_PROF)
  set(NIMLIB_SOURCES ${NIMLIB_SOURCES} memlog.f)
endif ()

add_library(nimlib-obj OBJECT ${NIMLIB_SOURCES})
list(APPEND OBJ_TARGETS $<TARGET_OBJECTS:nimlib-obj>)
target_link_libraries(nimlib-obj PUBLIC nimloc iolib)
target_link_libraries(nimlib-obj PUBLIC HDF5::HDF5)
if (NVTX_PROFILE)
  target_link_libraries(nimlib-obj PRIVATE nvtx)
endif ()
if (ENABLE_MPI)
  if (${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
    # nvhpc requires a public interface for downstream module includes
    # these should be private and thus not required downstream
    target_link_libraries(nimlib-obj PUBLIC MPI::MPI_Fortran)
  else ()
    target_link_libraries(nimlib-obj PRIVATE MPI::MPI_Fortran)
  endif ()
endif ()
target_include_directories(nimlib-obj PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)
add_library(nimlib STATIC $<TARGET_OBJECTS:nimlib-obj>)
target_link_libraries(nimlib PUBLIC nimlib-obj)

add_subdirectory(tests)

set(OBJ_TARGETS ${OBJ_TARGETS} PARENT_SCOPE)
