!-------------------------------------------------------------------------------
!! file with module containing routines to read a namelist
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* module containing routines to read a namelist
!-------------------------------------------------------------------------------
MODULE read_namelist_mod
  USE io
  USE local
  USE pardata_mod
  IMPLICIT NONE

CONTAINS

!-------------------------------------------------------------------------------
!*    report namelist errors
!-------------------------------------------------------------------------------
  SUBROUTINE print_namelist_error(nmlname)
    IMPLICIT NONE

    !> namelist name
    CHARACTER(*), INTENT(IN) :: nmlname

    CHARACTER(128) :: line

    BACKSPACE(in_unit)
    READ(in_unit,FMT='(A)') line
    CALL par%nim_stop('In namelist '//nmlname//': Invalid line: '//TRIM(line))
  END SUBROUTINE print_namelist_error

!-------------------------------------------------------------------------------
!* find namelist in file
!-------------------------------------------------------------------------------
  SUBROUTINE position_at_namelist(nmlname, status)
    IMPLICIT NONE

    !> namelist name
    CHARACTER(*), INTENT(IN)  :: nmlname
    !> IOSTAT from namelist read
    INTEGER(i4), INTENT(OUT) :: status

    CHARACTER(128)  :: file_str
    CHARACTER(128) :: test_str
    INTEGER(i4) :: i, n
!-------------------------------------------------------------------------------
!   rewind file
!-------------------------------------------------------------------------------
    REWIND(in_unit)
!-------------------------------------------------------------------------------
!   define test string, i.e. namelist group
!-------------------------------------------------------------------------------
    test_str='&'//TRIM(ADJUSTL(nmlname))
!-------------------------------------------------------------------------------
!   search for the record containing the namelist group
!-------------------------------------------------------------------------------
    n=0
    DO
      READ(in_unit,'(A)', IOSTAT=status) file_str
      IF (status/=0) THEN
        EXIT ! e.g. end of file
      ELSE
        IF (INDEX(ADJUSTL(file_str), test_str) == 1) THEN
          EXIT ! i.e. found record we're looking for
        ENDIF
      ENDIF
      n=n+1 ! increment record counter
    ENDDO
!-------------------------------------------------------------------------------
!   can possibly replace this section with "backspace(iunit)" after a
!   successful string compare, but not sure that's legal for
!   namelist records thus, the following:
!-------------------------------------------------------------------------------
    IF (status == 0) THEN
      REWIND(in_unit)
      DO i = 1, n
        READ(in_unit, '(a)')
      ENDDO
    ENDIF
  END SUBROUTINE position_at_namelist

!-------------------------------------------------------------------------------
!>  routine to strip out comments beginning with an exclamation point
!-------------------------------------------------------------------------------
  SUBROUTINE rmcomment(fileold,filenew)
    IMPLICIT NONE

    !> file name to strip comments from
    CHARACTER(*), INTENT(IN) :: fileold
    !> file name of new file stripped of comments
    CHARACTER(*), INTENT(IN) :: filenew

    CHARACTER(128) :: line
    INTEGER, PARAMETER :: nold=55,nnew=56
    INTEGER cmax, ios
    LOGICAL :: file_exist
!-------------------------------------------------------------------------------
!   open files, but make sure the old one exists first.
!-------------------------------------------------------------------------------
    INQUIRE(FILE=fileold,EXIST=file_exist)
    IF(.NOT. file_exist) THEN
      CALL par%nim_stop('The file "'//TRIM(fileold)//'" could not be found.')
    ENDIF
    OPEN(UNIT=nold,FILE=fileold,STATUS="OLD")
    OPEN(UNIT=nnew,FILE=filenew,STATUS='REPLACE')
!-------------------------------------------------------------------------------
!   strip comments. Note: line lengths limited to 127 characters
!-------------------------------------------------------------------------------
    DO
      READ(UNIT=nold,FMT='(a)',IOSTAT=ios) line
      IF (ios /= 0) EXIT
      cmax=1
      DO WHILE(line(cmax:cmax)/='!' .AND. cmax <= 127)
        cmax=cmax+1
      ENDDO
      IF(cmax > 1) WRITE(nnew,'(a)') TRIM(line(1:cmax-1))
    ENDDO
!-------------------------------------------------------------------------------
!   close files and exit
!-------------------------------------------------------------------------------
    CLOSE(nold)
    CLOSE(nnew)
  END SUBROUTINE rmcomment

END MODULE read_namelist_mod
