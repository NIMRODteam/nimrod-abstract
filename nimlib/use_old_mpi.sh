#!/bin/bash
#
# This is a script to rename variables in pardata so that it compiles with old mpi versions
#

cat pardata.f | sed 's/par%/prd%/g ; s/TYPE(mpi_request)/INTEGER(i4)/g ; s/TYPE(mpi_status)/INTEGER(i4)/g ; s/TYPE(mpi_comm)/INTEGER(i4)/g ; s/mpi_f08/mpi/g'  > pardata.old_mpi.f
mv pardata.f pardata.mpi_f08.f
mv pardata.old_mpi.f pardata.f

