!-------------------------------------------------------------------------------
!! Routines for handling a rectangular block type
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the structured rectangular block type and the associated procedures
!-------------------------------------------------------------------------------
MODULE rblock_mod
  USE local
  USE timer_mod
  USE gblock_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: rblock

  CHARACTER(*), PARAMETER :: mod_name='rblock'
!-------------------------------------------------------------------------------
!> Type for structured rectangular blocks.
!  See the abstract base type [[gblock(type)]] for type-bound procedure
!  interface details.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(gblock) :: rblock
    !> number of x-directed elements
    INTEGER(i4) :: mx=0
    !> number of y-directed elements
    INTEGER(i4) :: my=0
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(bl) :: alloc
    ! Abstract class deferred functions
    PROCEDURE, PASS(bl) :: dealloc
    PROCEDURE, PASS(bl) :: block_intg_formula_set
    PROCEDURE, PASS(bl) :: block_metric_set
    PROCEDURE, PASS(bl) :: h5_read
    PROCEDURE, PASS(bl) :: h5_write
    PROCEDURE, PASS(bl) :: distribute
    PROCEDURE, PASS(bl) :: collect
  END TYPE rblock

CONTAINS

!-------------------------------------------------------------------------------
!* allocate a rectangular grid block
!-------------------------------------------------------------------------------
  SUBROUTINE alloc(bl,id,ibl,mx,my,poly_degree,r0block)
    USE nodal_mod
    IMPLICIT NONE

    !> block to allocate
    CLASS(rblock), INTENT(INOUT) :: bl
    !> block global id
    INTEGER(i4), INTENT(IN) :: id
    !> block local storage index
    INTEGER(i4), INTENT(IN) :: ibl
    !> block local mx
    INTEGER(i4), INTENT(IN) :: mx
    !> block local my
    INTEGER(i4), INTENT(IN) :: my
    !> block poly_degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> if block touch R=0 with geom=tor
    LOGICAL, INTENT(IN) :: r0block

    CHARACTER(4) :: blid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc',iftn,idepth)
    bl%bl_type='rblock'
    WRITE(blid,fmt='(i4.4)') id
    bl%name='rblock'//TRIM(blid)
    bl%id=id
    bl%ibl=ibl
    bl%mx=mx
    bl%my=my
    bl%nel=mx*my
    bl%poly_degree=poly_degree
    bl%r0block=r0block
    bl%vsmesh_type="mesh-structured"
    bl%vsmesh_labels="R,Z"
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc

!-------------------------------------------------------------------------------
!* Deallocated the rblock
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc(bl)
    IMPLICIT NONE

    !> block to deallocate
    CLASS(rblock), INTENT(INOUT) :: bl

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc',iftn,idepth)
!-------------------------------------------------------------------------------
!   Reset rblock specific variables
!-------------------------------------------------------------------------------
    bl%mx=0
    bl%my=0
    bl%r0block=.FALSE.
!-------------------------------------------------------------------------------
!   deallocate GPU memory
!-------------------------------------------------------------------------------
    IF (ALLOCATED(bl%metric%jac)) THEN
      !$acc exit data delete(bl%metric%jac,bl%metric%ijac,bl%metric%bigr)       &
      !$acc delete(bl%metric%detj,bl%metric%wdetj) finalize                     &
      !$acc async(bl%id) if(bl%on_gpu)
      !$acc exit data delete(bl) finalize async(bl%id) if(bl%on_gpu)
    ENDIF
!-------------------------------------------------------------------------------
!   Deallocate data common to all block types
!-------------------------------------------------------------------------------
    CALL bl%dealloc_global_block_data()
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!*  set the locations and weights for quadrature points.
!-------------------------------------------------------------------------------
  SUBROUTINE block_intg_formula_set(bl,ngr,int_formula,surf_int_formula)
    USE poly_mod
    IMPLICIT NONE

    !> block to set
    CLASS(rblock), INTENT(INOUT) :: bl
    !> total number of gaussian quadrature points is (ngr+poly_degree-1)**ndim
    INTEGER(i4), INTENT(IN) :: ngr
    !> integration formula (gaussian or lobatto)
    CHARACTER(8), INTENT(IN) :: int_formula
    !> boundary integration formula (gaussian or lobatto)
    CHARACTER(8), INTENT(IN) :: surf_int_formula

    INTEGER(i4) :: ix,iy,ig
    REAL(r8), DIMENSION(ngr+bl%poly_degree-1) :: xg1d,wg1d
    TYPE(gauleg) :: gl
    TYPE(lobleg) :: ll
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'block_intg_formula_set',iftn,idepth)
!-------------------------------------------------------------------------------
!   set number of quadrature points and weights according to input.
!   the number of points is adjusted automatically with poly_degree.
!   the integration formulas and the the number of quadrature points are stored
!   in the gblock structure. This will allow us to write them out to the dump
!   file.
!-------------------------------------------------------------------------------
    bl%ng_bdry=ngr+bl%poly_degree-1
    bl%ng=(bl%ng_bdry)**2
    bl%int_formula=int_formula
    bl%surf_int_formula=surf_int_formula
    IF (int_formula=='gaussian'.OR.bl%r0block) THEN
      CALL gl%init(bl%ng_bdry-1)
      CALL gl%getnodes(xg1d)
      CALL gl%getweights(wg1d)
      CALL gl%dealloc()
    ELSE
      CALL ll%init(bl%ng_bdry-1)
      CALL ll%getnodes(xg1d)
      CALL ll%getweights(wg1d)
      CALL ll%dealloc()
    ENDIF
    ALLOCATE(bl%xg(bl%ng,2),bl%wg(bl%ng))
    ALLOCATE(bl%xg_bdry(bl%ng_bdry,1),bl%wg_bdry(bl%ng_bdry))
    ig=0
    DO iy=1,bl%ng_bdry
      DO ix=1,bl%ng_bdry
        ig=ig+1
        bl%xg(ig,1)=xg1d(ix)
        bl%xg(ig,2)=xg1d(iy)
        bl%wg(ig)=wg1d(ix)*wg1d(iy)
      ENDDO
    ENDDO
    IF (surf_int_formula=='lobatto') THEN
      CALL ll%init(bl%ng_bdry-1)
      CALL ll%getnodes(xg1d)
      CALL ll%getweights(wg1d)
      CALL ll%dealloc()
    ELSE
      CALL gl%init(bl%ng_bdry-1)
      CALL gl%getnodes(xg1d)
      CALL gl%getweights(wg1d)
      CALL gl%dealloc()
    ENDIF
    DO ix=1,bl%ng_bdry
      bl%xg_bdry(ix,1)=xg1d(ix)
      bl%wg_bdry(ix)=wg1d(ix)*wg1d(ngr+bl%poly_degree-1)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE block_intg_formula_set

!-------------------------------------------------------------------------------
!* calculate the metric-realted derivatives for the finite element grid.
!-------------------------------------------------------------------------------
  SUBROUTINE block_metric_set(bl,torgeom)
    USE math_tran
    USE quadrature_mod, ONLY: batch_interp_real
    IMPLICIT NONE

    !> block to set
    CLASS(rblock), INTENT(INOUT) :: bl
    !> true if in toroidal geometry
    LOGICAL, INTENT(IN) :: torgeom

    TYPE(batch_interp_real) :: gmesh
    INTEGER(i4) :: iel
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    REAL(r8) :: jac(bl%ng,2,2),ijac(bl%ng,2,2)

    CALL timer%start_timer_l2(mod_name,'block_metric_set',iftn,idepth)
!-------------------------------------------------------------------------------
!   allocate arrays used for saving grid derivatives at each of the
!   Gaussian quadrature points.
!
!   the quadrature points are indexed first, and (ix,iy) indices are combined.
!-------------------------------------------------------------------------------
    ALLOCATE(bl%metric%jac(bl%ng,bl%nel,2,2))
    ALLOCATE(bl%metric%ijac(bl%ng,bl%nel,2,2))
    ALLOCATE(bl%metric%bigr(bl%ng,bl%nel))
    ALLOCATE(bl%metric%detj(bl%ng,bl%nel))
    ALLOCATE(bl%metric%wdetj(bl%ng,bl%nel))
!-------------------------------------------------------------------------------
!   evaluate the grid derivatives which are needed to construct the
!   gradients of the basis functions.  finally, save the quadrature
!   weight times the coordinate-mapping Jacobian for efficiency during
!   finite element computations.
!-------------------------------------------------------------------------------
    ALLOCATE(gmesh%f(bl%ng,2),gmesh%df(bl%ng,4))
    DO iel=1,bl%nel
      !TODO: make this efficient on the GPU
      IF (bl%mesh%on_gpu) bl%mesh%on_gpu=.FALSE.
      CALL bl%mesh%eval(bl%xg,iel,gmesh,dorder=1,transform=.FALSE.)
      bl%metric%bigr(:,iel)=gmesh%f(:,1)
      bl%metric%jac(:,iel,:,:)=RESHAPE(gmesh%df,[bl%ng,2,2])
!-------------------------------------------------------------------------------
!     invert the Jacobian matrix and save partial derivatives.
!-------------------------------------------------------------------------------
      jac=bl%metric%jac(:,iel,:,:)
      CALL math_metric(jac,ijac,bl%metric%detj(:,iel))
      bl%metric%ijac(:,iel,:,:)=ijac
    ENDDO
!-------------------------------------------------------------------------------
!   multiply the jacobian by the quadrature weight.
!-------------------------------------------------------------------------------
    bl%metric%wdetj=bl%metric%detj
    IF (torgeom) THEN
      bl%metric%wdetj=bl%metric%wdetj*bl%metric%bigr
    ELSE
      bl%metric%bigr=1._r8
    ENDIF
    DO iel=1,bl%nel
      bl%metric%wdetj(:,iel)=bl%metric%wdetj(:,iel)*bl%wg
    ENDDO
    ! can be removed with above TODO completed
    IF (bl%on_gpu) bl%mesh%on_gpu=.TRUE.
    !$acc enter data copyin(bl) async(bl%id) if(bl%on_gpu)
    !$acc enter data copyin(bl%metric%jac) async(bl%id) if(bl%on_gpu)
    !$acc enter data copyin(bl%metric%ijac) async(bl%id) if(bl%on_gpu)
    !$acc enter data copyin(bl%metric%bigr) async(bl%id) if(bl%on_gpu)
    !$acc enter data copyin(bl%metric%detj) async(bl%id) if(bl%on_gpu)
    !$acc enter data copyin(bl%metric%wdetj) async(bl%id) if(bl%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE block_metric_set

!-------------------------------------------------------------------------------
!> read rblock specific data from h5 file then call h5_read_global_block_data
!  to read data common to all blocks
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read(bl,id,h5gid,io_field_list)
    USE io
    IMPLICIT NONE

    !> block to read
    CLASS(rblock), INTENT(INOUT) :: bl
    !> block global id
    INTEGER(i4), INTENT(IN) :: id
    !> h5 group to read from
    INTEGER(HID_T), INTENT(IN) :: h5gid
    !> I/O field list pointing to data to read
    TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)

    CHARACTER(4) :: blid
    INTEGER(HID_T) :: h5blid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read',iftn,idepth)
!-------------------------------------------------------------------------------
!   setup rblock group.
!-------------------------------------------------------------------------------
    WRITE(blid,fmt='(i4.4)') id
    CALL open_group(h5gid,TRIM(blid),h5blid,h5err)
    bl%name='rblock'//TRIM(blid)
!-------------------------------------------------------------------------------
!   read block descriptors.
!-------------------------------------------------------------------------------
    CALL read_attribute(h5blid,"mx",bl%mx,h5in,h5err)
    CALL read_attribute(h5blid,"my",bl%my,h5in,h5err)
!-------------------------------------------------------------------------------
!   read fields and data common to all block types
!-------------------------------------------------------------------------------
    CALL bl%h5_read_global_block_data(h5blid,blid,io_field_list)
    CALL close_group(TRIM(blid),h5blid,h5err)
    bl%vsmesh_type="mesh-structured"
    bl%vsmesh_labels="R,Z"
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read

!-------------------------------------------------------------------------------
!*  write rblock to an h5 file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_write(bl,h5gid,io_field_list)
    USE io
    IMPLICIT NONE

    !> block to write
    CLASS(rblock), INTENT(IN) :: bl
    !> h5 group to write in
    INTEGER(HID_T), INTENT(IN) :: h5gid
    !> I/O field list pointing to data to write to block
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)

    CHARACTER(4) :: blid !! bl%id stored as a string
    INTEGER(HID_T) :: h5blid !!this block's h5 group id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_write',iftn,idepth)
!-------------------------------------------------------------------------------
!   setup rblock group.
!-------------------------------------------------------------------------------
    WRITE(blid,fmt='(i4.4)') bl%id
    CALL make_group(h5gid,TRIM(blid),h5blid,h5in,h5err)
!-------------------------------------------------------------------------------
!   write block descriptors.
!-------------------------------------------------------------------------------
    CALL write_attribute(h5blid,"mx",bl%mx,h5in,h5err)
    CALL write_attribute(h5blid,"my",bl%my,h5in,h5err)
!-------------------------------------------------------------------------------
!   write fields and data common to all block types
!-------------------------------------------------------------------------------
    CALL bl%h5_write_global_block_data(h5blid,blid,io_field_list)
!-------------------------------------------------------------------------------
!   terminate
!-------------------------------------------------------------------------------
    CALL close_group(TRIM(blid),h5blid,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_write

!-------------------------------------------------------------------------------
!*  distribute block data with mode decomposition
!-------------------------------------------------------------------------------
  SUBROUTINE distribute(bl,io_field_list,inode)
    USE nodal_mod
    USE pardata_mod
    IMPLICIT NONE

    !> block to distribute
    CLASS(rblock), INTENT(INOUT) :: bl
    !> I/O field list to distribute
    TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    CHARACTER(64) :: field_type
    INTEGER(i4) :: ifld
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'distribute',iftn,idepth)
    CALL par%mode_bcast(bl%id,inode)
    CALL par%mode_bcast(bl%ibl,inode)
    CALL par%mode_bcast(bl%mx,inode)
    CALL par%mode_bcast(bl%my,inode)
    CALL par%mode_bcast(bl%poly_degree,inode)
    CALL par%mode_bcast(bl%r0block,inode)
    CALL par%mode_bcast(bl%on_gpu,inode)
    IF (par%ilayer/=inode)                                                      &
      CALL bl%alloc(bl%id,bl%ibl,bl%mx,bl%my,bl%poly_degree,bl%r0block)
!-------------------------------------------------------------------------------
!   distribute mesh
!-------------------------------------------------------------------------------
    IF (par%ilayer==0) field_type=bl%mesh%field_type
    CALL par%mode_bcast(field_type,LEN(field_type),inode)
    IF (par%ilayer/=0) CALL bl%alloc_real_fld(bl%mesh,field_type,bl%on_gpu)
    CALL bl%mesh%bcast(inode)
!-------------------------------------------------------------------------------
!   distribute fields
!-------------------------------------------------------------------------------
    DO ifld=1,SIZE(io_field_list)
      SELECT TYPE (iofield=>io_field_list(ifld)%p)
      CLASS IS (rfem_storage)
        IF (par%ilayer==0) field_type=iofield(bl%ibl)%f%field_type
        CALL par%mode_bcast(field_type,LEN(field_type),inode)
        IF (par%ilayer/=0)                                                      &
          CALL bl%alloc_real_fld(iofield(bl%ibl)%f,field_type,bl%on_gpu)
        CALL iofield(bl%ibl)%f%bcast(inode)
      CLASS IS (cfem_storage)
        IF (par%ilayer==0) field_type=iofield(bl%ibl)%f%field_type
        CALL par%mode_bcast(field_type,LEN(field_type),inode)
        IF (par%ilayer/=0)                                                      &
          CALL bl%alloc_comp_fld(iofield(bl%ibl)%f,field_type,bl%on_gpu)
        CALL iofield(bl%ibl)%f%send_and_trim(inode)
      END SELECT
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE distribute

!-------------------------------------------------------------------------------
!*  collect mode-distributed block data
!-------------------------------------------------------------------------------
  SUBROUTINE collect(bl,io_field_list,io_field_write,inode)
    USE nodal_mod
    USE pardata_mod
    IMPLICIT NONE

    !> block to collect
    CLASS(rblock), INTENT(IN) :: bl
    !> I/O field list pointing to data to collect
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)
    !> I/O field list pointing to data to collect to on layer zero
    TYPE(field_list_pointer), INTENT(IN) :: io_field_write(:)
    !> mpi mode process to collect fields on
    INTEGER(i4), INTENT(IN) :: inode

    CLASS(nodal_field_comp), ALLOCATABLE :: dummy_field
    INTEGER(i4) :: ifld
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'collect',iftn,idepth)
    DO ifld=1,SIZE(io_field_list)
      SELECT TYPE (iofield=>io_field_list(ifld)%p)
      CLASS IS (rfem_storage)
        ! Do nothing
      CLASS IS (cfem_storage)
        IF (par%ilayer==inode) THEN
          SELECT TYPE (iofield_write=>io_field_write(ifld)%p)
          CLASS IS (cfem_storage)
            CALL iofield(bl%ibl)%f%collect(iofield_write(bl%ibl)%f,inode)
          END SELECT
        ELSE
          CALL iofield(bl%ibl)%f%collect(dummy_field,inode)
        ENDIF
      END SELECT
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE collect

END MODULE
