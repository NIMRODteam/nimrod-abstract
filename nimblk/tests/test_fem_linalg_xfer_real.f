!-------------------------------------------------------------------------------
!! test FEM to linalg allocate and transfer routines for real types
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* test FEM to linalg allocate and transfer routines for real types
!-------------------------------------------------------------------------------
MODULE test_fem_linalg_xfer_real_mod
  USE alloc_linalg_mod
  USE local
  USE nimtest_utils
  USE nodal_mod
  USE matrix_mod
  USE pardata_mod
  USE vector_mod
  USE xfer_vector_to_fem_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: run_test_real

CONTAINS

!-------------------------------------------------------------------------------
!* driver routine for real tests, based on input
!-------------------------------------------------------------------------------
  SUBROUTINE run_test_real(field,coord_field,test_name)
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field
    CHARACTER(64), INTENT(IN) :: test_name

!-------------------------------------------------------------------------------
!   Call different test cases.
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("alloc_xfer")
      CALL test_alloc_xfer_real(field,coord_field)
    CASE ("alloc_mat")
      CALL test_alloc_mat_real(field,coord_field)
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_name))
    END SELECT
  END SUBROUTINE run_test_real

!-------------------------------------------------------------------------------
!> Test allocation of linear algebra and transfer of solution back to the FEM
!  space
!-------------------------------------------------------------------------------
  SUBROUTINE test_alloc_xfer_real(field,coord_field)
    USE quadrature_mod
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field

    TYPE(interp_real) :: eval_out,eval_ref
    CLASS(rvector), ALLOCATABLE :: vec
    CLASS(nodal_field_real), ALLOCATABLE :: field2
    INTEGER(i4) :: nf,ndf
    REAL(r8) :: rscalar
    REAL(r8), DIMENSION(coord_field%nqty) :: xi
!-------------------------------------------------------------------------------
!   allocate the linear algebra space, set the value and transfer back
!-------------------------------------------------------------------------------
    rscalar=0.5_r8
    CALL alloc_vector_for_fem(vec,field)
    CALL vec%assign_for_testing('constant',rscalar)
    CALL xfer_vector_to_fem(vec,field)
    CALL vec%dealloc
    DEALLOCATE(vec)
!-------------------------------------------------------------------------------
!   test the result with an evaluation
!-------------------------------------------------------------------------------
    nf=field%fqty*field%nqty
    ndf=field%dfqty*field%nqty
    ALLOCATE(eval_out%f(nf),eval_out%df(ndf))
    ALLOCATE(eval_ref%f(nf),eval_ref%df(ndf))
    xi=0.5_r8
    CALL field%eval(xi,1_i4,eval_out,dorder=1,                                  &
                    transform=.TRUE.,coord=coord_field)
    eval_ref%f=rscalar
    eval_ref%df=0._r8
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_xfer',         &
                 tolerance=1e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,TRIM(field%name)//' alloc_xfer deriv', &
                 tolerance=1e-14_r8)
!-------------------------------------------------------------------------------
!   now do the same test with 2 FEM spaces
!   allocate the linear algebra space, set the value and transfer back
!-------------------------------------------------------------------------------
    rscalar=5._r8
    CALL field%alloc_with_mold(field2)
    CALL alloc_vector_for_fem(vec,field,field2)
    CALL vec%assign_for_testing('constant',rscalar)
    CALL xfer_vector_to_fem(vec,field,field2)
    CALL vec%dealloc
    DEALLOCATE(vec)
!-------------------------------------------------------------------------------
!   test the result with an evaluation
!-------------------------------------------------------------------------------
    CALL field%eval(xi,1_i4,eval_out,dorder=1,                                  &
                    transform=.TRUE.,coord=coord_field)
    eval_ref%f=rscalar
    eval_ref%df=0._r8
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_xfer field 1', &
                 tolerance=2e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,                                       &
                 TRIM(field%name)//' alloc_xfer field 1 deriv',                 &
                 tolerance=2e-14_r8)
    CALL field2%eval(xi,1_i4,eval_out,dorder=1,                                 &
                     transform=.TRUE.,coord=coord_field)
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_xfer field 2', &
                 tolerance=2e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,                                       &
                 TRIM(field%name)//' alloc_xfer field 2 deriv',                 &
                 tolerance=2e-14_r8)
    CALL field2%dealloc
    DEALLOCATE(field2)
  END SUBROUTINE test_alloc_xfer_real

!-------------------------------------------------------------------------------
!> Test allocation of linear algebra including a matrix and transfer of
!  solution back to the FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE test_alloc_mat_real(field,coord_field)
    USE quadrature_mod
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field

    TYPE(interp_real) :: eval_out,eval_ref
    CLASS(rvector), ALLOCATABLE :: vec,vec2
    CLASS(rmatrix), ALLOCATABLE :: mat
    CHARACTER(1), ALLOCATABLE :: vcomp(:)
    CLASS(nodal_field_real), ALLOCATABLE :: field2
    INTEGER(i4) :: nf,ndf
    REAL(r8), DIMENSION(coord_field%nqty) :: xi
    REAL(r8) :: rscalar
!-------------------------------------------------------------------------------
!   allocate the linear algebra space, set the value, do a matvec with the
!   identity and transfer back to the FEM space
!-------------------------------------------------------------------------------
    rscalar=0.25_r8
    CALL alloc_vector_for_fem(vec,field)
    CALL vec%alloc_with_mold(vec2)
    ALLOCATE(vcomp(field%nqty))
    vcomp='s'
    CALL alloc_matrix_for_fem(mat,field,vcomp)
    DEALLOCATE(vcomp)
    CALL vec%assign_for_testing('constant',rscalar)
    CALL mat%set_identity
    CALL mat%matvec(vec,vec2)
    CALL xfer_vector_to_fem(vec2,field)
    CALL vec%dealloc
    CALL vec2%dealloc
    CALL mat%dealloc
    DEALLOCATE(vec,vec2,mat)
!-------------------------------------------------------------------------------
!   test the result with an evaluation
!-------------------------------------------------------------------------------
    nf=field%fqty*field%nqty
    ndf=field%dfqty*field%nqty
    ALLOCATE(eval_out%f(nf),eval_out%df(ndf))
    ALLOCATE(eval_ref%f(nf),eval_ref%df(ndf))
    xi=0.5_r8
    CALL field%eval(xi,1_i4,eval_out,dorder=1,                                  &
                    transform=.TRUE.,coord=coord_field)
    eval_ref%f=rscalar
    eval_ref%df=0._r8
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_mat',          &
                 tolerance=1e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,TRIM(field%name)//' alloc_mat deriv',  &
                 tolerance=1e-14_r8)
!-------------------------------------------------------------------------------
!   now do the same test with 2 FEM spaces
!   allocate the linear algebra space, set the value, do a matvec with the
!   identity and transfer back to the FEM space
!-------------------------------------------------------------------------------
    rscalar=4._r8
    CALL field%alloc_with_mold(field2)
    CALL alloc_vector_for_fem(vec,field,field2)
    CALL vec%alloc_with_mold(vec2)
    ALLOCATE(vcomp(field%nqty+field2%nqty))
    vcomp='s'
    CALL alloc_matrix_for_fem(mat,field,field2,vcomp)
    DEALLOCATE(vcomp)
    CALL vec%assign_for_testing('constant',rscalar)
    CALL mat%set_identity
    CALL mat%matvec(vec,vec2)
    CALL xfer_vector_to_fem(vec2,field,field2)
    CALL vec%dealloc
    CALL vec2%dealloc
    CALL mat%dealloc
    DEALLOCATE(vec,vec2,mat)
!-------------------------------------------------------------------------------
!   test the result with an evaluation
!-------------------------------------------------------------------------------
    CALL field%eval(xi,1_i4,eval_out,dorder=1,                                  &
                    transform=.TRUE.,coord=coord_field)
    eval_ref%f=rscalar
    eval_ref%df=0._r8
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_mat field 1',  &
                 tolerance=1e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,                                       &
                 TRIM(field%name)//' alloc_mat field 1 deriv',                  &
                 tolerance=1e-14_r8)
    CALL field2%eval(xi,1_i4,eval_out,dorder=1,                                 &
                     transform=.TRUE.,coord=coord_field)
    CALL wrequal(eval_out%f,eval_ref%f,TRIM(field%name)//' alloc_mat field 2',  &
                 tolerance=1e-14_r8)
    CALL wrequal(eval_out%df,eval_ref%df,                                       &
                 TRIM(field%name)//' alloc_mat field 2 deriv',                  &
                 tolerance=1e-14_r8)
    CALL field2%dealloc
    DEALLOCATE(field2)
  END SUBROUTINE test_alloc_mat_real

END MODULE test_fem_linalg_xfer_real_mod
