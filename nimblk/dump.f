!-------------------------------------------------------------------------------
!! Routines to read/write dump files.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Module with routines to read/write dump files.
!-------------------------------------------------------------------------------
MODULE dump_mod
  USE local
  USE timer_mod
  USE io
  USE seam_mod
  USE gblock_mod
  USE pardata_mod
  USE nodal_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: dump_read, dump_write

  CHARACTER(*), PARAMETER :: mod_name='dump'
CONTAINS

!-------------------------------------------------------------------------------
!* read from dump file and initialize parallel decomposition
!-------------------------------------------------------------------------------
  SUBROUTINE dump_read(dump_file,dump_time,dump_step,bl,seam,io_field_list,     &
                       all_blks_on_gpu)
    USE rblock_mod
    IMPLICIT NONE

    !> dump file name
    CHARACTER(*), INTENT(IN) :: dump_file
    !> time of dump file
    REAL(r8), INTENT(OUT) :: dump_time
    !> step of dump file
    INTEGER(i4), INTENT(OUT) :: dump_step
    !> block storage of size nbl
    TYPE(block_storage), ALLOCATABLE, INTENT(OUT) :: bl(:)
    !> seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> I/O field list with list of data to be read from each block
    TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)
    !> true if all blocks are on the GPU
    LOGICAL, OPTIONAL, INTENT(IN) :: all_blks_on_gpu

    INTEGER(i4) :: ibl,ifld,file_lock
    CHARACTER(8) :: idnm,bl_type
#ifdef __nvhpc
#ifdef NERSC_PERLMUTTER
    CHARACTER(128) :: msg
#endif
#endif
    INTEGER(HID_T) :: sid,blid
    LOGICAL :: dump_test
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'dump_read',iftn,idepth)
!-------------------------------------------------------------------------------
!   open dump file
!-------------------------------------------------------------------------------
#ifdef __nvhpc
#ifdef NERSC_PERLMUTTER
    WRITE(output_unit,*) 'dump_read A',par%node ! for perlmutter
    CALL FLUSH(output_unit)
#endif
#endif
    IF (par%node==0) THEN
      INQUIRE(file=TRIM(dump_file),exist=dump_test)
      IF (dump_test) THEN
        CALL open_oldh5file(TRIM(dump_file),fileid,rootgid,h5in,h5err)
      ELSE
        CALL par%nim_stop('Dump file '//TRIM(dump_file)//' does not exist.')
      ENDIF
!-------------------------------------------------------------------------------
!     read global attributes
!-------------------------------------------------------------------------------
      CALL open_group(rootgid,"dumpTime",sid,h5err)
      CALL read_attribute(sid,"vsTime",dump_time,h5in,h5err)
      CALL read_attribute(sid,"vsStep",dump_step,h5in,h5err)
      CALL close_group("dumpTime",sid,h5err)
      CALL read_attribute(rootgid,"nbl",par%nbl_total,h5in,h5err)
      CALL read_attribute(rootgid,"nmodes",par%nmodes_total,h5in,h5err)
      ALLOCATE(par%keff_total(par%nmodes_total))
      CALL read_h5(rootgid,"keff",par%keff_total,h5in,h5err)
      ALLOCATE(par%nindex_total(par%nmodes_total))
      CALL read_h5(rootgid,"nindex",par%nindex_total,h5in,h5err)
    ENDIF
!-------------------------------------------------------------------------------
!   broadcast and then initialize block pardata
!-------------------------------------------------------------------------------
    IF (par%nprocs>1) THEN
      CALL par%all_bcast(dump_time,0_i4)
      CALL par%all_bcast(dump_step,0_i4)
      CALL par%all_bcast(par%nbl_total,0_i4)
      CALL par%all_bcast(par%nmodes_total,0_i4)
      IF (par%node>0) ALLOCATE(par%keff_total(par%nmodes_total))
      CALL par%all_bcast(par%keff_total,par%nmodes_total,0_i4)
      IF (par%node>0) ALLOCATE(par%nindex_total(par%nmodes_total))
      CALL par%all_bcast(par%nindex_total,par%nmodes_total,0_i4)
    ENDIF
#ifdef __nvhpc
#ifdef NERSC_PERLMUTTER
    WRITE(output_unit,*) 'dump_read B',par%node ! for perlmutter
    CALL FLUSH(output_unit)
#endif
#endif
    CALL par%set_decomp(par%nmodes_total,par%nbl_total)
!-------------------------------------------------------------------------------
!   Allocate space for fields
!-------------------------------------------------------------------------------
    ALLOCATE(bl(par%nbl))
    DO ifld=1,SIZE(io_field_list)
      IF (io_field_list(ifld)%type=="rfem_storage") THEN
        ALLOCATE(rfem_storage::io_field_list(ifld)%p(par%nbl))
      ELSE ! cfem_storage
        ALLOCATE(cfem_storage::io_field_list(ifld)%p(par%nbl))
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   read seams/blocks sequentially by ilayer=0
!-------------------------------------------------------------------------------
    IF (par%ilayer==0) THEN
      IF (par%node_layer/=0) THEN
        CALL par%layer_recv(file_lock,par%node_layer-1)
        CALL open_oldh5file(TRIM(dump_file),fileid,rootgid,                     &
                            h5in,h5err,rw=.TRUE.)
      ENDIF
      CALL open_group(rootgid,"seams",sid,h5err)
      CALL seam%h5_read(sid)
      CALL close_group("seams",sid,h5err)
      CALL open_group(rootgid,"blocks",sid,h5err)
      DO ibl=1,par%nbl
        WRITE(idnm, fmt='(i4.4)') par%loc2glob(ibl)
        CALL open_group(sid,TRIM(idnm),blid,h5err)
!-------------------------------------------------------------------------------
!       allocate a block of the correct type and read associated fields
!-------------------------------------------------------------------------------
        CALL read_attribute(blid,"block_type",bl_type,h5in,h5err)
        CALL close_group(idnm,blid,h5err)
        SELECT CASE (TRIM(bl_type(1:6)))
        CASE ("rblock")
          ALLOCATE(rblock::bl(ibl)%b)
        CASE DEFAULT
          CALL par%nim_stop('dump_read :: unrecognized block type')
        END SELECT
        IF (PRESENT(all_blks_on_gpu)) THEN
          IF (all_blks_on_gpu) THEN
            bl(ibl)%b%on_gpu=.TRUE.
          ENDIF
        ENDIF
        CALL bl(ibl)%b%h5_read(par%loc2glob(ibl),sid,io_field_list)
        seam%s(ibl)%on_gpu=bl(ibl)%b%on_gpu
      ENDDO
      CALL close_group("blocks",sid,h5err)
      CALL close_h5file(fileid,rootgid,h5err)
      IF (par%node_layer/=par%nprocs_layer-1)                                   &
        CALL par%layer_send(par%node_layer,par%node_layer+1)
    ENDIF
!-------------------------------------------------------------------------------
!   distribute the seams/blocks/fields to all layers
!-------------------------------------------------------------------------------
    IF (par%nlayers>1) THEN
      DO ibl=1,par%nbl
        CALL par%mode_bcast(bl_type,LEN(bl_type),0)
        IF (par%ilayer/=0) THEN
          SELECT CASE (TRIM(bl_type(1:6)))
          CASE ("rblock")
            ALLOCATE(rblock::bl(ibl)%b)
          CASE DEFAULT
            CALL par%nim_stop('dump_read:: unrecognized block type')
          END SELECT
        ENDIF
        CALL bl(ibl)%b%distribute(io_field_list,0_i4)
      ENDDO
      CALL seam%distribute(0_i4)
    ENDIF
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE dump_read

!-------------------------------------------------------------------------------
!* write to dump file
!-------------------------------------------------------------------------------
  SUBROUTINE dump_write(dump_time,dump_step,bl,seam,io_field_list,file_name)
    IMPLICIT NONE

    !> time of dump file
    REAL(r8), INTENT(IN) :: dump_time
    !> step of dump file
    INTEGER(i4), INTENT(IN) :: dump_step
    !> block storage of size nbl
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> seam of size nbl
    TYPE(seam_type), INTENT(IN) :: seam
    !> I/O field list pointing to data to write to each block
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)
    !> file name to write, supercedes generated version (optional)
    CHARACTER(*), OPTIONAL, INTENT(IN) :: file_name

    INTEGER(i4) :: ibl,file_lock,ifld
    INTEGER(HID_T) :: sid
    CHARACTER(64) :: step_name,dump_file,dump_dir=".",dump_name="dump"
    LOGICAL :: dump_test
    TYPE(field_list_pointer), ALLOCATABLE :: io_field_write(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'dump_write',iftn,idepth)
    IF (dump_step>999999) THEN
      WRITE(step_name,fmt='(i7.7)') dump_step
    ELSE IF (dump_step>99999) THEN
      WRITE(step_name,fmt='(i6.6)') dump_step
    ELSE
      WRITE(step_name,fmt='(i5.5)') dump_step
    ENDIF
    IF (PRESENT(file_name)) THEN
      dump_file=file_name
    ELSE
      dump_file=TRIM(dump_name)//"."//TRIM(step_name)//".h5"
      IF (dump_dir/=".") THEN
        dump_file=TRIM(dump_dir)//TRIM(dump_file)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   open dump file
!-------------------------------------------------------------------------------
    IF (par%node==0) THEN
      INQUIRE(file=TRIM(dump_file),exist=dump_test)
      IF (dump_test) THEN
        CALL open_h5file('overwr',TRIM(dump_file),fileid,                       &
                         "NIMROD checkpoint data file",rootgid,h5in,h5err)
      ELSE
        CALL open_newh5file(TRIM(dump_file),fileid,                             &
                            "NIMROD checkpoint data file",rootgid,h5in,h5err)
      ENDIF
!-------------------------------------------------------------------------------
!     write global data
!-------------------------------------------------------------------------------
      CALL make_group(rootgid,"dumpTime",sid,h5in,h5err)
      CALL write_attribute(sid,"vsType","time",h5in,h5err)
      CALL write_attribute(sid,"vsTime",dump_time,h5in,h5err)
      CALL write_attribute(sid,"vsStep",dump_step,h5in,h5err)
      CALL close_group("dumpTime",sid,h5err)
      CALL write_attribute(rootgid,"nbl",par%nbl_total,h5in,h5err)
      CALL write_attribute(rootgid,"nmodes",par%nmodes_total,h5in,h5err)
      CALL dump_h5(rootgid,"keff",par%keff_total,h5in,h5err)
      CALL dump_h5(rootgid,"nindex",par%nindex_total,h5in,h5err)
    ENDIF
!-------------------------------------------------------------------------------
!   collect layer-distributed block data
!-------------------------------------------------------------------------------
    IF (par%ilayer==0) THEN
      ALLOCATE(io_field_write(SIZE(io_field_list)))
      DO ifld=1,SIZE(io_field_write)
        io_field_write(ifld)%name=io_field_list(ifld)%name
        SELECT TYPE (iofield=>io_field_list(ifld)%p)
        CLASS IS (rfem_storage)
          io_field_write(ifld)%p=>iofield
        CLASS IS (cfem_storage)
          IF (par%nlayers==1) THEN
            io_field_write(ifld)%p=>iofield
          ELSE
            ALLOCATE(cfem_storage::io_field_write(ifld)%p(par%nbl))
          ENDIF
        END SELECT
      ENDDO
    ENDIF
    IF (par%nlayers>1) THEN
      DO ibl=1,par%nbl
        CALL bl(ibl)%b%collect(io_field_list,io_field_write,0_i4)
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   write blocks/seams sequentially by ilayer=0
!-------------------------------------------------------------------------------
    IF (par%ilayer==0) THEN
      IF (par%node_layer/=0)  CALL par%layer_recv(file_lock,par%node_layer-1)
      IF (par%node_layer==0) THEN
        CALL make_group(rootgid,"seams",sid,h5in,h5err)
      ELSE
        CALL open_oldh5file(TRIM(dump_file),fileid,rootgid,                     &
                            h5in,h5err,rw=.TRUE.)
        CALL open_group(rootgid,"seams",sid,h5err)
      ENDIF
      CALL seam%h5_dump(sid)
      CALL close_group("seams",sid,h5err)
      IF (par%node_layer==0) THEN
        CALL make_group(rootgid,"blocks",sid,h5in,h5err)
      ELSE
        CALL open_group(rootgid,"blocks",sid,h5err)
      ENDIF
      DO ibl=1,par%nbl
        CALL bl(ibl)%b%h5_write(sid,io_field_write)
      ENDDO
      CALL close_group("blocks",sid,h5err)
      CALL close_h5file(fileid,rootgid,h5err)
      IF (par%node_layer/=par%nprocs_layer-1)                                   &
        CALL par%layer_send(par%node_layer,par%node_layer+1)
!-------------------------------------------------------------------------------
!     Deallocate full-layer fields if needed
!-------------------------------------------------------------------------------
      IF (par%nlayers>1) THEN
        DO ifld=1,SIZE(io_field_write)
          SELECT TYPE (iofield=>io_field_write(ifld)%p)
          CLASS IS (rfem_storage)
            ! Do nothing, always a pointer to io_field_list
          CLASS IS (cfem_storage)
            DO ibl=1,par%nbl
              CALL iofield(ibl)%f%dealloc
            ENDDO
          END SELECT
        ENDDO
      ENDIF
      DEALLOCATE(io_field_write)
    ENDIF
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE dump_write

END MODULE dump_mod
