######################################################################
#
# Add compiler specific definitions to flags interface library
#
######################################################################

#---------------------------------------------------------------------
# Set default fortran format to free and preprocess all files
#---------------------------------------------------------------------
set(CMAKE_Fortran_FORMAT FREE)
set(CMAKE_Fortran_PREPROCESS ON)

#---------------------------------------------------------------------
# Cray
#---------------------------------------------------------------------
if (${CMAKE_Fortran_COMPILER_ID} MATCHES "Cray")
  set(__cray TRUE)
  target_compile_options(nimrod_compile_options INTERFACE
      $<$<COMPILE_LANGUAGE:Fortran>:-h nosecond_underscore>
  )
#---------------------------------------------------------------------
# GCC
#---------------------------------------------------------------------
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "GNU")
  set(__gfortran TRUE)
  target_compile_options(nimrod_compile_options INTERFACE
      $<$<COMPILE_LANGUAGE:Fortran>:-fno-second-underscore>
      # Don't allow automatic reallocation (F2003 standard) as it may be
      # inefficient inside loops
      $<$<COMPILE_LANGUAGE:Fortran>:-fno-realloc-lhs>
      # set debug flags
      $<$<CONFIG:Debug>:-fcheck=all -fbacktrace>
      # add trap flags
      $<$<BOOL:${TRAP_FP_EXCEPTIONS}>:-ffpe-trap=invalid,zero,overflow>
      # warning flags
      -Wall -Wno-unused-dummy-argument
      # add warning for implicit interfaces
      $<$<COMPILE_LANGUAGE:Fortran>:-Wimplicit-interface -Wimplicit-procedure>
  )
  if (ENABLE_OPENACC)
    option(HAVE_OPENACC "if OpenACC is enabled" on)
    target_compile_options(nimrod_compile_options INTERFACE -fopenacc)
  endif ()
#---------------------------------------------------------------------
# Intel
#---------------------------------------------------------------------
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "Intel")
  set(__ifort TRUE)
  target_compile_options(nimrod_compile_options INTERFACE
      $<$<COMPILE_LANGUAGE:Fortran>:-assume no2underscores,protect_parens>
      # set debug flags
      $<$<CONFIG:Debug>:-check all -traceback>
  )
#---------------------------------------------------------------------
# nvhpc -- use included NVHPCConfig.cmake
#---------------------------------------------------------------------
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
  option(__nvhpc "using NVIDIA FC Compiler" on)
  message(STATUS "")
  message(STATUS "--------- Finding NVHPC utilities ---------")
  if (EXISTS $ENV{NVHPC_ROOT}/cmake/NVHPCConfig.cmake)
    set(NVHPC_FIND_COMPONENTS CUDA MATH HOSTUTILS)
    if (NVTX_PROFILE)
      list(APPEND NVHPC_FIND_COMPONENTS PROFILER)
    endif ()
    if (ENABLE_MPI)
      list(APPEND NVHPC_FIND_COMPONENTS MPI)
      set(NVHPC_FIND_REQUIRED_MPI TRUE)
    endif ()
    set(NVHPC_FIND_QUIETLY "")
    include($ENV{NVHPC_ROOT}/cmake/NVHPCConfig.cmake)
    print_target_properties(NVHPC::CUDA)
    print_target_properties(NVHPC::MATH)
    print_target_properties(NVHPC::HOSTUTILS)
    if (NVTX_PROFILE)
      print_target_properties(NVHPC::PROFILER)
    endif ()
  else ()
    # https://cmake.org/cmake/help/latest/module/FindCUDAToolkit.html
    find_package(CUDAToolkit REQUIRED)
    print_target_properties(CUDA::toolkit)
    print_target_properties(CUDA::cublas)
    print_target_properties(CUDA::cufftw)
    if (NVTX_PROFILE)
      print_target_properties(CUDA::nvtx3)
    endif ()
  endif ()
  target_compile_options(nimrod_compile_options INTERFACE
      $<$<COMPILE_LANGUAGE:Fortran>:-Mextend>
      # don't allow automatic reallocation (F2003 standard) as it may be
      # inefficient inside loops
      $<$<COMPILE_LANGUAGE:Fortran>:-Mallocatable=95>
      # require that all variables be declared.
      -Mdclchk
      # add trap flags
      $<$<BOOL:${TRAP_FP_EXCEPTIONS}>:-Ktrap=fp>
      # warning flag
      -Minform=warn
  )
  target_link_options(nimrod_compile_options INTERFACE
      # Disable executable stack
      -Wl,-znoexecstack
  )
  if (ENABLE_OPENACC)
    set(HAVE_OPENACC TRUE)
    set(HAVE_CUBLAS TRUE)
    set(HAVE_ACC_BLAS TRUE)
    # cc70 = V100; cc80 = A100; use ccnative by default
    set(NVHPC_GPU_FLAG "-gpu=lineinfo,cc${OPENACC_CC}")
    if (ENABLE_OPENACC_AUTOCOMPARE)
      string(APPEND NVHPC_GPU_FLAG ",autocompare")
      set(OPENACC_AUTOCOMPARE TRUE)
    endif ()
    target_compile_options(nimrod_compile_options INTERFACE
        # OpenACC flags
        $<$<COMPILE_LANGUAGE:Fortran>:-acc=gpu ${NVHPC_GPU_FLAG} -Minfo=accel>
    )
    target_link_options(nimrod_compile_options INTERFACE
        # OpenACC flags
        $<$<COMPILE_LANGUAGE:Fortran>:-acc=gpu>
	# link cuda math libraries
        $<$<COMPILE_LANGUAGE:Fortran>:-cudalib=nvblas,cublas,cufftw>
    )
    if (EXISTS $ENV{NVHPC_ROOT}/cmake/NVHPCConfig.cmake)
      target_link_libraries(nimrod_compile_options INTERFACE NVHPC::MATH)
      if (NVTX_PROFILE)
        target_link_libraries(nimrod_compile_options INTERFACE NVHPC::PROFILER)
      endif ()
    else ()
      target_link_libraries(nimrod_compile_options INTERFACE CUDA::cublas)
      get_target_property(link_lib CUDA::cublas LOCATION)
      get_filename_component(link_dir ${link_lib} DIRECTORY)
      target_link_directories(nimrod_compile_options INTERFACE
	                      ${link_dir})
      target_link_directories(nimrod_compile_options INTERFACE
	                      ${CUDAToolkit_LIBRARY_DIR})
      if (NVTX_PROFILE)
	target_link_libraries(nimrod_compile_options INTERFACE CUDA::nvtx3)
      endif ()
    endif ()
    string(REPLACE "-Mbounds" "" CMAKE_Fortran_FLAGS_DEBUG ${CMAKE_Fortran_FLAGS_DEBUG})
  else ()
    target_compile_options(nimrod_compile_options INTERFACE
        # bounds checking
        $<$<CONFIG:Debug>:-Mbounds>
    )
  endif ()
#---------------------------------------------------------------------
# Flang (AOCC)
#---------------------------------------------------------------------
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "Flang")
  set(__flang TRUE)
  target_compile_options(nimrod_compile_options INTERFACE
      # warning flag
      -Wall
      # Allow assumed rank array arguments
      -mmlir -allow-assumed-rank
  )
endif ()

message(STATUS "")
message(STATUS "--------- Compiler flags ---------")
message(STATUS "CMAKE_BUILD_TYPE= ${CMAKE_BUILD_TYPE}")
foreach (cmp C Fortran)
  foreach (bld RELEASE RELWITHDEBINFO MINSIZEREL DEBUG)
    message(STATUS "CMAKE_${cmp}_FLAGS_${bld}= ${CMAKE_${cmp}_FLAGS_${bld}}")
  endforeach ()
endforeach ()
get_target_property(NIMROD_COMPILE_FLAGS_LIST nimrod_compile_options
    INTERFACE_COMPILE_OPTIONS)
string(REPLACE ";" " " NIMROD_COMPILE_FLAGS "${NIMROD_COMPILE_FLAGS_LIST}")
message(STATUS "Additional compile flags = ${NIMROD_COMPILE_FLAGS}")
