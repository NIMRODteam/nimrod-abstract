######################################################################
#
# Parse information about the git repo.
#
######################################################################

message(STATUS "")
message(STATUS "--------- Repository information ---------")

# Get the current working branch
execute_process(
    COMMAND git rev-parse --abbrev-ref HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Get the latest abbreviated commit hash of the working branch
execute_process(
    COMMAND git log -1 --format=%h
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Get the origin url
execute_process(
    COMMAND git config --get remote.origin.url
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_URL
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Check for modifications
execute_process(
    COMMAND bash -c "git diff-index --quiet HEAD || echo 'with local modifications'"
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_MODIFICATIONS
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(PROJECT_VERSION ${GIT_BRANCH}-${GIT_COMMIT_HASH})
if (NOT PROJECT_URL)
  set(PROJECT_URL "https://gitlab.com/NIMRODteam/open/nimrod-aai")
endif ()
message(STATUS "URL= ${GIT_URL}")
message(STATUS "BRANCH= ${GIT_BRANCH} at ${GIT_COMMIT_HASH} ${GIT_MODIFICATIONS}")
message(STATUS "PROJECT_URL= ${PROJECT_URL}")
