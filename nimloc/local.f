!-------------------------------------------------------------------------------
!< basic kinds and basic functions
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing defintions of real and integer kinds and nondimensional
!  mathemetical constants
!-------------------------------------------------------------------------------
MODULE local
  USE ISO_Fortran_env, ONLY: int32,int64,real32,real64
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: i4,i8,r4,r8

  PUBLIC :: pi,twopi,onethird,twothird

  PUBLIC :: system_timer

  !> 32-bit integer type
  INTEGER, PARAMETER :: i4=int32
  !> 64-bit integer type
  INTEGER, PARAMETER :: i8=int64
  !> 32-bit real type
  INTEGER, PARAMETER :: r4=real32
  !> 64-bit real type
  INTEGER, PARAMETER :: r8=real64
  !> ratio of a circle circumference to its diameter
  REAL(r8), PARAMETER :: pi=3.1415926535897932385_r8
  !> ratio of a circle circumference to its radius
  REAL(r8), PARAMETER :: twopi=2._r8*pi
  !> one divided by three
  REAL(r8), PARAMETER :: onethird=1._r8/3._r8
  !> two divided by three
  REAL(r8), PARAMETER :: twothird=2._r8/3._r8
CONTAINS

!-------------------------------------------------------------------------------
!> System-dependent timer - returns elapsed CPU seconds
!  the 8-byte integer variables and count_tmp test are used to deal
!  with system-clock resettings and the limited maximum count.
!-------------------------------------------------------------------------------
  SUBROUTINE system_timer(time)
#ifdef HAVE_MPI
#ifdef HAVE_MPI_F08
    USE mpi_f08
#else
    USE mpi
#endif
#endif
    IMPLICIT NONE

    REAL(r8), INTENT(INOUT) :: time

#ifdef HAVE_MPI
!-------------------------------------------------------------------------------
!   mpi timer:
!-------------------------------------------------------------------------------
    time = mpi_wtime()
#else
    system_clock_timer: BLOCK
      INTEGER(i4), SAVE :: count_rate,count_max,count
      INTEGER(i8), SAVE :: last_count,count_tmp
      LOGICAL, SAVE :: first_call=.true.
!-------------------------------------------------------------------------------
!     f90 intrinsic timer:
!-------------------------------------------------------------------------------
      IF (first_call) THEN
        CALL SYSTEM_CLOCK(count=count,count_rate=count_rate,                    &
                          count_max=count_max)
        count_tmp=count
        first_call=.false.
      ELSE
        CALL SYSTEM_CLOCK(count=count)
        count_tmp=count
        DO WHILE (count_tmp<last_count-count_max/2)
          count_tmp=count_tmp+count_max
        ENDDO
      ENDIF
      time=REAL(count_tmp,r8)/count_rate
      last_count=count_tmp
    END BLOCK system_clock_timer
#endif

  END SUBROUTINE system_timer

END MODULE local
