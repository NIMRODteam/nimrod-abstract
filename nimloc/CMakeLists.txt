######################################################################          
#                                                                               
# CMakeLists.txt for nimloc
#                                                                               
######################################################################

set(NIMLOC_SOURCES
  convert_type.f
  local.f
)

add_library(nimloc-obj OBJECT ${NIMLOC_SOURCES})
list(APPEND OBJ_TARGETS $<TARGET_OBJECTS:nimloc-obj>)
target_link_libraries(nimloc-obj PUBLIC nimrod_compile_options)
add_dependencies(nimloc-obj config_f)
if (ENABLE_MPI)
  target_link_libraries(nimloc-obj PRIVATE MPI::MPI_Fortran)
endif ()
target_include_directories(nimloc-obj PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)
add_library(nimloc STATIC $<TARGET_OBJECTS:nimloc-obj>)
target_link_libraries(nimloc PUBLIC nimloc-obj)

set(OBJ_TARGETS ${OBJ_TARGETS} PARENT_SCOPE)
