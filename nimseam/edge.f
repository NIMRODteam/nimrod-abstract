!-------------------------------------------------------------------------------
!! module containing types for block boundary communication
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module containing types for block boundary communication
!-------------------------------------------------------------------------------
MODULE edge_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: edge_type

  CHARACTER(*), PARAMETER :: mod_name='edge'
!-------------------------------------------------------------------------------
!* Type pointing to element vertices
!-------------------------------------------------------------------------------
  TYPE :: vertex_type
    !> indexing associated with vertex (used by vector routines)
    INTEGER(i4), DIMENSION(2) :: intxy
    !> number of seams that contribute to this vertex
    INTEGER(i4) :: nimage
    !> order(0:nimage) array for consistent sums across processors
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: order
    !> array that holds indexing information, size(2,nimage)
    !  ptr(1,:) = block index, ptr(2,:) = vertex index
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: ptr
    !> version of ptr without references to seam 0
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: ptr2
    !> averaging factor to apply to avoid double counting contributions
    REAL(r8) :: ave_factor
  END TYPE vertex_type

!-------------------------------------------------------------------------------
!* Type pointing to element edges/sides between element vertices
!-------------------------------------------------------------------------------
  TYPE :: segment_type
    !> indexing associated with segment (used by vector routines)
    INTEGER(i4), DIMENSION(2) :: intxys
    !> indexing associated with "next" vertex
    INTEGER(i4), DIMENSION(2) :: intxyn
    !> indexing associated with "previous" vertex
    INTEGER(i4), DIMENSION(2) :: intxyp
    !> array that holds indexing information, size(2)
    !  ptr(1) = block index, ptr(2) = segment index
    INTEGER(i4), DIMENSION(2) :: ptr
    !> direction to load segment (used by vector routines)
    INTEGER(i4) :: load_dir
    !> boolean to switch between horizontal and vertical sides
    LOGICAL :: h_side
    !> averaging factor to apply to avoid double counting contributions
    REAL(r8) :: ave_factor
  END TYPE segment_type

!-------------------------------------------------------------------------------
! Basic edge type associated with each block
!-------------------------------------------------------------------------------
  TYPE :: edge_type
    !> block index associated with edge_type
    INTEGER(i4) :: id
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> number of vertices
    INTEGER(i4) :: nvert
    !> vertex(1:nvert) stores vertex edge information
    TYPE(vertex_type), DIMENSION(:), ALLOCATABLE :: vertex
    !> segment(1:nvert) stores segment edge information
    TYPE(segment_type), DIMENSION(:), ALLOCATABLE :: segment
    !> nqty loaded set by vector%edge_load_arr
    INTEGER(i4) :: nqty_loaded
    !> nside loaded set by vector%edge_load_arr
    INTEGER(i4) :: nside_loaded
    !> nmodes loaded set by vector%edge_load_arr
    INTEGER(i4) :: nmodes_loaded
    !> imode starting index set by vector%edge_load_arr
    INTEGER(i4) :: imode_start
    !> expoint(1:nvert), true if exterior point
    LOGICAL, DIMENSION(:), ALLOCATABLE :: expoint
    !> excorner(1:nvert), true if exterior corner
    LOGICAL, DIMENSION(:), ALLOCATABLE :: excorner
    !> r0point(1:nvert), true if point is at R=0
    LOGICAL, DIMENSION(:), ALLOCATABLE :: r0point
    !> exsegment(1:nvert), true if exterior segment
    LOGICAL, DIMENSION(:), ALLOCATABLE :: exsegment
    !> r0segment(1:nvert), true segment is at R=0
    LOGICAL, DIMENSION(:), ALLOCATABLE :: r0segment
    !> seam vertex input data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_in
    !> seam vertex output data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_out
    !> seam vertex save data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_save
    !> seam vertex hold data array used for ordering
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: vert_hold
    !> seam vertex input complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: vert_cin
    !> seam vertex output complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: vert_cout
    !> seam vertex complex save data array
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: vert_csave
    !> seam vertex normal unit vector
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_norm
    !> seam vertex tangent unit vector
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_tang
    !> seam vertex hold data array used for ordering
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: vert_chold
    !> seam segment input data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: seg_in
    !> seam segment output data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: seg_out
    !> seam segment save data array
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: seg_save
    !> seam segment input complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: seg_cin
    !> seam segment output complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: seg_cout
    !> seam segment complex save data array
    COMPLEX(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: seg_csave
    !> seam segment normal unit vector
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: seg_norm
    !> seam segment tangent unit vector
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: seg_tang
  CONTAINS

    PROCEDURE, PASS(edge) :: init
    PROCEDURE, PASS(edge) :: alloc_gpu_norm_tang
    PROCEDURE, PASS(edge) :: dealloc
    PROCEDURE, PASS(edge) :: copy
    PROCEDURE, PASS(edge) :: h5_read
    PROCEDURE, PASS(edge) :: h5_dump
    PROCEDURE, PASS(edge) :: distribute
    PROCEDURE, PASS(edge) :: zero_save
    PROCEDURE, PASS(edge) :: zero_csave
    PROCEDURE, NOPASS :: load_limits
  END TYPE edge_type

CONTAINS

!-------------------------------------------------------------------------------
!> initializes arrays used for border communication. note that this routine
!  requires having nvert, ptr, expoint and r0point defined within the seam
!  structure.
!-------------------------------------------------------------------------------
  SUBROUTINE init(edge,max_nqty,max_nqty_save,nside,nmodes,glob_nvert)
    USE pardata_mod
    IMPLICIT NONE

    !> seam to initialize
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> max quantity size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty
    !> max save size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty_save
    !> number of dof on each segment (for allocation)
    INTEGER(i4), INTENT(IN) :: nside
    !> number of modes (for allocation)
    INTEGER(i4), INTENT(IN) :: nmodes
    !> nvert for all other seams with global block indexing
    INTEGER(i4), INTENT(IN) :: glob_nvert(:)

    INTEGER(i4) :: iv,nv,np,npb,ipb,ip,n0,in,jbv,jvv,max_imags,max_np
    INTEGER(i4) :: ivp,jbpr,jvpr,ippr,max_nq,max_nq_save
    INTEGER(i4), DIMENSION(1) :: loc
    LOGICAL :: match
    LOGICAL, DIMENSION(:), ALLOCATABLE :: ord_mask
    REAL(r8), DIMENSION(:), ALLOCATABLE :: ord_arr
    CHARACTER(64) :: msg
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init',iftn,idepth)
!-------------------------------------------------------------------------------
!   require at least max_nq=2 for matrix exports
!-------------------------------------------------------------------------------
    max_nq=MAX(max_nqty,2)
    max_nq_save=max_nqty_save
!-------------------------------------------------------------------------------
!   create a second ptr array without references to seam0 and with
!   interior vertex label cross references for parallel coding.
!-------------------------------------------------------------------------------
    max_imags=0
    max_np=0
    DO iv=1,edge%nvert
      np=SIZE(edge%vertex(iv)%ptr,2)
      n0=0
      DO ip=1,np
        IF (edge%vertex(iv)%ptr(1,ip)==0) n0=n0+1
      ENDDO
      npb=np-n0
      edge%vertex(iv)%nimage = npb
      max_imags=MAX(npb,max_imags)
      ALLOCATE(edge%vertex(iv)%ptr2(2,npb))
      ipb=0
      DO ip=1,np
        IF (edge%vertex(iv)%ptr(1,ip)==0) CYCLE
        ipb=ipb+1
        edge%vertex(iv)%ptr2(1:2,ipb)=edge%vertex(iv)%ptr(1:2,ip)
      ENDDO
      IF (ipb/=npb) CALL par%nim_stop('Error in edge_init.')
      np=edge%vertex(iv)%nimage+1
      max_np=MAX(np,max_np)
      edge%vertex(iv)%ave_factor=1._r8/REAL(np,r8)
    ENDDO
!-------------------------------------------------------------------------------
!   determine a unique order for summing seam vertices with more than
!   two images to prevent the occurrence of different round-off errors
!   on different blocks, which leads to an instability.
!-------------------------------------------------------------------------------
    ALLOCATE(ord_mask(max_imags+1),ord_arr(max_imags+1))
    nv=edge%nvert
    DO iv=1,nv
      np=edge%vertex(iv)%nimage
      ALLOCATE(edge%vertex(iv)%order(0:np))
      ord_arr(1)=edge%id+1.e-9_r8*REAL(iv,r8)
      DO ip=1,np
        ord_arr(ip+1)=edge%vertex(iv)%ptr2(1,ip)                                &
               +1.e-9_r8*REAL(edge%vertex(iv)%ptr2(2,ip),r8)
      ENDDO
      ord_mask(1:np+1)=.true.
      DO ip=1,np+1
        loc=MINLOC(ord_arr(1:np+1),ord_mask(1:np+1))
        in=loc(1)
        edge%vertex(iv)%order(in-1)=ip
        ord_mask(in)=.false.
      ENDDO
      ord_mask(1:np+1)=.true.
      DO ip=0,np
        ord_mask(edge%vertex(iv)%order(ip))=.false.
      ENDDO
      DO ip=1,np+1
        IF (ord_mask(ip))                                                       &
          CALL par%nim_stop('Edge_type::init: vertex order not unique.')
      ENDDO
    ENDDO
    DEALLOCATE(ord_mask,ord_arr)
!-------------------------------------------------------------------------------
!   create internal references and communication pointers for
!   edge-segment centered quantities.  the latter uses the fact that
!   seams running around two adjacent blocks always advance in
!   opposite directions.
!-------------------------------------------------------------------------------
    ALLOCATE(edge%segment(nv))
    ALLOCATE(edge%exsegment(nv))
    edge%exsegment=.FALSE.
    ALLOCATE(edge%r0segment(nv))
    edge%r0segment=.FALSE.
    DO iv=1,nv
      ivp=iv-1
      IF (ivp==0) ivp=nv
      edge%segment(iv)%intxyn=edge%vertex(iv)%intxy
      edge%segment(iv)%intxyp=edge%vertex(ivp)%intxy
      edge%segment(iv)%ptr=0
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) edge%exsegment(iv)=.TRUE.
      IF (edge%r0point(iv).AND.edge%r0point(ivp)) edge%r0segment(iv)=.TRUE.
      IF ((edge%expoint(iv).OR.edge%r0point(iv)).AND.                           &
          (edge%expoint(ivp).OR.edge%r0point(ivp))) THEN
        edge%segment(iv)%ave_factor=1._r8
        CYCLE
      ELSE
        edge%segment(iv)%ave_factor=0.5_r8
      ENDIF
      match=.false.
      ptr_loop: DO ip=1,SIZE(edge%vertex(iv)%ptr2,2)
        jbv=edge%vertex(iv)%ptr2(1,ip)
        jvv=edge%vertex(iv)%ptr2(2,ip)
        IF (jvv==glob_nvert(jbv)) jvv=0
        DO ippr=1,SIZE(edge%vertex(ivp)%ptr2,2)
          jbpr=edge%vertex(ivp)%ptr2(1,ippr)
          jvpr=edge%vertex(ivp)%ptr2(2,ippr)
          IF (jbpr==jbv.AND.jvv==jvpr-1) THEN
            match=.true.
            edge%segment(iv)%ptr(1)=jbv
            edge%segment(iv)%ptr(2)=jvpr
            EXIT ptr_loop
          ENDIF
        ENDDO
      ENDDO ptr_loop
      IF (.NOT.match) THEN
        WRITE(msg,'(a,i3,a,i3)')                                                &
          'No edge match in edge_type::init, id=', edge%id,' iv=',iv
        CALL par%nim_stop(msg)
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   allocate fused versions of in/out/save arrays for GPU kernels
!-------------------------------------------------------------------------------
    ALLOCATE(edge%vert_in(max_nq,nv))
    ALLOCATE(edge%vert_cin(max_nq*nmodes,nv))
    ALLOCATE(edge%vert_out(max_nq,nv))
    ALLOCATE(edge%vert_cout(max_nq*nmodes,nv))
    ALLOCATE(edge%vert_save(max_nq_save,nv))
    ALLOCATE(edge%vert_csave(max_nq_save,nmodes,nv))
    ALLOCATE(edge%vert_hold(max_np+1,max_nq,nv))
    ALLOCATE(edge%vert_chold(max_np+1,max_nq*nmodes,nv))
    ALLOCATE(edge%seg_in(max_nq*nside,nv))
    ALLOCATE(edge%seg_cin(max_nq*nside*nmodes,nv))
    ALLOCATE(edge%seg_out(max_nq*nside,nv))
    ALLOCATE(edge%seg_cout(max_nq*nside*nmodes,nv))
    ALLOCATE(edge%seg_save(max_nq_save,nside,nv))
    ALLOCATE(edge%seg_csave(max_nq_save,nside,nmodes,nv))
!-------------------------------------------------------------------------------
!   copy/create seam to/on gpu, norm and tang are copied later
!-------------------------------------------------------------------------------
    ASSOCIATE(vertex=>edge%vertex,segment=>edge%segment,                        &
              expoint=>edge%expoint,excorner=>edge%excorner,                    &
              r0point=>edge%r0point,exsegment=>edge%exsegment,                  &
              r0segment=>edge%r0segment,vert_in=>edge%vert_in,                  &
              vert_cin=>edge%vert_cin,vert_out=>edge%vert_out,                  &
              vert_cout=>edge%vert_cout,vert_save=>edge%vert_save,              &
              vert_csave=>edge%vert_csave,seg_in=>edge%seg_in,                  &
              seg_cin=>edge%seg_cin,seg_out=>edge%seg_out,                      &
              seg_cout=>edge%seg_cout,seg_save=>edge%seg_save,                  &
              seg_csave=>edge%seg_csave)
      vert_in=0._r8
      vert_cin=0._r8
      vert_out=0._r8
      vert_cout=0._r8
      vert_save=0._r8
      vert_csave=0._r8
      seg_in=0._r8
      seg_cin=0._r8
      seg_out=0._r8
      seg_cout=0._r8
      seg_save=0._r8
      seg_csave=0._r8
      !$acc enter data copyin(edge,vertex,segment,expoint,excorner,r0point)     &
      !$acc copyin(exsegment,r0segment,vert_in,vert_cin,vert_out,vert_cout)     &
      !$acc copyin(vert_save,vert_csave,seg_in,seg_cin,seg_out,seg_cout)        &
      !$acc copyin(seg_save,seg_csave) async(edge%id) if(edge%on_gpu)
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init

!-------------------------------------------------------------------------------
!>  allocate and transfer norm/tang to the device
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_gpu_norm_tang(edge)
    IMPLICIT NONE

    !> seam to transfer norm/tang in
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_gpu_norm_tang',iftn,idepth)
    ASSOCIATE(vert_norm=>edge%vert_norm,vert_tang=>edge%vert_tang,              &
              seg_norm=>edge%seg_norm,seg_tang=>edge%seg_tang)
      IF (ALLOCATED(edge%vert_tang)) THEN
        !$acc enter data copyin(vert_tang) async(edge%id) if(edge%on_gpu)
        !$acc enter data copyin(vert_norm) async(edge%id) if(edge%on_gpu)
      ENDIF
      IF (ALLOCATED(edge%seg_tang)) THEN
        !$acc enter data copyin(seg_tang) async(edge%id) if(edge%on_gpu)
        !$acc enter data copyin(seg_norm) async(edge%id) if(edge%on_gpu)
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_gpu_norm_tang

!-------------------------------------------------------------------------------
!>  deallocate edge_type
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc(edge)
    IMPLICIT NONE

    !> seam to dealloc
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    LOGICAL :: do_dealloc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc',iftn,idepth)
!-------------------------------------------------------------------------------
!   seam zero is never on gpu
!-------------------------------------------------------------------------------
    do_dealloc=.FALSE.
    IF (edge%id/=0.AND.edge%on_gpu) do_dealloc=.TRUE.
!-------------------------------------------------------------------------------
!   deallocate edge_type
!-------------------------------------------------------------------------------
    ASSOCIATE(vertex=>edge%vertex,segment=>edge%segment,                        &
              expoint=>edge%expoint,excorner=>edge%excorner,                    &
              r0point=>edge%r0point,exsegment=>edge%exsegment,                  &
              r0segment=>edge%r0segment,vert_in=>edge%vert_in,                  &
              vert_cin=>edge%vert_cin,vert_out=>edge%vert_out,                  &
              vert_cout=>edge%vert_cout,vert_save=>edge%vert_save,              &
              vert_csave=>edge%vert_csave,seg_in=>edge%seg_in,                  &
              seg_cin=>edge%seg_cin,seg_out=>edge%seg_out,                      &
              seg_cout=>edge%seg_cout,seg_save=>edge%seg_save,                  &
              seg_csave=>edge%seg_csave,                                        &
              vert_norm=>edge%vert_norm,vert_tang=>edge%vert_tang,              &
              seg_norm=>edge%seg_norm,seg_tang=>edge%seg_tang)
      IF (ALLOCATED(edge%vertex)) THEN
        DO iv=1,edge%nvert
          IF (ALLOCATED(edge%vertex(iv)%order))                                 &
            DEALLOCATE(edge%vertex(iv)%order)
          IF (ALLOCATED(edge%vertex(iv)%ptr)) DEALLOCATE(edge%vertex(iv)%ptr)
          IF (ALLOCATED(edge%vertex(iv)%ptr2)) DEALLOCATE(edge%vertex(iv)%ptr2)
        ENDDO
        !$acc exit data delete(vertex) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vertex)
      ENDIF
      IF (ALLOCATED(edge%segment)) THEN
        !$acc exit data delete(segment) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%segment)
      ENDIF
      IF (ALLOCATED(edge%vert_in)) THEN
        !$acc exit data delete(vert_in) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_in)
      ENDIF
      IF (ALLOCATED(edge%vert_cin)) THEN
        !$acc exit data delete(vert_cin) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_cin)
      ENDIF
      IF (ALLOCATED(edge%vert_out)) THEN
        !$acc exit data delete(vert_out) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_out)
      ENDIF
      IF (ALLOCATED(edge%vert_cout)) THEN
        !$acc exit data delete(vert_cout) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_cout)
      ENDIF
      IF (ALLOCATED(edge%vert_save)) THEN
        !$acc exit data delete(vert_save) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_save)
      ENDIF
      IF (ALLOCATED(edge%vert_csave)) THEN
        !$acc exit data delete(vert_csave) finalize async(edge%id)              &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_csave)
      ENDIF
      IF (ALLOCATED(edge%vert_norm)) THEN
        !$acc exit data delete(vert_norm) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_norm)
      ENDIF
      IF (ALLOCATED(edge%vert_tang)) THEN
        !$acc exit data delete(vert_tang) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%vert_tang)
      ENDIF
      IF (ALLOCATED(edge%vert_hold)) THEN
        DEALLOCATE(edge%vert_hold)
      ENDIF
      IF (ALLOCATED(edge%vert_chold)) THEN
        DEALLOCATE(edge%vert_chold)
      ENDIF
      IF (ALLOCATED(edge%seg_in)) THEN
        !$acc exit data delete(seg_in) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_in)
      ENDIF
      IF (ALLOCATED(edge%seg_cin)) THEN
        !$acc exit data delete(seg_cin) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_cin)
      ENDIF
      IF (ALLOCATED(edge%seg_out)) THEN
        !$acc exit data delete(seg_out) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_out)
      ENDIF
      IF (ALLOCATED(edge%seg_cout)) THEN
        !$acc exit data delete(seg_cout) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_cout)
      ENDIF
      IF (ALLOCATED(edge%seg_save)) THEN
        !$acc exit data delete(seg_save) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_save)
      ENDIF
      IF (ALLOCATED(edge%seg_csave)) THEN
        !$acc exit data delete(seg_csave) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_csave)
      ENDIF
      IF (ALLOCATED(edge%seg_norm)) THEN
        !$acc exit data delete(seg_norm) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_norm)
      ENDIF
      IF (ALLOCATED(edge%seg_tang)) THEN
        !$acc exit data delete(seg_tang) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%seg_tang)
      ENDIF
      IF (ALLOCATED(edge%expoint)) THEN
        !$acc exit data delete(expoint) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%expoint)
      ENDIF
      IF (ALLOCATED(edge%excorner)) THEN
        !$acc exit data delete(excorner) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%excorner)
      ENDIF
      IF (ALLOCATED(edge%r0point)) THEN
        !$acc exit data delete(r0point) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%r0point)
      ENDIF
      IF (ALLOCATED(edge%exsegment)) THEN
        !$acc exit data delete(exsegment) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%exsegment)
      ENDIF
      IF (ALLOCATED(edge%r0segment)) THEN
        !$acc exit data delete(r0segment) finalize async(edge%id) if(do_dealloc)
        DEALLOCATE(edge%r0segment)
      ENDIF
    END ASSOCIATE
    !$acc exit data delete(edge) finalize async(edge%id) if(do_dealloc)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!>  copy edge_type, init must be called after the copy
!-------------------------------------------------------------------------------
  SUBROUTINE copy(edge,edge_in)
    IMPLICIT NONE

    !> seam to copy too
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> seam to copy from
    CLASS(edge_type), INTENT(IN) :: edge_in

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'copy',iftn,idepth)
    edge%id=edge_in%id
    edge%nvert=edge_in%nvert
    ALLOCATE(edge%vertex(edge%nvert))
    DO iv=1,edge%nvert
      ALLOCATE(edge%vertex(iv)%ptr(2,SIZE(edge_in%vertex(iv)%ptr,2)))
      edge%vertex(iv)%ptr=edge_in%vertex(iv)%ptr
      IF (edge%id>0) edge%vertex(iv)%intxy=edge_in%vertex(iv)%intxy
    ENDDO
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(edge%excorner(edge%nvert))
      ALLOCATE(edge%r0point(edge%nvert))
      edge%excorner=edge_in%excorner
      edge%r0point=edge_in%r0point
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE copy

!-------------------------------------------------------------------------------
!> read seam data from dump file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read(edge,id,gid)
    USE io
    IMPLICIT NONE

    !> seam to write
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> seam global index number
    INTEGER(i4), INTENT(IN) :: id
    !> h5 group to read seam from
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: iv,np,ipt,npt
    INTEGER(i4), ALLOCATABLE :: exc(:),r0pt(:),bptr(:,:),bxy(:,:)
    CHARACTER(64) :: seam_name
    INTEGER(HID_T) :: sid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read',iftn,idepth)
!-------------------------------------------------------------------------------
!   open seam group.
!-------------------------------------------------------------------------------
    WRITE(seam_name,fmt='(i4.4)') id
    CALL open_group(gid,TRIM(seam_name),sid,h5err)
!-------------------------------------------------------------------------------
!   read block descriptors.
!-------------------------------------------------------------------------------
    CALL read_attribute(sid,"id",edge%id,h5in,h5err)
    CALL read_attribute(sid,"nvert",edge%nvert,h5in,h5err)
!-------------------------------------------------------------------------------
!   read seam data.
!-------------------------------------------------------------------------------
    ALLOCATE(exc(edge%nvert),edge%vertex(edge%nvert))
    CALL read_h5(sid,"np",exc,h5in,h5err)
    npt=SUM(exc)
    ALLOCATE(bptr(2,npt))
    CALL read_h5(sid,"vertex",bptr,h5in,h5err)
    IF (edge%id>0) THEN
      ALLOCATE(bxy(2,edge%nvert))
      CALL read_h5(sid,"intxy",bxy,h5in,h5err)
    ENDIF
    ipt=1_i4
    DO iv=1,edge%nvert
      np=exc(iv)
      ALLOCATE(edge%vertex(iv)%ptr(2,np))
      edge%vertex(iv)%ptr=bptr(:,ipt:ipt+np-1)
      ipt=ipt+np
      IF (edge%id>0) edge%vertex(iv)%intxy=bxy(:,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   read seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(edge%excorner(edge%nvert))
      ALLOCATE(edge%r0point(edge%nvert))
      ALLOCATE(r0pt(edge%nvert))
      CALL read_h5(sid,"excorner",exc,h5in,h5err)
      CALL read_h5(sid,"r0point",r0pt,h5in,h5err)
      DO iv=1,edge%nvert
        edge%excorner(iv)=(exc(iv)>0)
        edge%r0point(iv)=(r0pt(iv)>0)
      ENDDO
      DEALLOCATE(r0pt)
    ENDIF
    DEALLOCATE(exc)
!-------------------------------------------------------------------------------
!   close group
!-------------------------------------------------------------------------------
    CALL close_group(TRIM(seam_name),sid,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read

!-------------------------------------------------------------------------------
!> write seam data to dump file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_dump(edge,gid)
    USE io
    IMPLICIT NONE

    !> seam to write
    CLASS(edge_type), INTENT(IN) :: edge
    !> h5 group to write seam to
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: iv,np,npt,ipt
    INTEGER(i4), ALLOCATABLE :: exc(:),r0pt(:),bptr(:,:),bxy(:,:)
    CHARACTER(64) :: seam_name
    INTEGER(HID_T) :: sid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_dump',iftn,idepth)
!-------------------------------------------------------------------------------
!   setup seam group.
!-------------------------------------------------------------------------------
    WRITE(seam_name,fmt='(i4.4)') edge%id
    CALL make_group(gid,TRIM(seam_name),sid,h5in,h5err)
!-------------------------------------------------------------------------------
!   write block descriptors.
!-------------------------------------------------------------------------------
    CALL write_attribute(sid,"id",edge%id,h5in,h5err)
    CALL write_attribute(sid,"nvert",edge%nvert,h5in,h5err)
!-------------------------------------------------------------------------------
!   write seam data, combine to write larger arrays.
!-------------------------------------------------------------------------------
    ALLOCATE(exc(edge%nvert))
    DO iv=1,edge%nvert
      exc(iv)=SIZE(edge%vertex(iv)%ptr,2)
    ENDDO
    CALL dump_h5(sid,"np",exc,h5in,h5err)
    npt=SUM(exc)
    ALLOCATE(bptr(2,npt))
    IF (edge%id>0) ALLOCATE(bxy(2,edge%nvert))
    ipt=1_i4
    DO iv=1,edge%nvert
      np=SIZE(edge%vertex(iv)%ptr,2)
      bptr(:,ipt:ipt+np-1)=edge%vertex(iv)%ptr
      ipt=ipt+np
      IF (edge%id>0) bxy(:,iv)=edge%vertex(iv)%intxy
    ENDDO
    CALL dump_h5(sid,"vertex",bptr,h5in,h5err)
    DEALLOCATE(bptr)
    IF (edge%id>0) THEN
      CALL dump_h5(sid,"intxy",bxy,h5in,h5err)
      DEALLOCATE(bxy)
    ENDIF
!-------------------------------------------------------------------------------
!   write seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(r0pt(edge%nvert))
      exc=0_i4
      r0pt=0_i4
      DO iv=1,edge%nvert
        IF (edge%excorner(iv)) exc(iv) = 1_i4
        IF (edge%r0point(iv)) r0pt(iv) = 1_i4
      ENDDO
      CALL dump_h5(sid,"excorner",exc,h5in,h5err)
      CALL dump_h5(sid,"r0point",r0pt,h5in,h5err)
      DEALLOCATE(r0pt)
    ENDIF
    DEALLOCATE(exc)
!-------------------------------------------------------------------------------
!   close group
!-------------------------------------------------------------------------------
    CALL close_group(TRIM(seam_name),sid,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_dump

!-------------------------------------------------------------------------------
!> distribute edge to all mode processors from ilayer zero
!-------------------------------------------------------------------------------
  SUBROUTINE distribute(edge,inode)
    USE pardata_mod
    IMPLICIT NONE

    !> seam to distribute from ilayer 0
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    INTEGER(i4) :: iv,np
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'distribute',iftn,idepth)
!-------------------------------------------------------------------------------
!   send seam data
!-------------------------------------------------------------------------------
    CALL par%mode_bcast(edge%id,inode)
    CALL par%mode_bcast(edge%nvert,inode)
    CALL par%mode_bcast(edge%on_gpu,inode)
    IF (.NOT.ALLOCATED(edge%vertex)) ALLOCATE(edge%vertex(edge%nvert))
    DO iv=1,edge%nvert
      IF (par%ilayer==inode) np=SIZE(edge%vertex(iv)%ptr,2)
      CALL par%mode_bcast(np,inode)
      IF (.NOT.ALLOCATED(edge%vertex(iv)%ptr))                                  &
        ALLOCATE(edge%vertex(iv)%ptr(2,np))
      CALL par%mode_bcast(edge%vertex(iv)%ptr,2_i4*np,inode)
      IF (edge%id>0) CALL par%mode_bcast(edge%vertex(iv)%intxy,2_i4,inode)
    ENDDO
!-------------------------------------------------------------------------------
!   send seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      IF (.NOT.ALLOCATED(edge%excorner)) ALLOCATE(edge%excorner(edge%nvert))
      IF (.NOT.ALLOCATED(edge%r0point)) ALLOCATE(edge%r0point(edge%nvert))
      CALL par%mode_bcast(edge%excorner,edge%nvert,inode)
      CALL par%mode_bcast(edge%r0point,edge%nvert,inode)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE distribute

!-------------------------------------------------------------------------------
!> set the seam_save data arrays to 0.
!-------------------------------------------------------------------------------
  SUBROUTINE zero_save(edge)
    IMPLICIT NONE

    !> seam to zero save array
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_save',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over vertex and segment structures to clear the seam_save
!   arrays.
!-------------------------------------------------------------------------------
    ASSOCIATE (vert_save=>edge%vert_save,seg_save=>edge%seg_save)
      !$acc kernels present(vert_save,seg_save) async(edge%id) if(edge%on_gpu)
      vert_save=0._r8
      seg_save=0._r8
      !$acc end kernels
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_save

!-------------------------------------------------------------------------------
!> set the seam_csave data arrays to 0.
!-------------------------------------------------------------------------------
  SUBROUTINE zero_csave(edge)
    IMPLICIT NONE

    !> seam to zero csave array
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_csave',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over vertex and segment structures to clear the seam_csave
!   arrays.
!-------------------------------------------------------------------------------
    ASSOCIATE (vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc kernels present(vert_csave,seg_csave) async(edge%id) if(edge%on_gpu)
      vert_csave=0._r8
      seg_csave=0._r8
      !$acc end kernels
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_csave

!-------------------------------------------------------------------------------
!> use dir_in (+1 or -1) to set starting and ending indices for
!  the loops over side bases.  this routine avoids duplicating code.
!-------------------------------------------------------------------------------
  PURE SUBROUTINE load_limits(dir_in,nbases,istart,iend)
    IMPLICIT NONE

    !> direction to set istart to iend
    INTEGER(i4), INTENT(IN) :: dir_in
    !> length: iend-start+1
    INTEGER(i4), INTENT(IN) :: nbases
    !> output start index
    INTEGER(i4), INTENT(OUT) :: istart
    !> output end index
    INTEGER(i4), INTENT(OUT) :: iend

    IF (dir_in>0) THEN
      istart=1
      iend=nbases
    ELSE
      istart=nbases
      iend=1
    ENDIF
  END SUBROUTINE load_limits

END MODULE edge_mod
