!-------------------------------------------------------------------------------
!! routines for performing block decomposition and seam communication.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> routines for performing block decomposition and seam communication.
!
! In parallel, additional communication structures are set up such that at
! most only one message is exchanged between two processors while seaming.
! Seam data for blocks owned by the same process are copied directly and skip
! mpi communication.
!-------------------------------------------------------------------------------
MODULE seam_mod
  USE local
  USE timer_mod
  USE edge_mod
  USE seam_comm_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: seam_type

  CHARACTER(*), PARAMETER :: mod_name='seam'
!-------------------------------------------------------------------------------
!> data stucture for seams and associated data
!-------------------------------------------------------------------------------
  TYPE :: seam_type
    !> edge_type array of size nbl
    TYPE(edge_type), DIMENSION(:), ALLOCATABLE :: s
    !> array that lists local block ibl of edge_types that are on the boundary
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: exblock_list
    !> array that lists local block ibl of edge_types touch R=0
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: r0block_list
    !> glob_nvert(1:nbl_tot) = nvert (# of seam pts) in global block/seam ibl
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: glob_nvert
    !> storage for parallel communication data
    TYPE(seam_comm_type) :: comm
  CONTAINS

    PROCEDURE, PASS(seam) :: init
    PROCEDURE, PASS(seam) :: dealloc
    PROCEDURE, PASS(seam) :: trim_to_local
    PROCEDURE, PASS(seam) :: h5_read
    PROCEDURE, PASS(seam) :: h5_dump
    PROCEDURE, PASS(seam) :: distribute
    PROCEDURE, PASS(seam) :: edge_network
  END TYPE seam_type

CONTAINS

!-------------------------------------------------------------------------------
!> setup seam communication data stuctures
!-------------------------------------------------------------------------------
  SUBROUTINE init(seam,max_nqty,max_nqty_save,nside,nmodes)
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to initialize
    CLASS(seam_type), INTENT(INOUT) :: seam
    !> max quantity size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty
    !> max quantity save size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty_save
    !> segement size (for allocation)
    INTEGER(i4), INTENT(IN) :: nside
    !> numerber of fourier modes (for allocation)
    INTEGER(i4), INTENT(IN) :: nmodes

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init',iftn,idepth)
!-------------------------------------------------------------------------------
!   allocate and set exblock and r0block flags
!-------------------------------------------------------------------------------
    CALL alloc_exr0block
!-------------------------------------------------------------------------------
!   initialize the edge_type for each block -- skip init for seam 0
!-------------------------------------------------------------------------------
    DO ibl=1,UBOUND(seam%s,1)
      CALL seam%s(ibl)%init(max_nqty,max_nqty_save,nside,nmodes,seam%glob_nvert)
    ENDDO
!-------------------------------------------------------------------------------
!   do the parallel initialization if needed
!-------------------------------------------------------------------------------
    IF (par%nprocs==1) THEN
      CALL timer%end_timer_l2(iftn,idepth)
      RETURN
    ENDIF
    CALL parallel_vert_init(max_nqty,nmodes,seam%s,seam%comm%nsend,             &
                            seam%comm%send,seam%comm%nrecv,                     &
                            seam%comm%recv,seam%comm%recv_request,              &
                            seam%comm%nself,seam%comm%self)
    CALL parallel_seg_init(max_nqty,nside,nmodes,seam%s,seam%comm%nsendseg,     &
                           seam%comm%sendseg,seam%comm%nrecvseg,                &
                           seam%comm%recvseg,seam%comm%recvseg_request,         &
                           seam%comm%nselfseg,seam%comm%selfseg)
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

!-------------------------------------------------------------------------------
!>  allocate and set exblock and r0block flags
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_exr0block
      IMPLICIT NONE

      INTEGER(i4) :: iv,ip,np,ibl,ibv,nexbl,nv
      INTEGER(i4) :: ibloc,nr0bl,nbl
      LOGICAL, DIMENSION(UBOUND(seam%s,1)) :: blex,blr0

      nbl=UBOUND(seam%s,1)
!-------------------------------------------------------------------------------
!     create external point and corner and r0point masks in seams that touch the
!     external seam.  work on a seam's vertex ONLY if I own the block
!     to which seam0 is connected.
!-------------------------------------------------------------------------------
      DO ibl=1,nbl
        nv=seam%s(ibl)%nvert
        ALLOCATE(seam%s(ibl)%expoint(nv))
        seam%s(ibl)%expoint=.false.
        ALLOCATE(seam%s(ibl)%excorner(nv))
        seam%s(ibl)%excorner=.false.
        ALLOCATE(seam%s(ibl)%r0point(nv))
        seam%s(ibl)%r0point=.false.
      ENDDO
      DO iv=1,seam%s(0)%nvert
        np=SIZE(seam%s(0)%vertex(iv)%ptr,2)
        DO ip=1,np
          ibl=seam%s(0)%vertex(iv)%ptr(1,ip)
          IF (par%nprocs==1) THEN
            ibloc=ibl
          ELSE IF (par%block2proc(ibl)==par%node) THEN
            ibloc=par%global2local(ibl)
          ELSE
            CYCLE
          ENDIF
          ibv=seam%s(0)%vertex(iv)%ptr(2,ip)
          IF (seam%s(0)%r0point(iv)) THEN
            seam%s(ibloc)%r0point(ibv)=.true.
          ELSE
            seam%s(ibloc)%expoint(ibv)=.true.
          ENDIF
          seam%s(ibloc)%excorner(ibv)=seam%s(0)%excorner(iv)
          IF (seam%s(0)%excorner(iv)) seam%s(ibloc)%expoint(ibv)=.true.
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     create an external block mask and set true if external or r0 block
!-------------------------------------------------------------------------------
      blex=.false.
      blr0=.false.
      DO ibl=1,nbl
        IF (ANY(seam%s(ibl)%expoint)) blex(ibl)=.true.
        IF (ANY(seam%s(ibl)%r0point)) blr0(ibl)=.true.
      ENDDO
!-------------------------------------------------------------------------------
!     create an array of block numbers which touch the exterior and r0 points.
!     Note that local block numbers are stored NOT global block numbers
!-------------------------------------------------------------------------------
      nexbl=0
      nr0bl=0
      DO ibl=1,nbl
        IF (blex(ibl)) nexbl=nexbl+1
        IF (blr0(ibl)) nr0bl=nr0bl+1
      ENDDO
      ALLOCATE(seam%exblock_list(nexbl))
      ALLOCATE(seam%r0block_list(nr0bl))
      nexbl=0
      nr0bl=0
      DO ibl=1,nbl
        IF (blex(ibl)) THEN
          nexbl=nexbl+1
          seam%exblock_list(nexbl)=ibl
        ENDIF
        IF (blr0(ibl)) THEN
          nr0bl=nr0bl+1
          seam%r0block_list(nr0bl)=ibl
        ENDIF
      ENDDO
    END SUBROUTINE alloc_exr0block

!-------------------------------------------------------------------------------
!>  setup seam vertex communication data stuctures
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_vert_init(max_nqty,nmodes,edge,nsend,send,              &
                                  nrecv,recv,recv_request,nself,self)
      USE pardata_mod
      IMPLICIT NONE

      !> max quantity size (for allocation)
      INTEGER(i4), INTENT(IN) :: max_nqty
      !> numerber of fourier modes (for allocation)
      INTEGER(i4), INTENT(IN) :: nmodes
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(IN) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsend
      !> send(1:nsend) of send_type, one for each proc a message will be sent to
      TYPE(send_type), ALLOCATABLE, INTENT(INOUT) :: send(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecv
      !> recv(1:nrecv) one for each proc a message will come from
      TYPE(recv_type), ALLOCATABLE, INTENT(INOUT) :: recv(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recv_request(:)
      !> # of seam points whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nself
      !> self(1:nself), one for each point, note that a pair of
      !  self-referencing pts will be stored twice
      TYPE(self_type), ALLOCATABLE, INTENT(INOUT) :: self(:)

      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: procs,workvec
      TYPE(mpi_request), DIMENSION(:), ALLOCATABLE :: extra_request
      TYPE(mpi_request), DIMENSION(:), ALLOCATABLE :: extra_request2
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: data_block,data_vertex
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: data_order
      INTEGER(i4) :: i,imageproc,ibl,ivert,image,isend,irecv,count
      INTEGER(i4) :: globalblock
      TYPE(mpi_status) :: status
!-------------------------------------------------------------------------------
!     setup send and self data structures
!     allocate a work vector of length nprocs and zero it
!-------------------------------------------------------------------------------
      ALLOCATE(procs(0:par%nprocs-1))
      procs = 0
!-------------------------------------------------------------------------------
!     procs(n) = 1 if any of my seam data will be sent to proc n (include self)
!     procs(n) = 0 otherwise
!     globalblock = global block # of an image point
!     imageproc = owner of the global block
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO ivert = 1,edge(ibl)%nvert
          DO image = 1,edge(ibl)%vertex(ivert)%nimage
            globalblock = edge(ibl)%vertex(ivert)%ptr2(1,image)
            imageproc = par%block2proc(globalblock)
            procs(imageproc) = 1
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     nsend = # of procs I will send seam-data to (exclude self)
!-------------------------------------------------------------------------------
      procs(par%node) = 0
      nsend = sum(procs)
      IF (nsend > 0) ALLOCATE(send(nsend))
!-------------------------------------------------------------------------------
!     procs(n) = # of seam-datums I will send to proc n (include self)
!-------------------------------------------------------------------------------
      procs = 0
      DO ibl=1,UBOUND(edge,1)
        DO ivert = 1,edge(ibl)%nvert
          DO image = 1,edge(ibl)%vertex(ivert)%nimage
            globalblock = edge(ibl)%vertex(ivert)%ptr2(1,image)
            imageproc = par%block2proc(globalblock)
            procs(imageproc) = procs(imageproc) + 1
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     use procs to allocate fields in send-data structure
!     (to send to other procs)
!     use procs(node) to allocate self-data structure
!     nself = # of seam-datums I must exchange between my own blocks
!     do not allocate self-data structure if nprocs = 1 or nself = 0
!     after allocation, set send(isend)%count and nself to 0,
!     so can use as counters in next stage
!     also after allocation, store which message (1:nsend) is sent to
!     imageproc in procs(imageproc) for use in next stage
!-------------------------------------------------------------------------------
      isend = 0
      DO imageproc = 0,par%nprocs-1
        IF (par%node == imageproc) THEN
          IF (par%nprocs > 1) THEN
            nself = procs(imageproc)
            IF (nself > 0) ALLOCATE(self(nself))
            nself = 0
          ENDIF
        ELSE IF (procs(imageproc) > 0) THEN
          isend = isend + 1
          send(isend)%proc = imageproc
          count = procs(imageproc)
          ALLOCATE(send(isend)%bl_loc(count))
          ALLOCATE(send(isend)%vertex(count))
          ALLOCATE(send(isend)%image(count))
          ALLOCATE(send(isend)%data(max_nqty*count))
          ALLOCATE(send(isend)%cdata(max_nqty*nmodes*count))
          send(isend)%count = 0
          procs(imageproc) = isend
        ENDIF
      ENDDO
!-------------------------------------------------------------------------------
!     loop over all seam-datums and store ptr2 info in send-data structure
!     datums being exchanged on-processor are stored in self-data structure
!     (don't bother if nprocs = 1)
!     use send(isend)%count and nself as incremented ptrs to next available
!     space, when done they will have been reset to correct totals
!     regarding order, _out is the receiving block, so out's order(image) is
!     the location the _in data will hold in _out's seam_hold.
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO ivert = 1,edge(ibl)%nvert
          DO image = 1,edge(ibl)%vertex(ivert)%nimage
            globalblock = edge(ibl)%vertex(ivert)%ptr2(1,image)
            imageproc = par%block2proc(globalblock)
            IF (par%node == imageproc) THEN
              IF (par%nprocs > 1) THEN
                nself = nself + 1
                self(nself)%block_out = ibl
                self(nself)%vertex_out = ivert
                self(nself)%block_in = par%global2local(globalblock)
                self(nself)%vertex_in = edge(ibl)%vertex(ivert)%ptr2(2,image)
                IF (edge(ibl)%vertex(ivert)%nimage==1) THEN
                  self(nself)%order = 1
                ELSE
                  self(nself)%order = edge(ibl)%vertex(ivert)%order(image)
                ENDIF
              ENDIF
            ELSE
              isend = procs(imageproc)
              count = send(isend)%count + 1
              send(isend)%bl_loc(count) = ibl
              send(isend)%vertex(count) = ivert
              send(isend)%image(count) = image
              send(isend)%count = count
            ENDIF
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     setup recv data structure
!     procs(n) = 1 if a seam-data message will be sent to proc n (exclude self)
!     procs(n) = 0 otherwise
!-------------------------------------------------------------------------------
      procs = 0
      DO isend = 1,nsend
        procs(send(isend)%proc) = 1
      ENDDO
!-------------------------------------------------------------------------------
!     sum procs vector (in minimal way) across all procs
!     nrecv = # of seam-data messages I will receive
!     then are finished with procs
!-------------------------------------------------------------------------------
      ALLOCATE(workvec(par%nprocs))
      workvec = 1
      CALL par%all_sum_scatter(procs,nrecv,workvec)
      DEALLOCATE(workvec)
      DEALLOCATE(procs)
!-------------------------------------------------------------------------------
!     allocate recv data structure
!-------------------------------------------------------------------------------
      IF (nrecv > 0) ALLOCATE(recv(nrecv))
!-------------------------------------------------------------------------------
!     send count of how many send-datums to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsend
        CALL par%all_send(send(isend)%count,send(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     receive counts and allocate recv data structure for each incoming message
!-------------------------------------------------------------------------------
      DO irecv = 1,nrecv
        CALL par%all_recv(count,par%mpi_any_source,status)
        recv(irecv)%proc = status%mpi_source
        recv(irecv)%count = count
        ALLOCATE(recv(irecv)%bl_loc(count))
        ALLOCATE(recv(irecv)%vertex(count))
        ALLOCATE(recv(irecv)%order(count))
        ALLOCATE(recv(irecv)%data(max_nqty*count))
        ALLOCATE(recv(irecv)%cdata(max_nqty*nmodes*count))
      ENDDO
!-------------------------------------------------------------------------------
!     post receives for incoming block and vertex numbers
!     need extra vector to store requests for 2nd message
!-------------------------------------------------------------------------------
      ALLOCATE(recv_request(nrecv))
      ALLOCATE(extra_request(nrecv))
      ALLOCATE(extra_request2(nrecv))
      DO irecv = 1,nrecv
        CALL par%all_irecv(recv(irecv)%bl_loc,recv(irecv)%count,                &
                           recv(irecv)%proc,extra_request(irecv))
        CALL par%all_irecv(recv(irecv)%vertex,recv(irecv)%count,                &
                           recv(irecv)%proc,extra_request2(irecv))
        CALL par%all_irecv(recv(irecv)%order,recv(irecv)%count,                 &
                           recv(irecv)%proc,recv_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     synchonize to insure all receives have been posted
!-------------------------------------------------------------------------------
      CALL par%all_barrier
!-------------------------------------------------------------------------------
!     send block and vertex #s for each send-datum to each receiving proc
!     these are local block # and local vertex # for where the send-datum goes
!     on the receiving proc, computed by the sender
!     order for each communicated datum is the same in for all representations.
!     here, the sending block's order is in its own order(0), and that is
!     communicated to the receiving block.
!-------------------------------------------------------------------------------
      DO isend = 1,nsend
        count = send(isend)%count
        ALLOCATE(data_block(count))
        ALLOCATE(data_vertex(count))
        ALLOCATE(data_order(count))
        DO i = 1,count
          ibl = send(isend)%bl_loc(i)
          ivert = send(isend)%vertex(i)
          image = send(isend)%image(i)
          globalblock = edge(ibl)%vertex(ivert)%ptr2(1,image)
          data_block(i) = par%global2local(globalblock)
          data_vertex(i) = edge(ibl)%vertex(ivert)%ptr2(2,image)
          data_order(i) = edge(ibl)%vertex(ivert)%order(0)
        ENDDO
        CALL par%all_send(data_block,count,send(isend)%proc)
        CALL par%all_send(data_vertex,count,send(isend)%proc)
        CALL par%all_send(data_order,count,send(isend)%proc)
        DEALLOCATE(data_block)
        DEALLOCATE(data_vertex)
        DEALLOCATE(data_order)
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     wait on 3rd message sent, since it will guarantee first two already
!     arrived free extra message request vector
!-------------------------------------------------------------------------------
      IF (nrecv > 0) THEN
        CALL par%waitall(nrecv,recv_request)
      ENDIF
!-------------------------------------------------------------------------------
!     if there are only two images at a vertex, have the communicated data
!     placed in the first element of seam_hold.
!-------------------------------------------------------------------------------
      DO irecv = 1,nrecv
        DO i = 1,recv(irecv)%count
          ibl=recv(irecv)%bl_loc(i)
          ivert=recv(irecv)%vertex(i)
          IF (edge(ibl)%vertex(ivert)%nimage==1) THEN
            recv(irecv)%order(i)=1
          ENDIF
        ENDDO
      ENDDO
    END SUBROUTINE parallel_vert_init

!-------------------------------------------------------------------------------
!>  setup segment communication data stuctures
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_seg_init(max_nqty,nside,nmodes,edge,nsendseg,sendseg,   &
                                 nrecvseg,recvseg,recvseg_request,nselfseg,     &
                                 selfseg)
      USE pardata_mod
      IMPLICIT NONE

      !> max quantity size (for allocation)
      INTEGER(i4), INTENT(IN) :: max_nqty
      !> segement size (for allocation)
      INTEGER(i4), INTENT(IN) :: nside
      !> numerber of fourier modes (for allocation)
      INTEGER(i4), INTENT(IN) :: nmodes
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(IN) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsendseg
      !> sendseg(1:nsendseg) one for each proc a message will be sent to
      TYPE(sendseg_type), ALLOCATABLE, INTENT(INOUT) :: sendseg(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecvseg
      !> recvseg(1:nrecvseg) one for each proc a message will come from
      TYPE(recvseg_type), ALLOCATABLE, INTENT(INOUT) :: recvseg(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recvseg_request(:)
      !> # of segments whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nselfseg
      !> selfseg(1:nselfseg) one for each segment, note that a
      !  pair of self-referencing segments will be stored twice
      TYPE(selfseg_type), ALLOCATABLE, INTENT(INOUT) :: selfseg(:)

      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: procs,workvec
      TYPE(mpi_request), DIMENSION(:), ALLOCATABLE :: extra_request
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: data_block
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: data_segment
      INTEGER(i4) :: i,imageproc,ibl,iseg,isend,irecv,count
      INTEGER(i4) :: globalblock
      TYPE(mpi_status) :: status
!-------------------------------------------------------------------------------
!     setup sendseg and selfseg data structures
!     allocate a work vector of length nprocs and zero it
!-------------------------------------------------------------------------------
      ALLOCATE(procs(0:par%nprocs-1))
      procs = 0
!-------------------------------------------------------------------------------
!     procs(n) = 1 if any of my seg data will be sent to proc n (include self)
!     procs(n) = 0 otherwise
!     globalblock = global block # of an image segment
!     imageproc = owner of the global block
!     no seg data if ptr(1) = 0
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO iseg = 1,edge(ibl)%nvert
          globalblock = edge(ibl)%segment(iseg)%ptr(1)
          IF (globalblock /= 0) THEN
            imageproc = par%block2proc(globalblock)
            procs(imageproc) = 1
          ENDIF
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     nsendseg = # of procs I will send segment-data to (exclude self)
!-------------------------------------------------------------------------------
      procs(par%node) = 0
      nsendseg = sum(procs)
      IF (nsendseg > 0) ALLOCATE(sendseg(nsendseg))
!-------------------------------------------------------------------------------
!     procs(n) = # of segment-datums I will send to proc n (include self)
!     no seg data if ptr(1) = 0
!-------------------------------------------------------------------------------
      procs = 0
      DO ibl=1,UBOUND(edge,1)
        DO iseg = 1,edge(ibl)%nvert
          globalblock = edge(ibl)%segment(iseg)%ptr(1)
          IF (globalblock /= 0) THEN
            imageproc = par%block2proc(globalblock)
            procs(imageproc) = procs(imageproc) + 1
          ENDIF
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     use procs to allocate fields in seg-data structure
!     (to send to other procs)
!     use procs(node) to allocate selfseg-data structure
!     nselfseg = # of seg-datums I must exchange between my own blocks
!     do not allocate selfseg-data structure if nprocs = 1 or nselfseg = 0
!     after allocation, set sendseg(isend)%count and nselfseg to 0,
!     so can use as counters in next stage
!     also after allocation, store which message (1:nsendseg) is sent to
!     imageproc in procs(imageproc) for use in next stage
!-------------------------------------------------------------------------------
      isend = 0
      DO imageproc = 0,par%nprocs-1
        IF (par%node == imageproc) THEN
          IF (par%nprocs > 1) THEN
            nselfseg = procs(imageproc)
            IF (nselfseg > 0) ALLOCATE(selfseg(nselfseg))
            nselfseg = 0
          ENDIF
        ELSE IF (procs(imageproc) > 0) THEN
          isend = isend + 1
          sendseg(isend)%proc = imageproc
          count = procs(imageproc)
          ALLOCATE(sendseg(isend)%bl_loc(count))
          ALLOCATE(sendseg(isend)%segment(count))
          ALLOCATE(sendseg(isend)%data(max_nqty*nside*count))
          ALLOCATE(sendseg(isend)%cdata(max_nqty*nside*nmodes*count))
          sendseg(isend)%count = 0
          procs(imageproc) = isend
        ENDIF
      ENDDO
!-------------------------------------------------------------------------------
!     loop over all seg-datums and store ptr info in seg-data structure
!     datums being exchanged on-processor are stored in selfseg-data structure
!     (don't bother if nprocs = 1)
!     use sendseg(isend)%count and nselfseg as incremented ptrs to next
!     available space, when done they will have been reset to correct totals
!     no seg data if ptr(1) = 0
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO iseg = 1,edge(ibl)%nvert
          globalblock = edge(ibl)%segment(iseg)%ptr(1)
          IF (globalblock /= 0) THEN
            imageproc = par%block2proc(globalblock)
            IF (par%node == imageproc) THEN
              IF (par%nprocs > 1) THEN
                nselfseg = nselfseg + 1
                selfseg(nselfseg)%block_out = ibl
                selfseg(nselfseg)%segment_out = iseg
                selfseg(nselfseg)%block_in = par%global2local(globalblock)
                selfseg(nselfseg)%segment_in = edge(ibl)%segment(iseg)%ptr(2)
              ENDIF
            ELSE
              isend = procs(imageproc)
              count = sendseg(isend)%count + 1
              sendseg(isend)%bl_loc(count) = ibl
              sendseg(isend)%segment(count) = iseg
              sendseg(isend)%count = count
            ENDIF
          ENDIF
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     setup recvseg data structure
!     procs(n) = 1 if a seg-data message will be sent to proc n (exclude self)
!     procs(n) = 0 otherwise
!-------------------------------------------------------------------------------
      procs = 0
      DO isend = 1,nsendseg
        procs(sendseg(isend)%proc) = 1
      ENDDO
!-------------------------------------------------------------------------------
!     sum procs vector (in minimal way) across all procs
!     nrecvseg = # of seg-data messages I will receive
!     then are finished with procs
!-------------------------------------------------------------------------------
      ALLOCATE(workvec(par%nprocs))
      workvec = 1
      CALL par%all_sum_scatter(procs,nrecvseg,workvec)
      DEALLOCATE(workvec)
      DEALLOCATE(procs)
!-------------------------------------------------------------------------------
!     allocate recvseg data structure
!-------------------------------------------------------------------------------
      IF (nrecvseg > 0) ALLOCATE(recvseg(nrecvseg))
!-------------------------------------------------------------------------------
!     send count of how many seg-datums to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsendseg
        CALL par%all_send(sendseg(isend)%count,sendseg(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     receive counts and allocate recvseg data structure for each
!     incoming message
!-------------------------------------------------------------------------------
      DO irecv = 1,nrecvseg
        CALL par%all_recv(count,par%mpi_any_source,status)
        recvseg(irecv)%proc = status%mpi_source
        recvseg(irecv)%count = count
        ALLOCATE(recvseg(irecv)%bl_loc(count))
        ALLOCATE(recvseg(irecv)%segment(count))
        ALLOCATE(recvseg(irecv)%data(max_nqty*nside*count))
        ALLOCATE(recvseg(irecv)%cdata(max_nqty*nside*nmodes*count))
      ENDDO
!-------------------------------------------------------------------------------
!     post receives for incoming block and segment numbers
!     need extra vector to store requests for 2nd message
!-------------------------------------------------------------------------------
      ALLOCATE(recvseg_request(nrecvseg))
      ALLOCATE(extra_request(nrecvseg))
      DO irecv = 1,nrecvseg
        CALL par%all_irecv(recvseg(irecv)%bl_loc,recvseg(irecv)%count,          &
                           recvseg(irecv)%proc,extra_request(irecv))
        CALL par%all_irecv(recvseg(irecv)%segment,recvseg(irecv)%count,         &
                           recvseg(irecv)%proc,recvseg_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     synchonize to insure all receives have been posted
!-------------------------------------------------------------------------------
      CALL par%all_barrier
!-------------------------------------------------------------------------------
!     send block and segment #s for each seg-datum to each receiving proc
!     these are local block # and local segment # for where the seg-datum goes
!     on the receiving proc, computed by the sender
!-------------------------------------------------------------------------------
      DO isend = 1,nsendseg
        count = sendseg(isend)%count
        ALLOCATE(data_block(count))
        ALLOCATE(data_segment(count))
        DO i = 1,count
          ibl = sendseg(isend)%bl_loc(i)
          iseg = sendseg(isend)%segment(i)
          globalblock = edge(ibl)%segment(iseg)%ptr(1)
          data_block(i) = par%global2local(globalblock)
          data_segment(i) = edge(ibl)%segment(iseg)%ptr(2)
        ENDDO
        CALL par%all_send(data_block,count,sendseg(isend)%proc)
        CALL par%all_send(data_segment,count,sendseg(isend)%proc)
        DEALLOCATE(data_block)
        DEALLOCATE(data_segment)
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     wait on 2nd message sent, since it will guarantee first one already
!     arrived free extra message request vector
!-------------------------------------------------------------------------------
      IF (nrecvseg > 0) THEN
        CALL par%waitall(nrecvseg,recvseg_request)
      ENDIF
    END SUBROUTINE parallel_seg_init

  END SUBROUTINE init

!-------------------------------------------------------------------------------
!> deallocate parallel seam communication data stuctures
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc(seam)
    IMPLICIT NONE

    !> seam_type structure to dealloc
    CLASS(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl,isend,irecv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc',iftn,idepth)
!-------------------------------------------------------------------------------
!   deallocate the seam array
!-------------------------------------------------------------------------------
    DO ibl=LBOUND(seam%s,1),UBOUND(seam%s,1)
      CALL seam%s(ibl)%dealloc
    ENDDO
    IF (ALLOCATED(seam%s)) DEALLOCATE(seam%s)
    IF (ALLOCATED(seam%exblock_list)) DEALLOCATE(seam%exblock_list)
    IF (ALLOCATED(seam%r0block_list)) DEALLOCATE(seam%r0block_list)
    IF (ALLOCATED(seam%glob_nvert)) DEALLOCATE(seam%glob_nvert)
!-------------------------------------------------------------------------------
!   deallocate the parallel communication type
!-------------------------------------------------------------------------------
    DO isend=1,seam%comm%nsend
      DEALLOCATE(seam%comm%send(isend)%bl_loc)
      DEALLOCATE(seam%comm%send(isend)%vertex)
      DEALLOCATE(seam%comm%send(isend)%image)
      DEALLOCATE(seam%comm%send(isend)%data)
      DEALLOCATE(seam%comm%send(isend)%cdata)
    ENDDO
    DO irecv=1,seam%comm%nrecv
      DEALLOCATE(seam%comm%recv(irecv)%bl_loc)
      DEALLOCATE(seam%comm%recv(irecv)%vertex)
      DEALLOCATE(seam%comm%recv(irecv)%order)
      DEALLOCATE(seam%comm%recv(irecv)%data)
      DEALLOCATE(seam%comm%recv(irecv)%cdata)
    ENDDO
    IF (ALLOCATED(seam%comm%recv_request)) DEALLOCATE(seam%comm%recv_request)
    IF (ALLOCATED(seam%comm%send)) DEALLOCATE(seam%comm%send)
    IF (ALLOCATED(seam%comm%recv)) DEALLOCATE(seam%comm%recv)
    IF (ALLOCATED(seam%comm%self)) DEALLOCATE(seam%comm%self)
    DO isend=1,seam%comm%nsendseg
      DEALLOCATE(seam%comm%sendseg(isend)%bl_loc)
      DEALLOCATE(seam%comm%sendseg(isend)%segment)
      DEALLOCATE(seam%comm%sendseg(isend)%data)
      DEALLOCATE(seam%comm%sendseg(isend)%cdata)
    ENDDO
    DO irecv=1,seam%comm%nrecvseg
      DEALLOCATE(seam%comm%recvseg(irecv)%bl_loc)
      DEALLOCATE(seam%comm%recvseg(irecv)%segment)
      DEALLOCATE(seam%comm%recvseg(irecv)%data)
      DEALLOCATE(seam%comm%recvseg(irecv)%cdata)
    ENDDO
    IF (ALLOCATED(seam%comm%recvseg_request)) DEALLOCATE(seam%comm%recvseg_request)
    IF (ALLOCATED(seam%comm%sendseg)) DEALLOCATE(seam%comm%sendseg)
    IF (ALLOCATED(seam%comm%recvseg)) DEALLOCATE(seam%comm%recvseg)
    IF (ALLOCATED(seam%comm%selfseg)) DEALLOCATE(seam%comm%selfseg)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!> trim to local seam
!-------------------------------------------------------------------------------
  SUBROUTINE trim_to_local(seam)
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to trim
    CLASS(seam_type), INTENT(INOUT) :: seam

    TYPE(edge_type) :: edge_tmp(0:par%nbl)
    INTEGER(i4) :: ibl,igbl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'trim_to_local',iftn,idepth)
!-------------------------------------------------------------------------------
!   copy local edges to temporary
!-------------------------------------------------------------------------------
    CALL edge_tmp(0)%copy(seam%s(0))
    DO ibl=1,par%nbl
      igbl=par%loc2glob(ibl)
      CALL edge_tmp(ibl)%copy(seam%s(igbl))
    ENDDO
!-------------------------------------------------------------------------------
!   resize and copy back
!-------------------------------------------------------------------------------
    DEALLOCATE(seam%s)
    ALLOCATE(seam%s(0:par%nbl))
    DO ibl=0,par%nbl
      CALL seam%s(ibl)%copy(edge_tmp(ibl))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE trim_to_local

!-------------------------------------------------------------------------------
!> read seam structure from h5 dump file.
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read(seam,gid)
    USE io
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to read
    CLASS(seam_type), INTENT(INOUT) :: seam
    !> h5 group to read seam from
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edges
!-------------------------------------------------------------------------------
    ALLOCATE(seam%s(0:par%nbl))
    CALL seam%s(0)%h5_read(0,gid) ! always read seam0
    DO ibl=1,par%nbl
      CALL seam%s(ibl)%h5_read(par%loc2glob(ibl),gid)
    ENDDO
!-------------------------------------------------------------------------------
!   read glob_nvert
!-------------------------------------------------------------------------------
    ALLOCATE(seam%glob_nvert(par%nbl_total))
    CALL read_h5(gid,"global_nvert",seam%glob_nvert,h5in,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read

!-------------------------------------------------------------------------------
!> write seam structure to h5 dump file.
!-------------------------------------------------------------------------------
  SUBROUTINE h5_dump(seam,gid)
    USE io
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to write
    CLASS(seam_type), INTENT(IN) :: seam
    !> h5 group to write seam to
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: ibl,nbl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_dump',iftn,idepth)
    nbl=UBOUND(seam%s,1)
!-------------------------------------------------------------------------------
!   write edges
!-------------------------------------------------------------------------------
    DO ibl=1,nbl
      CALL seam%s(ibl)%h5_dump(gid)
    ENDDO
!-------------------------------------------------------------------------------
!   write seam0 and glob_nvert
!-------------------------------------------------------------------------------
    IF (par%node==0) THEN
      CALL seam%s(0)%h5_dump(gid)
      CALL dump_h5(gid,"global_nvert",seam%glob_nvert,h5in,h5err)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_dump

!-------------------------------------------------------------------------------
!> distribute seams to all mode processors from ilayer zero
!-------------------------------------------------------------------------------
  SUBROUTINE distribute(seam,inode)
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to network
    CLASS(seam_type), INTENT(INOUT) :: seam
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'distribute',iftn,idepth)
!-------------------------------------------------------------------------------
!   distribute edges
!-------------------------------------------------------------------------------
    IF (.NOT.ALLOCATED(seam%s)) ALLOCATE(seam%s(0:par%nbl))
    DO ibl=0,par%nbl
      CALL seam%s(ibl)%distribute(inode)
    ENDDO
!-------------------------------------------------------------------------------
!   distribute glob_nvert
!-------------------------------------------------------------------------------
    IF (.NOT.ALLOCATED(seam%glob_nvert))                                        &
      ALLOCATE(seam%glob_nvert(par%nbl_total))
    CALL par%mode_bcast(seam%glob_nvert,par%nbl_total,inode)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE distribute

!-------------------------------------------------------------------------------
!> manages the communication between blocks.  note that the first
!  nmodes Fourier components of the complex arrays are communicated
!  if nmodes>0.  IF nmodes=0, the real arrays are communicated.
!-------------------------------------------------------------------------------
  SUBROUTINE edge_network(seam)
    USE pardata_mod
    IMPLICIT NONE

    !> seam_type structure to network
    CLASS(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: nqty,nmodes,nside,nfnq
    INTEGER(i4) :: iv,ib
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_network',iftn,idepth)
!-------------------------------------------------------------------------------
!   read options set during seam load
!-------------------------------------------------------------------------------
    nqty=seam%s(1)%nqty_loaded
    nside=seam%s(1)%nside_loaded
    nmodes=seam%s(1)%nmodes_loaded
    nfnq=nmodes*nqty
!-------------------------------------------------------------------------------
!   wait on GPU data transfer if needed
!-------------------------------------------------------------------------------
    !$acc wait
!-------------------------------------------------------------------------------
!   communicate element side-centered data, if needed.
!-------------------------------------------------------------------------------
    IF (nside>0) THEN
!-------------------------------------------------------------------------------
!     complex data.
!-------------------------------------------------------------------------------
      IF (nmodes>0) THEN
        DO ib=1,UBOUND(seam%s,1)
          DO iv=1,seam%s(ib)%nvert
            seam%s(ib)%seg_cout(1:nfnq*nside,iv)=                               &
              seam%s(ib)%seg_cin(1:nfnq*nside,iv)
          ENDDO
        ENDDO
        IF (par%nprocs == 1) THEN
          CALL edge_seg_comp_accumulate(nfnq,nside,seam%s)
        ELSE
          CALL parallel_seg_comm_comp(nfnq,nside,seam%s,seam%comm%nsendseg,     &
                           seam%comm%sendseg,seam%comm%nrecvseg,                &
                           seam%comm%recvseg,seam%comm%recvseg_request,         &
                           seam%comm%nselfseg,seam%comm%selfseg)
        ENDIF
      ELSE
!-------------------------------------------------------------------------------
!       real data.
!-------------------------------------------------------------------------------
        DO ib=1,UBOUND(seam%s,1)
          DO iv=1,seam%s(ib)%nvert
            seam%s(ib)%seg_out(1:nqty*nside,iv)          =                      &
              seam%s(ib)%seg_in(1:nqty*nside,iv)
          ENDDO
        ENDDO
        IF (par%nprocs == 1) THEN
          CALL edge_seg_accumulate(nqty,nside,seam%s)
        ELSE
          CALL parallel_seg_comm(nqty,nside,seam%s,seam%comm%nsendseg,          &
                           seam%comm%sendseg,seam%comm%nrecvseg,                &
                           seam%comm%recvseg,seam%comm%recvseg_request,         &
                           seam%comm%nselfseg,seam%comm%selfseg)
        ENDIF
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   communicate vertex-centered data.
!-------------------------------------------------------------------------------
    IF (par%nprocs == 1) THEN
      IF (nmodes>0) THEN
        CALL edge_comp_accumulate(nfnq,seam%s)
      ELSE
        CALL edge_accumulate(nqty,seam%s)
      ENDIF
    ELSE
      IF (nmodes>0) THEN
        CALL parallel_seam_comm_comp(nfnq,seam%s,seam%comm%nsend,               &
                            seam%comm%send,seam%comm%nrecv,                     &
                            seam%comm%recv,seam%comm%recv_request,              &
                            seam%comm%nself,seam%comm%self)
      ELSE
        CALL parallel_seam_comm(nqty,seam%s,seam%comm%nsend,                    &
                            seam%comm%send,seam%comm%nrecv,                     &
                            seam%comm%recv,seam%comm%recv_request,              &
                            seam%comm%nself,seam%comm%self)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   copy from traditional seam arrays and transfer to device
!-------------------------------------------------------------------------------
    IF (nmodes>0) THEN
      DO ib=1,UBOUND(seam%s,1)
        IF (seam%s(ib)%on_gpu) THEN
          ASSOCIATE (vert_cout=>seam%s(ib)%vert_cout,                           &
                     seg_cout=>seam%s(ib)%seg_cout)
            !$acc update device(vert_cout) async(seam%s(ib)%id)                 &
            !$acc if(seam%s(ib)%on_gpu)
            IF (nside/=0) THEN
              !$acc update device(seg_cout) async(seam%s(ib)%id)                &
              !$acc if(seam%s(ib)%on_gpu)
            ENDIF
          END ASSOCIATE
        ENDIF
      ENDDO
    ELSE
      DO ib=1,UBOUND(seam%s,1)
        IF (seam%s(ib)%on_gpu) THEN
          ASSOCIATE (vert_out=>seam%s(ib)%vert_out,seg_out=>seam%s(ib)%seg_out)
            !$acc update device(vert_out) async(seam%s(ib)%id)                  &
            !$acc if(seam%s(ib)%on_gpu)
            IF (nside/=0) THEN
              !$acc update device(seg_out) async(seam%s(ib)%id)                 &
              !$acc if(seam%s(ib)%on_gpu)
            ENDIF
          END ASSOCIATE
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

!-------------------------------------------------------------------------------
!>  accumulate all representations of edge vertex information from
!   seam_in into seam_out.
!-------------------------------------------------------------------------------
    SUBROUTINE edge_accumulate(nqty,edge)
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)

      INTEGER(i4) :: iv,ib,jv,jb,ip,ni,il
!-------------------------------------------------------------------------------
!     accumulate the different representations.  place them in an
!     array in a specified order (same on all sides of the border)
!     before summing to ensure the same round-off error.
!-------------------------------------------------------------------------------
      DO ib=1,UBOUND(edge,1)
        DO iv=1,edge(ib)%nvert
          ni=edge(ib)%vertex(iv)%nimage
          SELECT CASE(ni)
          CASE(0)
            edge(ib)%vert_out(1:nqty,iv)=edge(ib)%vert_in(1:nqty,iv)
          CASE(1)
            jb=edge(ib)%vertex(iv)%ptr2(1,1)
            jv=edge(ib)%vertex(iv)%ptr2(2,1)
            edge(ib)%vert_out(1:nqty,iv)=edge(ib)%vert_in(1:nqty,iv)            &
                                        +edge(jb)%vert_in(1:nqty,jv)
          CASE DEFAULT
            il=edge(ib)%vertex(iv)%order(0)
            edge(ib)%vert_hold(il,1:nqty,iv)=edge(ib)%vert_in(1:nqty,iv)
            DO ip=1,ni
              il=edge(ib)%vertex(iv)%order(ip)
              jb=edge(ib)%vertex(iv)%ptr2(1,ip)
              jv=edge(ib)%vertex(iv)%ptr2(2,ip)
              edge(ib)%vert_hold(il,1:nqty,iv)=edge(jb)%vert_in(1:nqty,jv)
            ENDDO
            edge(ib)%vert_out(1:nqty,iv)=                                       &
              SUM(edge(ib)%vert_hold(1:ni+1,1:nqty,iv),1)
          END SELECT
        ENDDO
      ENDDO
    END SUBROUTINE edge_accumulate

!-------------------------------------------------------------------------------
!>  accumulate all representations of edge vertex information from
!   seam_cin into seam_cout.  complex version of edge_accumulate
!-------------------------------------------------------------------------------
    SUBROUTINE edge_comp_accumulate(nfnq,edge)
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nfnq
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)

      INTEGER(i4) :: iv,ib,jv,jb,ip,ni,il
!-------------------------------------------------------------------------------
!     accumulate the different representations.  place them in an
!     array in a specified order (same on all sides of the border)
!     before summing to ensure the same round-off error.
!-------------------------------------------------------------------------------
      DO ib=1,UBOUND(edge,1)
        DO iv=1,edge(ib)%nvert
          ni=edge(ib)%vertex(iv)%nimage
          SELECT CASE(ni)
          CASE(0)
            edge(ib)%vert_cout(1:nfnq,iv)=edge(ib)%vert_cin(1:nfnq,iv)
          CASE(1)
            jb=edge(ib)%vertex(iv)%ptr2(1,1)
            jv=edge(ib)%vertex(iv)%ptr2(2,1)
            edge(ib)%vert_cout(1:nfnq,iv)=edge(ib)%vert_cin(1:nfnq,iv)          &
                                         +edge(jb)%vert_cin(1:nfnq,jv)
          CASE DEFAULT
            il=edge(ib)%vertex(iv)%order(0)
            edge(ib)%vert_chold(il,1:nfnq,iv)=edge(ib)%vert_cin(1:nfnq,iv)
            DO ip=1,ni
              il=edge(ib)%vertex(iv)%order(ip)
              jb=edge(ib)%vertex(iv)%ptr2(1,ip)
              jv=edge(ib)%vertex(iv)%ptr2(2,ip)
              edge(ib)%vert_chold(il,1:nfnq,iv)=edge(jb)%vert_cin(1:nfnq,jv)
            ENDDO
            edge(ib)%vert_cout(1:nfnq,iv)=                                      &
              SUM(edge(ib)%vert_chold(1:ni+1,1:nfnq,iv),1)
          END SELECT
        ENDDO
      ENDDO
    END SUBROUTINE edge_comp_accumulate

!-------------------------------------------------------------------------------
!>  accumulate all representations of edge segment information from
!   seam_in into seam_out.
!-------------------------------------------------------------------------------
    SUBROUTINE edge_seg_accumulate(nqty,nside,edge)
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> size of each segment
      INTEGER(i4), INTENT(IN) :: nside
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)

      INTEGER(i4) :: iv,ib,jv,jb,is,iqo,iqi
!-------------------------------------------------------------------------------
!     accumulate vector data
!
!     basis indices for vector data are now loaded along a block's
!     seaming direction (ccw), which means that seam_in and seam_out
!     have reversed orders.
!-------------------------------------------------------------------------------
      DO ib=1,UBOUND(edge,1)
        DO iv=1,edge(ib)%nvert
          jb=edge(ib)%segment(iv)%ptr(1)
          jv=edge(ib)%segment(iv)%ptr(2)
          IF (jb/=0) THEN
            iqo=1
            iqi=nqty*(nside-1)+1
            DO is=1,nside
              edge(ib)%seg_out(iqo:iqo+nqty-1,iv)=                              &
                 edge(ib)%seg_out(iqo:iqo+nqty-1,iv)                            &
                +edge(jb)%seg_in(iqi:iqi+nqty-1,jv)
              iqo=iqo+nqty
              iqi=iqi-nqty
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    END SUBROUTINE edge_seg_accumulate

!-------------------------------------------------------------------------------
!>  accumulate all representations of edge segment information from
!   seam_cin into seam_cout.  complex version of edge_seg_accumulate
!-------------------------------------------------------------------------------
    SUBROUTINE edge_seg_comp_accumulate(nqty,nside,edge)
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> size of each segment
      INTEGER(i4), INTENT(IN) :: nside
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)

      INTEGER(i4) :: iv,ib,jv,jb,is,iqi,iqo
!-------------------------------------------------------------------------------
!     accumulate vector data
!
!     basis indices for vector data are now loaded along a block's
!     seaming direction (ccw), which means that seam_in and seam_out
!     have reversed orders.
!-------------------------------------------------------------------------------
      DO ib=1,UBOUND(edge,1)
        DO iv=1,edge(ib)%nvert
          jb=edge(ib)%segment(iv)%ptr(1)
          jv=edge(ib)%segment(iv)%ptr(2)
          IF (jb/=0) THEN
            iqo=1
            iqi=nqty*(nside-1)+1
            DO is=1,nside
              edge(ib)%seg_cout(iqo:iqo+nqty-1,iv)=                             &
                 edge(ib)%seg_cout(iqo:iqo+nqty-1,iv)                           &
                +edge(jb)%seg_cin(iqi:iqi+nqty-1,jv)
              iqo=iqo+nqty
              iqi=iqi-nqty
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    END SUBROUTINE edge_seg_comp_accumulate

!-------------------------------------------------------------------------------
!>  knit seams together across procs
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_seam_comm(nqty,edge,nsend,send,                         &
                                  nrecv,recv,recv_request,nself,self)
      USE pardata_mod
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsend
      !> send(1:nsend) of send_type, one for each proc a message will be sent to
      TYPE(send_type), ALLOCATABLE, INTENT(INOUT) :: send(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecv
      !> recv(1:nrecv) one for each proc a message will come from
      TYPE(recv_type), ALLOCATABLE, INTENT(INOUT) :: recv(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recv_request(:)
      !> # of seam points whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nself
      !> self(1:nself), one for each point, note that a pair of
      !  self-referencing pts will be stored twice
      TYPE(self_type), ALLOCATABLE, INTENT(INOUT) :: self(:)

      INTEGER(i4) :: ibl,jbl,ivert,irecv,isend
      INTEGER(i4) :: i,j,jstart,jend,iself,jvert
      INTEGER(i4) :: ni,image
!-------------------------------------------------------------------------------
!     post receives for incoming seam data
!-------------------------------------------------------------------------------
      DO irecv = 1,nrecv
        CALL par%all_irecv(recv(irecv)%data,nqty*recv(irecv)%count,             &
                           recv(irecv)%proc,recv_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     send message with each send-datum (from seam_in) to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsend
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,send(isend)%count
          ibl = send(isend)%bl_loc(i)
          ivert = send(isend)%vertex(i)
          send(isend)%data(jstart:jend)=edge(ibl)%vert_in(1:nqty,ivert)
          jstart = jstart + nqty
          jend = jend + nqty
        ENDDO
        CALL par%all_send(send(isend)%data,nqty*send(isend)%count,              &
                          send(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     perform my own seam_in->seam_copies for pairs of pts I own
!     using self-data structure
!     conceptually identical to edge_accumulate routine
!-------------------------------------------------------------------------------
      DO iself = 1,nself
        ibl = self(iself)%block_out
        ivert = self(iself)%vertex_out
        jbl = self(iself)%block_in
        jvert = self(iself)%vertex_in
        image = self(iself)%order
        edge(ibl)%vert_hold(image,1:nqty,ivert)=edge(jbl)%vert_in(1:nqty,jvert)
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     as each seam-data message comes in, perform data->seam_hold copy
!-------------------------------------------------------------------------------
      DO j = 1,nrecv
        CALL par%waitany(nrecv,recv_request,irecv)
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,recv(irecv)%count
          ibl = recv(irecv)%bl_loc(i)
          ivert = recv(irecv)%vertex(i)
          image = recv(irecv)%order(i)
          edge(ibl)%vert_hold(image,1:nqty,ivert)=                              &
               recv(irecv)%data(jstart:jend)
          jstart = jstart + nqty
          jend = jend + nqty
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     now sum data from all images (including self) in a pre-determined order,
!     so that round-off error will be the same for all representations.
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO ivert = 1,edge(ibl)%nvert
          ni=edge(ibl)%vertex(ivert)%nimage
          SELECT CASE(ni)
          CASE(0)
            edge(ibl)%vert_out(1:nqty,ivert)=                                   &
               edge(ibl)%vert_in(1:nqty,ivert)
          CASE(1)
            edge(ibl)%vert_out(1:nqty,ivert)=                                   &
               edge(ibl)%vert_in(1:nqty,ivert)                                  &
              +edge(ibl)%vert_hold(1,1:nqty,ivert)
          CASE DEFAULT
            image=edge(ibl)%vertex(ivert)%order(0)
            edge(ibl)%vert_hold(image,1:nqty,ivert)=                            &
              edge(ibl)%vert_in(1:nqty,ivert)
            edge(ibl)%vert_out(1:nqty,ivert)=                                   &
              SUM(edge(ibl)%vert_hold(1:ni+1,1:nqty,ivert),1)
          END SELECT
        ENDDO
      ENDDO
    END SUBROUTINE parallel_seam_comm

!-------------------------------------------------------------------------------
!>  knit seams together across procs for complex vertex quantities.
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_seam_comm_comp(nqty,edge,nsend,send,                    &
                                       nrecv,recv,recv_request,nself,self)
      USE pardata_mod
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsend
      !> send(1:nsend) of send_type, one for each proc a message will be sent to
      TYPE(send_type), ALLOCATABLE, INTENT(INOUT) :: send(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecv
      !> recv(1:nrecv) one for each proc a message will come from
      TYPE(recv_type), ALLOCATABLE, INTENT(INOUT) :: recv(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recv_request(:)
      !> # of seam points whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nself
      !> self(1:nself), one for each point, note that a pair of
      !  self-referencing pts will be stored twice
      TYPE(self_type), ALLOCATABLE, INTENT(INOUT) :: self(:)

      INTEGER(i4) :: ibl,jbl,ivert,irecv,isend
      INTEGER(i4) :: i,j,jstart,jend,iself,jvert
      INTEGER(i4) :: ni,image
!-------------------------------------------------------------------------------
!     post receives for incoming seam data
!-------------------------------------------------------------------------------
      DO irecv = 1,nrecv
        CALL par%all_irecv(recv(irecv)%cdata,nqty*recv(irecv)%count,            &
                           recv(irecv)%proc,recv_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     send message with each send-datum (from seam_in) to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsend
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,send(isend)%count
          ibl = send(isend)%bl_loc(i)
          ivert = send(isend)%vertex(i)
          send(isend)%cdata(jstart:jend)=                                       &
               edge(ibl)%vert_cin(1:nqty,ivert)
          jstart = jstart + nqty
          jend = jend + nqty
        ENDDO
        CALL par%all_send(send(isend)%cdata,nqty*send(isend)%count,             &
                          send(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     perform my own seam_in->seam_copies for pairs of pts I own
!     using self-data structure
!     conceptually identical to edge_accumulate routine
!-------------------------------------------------------------------------------
      DO iself = 1,nself
        ibl = self(iself)%block_out
        ivert = self(iself)%vertex_out
        jbl = self(iself)%block_in
        jvert = self(iself)%vertex_in
        image = self(iself)%order
        edge(ibl)%vert_chold(image,1:nqty,ivert)=                               &
             edge(jbl)%vert_cin(1:nqty,jvert)
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     as each seam-data message comes in, perform data->seam_hold copy
!-------------------------------------------------------------------------------
      DO j = 1,nrecv
        CALL par%waitany(nrecv,recv_request,irecv)
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,recv(irecv)%count
          ibl = recv(irecv)%bl_loc(i)
          ivert = recv(irecv)%vertex(i)
          image = recv(irecv)%order(i)
          edge(ibl)%vert_chold(image,1:nqty,ivert) =                            &
               recv(irecv)%cdata(jstart:jend)
          jstart = jstart + nqty
          jend = jend + nqty
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     now sum data from all images (including self) in a pre-determined order,
!     so that round-off error will be the same for all representations.
!-------------------------------------------------------------------------------
      DO ibl=1,UBOUND(edge,1)
        DO ivert = 1,edge(ibl)%nvert
          ni=edge(ibl)%vertex(ivert)%nimage
          SELECT CASE(ni)
          CASE(0)
            edge(ibl)%vert_cout(1:nqty,ivert)=                                  &
               edge(ibl)%vert_cin(1:nqty,ivert)
          CASE(1)
            edge(ibl)%vert_cout(1:nqty,ivert)=                                  &
               edge(ibl)%vert_cin(1:nqty,ivert)                                 &
              +edge(ibl)%vert_chold(1,1:nqty,ivert)
          CASE DEFAULT
            image=edge(ibl)%vertex(ivert)%order(0)
            edge(ibl)%vert_chold(image,1:nqty,ivert)=                           &
              edge(ibl)%vert_cin(1:nqty,ivert)
            edge(ibl)%vert_cout(1:nqty,ivert)=                                  &
              SUM(edge(ibl)%vert_chold(1:ni+1,1:nqty,ivert),1)
          END SELECT
        ENDDO
      ENDDO
    END SUBROUTINE parallel_seam_comm_comp

!-------------------------------------------------------------------------------
!>  knit segments together across procs for normal edge-centered data.
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_seg_comm(nqty,nside,edge,nsendseg,sendseg,nrecvseg,     &
                                 recvseg,recvseg_request,nselfseg,selfseg)
      USE pardata_mod
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> size of each segment
      INTEGER(i4), INTENT(IN) :: nside
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsendseg
      !> sendseg(1:nsendseg) one for each proc a message will be sent to
      TYPE(sendseg_type), ALLOCATABLE, INTENT(INOUT) :: sendseg(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecvseg
      !> recvseg(1:nrecvseg) one for each proc a message will come from
      TYPE(recvseg_type), ALLOCATABLE, INTENT(INOUT) :: recvseg(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recvseg_request(:)
      !> # of segments whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nselfseg
      !> selfseg(1:nselfseg) one for each segment, note that a
      !  pair of self-referencing segments will be stored twice
      TYPE(selfseg_type), ALLOCATABLE, INTENT(INOUT) :: selfseg(:)

      INTEGER(i4) :: ibl,jbl,iseg,irecv,isend,nqns,iqi,iqo
      INTEGER(i4) :: i,j,jb,jstart,jend,iself,jseg
!-------------------------------------------------------------------------------
!     post receives for incoming segment data
!-------------------------------------------------------------------------------
      nqns=nqty*nside
      DO irecv = 1,nrecvseg
        CALL par%all_irecv(recvseg(irecv)%data,nqns*recvseg(irecv)%count,       &
                           recvseg(irecv)%proc,recvseg_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     send message with each seg-datum (from seam_in) to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsendseg
        jstart = 1
        jend = jstart + nqns - 1
        DO i = 1,sendseg(isend)%count
          ibl = sendseg(isend)%bl_loc(i)
          iseg = sendseg(isend)%segment(i)
          sendseg(isend)%data(jstart:jend) =                                    &
               edge(ibl)%seg_in(1:nqns,iseg)
          jstart = jstart + nqns
          jend = jend + nqns
        ENDDO
        CALL par%all_send(sendseg(isend)%data,nqns*sendseg(isend)%count,        &
                          sendseg(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     perform my own seam_in->seam_out summations for pairs of segments I own
!     using selfseg-data structure
!     conceptually identical to edge_seg_accumulate routine, now with
!     side-basis ordering reversed between adjacent blocks
!-------------------------------------------------------------------------------
      DO iself = 1,nselfseg
        ibl = selfseg(iself)%block_out
        iseg = selfseg(iself)%segment_out
        jbl = selfseg(iself)%block_in
        jseg = selfseg(iself)%segment_in
        iqi = 1
        iqo = nqty*(nside-1) + 1
        DO jb = 1,nside
          edge(ibl)%seg_out(iqo:iqo+nqty-1,iseg) =                              &
               edge(ibl)%seg_out(iqo:iqo+nqty-1,iseg) +                         &
               edge(jbl)%seg_in(iqi:iqi+nqty-1,jseg)
          iqi = iqi + nqty
          iqo = iqo - nqty
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     as each seg-data message comes in, perform data->seam_out summation
!-------------------------------------------------------------------------------
      DO j = 1,nrecvseg
        CALL par%waitany(nrecvseg,recvseg_request,irecv)
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,recvseg(irecv)%count
          ibl = recvseg(irecv)%bl_loc(i)
          iseg = recvseg(irecv)%segment(i)
          iqo = nqty*(nside-1) + 1
          DO jb = 1,nside
            edge(ibl)%seg_out(iqo:iqo+nqty-1,iseg) =                            &
                 edge(ibl)%seg_out(iqo:iqo+nqty-1,iseg) +                       &
                 recvseg(irecv)%data(jstart:jend)
            iqo = iqo - nqty
            jstart = jstart + nqty
            jend = jend + nqty
          ENDDO
        ENDDO
      ENDDO
    END SUBROUTINE parallel_seg_comm

!-------------------------------------------------------------------------------
!>  knit segments together across procs for complex edge-centered data.
!-------------------------------------------------------------------------------
    SUBROUTINE parallel_seg_comm_comp(nqty,nside,edge,nsendseg,sendseg,nrecvseg,&
                                      recvseg,recvseg_request,nselfseg,selfseg)
      USE pardata_mod
      IMPLICIT NONE

      !> comm quantity size
      INTEGER(i4), INTENT(IN) :: nqty
      !> size of each segment
      INTEGER(i4), INTENT(IN) :: nside
      !> edge(0:nbl) array of block edges
      TYPE(edge_type), INTENT(INOUT) :: edge(0:)
      !> # of messages I will send
      INTEGER(i4), INTENT(INOUT) :: nsendseg
      !> sendseg(1:nsendseg) one for each proc a message will be sent to
      TYPE(sendseg_type), ALLOCATABLE, INTENT(INOUT) :: sendseg(:)
      !> # of messages I will receive
      INTEGER(i4), INTENT(INOUT) :: nrecvseg
      !> recvseg(1:nrecvseg) one for each proc a message will come from
      TYPE(recvseg_type), ALLOCATABLE, INTENT(INOUT) :: recvseg(:)
      !> array of requests for posting asynchronous receives
      TYPE(mpi_request), ALLOCATABLE, INTENT(INOUT) :: recvseg_request(:)
      !> # of segments whose images are also owned by me
      INTEGER(i4), INTENT(INOUT) :: nselfseg
      !> selfseg(1:nselfseg) one for each segment, note that a
      !  pair of self-referencing segments will be stored twice
      TYPE(selfseg_type), ALLOCATABLE, INTENT(INOUT) :: selfseg(:)

      INTEGER(i4) :: ibl,jbl,iseg,irecv,isend,nqns,iqi,iqo
      INTEGER(i4) :: i,j,jb,jstart,jend,iself,jseg
!-------------------------------------------------------------------------------
!     post receives for incoming segment data
!-------------------------------------------------------------------------------
      nqns=nqty*nside
      DO irecv = 1,nrecvseg
        CALL par%all_irecv(recvseg(irecv)%cdata,nqns*recvseg(irecv)%count,      &
                           recvseg(irecv)%proc,recvseg_request(irecv))
      ENDDO
!-------------------------------------------------------------------------------
!     send message with each seg-datum (from seam_in) to each receiving proc
!-------------------------------------------------------------------------------
      DO isend = 1,nsendseg
        jstart = 1
        jend = jstart + nqns - 1
        DO i = 1,sendseg(isend)%count
          ibl = sendseg(isend)%bl_loc(i)
          iseg = sendseg(isend)%segment(i)
          sendseg(isend)%cdata(jstart:jend)=edge(ibl)%seg_cin(1:nqns,iseg)
          jstart = jstart + nqns
          jend = jend + nqns
        ENDDO
        CALL par%all_send(sendseg(isend)%cdata,nqns*sendseg(isend)%count,       &
                          sendseg(isend)%proc)
      ENDDO
!-------------------------------------------------------------------------------
!     perform my own seam_in->seam_out summations for pairs of segments I own
!     using selfseg-data structure
!     conceptually identical to edge_seg_accumulate routine, now with
!     side-basis ordering reversed between adjacent blocks
!-------------------------------------------------------------------------------
      DO iself = 1,nselfseg
        ibl = selfseg(iself)%block_out
        iseg = selfseg(iself)%segment_out
        jbl = selfseg(iself)%block_in
        jseg = selfseg(iself)%segment_in
        iqi = 1
        iqo = nqty*(nside-1) + 1
        DO jb = 1,nside
          edge(ibl)%seg_cout(iqo:iqo+nqty-1,iseg) =                             &
               edge(ibl)%seg_cout(iqo:iqo+nqty-1,iseg) +                        &
               edge(jbl)%seg_cin(iqi:iqi+nqty-1,jseg)
          iqi = iqi + nqty
          iqo = iqo - nqty
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     loop until all messages are received
!     as each seg-data message comes in, perform data->seam_out summation
!-------------------------------------------------------------------------------
      DO j = 1,nrecvseg
        CALL par%waitany(nrecvseg,recvseg_request,irecv)
        jstart = 1
        jend = jstart + nqty - 1
        DO i = 1,recvseg(irecv)%count
          ibl = recvseg(irecv)%bl_loc(i)
          iseg = recvseg(irecv)%segment(i)
          iqo = nqty*(nside-1) + 1
          DO jb = 1,nside
            edge(ibl)%seg_cout(iqo:iqo+nqty-1,iseg) =                           &
                 edge(ibl)%seg_cout(iqo:iqo+nqty-1,iseg) +                      &
                 recvseg(irecv)%cdata(jstart:jend)
            iqo = iqo - nqty
            jstart = jstart + nqty
            jend = jend + nqty
          ENDDO
        ENDDO
      ENDDO
    END SUBROUTINE parallel_seg_comm_comp

  END SUBROUTINE edge_network

END MODULE seam_mod
