!-------------------------------------------------------------------------------
!! topologically simple initialization routines for seams
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* topologically simple initialization routines for seams
!-------------------------------------------------------------------------------
MODULE simple_seam_mod
  USE local
  USE seam_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: seam_rect_alloc,seam_circ_alloc,set_edge_norm_tang_rect

CONTAINS

!-------------------------------------------------------------------------------
!* initializes the grid seams for topologicially simple rectangular blocks.
!-------------------------------------------------------------------------------
  SUBROUTINE seam_rect_alloc(seam,nxbl,nybl,mxbl,mybl,periodicity,r0left)
    USE pardata_mod
    IMPLICIT NONE

    !> seam structure to initialize
    TYPE(seam_type), INTENT(OUT) :: seam
    !> number of blocks in the x-dir
    INTEGER(i4), INTENT(IN) :: nxbl
    !> number of blocks in the y-dir
    INTEGER(i4), INTENT(IN) :: nybl
    !> block size in the x-dir
    INTEGER(i4), INTENT(IN) :: mxbl(nxbl)
    !> block size in the y-dir
    INTEGER(i4), INTENT(IN) :: mybl(nybl)
    !> periodicity flag (options: both, y-dir, none)
    CHARACTER(*), INTENT(IN) :: periodicity
    !> true if left side touches r0 boundary
    LOGICAL, INTENT(IN) :: r0left

    INTEGER(i4) :: ixbl,iybl,ib,mxb,myb,iv,ix,iy,ip,nv,nbl
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: np,jd
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: jb,jv
    INTEGER(i4), DIMENSION(2,-nybl:nxbl*nybl+nybl+1) :: kb
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ixv,iyv
!-------------------------------------------------------------------------------
!   allocate arrays.
!-------------------------------------------------------------------------------
    nbl=nxbl*nybl
    ALLOCATE(seam%s(0:nbl))
    ALLOCATE(seam%glob_nvert(nbl))
    DO ib=0,nbl
      seam%s(ib)%id=ib
    ENDDO
!-------------------------------------------------------------------------------
!   create a temporary array containing block dimensions with
!   a rim of ghost blocks to prevent operand range errors later.
!-------------------------------------------------------------------------------
    kb=0
    ib=0
    DO ixbl=1,nxbl
      DO iybl=1,nybl
        ib=ib+1
        kb(1,ib)=mxbl(ixbl)
        kb(2,ib)=mybl(iybl)
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   initialize the external boundary seam%s(0)
!-------------------------------------------------------------------------------
    CALL seam0_rect_alloc
!-------------------------------------------------------------------------------
!   start loop over interior blocks.
!-------------------------------------------------------------------------------
    ib=0
    DO ixbl=1,nxbl
      DO iybl=1,nybl
        ib=ib+1
!-------------------------------------------------------------------------------
!       allocate arrays.
!-------------------------------------------------------------------------------
        mxb=mxbl(ixbl)
        myb=mybl(iybl)
        nv=2*mxb+2*myb
        seam%s(ib)%nvert=nv
        seam%glob_nvert(ib)=nv
        ALLOCATE(seam%s(ib)%vertex(nv))
        ALLOCATE(np(nv))
        ALLOCATE(jb(nv,3))
        ALLOCATE(jv(nv,3))
        ALLOCATE(jd(nv))
        np=[(1,ix=1,mxb-1),3,(1,iy=1,myb-1),3,                                  &
             (1,ix=1,mxb-1),3,(1,iy=1,myb-1),3]
!-------------------------------------------------------------------------------
!       fill block pointers.
!-------------------------------------------------------------------------------
        jb=0
        jb(:,1)=[(ib-1_i4,ix=1,mxb),(ib+nybl,iy=1,myb),                         &
                  (ib+1_i4,ix=1,mxb),(ib-nybl,iy=1,myb)]
        IF (TRIM(periodicity)/='none') THEN
          IF (nybl==1) THEN
            jb(1:mxb,1)=ib
            jb(mxb+myb+1:2*mxb+myb,1)=ib
          ELSE IF (iybl==1) THEN
            jb(1:mxb,1)=ib+nybl-1
          ELSE IF (iybl==nybl) THEN
            jb(mxb+myb+1:2*mxb+myb,1)=ib-nybl+1
          ENDIF
          IF (TRIM(periodicity)=='both') THEN
            IF (nxbl==1) THEN
              jb(mxb+1:mxb+myb,1)=ib
              jb(2*mxb+myb+1:2*(mxb+myb),1)=ib
            ELSE IF (ixbl==1) THEN
              jb(2*mxb+myb+1:2*(mxb+myb),1)=ib+nybl*(nxbl-1)
            ELSE IF (ixbl==nxbl) THEN
              jb(mxb+1:mxb+myb,1)=ib-nybl*(nxbl-1)
            ENDIF
          ENDIF
        ENDIF
        jb(mxb,2)=jb(mxb,1)+nybl
        IF (jb(mxb,2)>nbl) jb(mxb,2)=jb(mxb,2)-nbl
        jb(mxb,3)=ib+nybl
        IF (jb(mxb,3)>nbl) jb(mxb,3)=jb(mxb,3)-nbl
        jb(mxb+myb,2)=jb(mxb+myb+1,1)+nybl
        IF (jb(mxb+myb,2)>nbl) jb(mxb+myb,2)=jb(mxb+myb,2)-nbl
        jb(mxb+myb,3)=jb(mxb+myb+1,1)
        jb(2*mxb+myb,2)=jb(2*mxb+myb,1)-nybl
        IF (jb(2*mxb+myb,2)<1) jb(2*mxb+myb,2)=jb(2*mxb+myb,2)+nbl
        jb(2*mxb+myb,3)=ib-nybl
        IF (jb(2*mxb+myb,3)<1) jb(2*mxb+myb,3)=jb(2*mxb+myb,3)+nbl
        jb(2*mxb+2*myb,2)=jb(1,1)-nybl
        IF (jb(2*mxb+2*myb,2)<1) jb(2*mxb+2*myb,2)=jb(2*mxb+2*myb,2)+nbl
        jb(2*mxb+2*myb,3)=jb(1,1)
!-------------------------------------------------------------------------------
!       fill vertex pointers.
!-------------------------------------------------------------------------------
        jv=0
        jv(:,1)=[(2*mxb+kb(2,jb(iv,1))-iv,iv=1,mxb),                            &
             (2*kb(1,jb(iv,1))+2*myb+mxb-iv,iv=mxb+1,mxb+myb),                  &
             (2*mxb+myb-iv,iv=1+mxb+myb,2*mxb+myb),                             &
             (kb(1,jb(iv,1))+2*myb+2*mxb-iv,iv=2*mxb+myb+1,2*(mxb+myb))]
        jv(mxb,2)=2*kb(1,jb(mxb,2))+kb(2,jb(mxb,2))
        jv(mxb,3)=2*kb(1,jb(mxb,3))+2*kb(2,jb(mxb,3))
        jv(mxb+myb,2)=2*kb(1,jb(mxb+myb,2))+2*kb(2,jb(mxb+myb,2))
        jv(mxb+myb,3)=kb(1,jb(mxb+myb,3))
        jv(2*mxb+myb,1)=2*(kb(1,jb(2*mxb+myb,1))+kb(2,jb(2*mxb+myb,1)))
        jv(2*mxb+myb,2)=kb(1,jb(2*mxb+myb,2))
        jv(2*mxb+myb,3)=kb(1,jb(2*mxb+myb,3))+kb(2,jb(2*mxb+myb,3))
        jv(2*(mxb+myb),2)=kb(1,jb(2*(mxb+myb),2))+kb(2,jb(2*(mxb+myb),2))
        jv(2*(mxb+myb),3)=2*kb(1,jb(2*(mxb+myb),3))+kb(2,jb(2*(mxb+myb),3))
!-------------------------------------------------------------------------------
!       initialize pointer delete indicator for external boundaries.
!-------------------------------------------------------------------------------
        jd=0
!-------------------------------------------------------------------------------
!       trim left pointers.
!-------------------------------------------------------------------------------
        IF (ixbl==1.AND.TRIM(periodicity)/='both') THEN
          iv=edge_match(ib,2_i4*mxb+myb+1_i4)
          jb(2*mxb+myb+1:2*(mxb+myb),1)=0
          jv(2*mxb+myb+1:2*(mxb+myb),1)=[(iv+iy,iy=0_i4,myb-1_i4)]
          jd(2*(mxb+myb))=1
          IF (iybl==1.AND.TRIM(periodicity)/='y-dir') THEN
            jd(2*(mxb+myb))=2
          ENDIF
          IF (iybl==nybl) THEN
            IF (TRIM(periodicity)=='y-dir') THEN
              jb(2*mxb+myb,3)=0
              jv(2*mxb+myb,3)=edge_match(ib,2_i4*mxb+myb)
              jd(2*mxb+myb)=1
            ENDIF
          ELSE
            jb(2*mxb+myb,3)=0
            jv(2*mxb+myb,3)=iv-1
            jd(2*mxb+myb)=1
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       trim right pointers.
!-------------------------------------------------------------------------------
        IF (ixbl==nxbl.AND.TRIM(periodicity)/='both') THEN
          iv=edge_match(ib,mxb+1_i4)
          jb(mxb+1:mxb+myb,1)=0
          jv(mxb+1:mxb+myb,1)=[(iv+iy,iy=0_i4,myb-1_i4)]
          jd(mxb+myb)=1
          IF (iybl==nybl.AND.TRIM(periodicity)/='y-dir') THEN
            jd(mxb+myb)=2
          ENDIF
          IF (iybl==1) THEN
            IF (TRIM(periodicity)=='y-dir') THEN
              jb(mxb,3)=0
              jv(mxb,3)=edge_match(ib,mxb)
              jd(mxb)=1
            ENDIF
          ELSE
            jb(mxb,3)=0
            jv(mxb,3)=iv-1
            jd(mxb)=1
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       trim bottom pointers.
!-------------------------------------------------------------------------------
        IF (iybl==1.AND.TRIM(periodicity)=='none') THEN
          iv=edge_match(ib,1_i4)
          jb(1:mxb,1)=0
          jv(1:mxb,1)=[(iv+ix,ix=0_i4,mxb-1_i4)]
          jd(mxb)=1
          IF (ixbl==nxbl) THEN
            jd(mxb)=2
          ENDIF
          IF (ixbl/=1) THEN
            jb(2*(mxb+myb),3)=0
            jv(2*(mxb+myb),3)=iv-1
            jd(2*(mxb+myb))=1
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       trim top pointers.
!-------------------------------------------------------------------------------
        IF (iybl==nybl.AND.TRIM(periodicity)=='none') THEN
          iv=edge_match(ib,mxb+myb+1_i4)
          jb(1+mxb+myb:2*mxb+myb,1)=0
          jv(1+mxb+myb:2*mxb+myb,1)=[(iv+ix,ix=0_i4,mxb-1_i4)]
          jd(2*mxb+myb)=1
          IF (ixbl==1) THEN
            jd(2*mxb+myb)=2
          ENDIF
          IF (ixbl/=nxbl) THEN
            jb(mxb+myb,3)=0
            jv(mxb+myb,3)=iv-1
            jd(mxb+myb)=1
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       fill pointers.
!-------------------------------------------------------------------------------
        DO iv=1,nv
          IF (np(iv)-jd(iv)<=0) THEN
            CALL par%nim_stop('Error assigning seam pointers.')
          ENDIF
          ALLOCATE(seam%s(ib)%vertex(iv)%ptr(2_i4,np(iv)-jd(iv)))
          seam%s(ib)%vertex(iv)%ptr(1,1)=jb(iv,1)
          seam%s(ib)%vertex(iv)%ptr(2,1)=jv(iv,1)
          IF (jd(iv)==0) THEN
            DO ip=2,np(iv)
              seam%s(ib)%vertex(iv)%ptr(1,ip)=jb(iv,ip)
              seam%s(ib)%vertex(iv)%ptr(2,ip)=jv(iv,ip)
            ENDDO
          ELSE IF (jd(iv)==1) THEN
            seam%s(ib)%vertex(iv)%ptr(1,2)=jb(iv,3)
            seam%s(ib)%vertex(iv)%ptr(2,2)=jv(iv,3)
          ENDIF
        ENDDO
!-------------------------------------------------------------------------------
!       finish loop over blocks.
!-------------------------------------------------------------------------------
        DEALLOCATE(np)
        DEALLOCATE(jb)
        DEALLOCATE(jv)
        DEALLOCATE(jd)
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   find and save block-internal vertex coordinates.
!-------------------------------------------------------------------------------
    DO ib=1,nbl
      mxb=kb(1,ib)
      myb=kb(2,ib)
      ALLOCATE(ixv(2*(mxb+myb)))
      ALLOCATE(iyv(2*(mxb+myb)))
      ixv=[(ix,ix=1,mxb),(mxb,iy=1,myb),                                        &
           (ix,ix=mxb-1,0,-1),(0_i4,iy=myb-1,0,-1)]
      iyv=[(0_i4,ix=1,mxb),(iy,iy=1,myb),                                       &
           (myb,ix=mxb-1,0,-1),(iy,iy=myb-1,0,-1)]
      DO iv=1,seam%s(ib)%nvert
        seam%s(ib)%vertex(iv)%intxy(1)=ixv(iv)
        seam%s(ib)%vertex(iv)%intxy(2)=iyv(iv)
      ENDDO
      DEALLOCATE(ixv)
      DEALLOCATE(iyv)
    ENDDO
  CONTAINS

!-------------------------------------------------------------------------------
!>  matches the external boundary pointer with internal block and
!   vertex indices.  returns the external seam vertex.
!-------------------------------------------------------------------------------
    FUNCTION edge_match(intbl,intvrt) RESULT(iv0)
      USE edge_mod
      IMPLICIT NONE

      !> block to match
      INTEGER(i4), INTENT(IN) :: intbl
      !> vertex to match
      INTEGER(i4), INTENT(IN) :: intvrt

      INTEGER(i4) :: iv0,ip
      LOGICAL :: match
      CHARACTER(64) :: message
!-------------------------------------------------------------------------------
!     loop over external vertices
!-------------------------------------------------------------------------------
      iv0=1
      match=.false.
      ext: DO
        ptr: DO ip=1,SIZE(seam%s(0)%vertex(iv0)%ptr,2)
          IF (seam%s(0)%vertex(iv0)%ptr(1,ip)==intbl) THEN
            IF (seam%s(0)%vertex(iv0)%ptr(2,ip)==intvrt) THEN
              match=.true.
            ENDIF
          ENDIF
        ENDDO ptr
        IF (match) EXIT ext
        iv0=iv0+1
        IF (iv0>seam%s(0)%nvert) THEN
          WRITE(message,'(2a)') 'Error matching external seam in edge_match.'
          CALL par%nim_stop(message)
        ENDIF
      ENDDO ext
    END FUNCTION edge_match

!-------------------------------------------------------------------------------
!>  create the seam representing the external boundary.
!-------------------------------------------------------------------------------
    SUBROUTINE seam0_rect_alloc
      IMPLICIT NONE

      INTEGER(i4) :: ixbl,iybl,ib,iv,ix,iy,nv,ivlim

!-------------------------------------------------------------------------------
!     evaluate pointers for the external boundary.  this is a separate
!     structure which is not used after nimrod is initialized.
!-------------------------------------------------------------------------------
      IF (TRIM(periodicity)=='y-dir') THEN
        nv=0
        DO iybl=1,nybl
          nv=nv+2*mybl(iybl)
        ENDDO
      ELSE IF (TRIM(periodicity)=='both') THEN
        nv=0
      ELSE
        nv=0
        DO ixbl=1,nxbl
          nv=nv+2*mxbl(ixbl)
        ENDDO
        DO iybl=1,nybl
          nv=nv+2*mybl(iybl)
        ENDDO
      ENDIF
      seam%s(0)%nvert=nv
      seam%s(0)%id=0
      IF (TRIM(periodicity)=='both') RETURN
!-------------------------------------------------------------------------------
!     start of periodicity/='both' IF-THEN block.
!-------------------------------------------------------------------------------
      ALLOCATE(seam%s(0)%vertex(nv))
      ALLOCATE(seam%s(0)%excorner(nv))
      seam%s(0)%excorner=.false.
      ALLOCATE(seam%s(0)%r0point(nv))
      seam%s(0)%r0point=.false.
      ib=1
      iv=1
!-------------------------------------------------------------------------------
!     traverse bottom of blocks.
!-------------------------------------------------------------------------------
      yper1: IF (TRIM(periodicity)/='y-dir') THEN
        xblock1: DO ixbl=1,nxbl-1
          xvert1: DO ix=1,kb(1,ib)-1
            ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
            seam%s(0)%vertex(iv)%ptr(1,1)=ib
            seam%s(0)%vertex(iv)%ptr(2,1)=ix
            iv=iv+1
          ENDDO xvert1
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)
          ib=ib+nybl
          seam%s(0)%vertex(iv)%ptr(1,2)=ib
          seam%s(0)%vertex(iv)%ptr(2,2)=2*(kb(1,ib)+kb(2,ib))
          iv=iv+1
        ENDDO xblock1
!-------------------------------------------------------------------------------
!       corner block.
!-------------------------------------------------------------------------------
        xvert2: DO ix=1,kb(1,ib)
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=ix
          iv=iv+1
        ENDDO xvert2
      ELSE yper1
!-------------------------------------------------------------------------------
!       no bottom if periodic in y-direction.
!-------------------------------------------------------------------------------
        ib=ib+nybl*(nxbl-1)
      ENDIF yper1
!-------------------------------------------------------------------------------
!     up right side of blocks.
!-------------------------------------------------------------------------------
      IF (TRIM(periodicity)/='y-dir') seam%s(0)%excorner(iv-1)=.true.
      yblock1: DO iybl=1,nybl-1
        yvert1: DO iy=1,kb(2,ib)-1
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+iy
          iv=iv+1
        ENDDO yvert1
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+kb(2,ib)
        ib=ib+1
        seam%s(0)%vertex(iv)%ptr(1,2)=ib
        seam%s(0)%vertex(iv)%ptr(2,2)=kb(1,ib)
        iv=iv+1
      ENDDO yblock1
!-------------------------------------------------------------------------------
!     corner block.
!-------------------------------------------------------------------------------
      IF (TRIM(periodicity)=='y-dir') THEN
        ivlim=kb(2,ib)-1
      ELSE
        ivlim=kb(2,ib)
      ENDIF
      yvert2: DO iy=1,ivlim
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+iy
        iv=iv+1
      ENDDO yvert2
      IF (TRIM(periodicity)=='y-dir') THEN
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+kb(2,ib)
        seam%s(0)%vertex(iv)%ptr(1,2)=ib+1-nybl
        seam%s(0)%vertex(iv)%ptr(2,2)=kb(1,ib)
        iv=iv+1
      ELSE
        seam%s(0)%excorner(iv-1)=.true.
      ENDIF
!-------------------------------------------------------------------------------
!     traverse top of blocks.
!-------------------------------------------------------------------------------
      yper2: IF (TRIM(periodicity)/='y-dir') THEN
        xblock2: DO ixbl=nxbl,2,-1
          xvert3: DO ix=1,kb(1,ib)-1
            ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
            seam%s(0)%vertex(iv)%ptr(1,1)=ib
            seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+kb(2,ib)+ix
            iv=iv+1
          ENDDO xvert3
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=2*kb(1,ib)+kb(2,ib)
          ib=ib-nybl
          seam%s(0)%vertex(iv)%ptr(1,2)=ib
          seam%s(0)%vertex(iv)%ptr(2,2)=kb(1,ib)+kb(2,ib)
          iv=iv+1
        ENDDO xblock2
!-------------------------------------------------------------------------------
!       corner block.
!-------------------------------------------------------------------------------
        xvert4: DO ix=1,kb(1,ib)
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=kb(1,ib)+kb(2,ib)+ix
          iv=iv+1
        ENDDO xvert4
      ELSE yper2
!-------------------------------------------------------------------------------
!       no top if periodic in y-direction.
!-------------------------------------------------------------------------------
        ib=ib-nybl*(nxbl-1)
      ENDIF yper2
!-------------------------------------------------------------------------------
!     down left side of blocks.
!-------------------------------------------------------------------------------
      IF (TRIM(periodicity)/='y-dir') THEN
        IF (r0left) seam%s(0)%r0point(iv-1)=.true.
        seam%s(0)%excorner(iv-1)=.true.
      ENDIF
      yblock2: DO iybl=nybl,2,-1
        yvert3: DO iy=1,kb(2,ib)-1
          ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
          seam%s(0)%vertex(iv)%ptr(1,1)=ib
          seam%s(0)%vertex(iv)%ptr(2,1)=2*kb(1,ib)+kb(2,ib)+iy
          IF (r0left) seam%s(0)%r0point(iv)=.true.
          iv=iv+1
        ENDDO yvert3
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=2*(kb(1,ib)+kb(2,ib))
        ib=ib-1
        seam%s(0)%vertex(iv)%ptr(1,2)=ib
        seam%s(0)%vertex(iv)%ptr(2,2)=2*kb(1,ib)+kb(2,ib)
        IF (r0left) seam%s(0)%r0point(iv)=.true.
        iv=iv+1
      ENDDO yblock2
!-------------------------------------------------------------------------------
!     corner block.
!-------------------------------------------------------------------------------
      IF (TRIM(periodicity)=='y-dir') THEN
        ivlim=kb(2,ib)-1
      ELSE
        ivlim=kb(2,ib)
      ENDIF
      yvert4: DO iy=1,ivlim
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,1_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=2*kb(1,ib)+kb(2,ib)+iy
        IF (r0left) seam%s(0)%r0point(iv)=.true.
        iv=iv+1
      ENDDO yvert4
      IF (TRIM(periodicity)=='y-dir') THEN
        ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,2_i4))
        seam%s(0)%vertex(iv)%ptr(1,1)=ib
        seam%s(0)%vertex(iv)%ptr(2,1)=2*(kb(1,ib)+kb(2,ib))
        seam%s(0)%vertex(iv)%ptr(1,2)=ib+nybl-1
        seam%s(0)%vertex(iv)%ptr(2,2)=2*kb(1,ib)+kb(2,ib)
        IF (r0left) seam%s(0)%r0point(iv)=.true.
        iv=iv+1
      ELSE
        seam%s(0)%excorner(iv-1)=.true.
      ENDIF
      IF (iv/=nv+1) THEN
        CALL par%nim_stop('Error in seam%s(0) initialization.')
      ENDIF
    END SUBROUTINE seam0_rect_alloc

  END SUBROUTINE seam_rect_alloc

!-------------------------------------------------------------------------------
!* initializes the grid seams for topologicially circ mesh blocks.
!-------------------------------------------------------------------------------
  SUBROUTINE seam_circ_alloc(seam,nxbl,nybl,nrbl,ntbl,mxbl,mybl,mrbl,mtbl)
    USE pardata_mod
    IMPLICIT NONE

    !> seam structure to initialize
    TYPE(seam_type), INTENT(OUT) :: seam
    !> number of core blocks in the x-dir
    INTEGER(i4), INTENT(IN) :: nxbl
    !> number of core blocks in the y-dir
    INTEGER(i4), INTENT(IN) :: nybl
    !> number of leaf blocks in the r-dir
    INTEGER(i4), INTENT(IN) :: nrbl
    !> number of leaf blocks in the theta-dir
    INTEGER(i4), INTENT(IN) :: ntbl
    !> core block size in the x-dir
    INTEGER(i4), INTENT(IN) :: mxbl(nxbl)
    !> core block size in the y-dir
    INTEGER(i4), INTENT(IN) :: mybl(nybl)
    !> leaf block size in the r-dir
    INTEGER(i4), INTENT(IN) :: mrbl(nrbl)
    !> leaf block size in the theta-dir
    INTEGER(i4), INTENT(IN) :: mtbl(ntbl)

    INTEGER(i4) :: ib,iv,ip,nv,nbl,jp,kp
    INTEGER(i4) :: npv,tbl,abl,nv_annulus,av,np
    INTEGER(i4) :: ncbl,nlbl,nap,ncp
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: jb,jv
    TYPE(seam_type):: core_seam,annulus_seam
!-------------------------------------------------------------------------------
!   allocate arrays.
!-------------------------------------------------------------------------------
    ncbl=nxbl*nybl
    nlbl=ntbl*nrbl
    nbl=ncbl+nlbl
    ALLOCATE(seam%s(0:nbl))
    ALLOCATE(seam%glob_nvert(nbl))
    DO ib=0,nbl
      seam%s(ib)%id=ib
    ENDDO
!-------------------------------------------------------------------------------
!   Call seam rect twice to create seams for core region and annulus regions
!-------------------------------------------------------------------------------
    CALL seam_rect_alloc(core_seam,nxbl,nybl,mxbl,mybl,"none",.FALSE.)
    CAll seam_rect_alloc(annulus_seam,nrbl,ntbl,mrbl,mtbl,"y-dir",.FALSE.)
!-------------------------------------------------------------------------------
!   Copy core and annulus seams into global seam
!-------------------------------------------------------------------------------
    DO ib=1,ncbl
      nv=core_seam%s(ib)%nvert
      seam%s(ib)%nvert=nv
      seam%glob_nvert(ib)=nv
      ALLOCATE(seam%s(ib)%vertex(nv))
      DO iv=1,nv
        npv=SIZE(core_seam%s(ib)%vertex(iv)%ptr,2)
        ALLOCATE(seam%s(ib)%vertex(iv)%ptr(2_i4,npv))
        DO ip=1,npv
          seam%s(ib)%vertex(iv)%ptr(1,ip)=core_seam%s(ib)%vertex(iv)%ptr(1,ip)
          seam%s(ib)%vertex(iv)%ptr(2,ip)=core_seam%s(ib)%vertex(iv)%ptr(2,ip)
        ENDDO
        seam%s(ib)%vertex(iv)%intxy(1)=core_seam%s(ib)%vertex(iv)%intxy(1)
        seam%s(ib)%vertex(iv)%intxy(2)=core_seam%s(ib)%vertex(iv)%intxy(2)
      ENDDO
    ENDDO
    DO ib=1,nlbl
      tbl=ib+ncbl
      nv=annulus_seam%s(ib)%nvert
      seam%s(tbl)%nvert=nv
      seam%glob_nvert(tbl)=nv
      ALLOCATE(seam%s(tbl)%vertex(nv))
      DO iv=1,nv
        npv=SIZE(annulus_seam%s(ib)%vertex(iv)%ptr,2)
        ALLOCATE(seam%s(tbl)%vertex(iv)%ptr(2_i4,npv))
        DO ip=1,npv
          abl=annulus_seam%s(ib)%vertex(iv)%ptr(1,ip)
          IF (.NOT.(abl==0)) THEN
            abl=abl+ncbl
          ENDIF
          seam%s(tbl)%vertex(iv)%ptr(1,ip)=abl
          seam%s(tbl)%vertex(iv)%ptr(2,ip)=annulus_seam%s(ib)%vertex(iv)%ptr(2,ip)
        ENDDO
        seam%s(tbl)%vertex(iv)%intxy(1)=annulus_seam%s(ib)%vertex(iv)%intxy(1)
        seam%s(tbl)%vertex(iv)%intxy(2)=annulus_seam%s(ib)%vertex(iv)%intxy(2)
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   Copy outer half of annulus seam0 into global seam0
!-------------------------------------------------------------------------------
    nv=annulus_seam%s(0)%nvert-core_seam%s(0)%nvert
    seam%s(0)%nvert=nv
    seam%s(0)%id=0
    ALLOCATE(seam%s(0)%vertex(nv))
    ALLOCATE(seam%s(0)%excorner(nv))
    seam%s(0)%excorner=.false.
    ALLOCATE(seam%s(0)%r0point(nv))
    seam%s(0)%r0point=.false.
    DO iv=1,nv
      npv=SIZE(annulus_seam%s(0)%vertex(iv)%ptr,2)
      ALLOCATE(seam%s(0)%vertex(iv)%ptr(2_i4,npv))
      DO ip=1,npv
        abl=annulus_seam%s(0)%vertex(iv)%ptr(1,ip)
        seam%s(0)%vertex(iv)%ptr(1,ip)=abl+ncbl
        seam%s(0)%vertex(iv)%ptr(2,ip)=annulus_seam%s(0)%vertex(iv)%ptr(2,ip)
      ENDDO
      seam%s(0)%excorner(iv)=annulus_seam%s(0)%excorner(iv)
      seam%s(0)%r0point(iv)=annulus_seam%s(0)%r0point(iv)
    ENDDO
!-------------------------------------------------------------------------------
!   Fix seams along core/annulus boundary
!-------------------------------------------------------------------------------
    nv=core_seam%s(0)%nvert
    nv_annulus=annulus_seam%s(0)%nvert
    DO iv=1, nv
      av=nv_annulus-iv
      IF (iv==nv) THEN
        av=nv_annulus
      ENDIF
      ncp=SIZE(core_seam%s(0)%vertex(iv)%ptr,2)
      nap=SIZE(annulus_seam%s(0)%vertex(av)%ptr,2)
      np=ncp+nap
      ALLOCATE(jb(np))
      ALLOCATE(jv(np))
      DO ip=1,ncp
        jb(ip)=core_seam%s(0)%vertex(iv)%ptr(1,ip)
        jv(ip)=core_seam%s(0)%vertex(iv)%ptr(2,ip)
      ENDDO
      DO ip=1,nap
        jb(ip+ncp)=annulus_seam%s(0)%vertex(av)%ptr(1,ip)+ncbl
        jv(ip+ncp)=annulus_seam%s(0)%vertex(av)%ptr(2,ip)
      ENDDO
      DO ip=1,np
        DEALLOCATE(seam%s(jb(ip))%vertex(jv(ip))%ptr)
        ALLOCATE(seam%s(jb(ip))%vertex(jv(ip))%ptr(2_i4,np-1))
        DO jp=1,np-1
          kp=(np+ip-1)-(jp-1)
          IF (kp<1) THEN
            kp=kp+np
          ELSEIF (kp>np) THEN
            kp=kp-np
          ENDIF
          seam%s(jb(ip))%vertex(jv(ip))%ptr(1,jp)=jb(kp)
          seam%s(jb(ip))%vertex(jv(ip))%ptr(2,jp)=jv(kp)
        ENDDO
      ENDDO
      DEALLOCATE(jb)
      DEALLOCATE(jv)
    ENDDO
  END SUBROUTINE seam_circ_alloc

!-------------------------------------------------------------------------------
!> set norm and tang for the edge based for logically retangular grids
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_norm_tang_rect(edge,mx,my,n_int)
    USE edge_mod
    IMPLICIT NONE

    !> edge to set
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> block mx
    INTEGER(i4), INTENT(IN) :: mx
    !> block my
    INTEGER(i4), INTENT(IN) :: my
    !> number of interior points
    INTEGER(i4), INTENT(IN) :: n_int

    INTEGER(i4) :: ivp,iv,ix,iy,is

    ivp=edge%nvert
    IF (ANY(edge%expoint)) THEN
      ALLOCATE(edge%vert_norm(2,edge%nvert),edge%vert_tang(2,edge%nvert))
      IF (n_int>0) THEN
        ALLOCATE(edge%seg_norm(2,n_int,edge%nvert),                             &
                 edge%seg_tang(2,n_int,edge%nvert))
      ENDIF
    ENDIF
    iv=1
    DO ix=1,mx
      IF (edge%expoint(iv)) THEN
        edge%vert_tang(:,iv)=[ 1._r8,  0._r8]
        edge%vert_norm(:,iv)=[ 0._r8, -1._r8]
      ENDIF
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) THEN
        DO is=1,n_int
          edge%seg_tang(:,is,iv)=[ 1._r8,  0._r8]
          edge%seg_norm(:,is,iv)=[ 0._r8, -1._r8]
        ENDDO
      ENDIF
      ivp=iv
      iv=iv+1
    ENDDO
    IF (edge%excorner(ivp)) THEN
      edge%vert_tang(:,ivp)=[ SQRT(2._r8),  SQRT(2._r8)]
      edge%vert_norm(:,ivp)=[ SQRT(2._r8), -SQRT(2._r8)]
    ENDIF
    DO iy=1,my
      IF (edge%expoint(iv)) THEN
        edge%vert_tang(:,iv)=[ 0._r8,  1._r8]
        edge%vert_norm(:,iv)=[ 1._r8,  0._r8]
      ENDIF
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) THEN
        DO is=1,n_int
          edge%seg_tang(:,is,iv)=[ 0._r8,  1._r8]
          edge%seg_norm(:,is,iv)=[ 1._r8,  0._r8]
        ENDDO
      ENDIF
      ivp=iv
      iv=iv+1
    ENDDO
    IF (edge%excorner(ivp)) THEN
      edge%vert_tang(:,ivp)=[-SQRT(2._r8),  SQRT(2._r8)]
      edge%vert_norm(:,ivp)=[ SQRT(2._r8),  SQRT(2._r8)]
    ENDIF
    DO ix=1,mx
      IF (edge%expoint(iv)) THEN
        edge%vert_tang(:,iv)=[-1._r8,  0._r8]
        edge%vert_norm(:,iv)=[ 0._r8,  1._r8]
      ENDIF
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) THEN
        DO is=1,n_int
          edge%seg_tang(:,is,iv)=[-1._r8,  0._r8]
          edge%seg_norm(:,is,iv)=[ 0._r8,  1._r8]
        ENDDO
      ENDIF
      ivp=iv
      iv=iv+1
    ENDDO
    IF (edge%excorner(ivp)) THEN
      edge%vert_tang(:,ivp)=[-SQRT(2._r8), -SQRT(2._r8)]
      edge%vert_norm(:,ivp)=[-SQRT(2._r8),  SQRT(2._r8)]
    ENDIF
    DO iy=1,my
      IF (edge%expoint(iv)) THEN
        edge%vert_tang(:,iv)=[ 0._r8, -1._r8]
        edge%vert_norm(:,iv)=[-1._r8,  0._r8]
      ENDIF
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) THEN
        DO is=1,n_int
          edge%seg_tang(:,is,iv)=[ 0._r8, -1._r8]
          edge%seg_norm(:,is,iv)=[-1._r8,  0._r8]
        ENDDO
      ENDIF
      ivp=iv
      iv=iv+1
    ENDDO
    IF (edge%excorner(ivp)) THEN
      edge%vert_tang(:,ivp)=[ SQRT(2._r8), -SQRT(2._r8)]
      edge%vert_norm(:,ivp)=[-SQRT(2._r8), -SQRT(2._r8)]
    ENDIF
    CALL edge%alloc_gpu_norm_tang
  END SUBROUTINE set_edge_norm_tang_rect

END MODULE simple_seam_mod
