#!/bin/bash

# This runs the format checking scripts.  Use a built-in list of directories to
# check. Search for .f and .py extensions.  Passing a file will cause the
# formatter to run on only that file with output to stdout.

if test $# -eq 1; then
 cmd="python3 nimtests/check_fortran_format.py $1"
 echo $cmd
 exec $cmd
 exit 0
fi

# limit checks to certain directories, eventual project-wide coverage would be
# ideal but limiting allows us to exclude directories with predominately legacy
# formatting (e.g. nimfft and extlibs)
declare -a dirs=(
  "example"
  "nimloc"
  "nimlib"
  "nimlib/tests"
  "nimseam"
  "nimseam/tests"
  "nimfem"
  "nimfem/tests"
  "nimlinalg"
  "nimlinalg/tests"
  "nimiter"
  "nimiter/tests"
  "nimblk"
  "nimblk/tests"
  "nimgrid"
  "nimtests"
)

# files not excludes (these are included through #include)
exclude_files="nimlinalg/vec_rect_2D_real.f nimlinalg/vec_rect_2D_cv1m.f nimlinalg/vec_rect_2D_comp.f nimlinalg/vec_rect_2D_int.f nimlinalg/vec_rect_2D_real_acc.f nimlinalg/vec_rect_2D_cv1m_acc.f nimlinalg/vec_rect_2D_comp_acc.f nimlinalg/vec_rect_2D_int_acc.f"

echo "Formatting Report:"
echo "File: Number of Errors"
echo "Only files with formatting errors are listed."
echo ""

totalErrors=0
nfiles=0
for dir in "${dirs[@]}"; do
  # fortran tests
  files=`ls $dir/*.f`
  for file in $exclude_files; do
    files=`echo $files | sed "s|$file ||"`
  done
  for file in $files; do
    nerrors=`python3 nimtests/check_fortran_format.py $file | grep '\.f:' | wc -l`
    if (( nerrors > 0 )); then
      echo "$file: $nerrors"
      totalErrors=`echo "$nerrors + $totalErrors" | bc`
    fi
    ((nfiles++))
  done
  # flake8 tests
  files=`ls $dir/*.py 2>/dev/null`
  for file in $files; do
    args=""
    if [ "$file" == "nimtests/process_namelist.py" ]; then
      args="--max-line-length 100 --extend-ignore W605"
    fi
    nerrors=`flake8 $args $file | wc -l`
    if (( nerrors > 0 )); then
      echo "$file: $nerrors"
      totalErrors=`echo "$nerrors + $totalErrors" | bc`
    fi
    ((nfiles++))
  done
done

echo ""
echo "Total number of formatting errors: $totalErrors"
echo "Total number of files checked: $nfiles"
echo ""

# Do not increase this number
acceptErrors=0

retError=`echo "$totalErrors - $acceptErrors > 0" | bc`

exit $retError
