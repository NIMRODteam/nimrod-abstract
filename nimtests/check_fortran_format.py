#!/usr/bin/env python3
import optparse
from subprocess import call
import os


class formatStyle():
    ''' class for style definitions and counters '''

    def __init__(self):
        ''' default parameters '''
        # initial indentation
        self.initIndent = 0
        # indentation size
        self.indentLength = 2
        # keywords that start indented blocks of code
        self.indentList = ['do', 'while', 'select', 'module', 'type', 'type,',
                           'if', 'abstract', 'interface', 'associate', 'block']
        # keywords that break up indentation blocks
        self.noIndentList = ['contains', 'case', 'else', 'class is', 'type is',
                             'class default', 'type default']
        # keywords that end indentation blocks
        self.endIndentList = ['end', 'endif', 'enddo']
        # max line length
        self.maxLength = 80
        # require continutation characters at maxLength+1
        self.enforceContCharAtMax = True
        # fixed or free format
        self.fixedFormat = False
        # list of keywords for capitalization
        self.keywordList = \
            ['subroutine', 'program', 'function', 'do', 'while', 'case',
             'module', 'type', 'procedure', 'intent', 'real', 'block',
             'complex', 'integer', 'logical', 'dimension',
             'allocatable', 'target', 'pointer', 'contiguous',
             'pass', 'private', 'public', 'implicit', 'none', 'use',
             'end', 'protected', 'class', 'optional', 'allocate',
             'allocated', 'associated', 'then', 'select', 'present',
             'in', 'out', 'inout', '.and.', '.or.', '.neq.', '.gt.',
             '.lt.', 'abstract', 'character', 'pass', 'deferred',
             'procedure', 'interface', 'import', 'only', 'generic',
             'default', 'pure', 'elemental', 'associate']
        # blank line keywords (next line)
        self.blankLineKeywords = \
            ['end interface', 'end type', 'implicit none',
             'private', 'public', 'contains', 'end module',
             'end subroutine', 'end function', 'end block']
        # list of keywords functions that contain declaration blocks
        self.functionKeywords = ['subroutine', 'program', 'function']
        # list of keywords for variable declaration blocks
        self.variableKeywords = \
            ['real', 'double', 'complex', 'integer', 'logical',
             'character', 'type', 'class', 'procedure']
        # continuation character
        self.contChar = "&"
        # comment line
        self.commentLine = '!---------------------------------------' +\
                           '----------------------------------------'
        # internal state variables
        self.fileName = "None"
        self.interactive = False
        self.lineNumber = 0
        self.currentIndent = 0

    def initCounters(self, fileName):
        ''' initialize counters '''
        self.fileName = fileName
        self.currentIndent = self.initIndent
        self.inCommentBlock = False
        self.blankLine = False
        self.lineNumber = 0
        self.contLine = False
        self.inCommentBlock = False
        self.inSelectBlock = False
        self.inFunction = False
        self.functionStart = False
        self.inInterface = False
        self.inType = False
        self.procedures = []
        self.procedureContLine = False
        self.deferProcedures = []
        self.deferProcedure = False
        self.inCode = False
        self.inVariableIntentBlock = False
        self.inVariableDeclarationBlock = False
        self.foundImplicitNone = False
        self.foundVariableIntentBlock = False
        self.foundVariableDeclarationBlock = False
        self.variableDeclarationBlockComment = False
        self.multiLineIf = False
        self.ifdef = 0
        self.found_else = False

    def printMsg(self, msg, line):
        strLineNumber = "%5d" % self.lineNumber
        print(self.fileName+":"+strLineNumber+msg)
        if line is not None:
            print(line[0:-1])
        if self.interactive:
            tf = open('/tmp/formatErrorMessage.tmp', 'w')
            tf.write(self.fileName+":"+strLineNumber+msg+'\n')
            if line is not None:
                tf.write(line[0:-1])
            tf.write('\nTo exit type after the colon:')
            tf.flush()
            tf.close()
            call(['vim', '-o', '+'+strLineNumber, self.fileName, tf.name])
            # Update line
            if line is not None:
                infile = open(self.fileName, "r")
                for ii in range(self.lineNumber-1):
                    infile.next()
                line = infile.next()
                infile.close()
            # Check to exit
            tf = open('/tmp/formatErrorMessage.tmp', 'r')
            for tline in tf:
                if (tline[0:29] == 'To exit type after the colon:'):
                    if (len(tline.strip()) > 29):
                        exit()

    def checkLength(self, line):
        ''' check the line length '''
        if not self.interactive:
            if len(line) == 1:
                if self.blankLine:
                    self.blankLine = False
                elif self.inVariableIntentBlock and \
                        not self.foundVariableIntentBlock:
                    self.foundVariableIntentBlock = True
                    self.inVariableIntentBlock = False
                elif self.inVariableDeclarationBlock and \
                        not self.foundVariableDeclarationBlock or \
                        self.variableDeclarationBlockComment:
                    self.foundVariableDeclarationBlock = True
                    self.inVariableDeclarationBlock = False
                else:
                    self.printMsg(" extra blank line", None)
            if self.blankLine and not self.contLine:
                if not (self.inInterface
                        and line.strip().lower().startswith('end interface')):
                    self.printMsg(" missing expected blank line", None)
                self.blankLine = False
        if len(line.strip()) > 1 and line[-2] == " ":
            self.printMsg(" extra whitespace", None)
        lineMaxLength = self.maxLength
        if len(line.strip()) > 1 and line[-2] == self.contChar:
            lineMaxLength += 1
        if len(line.strip()) > lineMaxLength:
            self.printMsg(" line length of " + str(len(line.strip())) +
                          " exceeds max length of " + str(lineMaxLength), line)

    def checkIndentation(self, line):
        ''' check indentation '''
        if (line[0] == '#' or
                line[0:self.maxLength] == self.commentLine[0:self.maxLength]):
            return
        else:
            words = line.split()
            if len(words) == 0:
                return
            nindent = 0
            if self.inCommentBlock:
                if len(line.strip()) < 2:
                    return  # blank line
                # include FORD characters
                while line[nindent] == " " or line[nindent] == ">" or \
                        line[nindent] == "<" or line[nindent] == "!" or \
                        line[nindent] == "*" or line[nindent] == ":":
                    nindent += 1
            else:
                while line[nindent] == " ":
                    nindent += 1
            if not self.inCommentBlock:
                lword = words[0].lower()
                lword1 = ""
                if (len(words) > 1):
                    if (lword[-1] == ":"):
                        lword = words[1].lower()
                    else:
                        lword1 = words[1].lower()
                # Check for keywords without indention
                noIndent = 0
                for niwords in self.noIndentList:
                    niwordsList = niwords.split()
                    niword = niwordsList[0]
                    if lword[0:len(niword)] == niword:
                        if len(niwordsList) > 1:
                            if len(words) >= len(niwordsList) \
                                    and lword == niword:
                                ii = 1
                                allMatch = True
                                for niword in niwordsList[1:]:
                                    if ii+1 == len(niwordsList):
                                        if words[ii].lower()[0:len(niword)] \
                                                != niword:
                                            allMatch = False
                                    else:
                                        if words[ii].lower() != niword:
                                            allMatch = False
                                    ii += 1
                                if allMatch:
                                    noIndent = 2
                        else:
                            noIndent = 2
                # Check for keyword that reduces indentation
                if lword in self.endIndentList:
                    self.currentIndent -= self.indentLength
                # Check if we just started a function and increase indentation
                if self.functionStart:
                    self.currentIndent += self.indentLength
                    self.functionStart = False
                # Check current indentation
                if not self.contLine:
                    if nindent != self.currentIndent-noIndent:
                        self.printMsg(" incorrect indentation of "
                                      + str(nindent) + " expected "
                                      + str(self.currentIndent-noIndent), line)
                elif nindent < self.currentIndent + 2:
                    self.printMsg(" incorrect continuation line indentation of"
                                  + " " + str(nindent) + " expected at least "
                                  + str(self.currentIndent+4), line)
                if "(" in lword:
                    index = lword.find("(")
                    tmpword = lword[0:index]
                    if tmpword in ["if", "case", "associate"]:
                        lword = tmpword
                # Check for keyword that increases indentation
                if self.multiLineIf:
                    if words[-1].lower() == "then":
                        self.multiLineIf = False
                        self.currentIndent += self.indentLength
                    elif not (len(line.strip()) > 1
                              and line[-2] == self.contChar):
                        self.multiLineIf = False
                    return
                elif lword in self.indentList:
                    # if statements are complicated
                    if lword == "if" and words[-1].lower() != "then":
                        if len(line.strip()) > 1 and line[-2] == self.contChar:
                            self.multiLineIf = True
                        return
                    if lword == "type" and lword1[0:2] == "is":
                        return
                    if lword == "module" and lword1[0:9] == "procedure":
                        return
                    self.currentIndent += self.indentLength
            else:  # in a comment block
                # Don't enforce small indentation as it is too complicated
                if self.currentIndent < 3:
                    return
                if nindent != self.currentIndent:
                    self.printMsg(" incorrect indentation of " + str(nindent)
                                  + " expected " + str(self.currentIndent),
                                  line)

    def checkCommentBlock(self, line):
        ''' check comment block '''
        if (not self.inCommentBlock):
            if (line[0] == '!'):
                if (line[0:self.maxLength]
                        != self.commentLine[0:self.maxLength]
                        or len(line.strip()) < self.maxLength):
                    self.printMsg(" incorrect comment block start", line)
                else:
                    self.inCommentBlock = True
                    self.commentBlockEnd = False
        else:
            if (line[0:self.maxLength] == self.commentLine[0:self.maxLength]):
                self.commentBlockEnd = True
                self.inCommentBlock = False
            elif (line[0] == '!'):
                self.commentBlockEnd = False
            elif (not self.commentBlockEnd):
                self.printMsg(" unexpected end to comment block", line)
                self.inCommentBlock = False

    def checkKeywords(self, line):
        ''' check keyword format '''
        # Remove comments for keyword checks
        commentline = line.split("!")
        commentline = commentline[0]
        if len(commentline) == 0:
            commentline = "!"
        # Remove strings for keyword checks
        stringline = commentline.replace("\"", "\'").split("\'")
        if commentline[0] == "\"" or commentline[0] == "\'":
            stripline = stringline[1::2]
        else:
            stripline = stringline[0::2]
        firstFragment = True
        for fragment in stripline:
            words = fragment.split()
            lwords = []
            for word in words:
                lword = word.lower()
                lwords.append(lword)
                for keyword in self.keywordList:
                    if lword == keyword:
                        if word != keyword.upper():
                            self.printMsg(" incorrect case in " + word, line)
                    elif keyword + "(" in lword:
                        ind = lword.find(keyword)
                        if ind > 0:
                            continue
                        kword = word[ind:ind + len(keyword)]
                        if kword != keyword.upper():
                            self.printMsg(" incorrect case in "+kword, line)
            # general checks
            if firstFragment and not self.interactive and len(words) != 0:
                firstFragment = False
                # check if we are in a type block
                if not self.inType:
                    if lwords[0] == "type" and len(lwords) > 1 \
                            and lwords[1] == "::" or lwords[0] == "type,":
                        self.inType = True
                else:
                    if lwords[0] == "end":
                        if len(lwords) > 1 and lwords[1] == "type":
                            self.inType = False
                    # ignore ifdef function definitions
                    if self.ifdef > 0:
                        continue
                    elif lwords[0] == "procedure" \
                            or lwords[0] == "procedure," \
                            or lwords[0].startswith("procedure(") \
                            and "pointer" not in lwords \
                            and "pointer," not in lwords:
                        if "deferred" in lwords:
                            self.deferProcedure = True
                        if lwords[0].startswith("procedure("):
                            ftnName = words[0].split('(')[1].split(')')[0]
                            if self.deferProcedure:
                                self.deferProcedures.append(ftnName)
                                self.deferProcedure = False
                            else:
                                self.procedures.append(ftnName)
                        elif words[-1].endswith("&"):
                            self.procedureContLine = True
                        else:
                            if self.deferProcedure:
                                self.deferProcedures.append(words[-1])
                                self.deferProcedure = False
                            else:
                                self.procedures.append(words[-1])
                    elif self.procedureContLine:
                        if "deferred" in lwords:
                            self.deferProcedure = True
                        if not words[-1].endswith("&"):
                            if self.deferProcedure:
                                self.deferProcedures.append(words[-1])
                                self.deferProcedure = False
                            else:
                                self.procedures.append(words[-1])
                            self.procedureContLine = False
                # check for function definitions and order relative
                # to procedure declaration order
                for fkw in self.functionKeywords:
                    if self.procedures and fkw in lwords:
                        if lwords[0] != 'end':
                            kwind = lwords.index(fkw)
                            ftnName = words[kwind + 1].split('(')[0]
                            if not self.deferProcedures and \
                                    ftnName == self.procedures[0]:
                                # found it in order
                                print("Found "+self.procedures[0])
                                self.procedures.pop(0)
                            elif self.deferProcedures and \
                                    ftnName == self.deferProcedures[0]:
                                # found it in order
                                print("Found "+self.deferProcedures[0])
                                self.deferProcedures.pop(0)
                            elif ftnName in \
                                    self.deferProcedures + self.procedures:
                                self.printMsg(" Expected "
                                              + (self.deferProcedures
                                                  + self.procedures)[0]
                                              + " before " + ftnName + " based"
                                              + " on ordering specified in"
                                              + " type", line)
                                try:
                                    self.procedures.remove(ftnName)
                                except Exception:
                                    pass
                                try:
                                    self.deferProcedures.remove(ftnName)
                                except Exception:
                                    pass
                # check if we are in an interface block
                if not self.inInterface:
                    if lwords[0] == "interface" or len(lwords) > 1 \
                            and lwords[0] == "abstract" \
                            and lwords[1] == "interface":
                        self.inInterface = True
                else:
                    if lwords[0] == "end":
                        if len(lwords) > 1 and lwords[1] == "interface":
                            self.inInterface = False
                    else:
                        for fkw in self.functionKeywords:
                            if fkw in lwords:
                                self.functionStart = True
                # Checks for the need for a blank line
                for blkw in self.blankLineKeywords:
                    blwords = blkw.split()
                    if len(words) < len(blwords):
                        continue
                    allWordsMatch = True
                    for ii in range(len(blwords)):
                        if lwords[ii] != blwords[ii]:
                            allWordsMatch = False
                    if allWordsMatch:
                        self.blankLine = True
                if self.inInterface:
                    continue
                # check for proper spacing of the variable declaration blocks
                if self.inFunction and not self.contLine and not self.inCode:
                    foundVariableKeyword = False
                    if len(words) == 2:
                        if lwords[0] == "implicit" and lwords[1] == "none":
                            self.foundImplicitNone = True
                    for vkw in self.variableKeywords:
                        if len(lwords[0]) >= len(vkw) and \
                             lwords[0][0:len(vkw)] == vkw:
                            if len(lwords) > 1 and \
                                    (lwords[1] == "is"
                                     or lwords[1] == "default"):
                                continue
                            foundVariableKeyword = True
                            lower_line = line.lower()
                            if "intent" in lower_line:
                                self.inVariableIntentBlock = True
                                if self.foundVariableIntentBlock:
                                    self.printMsg(" Extra space in intent"
                                                  + " variable block", None)
                                if self.inVariableDeclarationBlock:
                                    self.printMsg(" Variable without intent "
                                                  + "before intent block:\n"
                                                  + "reorder or add intent to"
                                                  + " passed variables",
                                                  None)
                            elif "procedure" in lower_line:
                                # no intent needed, do nothing
                                continue
                            else:
                                if line[-2] == self.contChar:
                                    # expect intent on next line
                                    continue
                                self.inVariableDeclarationBlock = True
                                if self.foundVariableDeclarationBlock and not \
                                        self.variableDeclarationBlockComment:
                                    self.printMsg(" Extra space in variable "
                                                  + "declaration block", None)
                                    self.inVariableDeclarationBlock = False
                                if self.inVariableIntentBlock:
                                    self.printMsg(" Expected space after "
                                                  + "intent variable block",
                                                  None)
                                    self.inVariableIntentBlock = False
                    if not foundVariableKeyword:
                        if self.variableDeclarationBlockComment and \
                                len(words[0]) > 0 and words[0][0] == '!':
                            continue
                        elif len(words[0]) > 0 and words[0][0] == '!':
                            if line[0:self.maxLength] \
                                    == self.commentLine[0:self.maxLength]:
                                if self.inVariableIntentBlock:
                                    self.foundVariableIntentBlock = True
                                    self.inVariableIntentBlock = False
                                elif self.inVariableDeclarationBlock:
                                    self.foundVariableDeclarationBlock = True
                                    self.variableDeclarationBlockComment = True
                        elif self.inVariableIntentBlock:
                            self.printMsg(" Expected space or comment after " +
                                          "intent variable block", None)
                            self.inVariableIntentBlock = False
                        elif self.inVariableDeclarationBlock and not \
                                self.variableDeclarationBlockComment:
                            self.printMsg(" Expected space or comment after " +
                                          "variable block", None)
                            self.inVariableDeclarationBlock = False
                        elif self.foundVariableDeclarationBlock:
                            self.inCode = True
                            self.inVariableDeclarationBlock = False
                            self.variableDeclarationBlockComment = False
                            if not self.foundImplicitNone:
                                self.printMsg(" Missing implicit none", None)
                # If we just entered a function reset the variable declaration
                # block counters
                if not self.inFunction:
                    if len(lwords) > 1 and not lwords[0] == "end":
                        for fkw in self.functionKeywords:
                            if fkw in lwords:
                                self.inFunction = True
                                self.foundImplicitNone = False
                                self.foundVariableIntentBlock = False
                                self.foundVariableDeclarationBlock = False
                                self.variableDeclarationBlockComment = False
                                self.inCode = False
                                self.functionStart = True
                else:
                    for fkw in self.functionKeywords:
                        if (len(lwords) > 1 and lwords[0] == "end"
                                and lwords[1] == fkw) \
                                or lwords[0] == "contains":
                            self.inFunction = False

    def checkContinuation(self, line):
        ''' check continuation character is in the correct column
            also mark contLine for the next line -> call last
        '''
        if (len(line.strip()) > 0 and line.strip()[0] == '!'):
            if not (len(line.strip()) > 4 and line.strip()[0:5] == '!$acc'):
                return
        if self.enforceContCharAtMax:
            if len(line.strip()) > 1 and line[-2] == self.contChar:
                if not (len(line.strip()) > 4 and
                        line.strip()[0:5] == '!$acc'):
                    self.contLine = True
                if len(line) != (self.maxLength+2):
                    self.printMsg(" continuation character at column "
                                  + str(len(line) - 1) + " out of place", line)
            else:
                self.contLine = False

    def checkArrayBrackets(self, line):
        if "(/" in line:
            self.printMsg(" use of (/ /) should be replaced with [ ]"
                          + " for array literals", line)

    def checkTabs(self, line):
        if "\t" in line:
            self.printMsg(" remove tab", line)


def checkFile(fileName, myStyle, do_init=True):
    ''' check a file according to formatStyle '''
    infile = open(fileName, "r")
    if do_init:
        myStyle.initCounters(fileName)
    for line in infile:
        myStyle.lineNumber += 1
        if (len(line.strip()) > 0 and line.strip()[0] == '#'):
            words = line.split()
            # parse include files with recursion
            if (words[0] == "#include" and words[1] != '"config.f"'):
                path = os.path.dirname(os.path.relpath(fileName))
                includeFileName = path + '/' + words[1].strip('\"')
                print(includeFileName)
                saveLineNumber = myStyle.lineNumber
                myStyle.lineNumber = 0
                myStyle.fileName = includeFileName
                checkFile(includeFileName, myStyle, do_init=False)
                myStyle.lineNumber = saveLineNumber
                myStyle.fileName = fileName
            # count ifdef level
            if words[0] == "#ifdef" or \
               words[0] == "#if" and words[1].startswith("defined"):
                myStyle.ifdef += 1
                myStyle.found_else = False
            if myStyle.ifdef > 0:
                if words[0] == "#else":
                    myStyle.ifdef += -1
                    myStyle.found_else = True
                elif words[0] == "#endif" and not myStyle.found_else:
                    myStyle.ifdef += -1
            continue  # ignore other preprocessor statements
        myStyle.checkLength(line)
        myStyle.checkCommentBlock(line)
        myStyle.checkIndentation(line)
        myStyle.checkKeywords(line)
        myStyle.checkArrayBrackets(line)
        myStyle.checkTabs(line)
        # call this last
        myStyle.checkContinuation(line)
    infile.close()


def main():
    parser = optparse.OptionParser(usage='%prog [options] file.f')
    parser.add_option('-f', '--format', dest='format', default=False,
                      action='store_true', help='Autoformat file')
    parser.add_option('-l', '--legacy', dest='legacy', default=False,
                      action='store_true', help='Use legacy format')
    parser.add_option('-i', '--interactive', dest='interactive',
                      default=False, action='store_true',
                      help='Enable interactive mode (requires vim) \n'
                      + 'This mode disables blank line checking')
    options, args = parser.parse_args()
    if len(args) == 1:
        fileName = args[0]
    else:
        parser.print_usage()
        return

    myStyle = formatStyle()
    if options.legacy:
        myStyle.maxLength = 72
        myStyle.initIndent = 7

    myStyle.interactive = options.interactive

    checkFile(fileName, myStyle)
    return


if __name__ == "__main__":
    main()
