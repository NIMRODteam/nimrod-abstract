######################################################################
#
# Define testing functions addNimUnitTest and addNimTest
#
######################################################################

message(STATUS "")
message(STATUS "--------- Adding tests ---------")

enable_testing()

# Functions for actually invoking the tests
include(CMakeParseArguments)

if (ENABLE_OPENACC) 
  message(STATUS "Using compute-sanitizer for CTEST_MEMORYCHECK_COMMAND")
  find_program(MEMORYCHECK_COMMAND compute-sanitizer)
  set(MEMORYCHECK_TYPE CudaSanitizer)
  set(MEMORYCHECK_COMMAND_OPTIONS "--require-cuda-init=no --error-exitcode=1 --target-processes=all")
  #string(APPEND MEMORYCHECK_COMMAND_OPTIONS " --leak-check=full --tool=memcheck")
  string(APPEND MEMORYCHECK_COMMAND_OPTIONS " --tool=racecheck")
  #string(APPEND MEMORYCHECK_COMMAND_OPTIONS " --tool=initcheck")
else ()
  message(STATUS "Using valgrind for CTEST_MEMORYCHECK_COMMAND")
  find_program(MEMORYCHECK_COMMAND valgrind)
  set(MEMORYCHECK_TYPE Valgrind)
  set(MEMORYCHECK_COMMAND_OPTIONS "--error-exitcode=1 --trace-children=yes --leak-check=full --track-origins=yes")
  #string(APPEND MEMORYCHECK_COMMAND_OPTIONS " --gen-suppressions=all")
  set(MEMORYCHECK_SUPPRESSIONS_FILE "${PROJECT_SOURCE_DIR}/nimtests/suppressions.txt"
      CACHE FILEPATH "Memcheck suppression file")
endif ()

include(CTest)

set(NIM_TIMEOUT 720)

# useful make target
add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND}
      --force-new-ctest-process --timeout ${NIM_TIMEOUT} --output-on-failure)

if (ENABLE_CODE_COVERAGE)
  include(nimtests/CodeCoverage.cmake)
  append_coverage_compiler_flags()
  setup_target_for_coverage_fastcov(
    NAME ctest_coverage
    EXCLUDE extlibs nimtests
    EXECUTABLE ctest j4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
  )
endif ()

#------------------------------------------------------------------------------
# addNimUnitTest
# 
# Add source file and test types/args of contained tests.
# Expect more than one test per executable to limit linking steps
# 
# Args:
#   EXEC:         Executable name (supercedes SOURCEFILES)
#   LABELS:       Test labels
#   LINK_LIBS:    Libraries the test needs
#   NLAYERS:      Number of layers to use (if needed)
#   NMODES:       Number of modes to use (if needed)
#   RMFILES:      Files to remove before running
#   SOURCEFILES:  List of source files, first file determines exec name
#   TEST_TYPE:    Test type list
#   TESTARGS:     Test argument list for each test type
#   USE_MPI:      Run the test with mpi (if built with MPI)
#------------------------------------------------------------------------------

function(addNimUnitTest)
  # Parse out the args
  set(opts DEBUG USE_MPI) # no-value args
  set(oneValArgs EXEC)
  set(multValArgs SOURCEFILES LINK_LIBS NLAYERS NMODES TEST_TYPE TESTARGS LABELS RMFILES) # e.g., lists
  cmake_parse_arguments(FD "${opts}" "${oneValArgs}" "${multValArgs}" ${ARGN})

  if (FD_EXEC)
    set(EXEC ${FD_EXEC})
  else ()
    list(GET FD_SOURCEFILES 0 sfile)  # First file determines names
    get_filename_component(sname ${sfile} NAME_WE)
    set(EXEC ${sname})
    list(APPEND FD_LINK_LIBS nimutest)
    message(STATUS "Adding executable ${EXEC} from ${FD_SOURCEFILES}")
    add_executable(${EXEC} ${FD_SOURCEFILES})
    target_link_libraries(${EXEC} PUBLIC ${FD_LINK_LIBS})
    cmake_path(GET CMAKE_CURRENT_SOURCE_DIR PARENT_PATH parent_path)
    cmake_path(GET parent_path STEM dir_name)
    install(TARGETS ${EXEC} 
      DESTINATION bin/unit-test/${dir_name}
      PERMISSIONS OWNER_READ OWNER_EXECUTE OWNER_WRITE
      GROUP_READ GROUP_EXECUTE GROUP_WRITE 
      WORLD_READ WORLD_EXECUTE)
  endif ()

  if (FD_RMFILES)
    set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                 PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${FD_RMFILES})
  endif ()

  get_filename_component(srcDir "${CMAKE_CURRENT_SOURCE_DIR}/.." ABSOLUTE)
  get_filename_component(defaultLabel "${srcDir}" NAME)
  if (NOT FD_LABELS)
    set(LABELS ${EXEC})
  else ()
    set(LABELS ${FD_LABELS})
  endif ()
  message(STATUS "  Labeling tests: ${defaultLabel} ${LABELS}")

  if (FD_TESTARGS)
    if (FD_TEST_TYPE)
      if (FD_NMODES AND FD_NLAYERS)
        foreach (arg ${FD_TESTARGS})
          foreach (type ${FD_TEST_TYPE})
	    foreach (nmodes ${FD_NMODES})
	      foreach (nlayers ${FD_NLAYERS})
		if (nmodes GREATER_EQUAL nlayers)
                  list(APPEND TESTARGS "${type} ${arg} ${nmodes} ${nlayers}")
		endif ()
              endforeach ()
            endforeach ()
          endforeach ()
        endforeach ()
      else ()
        foreach (arg ${FD_TESTARGS})
          foreach (type ${FD_TEST_TYPE})
            list(APPEND TESTARGS "${type} ${arg}")
          endforeach ()
        endforeach ()
      endif ()
    else ()
      set(TESTARGS ${FD_TESTARGS})
    endif ()
  else ()
    set(TESTARGS default)
  endif ()

  foreach (arg ${TESTARGS})
    if ("${arg}" STREQUAL "default")
      set(TESTNAME ${EXEC})
      set(ARGS "") 
    else ()
      set(TESTNAME0 ${EXEC}_${arg})
      string(REPLACE " " "_" TESTNAME ${TESTNAME0})
      string(REPLACE " " ";" ARGS ${arg})
    endif ()
    if (FD_USE_MPI AND MPI_FOUND)
      set(TESTNAME "mpi${TESTNAME}")
    endif ()

    message(STATUS "  Adding ${TESTNAME}")

    if (MPI_FOUND AND FD_USE_MPI)
      add_test(NAME ${TESTNAME}
	COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${MPI_TEST_NPROCS} $<TARGET_FILE:${EXEC}> ${ARGS})
    elseif (MPI_FOUND AND ${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
      # nvhpc requires a mpiexec because all programs are linked to mpi,
      # see nimlib/CMakeLists.txt
      add_test(NAME ${TESTNAME}
	COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} 1 $<TARGET_FILE:${EXEC}> ${ARGS})
    else ()
      add_test(NAME ${TESTNAME}
        COMMAND ${EXEC} ${ARGS})
    endif ()

    set_tests_properties(${TESTNAME} PROPERTIES 
      TIMEOUT ${NIM_TIMEOUT} 
      FAIL_REGULAR_EXPRESSION "not ok;ERROR"
      LABELS "${defaultLabel} ${LABELS}")
  endforeach ()
endfunction()

#----------------------------------------------------------------
# addNimTest
# 
# Add executable and test args of contained tests.
# The preprocessor if present will be run once.
# Expect more than one test per executable to limit linking steps.
# 
# Args:
#   CPFILES:      Files to copy into test directory
#   EXEC:         Executable
#   LABELS:       Test labels
#   PREPROC:      Preprocessor to run before tests
#   PREPROCARGS:  Arguments to the preprocessor
#   RMFILES:      Files to remove before running
#   TESTARGS:     Test argument as a list
#   USE_MPI:      Run the test with mpi (if built with MPI)
#------------------------------------------------------------------------------

function(addNimTest)
  set(opts DEBUG USE_MPI) # no-value args
  set(oneValArgs EXEC PREPROC)
  set(multValArgs CPFILES TESTARGS PREPROCARGS LABELS RMFILES) # e.g., lists
  cmake_parse_arguments(FD "${opts}" "${oneValArgs}" "${multValArgs}" ${ARGN})

  set(EXEC ${FD_EXEC})
  message(STATUS "Adding example executable ${EXEC}")

  if (FD_RMFILES)
    set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                 PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${FD_RMFILES})
  endif ()

  if (FD_CPFILES)
    foreach (file ${FD_CPFILES})
      file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/${file}
           DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
    endforeach ()
  endif ()

  get_filename_component(srcDir "${CMAKE_CURRENT_SOURCE_DIR}/.." ABSOLUTE)
  get_filename_component(defaultLabel "${srcDir}" NAME)
  if (NOT FD_LABELS)
    set(LABELS ${EXEC})
  else ()
    set(LABELS ${FD_LABELS})
  endif ()
  message(STATUS "  Labeling tests: ${EXEC} ${LABELS}")

  if (FD_TESTARGS)
    set(TESTARGS ${FD_TESTARGS})
  else ()
    set(TESTARGS default)
  endif ()

  if (FD_PREPROC)
    if (FD_PREPROCARGS)
      set(TESTNAME0 ${FD_PREPROC}_${FD_PREPROCARGS})
      string(REPLACE " " "_" PREPROCNAME ${TESTNAME0})
    else ()
      set(PREPROCNAME ${FD_PREPROC})
    endif ()
    message(STATUS "  Adding ${PREPROCNAME}")
    string(REPLACE " " ";" PREPROCARGS ${FD_PREPROCARGS})
    if (MPI_FOUND AND ${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
      # nvhpc requires a mpiexec because all programs are linked to mpi,
      # see nimlib/CMakeLists.txt
      add_test(NAME ${PREPROCNAME}
        COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} 1 $<TARGET_FILE:${FD_PREPROC}> ${PREPROCARGS})
    else ()
      add_test(NAME ${PREPROCNAME}
        COMMAND ${FD_PREPROC} ${PREPROCARGS})
    endif ()
    set_tests_properties(${PREPROCNAME} PROPERTIES 
      TIMEOUT ${NIM_TIMEOUT} 
      FAIL_REGULAR_EXPRESSION "not ok;ERROR"
      LABELS "${EXEC} ${LABELS}")
  endif ()

  foreach (arg ${TESTARGS})
    if ("${arg}" STREQUAL "default")
      set(TESTNAME ${EXEC})
      set(ARGS "") 
    else ()
      set(TESTNAME0 ${EXEC}_${arg})
      string(REPLACE " " "_" TESTNAME ${TESTNAME0})
      string(REPLACE " " ";" ARGS ${arg})
    endif ()
    if (FD_USE_MPI AND MPI_FOUND)
      set(TESTNAME "mpi${TESTNAME}")
    endif ()

    message(STATUS "  Adding ${TESTNAME}")

    if (FD_USE_MPI AND MPI_FOUND)
      add_test(NAME ${TESTNAME}
	COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${MPI_TEST_NPROCS} $<TARGET_FILE:${EXEC}> ${ARGS})
    elseif (MPI_FOUND AND ${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
      # nvhpc requires a mpiexec because all programs are linked to mpi,
      # see nimlib/CMakeLists.txt
      add_test(NAME ${TESTNAME}
	COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} 1 $<TARGET_FILE:${EXEC}> ${ARGS})
    else ()
      add_test(NAME ${TESTNAME}
        COMMAND ${EXEC} ${ARGS})
    endif ()

    set_tests_properties(${TESTNAME} PROPERTIES 
      DEPENDS ${PREPROCNAME}
      TIMEOUT ${NIM_TIMEOUT} 
      FAIL_REGULAR_EXPRESSION "not ok;ERROR"
      LABELS "${EXEC} ${LABELS}")
  endforeach ()
endfunction()
