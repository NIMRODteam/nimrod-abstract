#!/usr/bin/env python3
import re
import shlex


class process_namelist:
    '''
    Provides tools to process NIMROD namelist files
    '''
    _maxc = 80  # max columns for output code
    _comment_line = '!-----------------------------------------------' \
                    + '--------------------------------\n'
    _typename = ''  # name of type that contains the namelist

    @staticmethod
    def getNamelistDict(infile):
        '''
        Get a namelist from a Fortran input file.
        Expect formatting where
        '!:  namelist my_namelist_name'
        defines namelist blocks and
        '!:  computed'
        defines computed variables that are not in the namelist
        :param infile: input file name (and path)
        :return: namelist as a dictionary,
                 input_dict[nmlname][varname] = [vartype,arrvar]
        '''
        input_dict = {}
        nmlname = None
        with open(infile, 'r') as f:
            for line in f.readlines():
                # print(line)
                line = line.strip()
                if line.lower().startswith(('end','contains','!: computed',
                                            '!: computed')):
                    break
                # expect namelist comment header of
                # !!    namelist my_namelist_name
                if line.lower().startswith(('!: namelist','!:  namelist')):
                    nmlname = line.split()[-1]
                    input_dict[nmlname] = {}
                if line.lower().startswith(('character', 'real', 'integer',
                                            'logical', 'complex')):
                    if 'parameter' in line.lower():
                        continue
                    line = line.split('!')[0].strip()  # remove comments
                    line = shlex.split(line)
                    line = [list(filter(None, re.split(r'=|\(|\)', istr)))
                            for istr in line]
                    line = [istr for ilst in line for istr in ilst]
                    ind = line.index('::')
                    varname = line[ind+1]
                    vartype = line[0]
                    if vartype.lower().startswith(('character', 'real',
                                                   'integer', 'complex')):
                        vartype += '(' + line[1] + ')'
                    arrvar = 0
                    if vartype.lower().startswith('character'):
                        arrvar = 2
                    if line[3].lower().startswith('dimension') \
                       or ind+4 == len(line):  # for implied dimension
                        if arrvar == 2:  # character array
                            arrvar = 3
                        else:
                            arrvar = 1
                    if ind+4 == len(line):  # for implied dimension
                        arrvar = 1
                    input_dict[nmlname][varname] = [vartype, arrvar]
        return input_dict

    @staticmethod
    def getNamelistBlock(input_dict):
        '''
        Return a string with the NAMELIST block code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            line = '    NAMELIST/' + nml + '/'
            for varname_uq, varinfo in vardict.items():
                if len(line)+len(varname_uq)+1 <= process_namelist._maxc:
                    line = line + varname_uq + ','
                else:
                    while len(line) < process_namelist._maxc:
                        line = line + ' '
                    line = line+'&'
                    code += line + '\n'
                    line = '          ' + varname_uq + ','
            code += line[:-1] + '\n'
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistBcast(input_dict):
        '''
        Return a string with the NAMELIST bcast code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            code += process_namelist._comment_line
            code += '!   ' + nml + '\n'
            code += process_namelist._comment_line
            for varname_uq, varinfo in vardict.items():
                varname = process_namelist._typename + varname_uq
                if varinfo[1] == 1:  # arrays
                    code += '    nn=SIZE(' + varname + ')\n'
                    varlen = 'nn'
                elif varinfo[1] == 2:  # characters
                    code += '    nn=LEN(' + varname + ')\n'
                    varlen = 'nn'
                elif varinfo[1] == 3:  # character arrays
                    code += '    nn=LEN(' + varname + ')*SIZE(' \
                                          + varname + ')\n'
                    varlen = 'nn'
                else:
                    varlen = '1'
                if varlen == '1':
                    code += '    CALL par%all_bcast(' + varname + ',0)\n'
                else:
                    code += '    CALL par%all_bcast(' + varname + ',' \
                                                      + varlen + ',0)\n'
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistWrite(input_dict):
        '''
        Return a string with the NAMELIST write code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            for varname_uq, varinfo in vardict.items():
                varname = process_namelist._typename + varname_uq
                line = '    CALL write_attribute(gid,\"' + varname + '\",'
                if len(line)+len(varname)+12 <= process_namelist._maxc:
                    line = line + varname + ',h5in,h5err)\n'
                else:
                    while len(line) < process_namelist._maxc:
                        line = line + ' '
                    line = line+'&'
                    code += line + '\n'
                    line = '                         ' \
                           + varname + ',h5in,h5err)\n'
                code += line
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistRead(input_dict):
        '''
        Return a string with the NAMELIST read code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            for varname_uq, varinfo in vardict.items():
                varname = process_namelist._typename + varname_uq
                line = '    CALL read_attribute(gid,\"' + varname + '\",'
                if len(line)+len(varname)+12 <= process_namelist._maxc:
                    line = line + varname + ',h5in,h5err)\n'
                else:
                    while len(line) < process_namelist._maxc:
                        line = line + ' '
                    line = line+'&'
                    code += line + '\n'
                    line = '                        ' \
                           + varname + ',h5in,h5err)\n'
                code += line
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistFileRead(input_dict):
        '''
        Return a string with the NAMELIST read from file
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            code += '    CALL position_at_namelist(\''+nml+'\', read_stat)\n'
            code += '    IF (read_stat==0) THEN\n'
            code += '      READ(UNIT=in_unit,NML='+nml+',IOSTAT=read_stat)\n'
            code += '      IF (read_stat/=0) THEN\n'
            code += '        CALL print_namelist_error(\''+nml+'\')\n'
            code += '      ENDIF\n'
            code += '    ENDIF\n'
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistCopy(input_dict):
        '''
        Return a string with the NAMELIST copy code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            for varname_uq, varinfo in vardict.items():
                varname = process_namelist._typename + varname_uq
                code += '    ' + varname + '=' + varname_uq + '\n'
        code += process_namelist._comment_line
        return code

    @staticmethod
    def getNamelistType(input_dict):
        '''
        Return a string with the NAMELIST type code
        :param: namelist as a dictionary
        :return: string of code
        '''
        code = process_namelist._comment_line
        for nml, vardict in input_dict.items():
            for varname_uq, varinfo in vardict.items():
                # TODO: handle arrays
                code += '    ' + varinfo[0] + ' :: ' + varname_uq + '\n'
        code += process_namelist._comment_line
        return code

    @staticmethod
    def updateNamelist(infile, typename=None):
        '''
        Generate namelist code and update the input file
        :param infile: input file name (and path)
        :param typename: if namelist is contained within a type, the name of
            the type within functions
        '''
        if typename:
            process_namelist._typename = typename + '%'  # index into type
        else:
            process_namelist._typename = ''
        input_dict = process_namelist.getNamelistDict(infile)
        code1 = process_namelist.getNamelistBlock(input_dict)
        code2 = process_namelist.getNamelistBcast(input_dict)
        code3 = process_namelist.getNamelistWrite(input_dict)
        code4 = process_namelist.getNamelistRead(input_dict)
        code5 = process_namelist.getNamelistFileRead(input_dict)
        code6 = process_namelist.getNamelistCopy(input_dict)
        code7 = process_namelist.getNamelistType(input_dict)

        sbegin = '!   BEGIN GENERATED NAMELIST '
        send = '!   END GENERATED NAMELIST '
        ctype = 'BLOCK'
        chop1 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code1 = sbegin + ctype + '\n' + code1 + send + ctype
        ctype = 'BCAST'
        chop2 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code2 = sbegin + ctype + '\n' + code2 + send + ctype
        ctype = 'H5 WRITE'
        chop3 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code3 = sbegin + ctype + '\n' + code3 + send + ctype
        ctype = 'H5 READ'
        chop4 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code4 = sbegin + ctype + '\n' + code4 + send + ctype
        ctype = 'FILE READ'
        chop5 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code5 = sbegin + ctype + '\n' + code5 + send + ctype
        ctype = 'COPY'
        chop6 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code6 = sbegin + ctype + '\n' + code6 + send + ctype
        ctype = 'TYPE'
        chop7 = re.compile(sbegin + ctype + '.*?' + send + ctype, re.DOTALL)
        code7 = sbegin + ctype + '\n' + code7 + send + ctype

        f = open(infile, 'r')
        data = f.read()
        f.close()
        data1 = chop1.sub(code1, data)
        data2 = chop2.sub(code2, data1)
        data3 = chop3.sub(code3, data2)
        data4 = chop4.sub(code4, data3)
        data5 = chop5.sub(code5, data4)
        data6 = chop6.sub(code6, data5)
        data7 = chop7.sub(code7, data6)
        f = open(infile, 'w')
        f.write(data7)
        f.close()
