######################################################################          
#                                                                               
# CMakeLists.txt for nimgrid
#                                                                               
######################################################################

set(NIMGRID_SOURCES
    grid_utils.f
    grid_input.f
   )

add_library(nimgrid-obj OBJECT ${NIMGRID_SOURCES})
list(APPEND OBJ_TARGETS $<TARGET_OBJECTS:nimgrid-obj>)
target_link_libraries(nimgrid-obj PUBLIC nimblk)
target_include_directories(nimgrid-obj PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)
add_library(nimgrid STATIC $<TARGET_OBJECTS:nimgrid-obj>)
target_link_libraries(nimgrid PUBLIC nimgrid-obj)

# Tested with examples
#add_subdirectory(tests) 

set(OBJ_TARGETS ${OBJ_TARGETS} PARENT_SCOPE)
