!-------------------------------------------------------------------------------
!! Utilities for linear algebra operations that involve communication
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Utilities for linear algebra operations that involve communication
!-------------------------------------------------------------------------------
MODULE linalg_utils_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: matvec,resid_norm,norm,dot,init_compressed,export_compressed

  PUBLIC :: serial_comm_sparsity,serial_comm_matrix

  INTERFACE matvec
    MODULE PROCEDURE matvec_real,matvec_cv1m,matvec_comp
  END INTERFACE matvec

  INTERFACE resid_norm
    MODULE PROCEDURE resid_norm_real,resid_norm_cv1m,resid_norm_comp
  END INTERFACE resid_norm

  INTERFACE norm
    MODULE PROCEDURE norm_real,norm_cv1m,norm_comp
  END INTERFACE norm

  INTERFACE dot
    MODULE PROCEDURE dot_real,dot_cv1m,dot_comp
  END INTERFACE dot

  INTERFACE init_compressed
    MODULE PROCEDURE init_compressed_real !,init_compressed_cm1m
  END INTERFACE init_compressed

  INTERFACE export_compressed
    MODULE PROCEDURE export_compressed_real !,export_compressed_cm1m
  END INTERFACE export_compressed

  INTERFACE serial_comm_sparsity
    MODULE PROCEDURE serial_comm_sparsity_real
  END INTERFACE serial_comm_sparsity

  INTERFACE serial_comm_matrix
    MODULE PROCEDURE serial_comm_matrix_real
  END INTERFACE serial_comm_matrix

!-------------------------------------------------------------------------------
! for testing without FEM system matrices apply the ave_factor during seaming
!-------------------------------------------------------------------------------
  LOGICAL, PUBLIC :: apply_ave_factor=.FALSE.
  CHARACTER(*), PARAMETER :: mod_name='linalg_utils'
CONTAINS

!-------------------------------------------------------------------------------
!> Real matrix vector multiplication with communication between blocks
!
!  This assumes that seaming has been applied to the vector, but not the matrix.
!  Thus, mat_(not seamed) * operand_(seamed) = output_(not seamed)
!  and a seaming operation is applied to return output_(seamed)
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_real(mat,operand,output,seam)
    USE matrix_mod, ONLY: rmat_storage
    USE vector_mod, ONLY: rvec_storage
    USE seam_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(rmat_storage), INTENT(IN) :: mat(:)
    !> vector to multiply
    CLASS(rvec_storage), INTENT(IN) :: operand(:)
    !> result
    CLASS(rvec_storage), INTENT(INOUT) :: output(:)
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_real',iftn,idepth)
    DO ibl=1,SIZE(mat)
      CALL mat(ibl)%m%matvec(operand(ibl)%v,output(ibl)%v)
      CALL output(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    DO ibl=1,SIZE(mat)
      CALL output(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_real

!-------------------------------------------------------------------------------
!> Complex matrix single-mode vector multiplication with communication between
!  blocks
!
!  This assumes that seaming has been applied to the vector, but not the matrix.
!  Thus, mat_(not seamed) * operand_(seamed) = output_(not seamed)
!  and a seaming operation is applied to return output_(seamed)
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_cv1m(mat,operand,output,seam)
    USE matrix_mod, ONLY: cmat_storage
    USE vector_mod, ONLY: cv1m_storage
    USE seam_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(cmat_storage), INTENT(IN) :: mat(:)
    !> vector to multiply
    CLASS(cv1m_storage), INTENT(IN) :: operand(:)
    !> result
    CLASS(cv1m_storage), INTENT(INOUT) :: output(:)
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_cv1m',iftn,idepth)
    DO ibl=1,SIZE(mat,1)
      CALL mat(ibl)%m%matvec(operand(ibl)%v,output(ibl)%v)
      CALL output(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    DO ibl=1,SIZE(mat,1)
      CALL output(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_cv1m

!-------------------------------------------------------------------------------
!> Complex matrix vector multiplication with communication between blocks
!
!  This assumes that seaming has been applied to the vector, but not the matrix.
!  Thus, mat_(not seamed) * operand_(seamed) = output_(not seamed)
!  and a seaming operation is applied to return output_(seamed)
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_comp(mat,operand,output,seam)
    USE matrix_mod, ONLY: cmat_storage
    USE vector_mod, ONLY: cvec_storage
    USE seam_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(cmat_storage), INTENT(IN) :: mat(:,:)
    !> vector to multiply
    CLASS(cvec_storage), INTENT(IN) :: operand(:)
    !> result
    CLASS(cvec_storage), INTENT(INOUT) :: output(:)
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl,imode
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_comp',iftn,idepth)
    DO ibl=1,SIZE(mat,1)
      DO imode=1,SIZE(mat,2)
        CALL mat(ibl,imode)%m%matvec(operand(ibl)%v,output(ibl)%v,imode,imode)
      ENDDO
      CALL output(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    DO ibl=1,SIZE(mat,1)
      CALL output(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_comp

!-------------------------------------------------------------------------------
!* compute the norm of the residual (rhs - A.x) for a real system.
!-------------------------------------------------------------------------------
  SUBROUTINE resid_norm_real(mat,xx,rhs,res,error,seam,norm_type,er0)
    USE matrix_mod, ONLY: rmat_storage
    USE vector_mod, ONLY: rvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> matrix A
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> solution vector x
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: xx
    !> RHS vector
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: rhs
    !> residual vector (rhs-A.x)
    TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: res
    !> norm, normalized by er0
    REAL(r8), INTENT(OUT) :: error
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: er0

    INTEGER(i4) :: ibl,ist,ien,normArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'resid_norm_real',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(res)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+res(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+res(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    DO ibl=1,SIZE(xx)
      CALL mat(ibl)%m%matvec(xx(ibl)%v,res(ibl)%v)
      CALL res(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    normArr=0.
    ist=1
    DO ibl=1,SIZE(xx)
      CALL res(ibl)%v%edge_unload_arr(seam%s(ibl))
      CALL res(ibl)%v%add_vec(rhs(ibl)%v,v1fac=-1._r8) ! res=rhs-A.x
      IF (norm_type=='l2') THEN
        ien=ist+res(ibl)%v%l2_dot_size-1
        CALL res(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
      ELSE
        ien=ist+res(ibl)%v%inf_norm_size-1
        CALL res(ibl)%v%inf_norm(normArr(ist:ien))
      ENDIF
      ist=ien+1
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      error=SUM(normArr)
    ELSE
      error=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(error,tmp)
      ELSE
        CALL par%layer_max(error,tmp)
      ENDIF
      error=tmp
    ENDIF
    IF (PRESENT(er0)) THEN
      error=error/er0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE resid_norm_real

!-------------------------------------------------------------------------------
!* compute the norm of the residual (rhs - A.x) for a cv1m system.
!-------------------------------------------------------------------------------
  SUBROUTINE resid_norm_cv1m(mat,xx,rhs,res,error,seam,norm_type,er0)
    USE matrix_mod, ONLY: cmat_storage
    USE vector_mod, ONLY: cv1m_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> matrix A
    CLASS(cmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> solution vector x
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: xx
    !> RHS vector
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: rhs
    !> residual vector (rhs-A.x)
    TYPE(cv1m_storage), DIMENSION(:), INTENT(INOUT) :: res
    !> norm, normalized by er0
    REAL(r8), INTENT(OUT) :: error
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: er0

    INTEGER(i4) :: ibl,ist,ien,normArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'resid_norm_cv1m',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(res)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+res(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+res(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    DO ibl=1,SIZE(xx)
      CALL mat(ibl)%m%matvec(xx(ibl)%v,res(ibl)%v)
      CALL res(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    normArr=0.
    ist=1
    DO ibl=1,SIZE(xx)
      CALL res(ibl)%v%edge_unload_arr(seam%s(ibl))
      CALL res(ibl)%v%add_vec(rhs(ibl)%v,v1fac=-1._r8) ! res=rhs-A.x
      IF (norm_type=='l2') THEN
        ien=ist+res(ibl)%v%l2_dot_size-1
        CALL res(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
      ELSE
        ien=ist+res(ibl)%v%inf_norm_size-1
        CALL res(ibl)%v%inf_norm(normArr(ist:ien))
      ENDIF
      ist=ien+1
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      error=SUM(normArr)
    ELSE
      error=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(error,tmp)
      ELSE
        CALL par%layer_max(error,tmp)
      ENDIF
      error=tmp
    ENDIF
    IF (PRESENT(er0)) THEN
      error=error/er0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE resid_norm_cv1m

!-------------------------------------------------------------------------------
!* compute the norm of the residual (rhs - A.x) for a complex system.
!-------------------------------------------------------------------------------
  SUBROUTINE resid_norm_comp(mat,xx,rhs,res,error,seam,norm_type,er0)
    USE matrix_mod, ONLY: cmat_storage
    USE vector_mod, ONLY: cvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> matrix A
    CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
    !> solution vector x
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: xx
    !> RHS vector
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: rhs
    !> residual vector (rhs-A.x)
    TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: res
    !> norm, normalized by er0
    REAL(r8), INTENT(OUT) :: error
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: er0

    INTEGER(i4) :: ibl,ist,ien,normArrSize,imode
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'resid_norm_comp',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(res)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+res(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+res(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    DO ibl=1,SIZE(xx)
      DO imode=1,par%nmodes
        CALL mat(ibl,imode)%m%matvec(xx(ibl)%v,res(ibl)%v,imode,imode)
      ENDDO
      CALL res(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=apply_ave_factor)
    ENDDO
    CALL seam%edge_network
    normArr=0.
    ist=1
    DO ibl=1,SIZE(xx)
      CALL res(ibl)%v%edge_unload_arr(seam%s(ibl))
      CALL res(ibl)%v%add_vec(rhs(ibl)%v,v1fac=-1._r8) ! res=rhs-A.x
      IF (norm_type=='l2') THEN
        ien=ist+res(ibl)%v%l2_dot_size-1
        CALL res(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
      ELSE
        ien=ist+res(ibl)%v%inf_norm_size-1
        CALL res(ibl)%v%inf_norm(normArr(ist:ien))
      ENDIF
      ist=ien+1
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      error=SUM(normArr)
    ELSE
      error=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(error,tmp)
      ELSE
        CALL par%layer_max(error,tmp)
      ENDIF
      error=tmp
    ENDIF
    IF (PRESENT(er0)) THEN
      error=error/er0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE resid_norm_comp

!-------------------------------------------------------------------------------
!* compute the norm of vec
!-------------------------------------------------------------------------------
  REAL(r8) FUNCTION norm_real(vec,seam,norm_type,norm0) RESULT(norm)
    USE vector_mod, ONLY: rvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> vector
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: vec
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: norm0

    INTEGER(i4) :: ibl,ist,ien,normArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'norm_real',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+vec(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+vec(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    normArr=0.
    ist=1
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        ien=ist+vec(ibl)%v%l2_dot_size-1
        CALL vec(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
        ist=ien+1
      ELSE
        ien=ist+vec(ibl)%v%inf_norm_size-1
        CALL vec(ibl)%v%inf_norm(normArr(ist:ien))
        ist=ien+1
      ENDIF
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      norm=SUM(normArr)
    ELSE
      norm=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(norm,tmp)
      ELSE
        CALL par%layer_max(norm,tmp)
      ENDIF
      norm=tmp
    ENDIF
    IF (PRESENT(norm0)) THEN
      norm=norm/norm0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION norm_real

!-------------------------------------------------------------------------------
!* compute the norm of vec
!-------------------------------------------------------------------------------
  REAL(r8) FUNCTION norm_cv1m(vec,seam,norm_type,norm0) RESULT(norm)
    USE vector_mod, ONLY: cv1m_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> vector
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: vec
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: norm0

    INTEGER(i4) :: ibl,ist,ien,normArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'norm_cv1m',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+vec(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+vec(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    normArr=0.
    ist=1
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        ien=ist+vec(ibl)%v%l2_dot_size-1
        CALL vec(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
        ist=ien+1
      ELSE
        ien=ist+vec(ibl)%v%inf_norm_size-1
        CALL vec(ibl)%v%inf_norm(normArr(ist:ien))
        ist=ien+1
      ENDIF
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      norm=SUM(normArr)
    ELSE
      norm=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(norm,tmp)
      ELSE
        CALL par%layer_max(norm,tmp)
      ENDIF
      norm=tmp
    ENDIF
    IF (PRESENT(norm0)) THEN
      norm=norm/norm0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION norm_cv1m

!-------------------------------------------------------------------------------
!* compute the norm of vec
!-------------------------------------------------------------------------------
  REAL(r8) FUNCTION norm_comp(vec,seam,norm_type,norm0) RESULT(norm)
    USE vector_mod, ONLY: cvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> vector
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: vec
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam
    !> norm to use, options: 'inf' and 'l2'
    CHARACTER(*), INTENT(IN) :: norm_type
    !> normalization for norm
    REAL(r8), OPTIONAL, INTENT(IN) :: norm0

    INTEGER(i4) :: ibl,ist,ien,normArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: normArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'norm_comp',iftn,idepth)
    normArrSize=0
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        normArrSize=normArrSize+vec(ibl)%v%l2_dot_size
      ELSE
        normArrSize=normArrSize+vec(ibl)%v%inf_norm_size
      ENDIF
    ENDDO
    ALLOCATE(normArr(normArrSize))
    normArr=0.
    ist=1
    DO ibl=1,SIZE(vec)
      IF (norm_type=='l2') THEN
        ien=ist+vec(ibl)%v%l2_dot_size-1
        CALL vec(ibl)%v%l2_norm2(normArr(ist:ien),seam%s(ibl))
        ist=ien+1
      ELSE
        ien=ist+vec(ibl)%v%inf_norm_size-1
        CALL vec(ibl)%v%inf_norm(normArr(ist:ien))
        ist=ien+1
      ENDIF
    ENDDO
    !$acc wait
    IF (norm_type=='l2') THEN
      norm=SUM(normArr)
    ELSE
      norm=MAXVAL(normArr)
    ENDIF
    IF (par%nprocs_layer > 1) THEN
      IF (norm_type=='l2') THEN
        CALL par%layer_sum(norm,tmp)
      ELSE
        CALL par%layer_max(norm,tmp)
      ENDIF
      norm=tmp
    ENDIF
    IF (PRESENT(norm0)) THEN
      norm=norm/norm0
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION norm_comp

!-------------------------------------------------------------------------------
!* compute the dot product of vec1 and vec2
!-------------------------------------------------------------------------------
  REAL(r8) FUNCTION dot_real(vec1,vec2,seam) RESULT(dot)
    USE vector_mod, ONLY: rvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> First vector for dot product
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: vec1
    !> Second vector for dot product
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: vec2
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam

    INTEGER(i4) :: ibl,ist,ien,dotArrSize
    REAL(r8) :: tmp
    REAL(r8), ALLOCATABLE :: dotArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_real',iftn,idepth)
    dotArrSize=0
    DO ibl=1,SIZE(vec1)
      dotArrSize=dotArrSize+vec1(ibl)%v%l2_dot_size
    ENDDO
    ALLOCATE(dotArr(dotArrSize))
    dotArr=0.
    ist=1
    DO ibl=1,SIZE(vec1)
      ien=ist+vec1(ibl)%v%l2_dot_size-1
      CALL vec1(ibl)%v%dot(vec2(ibl)%v,dotArr(ist:ien),seam%s(ibl))
      ist=ien+1
    ENDDO
    !$acc wait
    dot=SUM(dotArr)
    IF (par%nprocs_layer > 1) THEN
      CALL par%layer_sum(dot,tmp)
      dot=tmp
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION dot_real

!-------------------------------------------------------------------------------
!* compute the dot product of vec1 and vec2
!-------------------------------------------------------------------------------
  COMPLEX(r8) FUNCTION dot_cv1m(vec1,vec2,seam) RESULT(dot)
    USE vector_mod, ONLY: cv1m_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> First vector for dot product
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: vec1
    !> Second vector for dot product
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: vec2
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam

    INTEGER(i4) :: ibl,ist,ien,dotArrSize
    COMPLEX(r8) :: tmp
    COMPLEX(r8), ALLOCATABLE :: dotArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_cv1m',iftn,idepth)
    dotArrSize=0
    DO ibl=1,SIZE(vec1)
      dotArrSize=dotArrSize+vec1(ibl)%v%l2_dot_size
    ENDDO
    ALLOCATE(dotArr(dotArrSize))
    dotArr=0.
    ist=1
    DO ibl=1,SIZE(vec1)
      ien=ist+vec1(ibl)%v%l2_dot_size-1
      CALL vec1(ibl)%v%dot(vec2(ibl)%v,dotArr(ist:ien),seam%s(ibl))
      ist=ien+1
    ENDDO
    !$acc wait
    dot=SUM(dotArr)
    IF (par%nprocs_layer > 1) THEN
      CALL par%layer_sum(dot,tmp)
      dot=tmp
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION dot_cv1m

!-------------------------------------------------------------------------------
!* compute the dot product of vec1 and vec2
!-------------------------------------------------------------------------------
  COMPLEX(r8) FUNCTION dot_comp(vec1,vec2,seam) RESULT(dot)
    USE vector_mod, ONLY: cvec_storage
    USE seam_mod
    USE pardata_mod, ONLY: par
    IMPLICIT NONE

    !> First vector for dot product
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: vec1
    !> Second vector for dot product
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: vec2
    !> associated seam
    TYPE(seam_type), INTENT(IN) :: seam

    INTEGER(i4) :: ibl,ist,ien,dotArrSize
    COMPLEX(r8) :: tmp
    COMPLEX(r8), ALLOCATABLE :: dotArr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_comp',iftn,idepth)
    dotArrSize=0
    DO ibl=1,SIZE(vec1)
      dotArrSize=dotArrSize+vec1(ibl)%v%l2_dot_size
    ENDDO
    ALLOCATE(dotArr(dotArrSize))
    dotArr=0.
    ist=1
    DO ibl=1,SIZE(vec1)
      ien=ist+vec1(ibl)%v%l2_dot_size-1
      CALL vec1(ibl)%v%dot(vec2(ibl)%v,dotArr(ist:ien),seam%s(ibl))
      ist=ien+1
    ENDDO
    !$acc wait
    dot=SUM(dotArr)
    IF (par%nprocs_layer > 1) THEN
      CALL par%layer_sum(dot,tmp)
      dot=tmp
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION dot_comp

!-------------------------------------------------------------------------------
!> allocate and initialize data structures needed to convert to a compressed
!  matrix format. The mat structure is block decomposed but the matrix is
!  stored in a distributed memory, block-coalesced format
!-------------------------------------------------------------------------------
  SUBROUTINE init_compressed_real(mat,csm,seam,eliminated)
    USE compressed_conversion_mod
    USE compressed_matrix_real_mod
    USE matrix_mod, ONLY: rmat_storage
    USE vector_mod, ONLY: ivec_storage
    USE seam_mod
    USE pardata_mod
    IMPLICIT NONE

    !> matrix A
    CLASS(rmat_storage), DIMENSION(:), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: eliminated

    INTEGER(i4), DIMENSION(par%nbl) :: ndof_blk,nnz_blk
    INTEGER(i4), DIMENSION(par%nbl_total) :: firstrow_blktot,recv_arr
    INTEGER(i4) :: ibl,ndof_proc,firstrow_proc,lastrow_proc
    INTEGER(i4) :: nnz_proc,firstnz_proc,lastnz_proc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_compressed_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   compute the local # of DOFs
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%init_dof_map(seam%s(ibl),eliminated,ndof_blk(ibl))
    ENDDO
    ndof_proc=SUM(ndof_blk)
!-------------------------------------------------------------------------------
!   call edge_network to find total the number of elements and boundaries
!   associated with vertex nodes.
!-------------------------------------------------------------------------------
    CALL seam%edge_network
!-------------------------------------------------------------------------------
!   determine the number of nonzeros
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%init_nnz_map(seam%s(ibl),eliminated,nnz_blk(ibl))
    ENDDO
    nnz_proc=SUM(nnz_blk)
!-------------------------------------------------------------------------------
!   send a round-robin count of the DOFs and NZs to get the first and last
!   global DOFs and NZs owned by the process and blocks
!-------------------------------------------------------------------------------
    firstrow_proc=0
    firstnz_proc=0
    IF (par%node_layer/=0) THEN
      CALL par%layer_recv(firstrow_proc,par%node_layer-1)
      CALL par%layer_recv(firstnz_proc,par%node_layer-1)
    ENDIF
    lastrow_proc=firstrow_proc+ndof_proc-1
    lastnz_proc=firstnz_proc+nnz_proc-1
    IF (par%node_layer/=par%nprocs_layer-1) THEN
      CALL par%layer_send(lastrow_proc+1,par%node_layer+1)
      CALL par%layer_send(lastnz_proc+1,par%node_layer+1)
    ENDIF
    mat(1)%m%mat_conv%firstnz=firstnz_proc
    IF (par%nbl>1) THEN
      DO ibl=2,par%nbl
        mat(ibl)%m%mat_conv%firstnz=mat(ibl-1)%m%mat_conv%firstnz+nnz_blk(ibl-1)
        mat(ibl-1)%m%mat_conv%lastnz=mat(ibl)%m%mat_conv%firstnz-1
      ENDDO
    ENDIF
    mat(par%nbl)%m%mat_conv%lastnz=lastnz_proc
!-------------------------------------------------------------------------------
!   allocate csm
!-------------------------------------------------------------------------------
    CALL csm%alloc(ndof_proc,firstrow_proc,lastrow_proc,                        &
                   nnz_proc,firstnz_proc,lastnz_proc,mat(1)%m%id)
!-------------------------------------------------------------------------------
!   add the first row to the dof_map to convert to a global DOF map
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      mat(ibl)%m%mat_conv%firstrow=firstrow_proc
      firstrow_blktot(par%loc2glob(ibl))=mat(ibl)%m%mat_conv%firstrow
      CALL mat(ibl)%m%dof_map%add_row_count(mat(ibl)%m%mat_conv%firstrow)
      CALL mat(ibl)%m%dof_map%edge_load_arr(seam%s(ibl))
      firstrow_proc=firstrow_proc+ndof_blk(ibl)
      mat(ibl)%m%mat_conv%lastrow=firstrow_proc-1
    ENDDO
    CALL par%layer_sum(firstrow_blktot,recv_arr,par%nbl_total)
    firstrow_blktot=recv_arr
!-------------------------------------------------------------------------------
!   Seam the DOF map to get the DOFs associated with other procs
!-------------------------------------------------------------------------------
    CALL seam%edge_network
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%dof_map%edge_unload_arr(seam%s(ibl))
    ENDDO
!-------------------------------------------------------------------------------
!   Initialize the local sparsity pattern and start the process of setting the
!   compressed conversion communication pattern structures
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%init_local_sparsity(csm,seam%s(ibl),eliminated,           &
                                          firstrow_blktot)
    ENDDO
    !$acc wait
#ifdef HAVE_MPI
!-------------------------------------------------------------------------------
!   send the communicated data structures
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%send_comm_sparsity
    ENDDO
#else
!-------------------------------------------------------------------------------
!   copy data structures
!-------------------------------------------------------------------------------
    CALL serial_comm_sparsity(mat)
#endif
!-------------------------------------------------------------------------------
!   set the received data structures within the local sparsity pattern
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%set_comm_sparsity(csm)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_compressed_real

!-------------------------------------------------------------------------------
!> export matrix data to a compressed matrix format. The mat structure is block
!  decomposed but the matrix is stored in a distributed memory, block-coalesced
!  format
!-------------------------------------------------------------------------------
  SUBROUTINE export_compressed_real(mat,csm)
    USE compressed_matrix_real_mod
    USE matrix_mod, ONLY: rmat_storage
    USE pardata_mod
    IMPLICIT NONE

    !> matrix A
    CLASS(rmat_storage), DIMENSION(:), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'export_compressed_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   start communication by loading data
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%init_matrix_to_compressed_comm
    ENDDO
#ifdef HAVE_MPI
    !$acc wait
!-------------------------------------------------------------------------------
!   post sends
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%send_matrix_to_compressed_comm
    ENDDO
#else
    CALL serial_comm_matrix(mat)
#endif
!-------------------------------------------------------------------------------
!   load local values
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%export_matrix_to_compressed(csm)
    ENDDO
!-------------------------------------------------------------------------------
!   finish communication and load values from other blocks
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%finalize_matrix_to_compressed_comm(csm)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE export_compressed_real

!-------------------------------------------------------------------------------
!> Send communicated sparsity pattern and associated data for serial
!  computations
!-------------------------------------------------------------------------------
  SUBROUTINE serial_comm_sparsity_real(mat)
    USE matrix_mod, ONLY: rmat_storage
    USE pardata_mod
    IMPLICIT NONE

    !> matrix A
    CLASS(rmat_storage), DIMENSION(:), INTENT(INOUT) :: mat

    INTEGER(i4) :: ibl,irecv,isend,blk_recv,blk_send,nalloc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'serial_comm_sparsity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   first transfer nnz_send and allocate arrays
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      DO irecv=1,mat(ibl)%m%mat_conv%nrecv
        blk_recv=mat(ibl)%m%mat_conv%blk_recv(irecv)
        DO isend=1,mat(blk_recv)%m%mat_conv%nsend
          blk_send=mat(blk_recv)%m%mat_conv%blk_send(isend)
          IF (mat(ibl)%m%id==blk_send) THEN
            mat(ibl)%m%mat_conv%nnz_recv(irecv)=                                &
              mat(blk_recv)%m%mat_conv%nnz_send(isend)
            EXIT ! done with send loop
          ENDIF
        ENDDO
        nalloc=mat(ibl)%m%mat_conv%nnz_recv(irecv)
        ALLOCATE(mat(ibl)%m%recv_vals(irecv)%vals(nalloc))
        ALLOCATE(mat(ibl)%m%mat_conv%recv_addr(irecv)%map_ival(nalloc))
        ALLOCATE(mat(ibl)%m%mat_conv%recv_addr(irecv)%map_irow(nalloc))
        ALLOCATE(mat(ibl)%m%mat_conv%recv_addr(irecv)%map_jcol(nalloc))
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   transfer irow and jcol
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      DO irecv=1,mat(ibl)%m%mat_conv%nrecv
        blk_recv=mat(ibl)%m%mat_conv%blk_recv(irecv)
        DO isend=1,mat(blk_recv)%m%mat_conv%nsend
          blk_send=mat(blk_recv)%m%mat_conv%blk_send(isend)
          IF (mat(ibl)%m%id==blk_send) THEN
            mat(ibl)%m%mat_conv%recv_addr(irecv)%map_irow=                      &
              mat(blk_recv)%m%mat_conv%send_addr(isend)%map_irow
            mat(ibl)%m%mat_conv%recv_addr(irecv)%map_jcol=                      &
              mat(blk_recv)%m%mat_conv%send_addr(isend)%map_jcol
            EXIT ! done with send loop
          ENDIF
        ENDDO
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE serial_comm_sparsity_real

!-------------------------------------------------------------------------------
!* Send communicated matrix values for serial computations
!-------------------------------------------------------------------------------
  SUBROUTINE serial_comm_matrix_real(mat)
    USE matrix_mod, ONLY: rmat_storage
    USE pardata_mod
    IMPLICIT NONE

    !> matrix A
    CLASS(rmat_storage), DIMENSION(:), INTENT(INOUT) :: mat

    INTEGER(i4) :: ibl,irecv,isend,blk_recv,blk_send
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'serial_comm_matrix_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   transfer irow and jcol
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      DO irecv=1,mat(ibl)%m%mat_conv%nrecv
        blk_recv=mat(ibl)%m%mat_conv%blk_recv(irecv)
        DO isend=1,mat(blk_recv)%m%mat_conv%nsend
          blk_send=mat(blk_recv)%m%mat_conv%blk_send(isend)
          IF (mat(ibl)%m%id==blk_send) THEN
            mat(ibl)%m%recv_vals(irecv)%vals=                                   &
              mat(blk_recv)%m%send_vals(isend)%vals
            EXIT ! done with send loop
          ENDIF
        ENDDO
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE serial_comm_matrix_real

END MODULE linalg_utils_mod
