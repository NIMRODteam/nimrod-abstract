!-------------------------------------------------------------------------------
!* test vector routines driver
!-------------------------------------------------------------------------------
#include "config.f"
PROGRAM test_vector
  USE local
  USE vector_mod
  USE matrix_mod
  USE seam_mod
  USE pardata_mod
  USE timer_mod
  USE nimtest_utils
  USE linalg_registry_mod
  IMPLICIT NONE

  TYPE(rvec_storage), ALLOCATABLE :: rvec(:)
  TYPE(cv1m_storage), ALLOCATABLE :: cv1m(:)
  TYPE(cvec_storage), ALLOCATABLE :: cvec(:)
  TYPE(rmat_storage), ALLOCATABLE :: rmat(:)
  TYPE(cmat_storage), ALLOCATABLE :: cmat(:,:)
  TYPE(seam_type) :: seam
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  global_test_tolerance=1.e-12
  CALL par%init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_vector',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_test_type
!-------------------------------------------------------------------------------
! use only a single block for these tests
!-------------------------------------------------------------------------------
  single_block=.TRUE.
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_linalg(rvec,cv1m,cvec,rmat,cmat,seam,itest,skip_mat=.TRUE.)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("alloc_dealloc")
      ! do nothing
    CASE ("alloc_with_mold")
      CALL test_alloc_with_mold(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("zero")
      CALL test_zero(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("assign_vector")
      CALL test_assign_vector(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("mult")
      CALL test_mult(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("add")
      CALL test_add(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("l2norm_dot")
      CALL test_l2norm_dot(rvec(1)%v,cv1m(1)%v,cvec(1)%v,seam%s(1))
    CASE ("assemble")
      CALL test_assemble(rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("bc_regularity")
      CALL test_bc_regularity(rvec(1)%v,cv1m(1)%v,cvec(1)%v,seam%s(1))
    CASE ("load_unload")
      CALL test_load_unload(rvec(1)%v,cv1m(1)%v,cvec(1)%v,seam%s(1))
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_name))
      CALL print_usage
    END SELECT
    CALL teardown_linalg(rvec,cv1m,cvec,rmat,cmat,seam)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: ./test_vector <test type> <test name>')
    CALL print_types
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_dealloc')
    CALL par%nim_write('    alloc_with_mold')
    CALL par%nim_write('    zero')
    CALL par%nim_write('    assign_vector')
    CALL par%nim_write('    mult')
    CALL par%nim_write('    add')
    CALL par%nim_write('    l2norm_dot')
    CALL par%nim_write('    assemble')
    CALL par%nim_write('    bc_regularity')
    CALL par%nim_write('    load_unload')
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
!* test alloc_with_mold
!-------------------------------------------------------------------------------
  SUBROUTINE test_alloc_with_mold(rvec,cv1m,cvec)
    USE vector_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    CLASS(rvector), ALLOCATABLE :: rvec2
    CLASS(cvector1m), ALLOCATABLE :: cv1m2
    CLASS(cvector), ALLOCATABLE :: cvec2

!-------------------------------------------------------------------------------
!   real vector
!-------------------------------------------------------------------------------
    CALL rvec%alloc_with_mold(rvec2)
    CALL rvec2%dealloc
    CALL rvec%alloc_with_mold(rvec2,nqty=2,pd=3)
    CALL rvec2%dealloc
!-------------------------------------------------------------------------------
!   complex 1 mode vector
!-------------------------------------------------------------------------------
    CALL cv1m%alloc_with_mold(cv1m2)
    CALL cv1m2%dealloc
    CALL cv1m%alloc_with_mold(cv1m2,nqty=2,pd=3)
    CALL cv1m2%dealloc
!-------------------------------------------------------------------------------
!   complex vector
!-------------------------------------------------------------------------------
    CALL cvec%alloc_with_mold(cvec2)
    CALL cvec2%dealloc
    CALL cvec%alloc_with_mold(cvec2,nqty=2,pd=3,nmodes=5)
    CALL cvec2%dealloc
  END SUBROUTINE test_alloc_with_mold

!-------------------------------------------------------------------------------
!* test assignment to zero
!-------------------------------------------------------------------------------
  SUBROUTINE test_zero(rvec,cv1m,cvec)
    USE vector_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    REAL(r8) :: rscalar = 5._r8
    COMPLEX(r8) :: cscalar = CMPLX(6._r8,7._r8,r8)
    REAL(r8), ALLOCATABLE :: norm(:)

!-------------------------------------------------------------------------------
!   allocate norm for the real vector
!-------------------------------------------------------------------------------
    ALLOCATE(norm(rvec%inf_norm_size))
    norm=0._r8
!-------------------------------------------------------------------------------
!   assign to the real vector
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec%zero()
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(0.0_r8,MAXVAL(norm),"rvec%zero")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   assign to the complex 1 mode vector
!-------------------------------------------------------------------------------
    ALLOCATE(norm(cv1m%inf_norm_size))
    norm=0._r8
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m%zero()
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(0.0_r8,MAXVAL(norm),"cv1m%zero")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   assign to the complex vector
!-------------------------------------------------------------------------------
    ALLOCATE(norm(cvec%inf_norm_size))
    norm=0._r8
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec%zero()
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(0.0_r8,MAXVAL(norm),"cvec%zero")
    DEALLOCATE(norm)
  END SUBROUTINE test_zero

!-------------------------------------------------------------------------------
!* test scalar assignment
!-------------------------------------------------------------------------------
  SUBROUTINE test_assign_vector(rvec,cv1m,cvec)
    USE vector_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    CLASS(rvector), ALLOCATABLE :: rvec_out
    CLASS(cvector1m), ALLOCATABLE :: cv1m_out
    CLASS(cvector), ALLOCATABLE :: cvec_out
    REAL(r8) :: rscalar = 5._r8
    COMPLEX(r8) :: cscalar = CMPLX(6._r8,7._r8,r8)
    REAL(r8), ALLOCATABLE :: norm(:)

    CALL rvec%alloc_with_mold(rvec_out)
    CALL cv1m%alloc_with_mold(cv1m_out)
    CALL cvec%alloc_with_mold(cvec_out)
!-------------------------------------------------------------------------------
!   assign to the real vector
!-------------------------------------------------------------------------------
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL rvec_out%zero
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec_out%assign_rvec(rvec)
    CALL wrtrue(rvec%test_if_equal(rvec_out),"rvec%assign_rvec")
    CALL rvec_out%zero
    CALL rvec%assign_for_testing('constant',rscalar+1._r8)
    CALL rvec_out%assignp_rvec(rvec)
    IF (rvec%nqty==1) THEN
      CALL wrtrue(rvec%test_if_equal(rvec_out),"rvec%assignp_rvec")
    ELSE
      CALL wrfalse(rvec%test_if_equal(rvec_out),"rvec%assignp_rvec")
    ENDIF
    CALL rvec_out%zero
    CALL rvec_out%assignp_rvec(rvec,v1st=rvec%nqty,v2st=1,nq=1)
    IF (rvec%nqty==1) THEN
      CALL wrtrue(rvec%test_if_equal(rvec_out),"rvec%assignp_rvec 2")
    ELSE
      CALL wrfalse(rvec%test_if_equal(rvec_out),"rvec%assignp_rvec 2")
    ENDIF
    CALL rvec%zero
    CALL rvec%assign_cv1m(cv1m,'real')
    ALLOCATE(norm(rvec%inf_norm_size))
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar,r8),MAXVAL(norm),"rvec%assign_cv1m real")
    CALL rvec%zero
    CALL rvec%assign_cv1m(cv1m,'imag')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assign_cv1m imag")
    CALL rvec%zero
    CALL rvec%assignp_cv1m(cv1m,'real')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar),MAXVAL(norm),"rvec%assignp_cv1m real")
    CALL rvec%zero
    CALL rvec%assignp_cv1m(cv1m,'imag')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assignp_cv1m imag")
    CALL rvec%zero
    CALL rvec%assignp_cv1m(cv1m,'real',v1st=rvec%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar),MAXVAL(norm),"rvec%assignp_cv1m real 2")
    CALL rvec%zero
    CALL rvec%assignp_cv1m(cv1m,'imag',v1st=1,v2st=cv1m%nqty,nq=1)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assignp_cv1m imag 2")
    CALL rvec%zero
    CALL rvec%assign_cvec(cvec,1,'real')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar,r8),MAXVAL(norm),"rvec%assign_cvec real")
    CALL rvec%zero
    CALL rvec%assign_cvec(cvec,cvec%nmodes,'imag')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assign_cvec imag")
    CALL rvec%zero
    CALL rvec%assignp_cvec(cvec,cvec%nmodes,'real')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar),MAXVAL(norm),"rvec%assignp_cvec real")
    CALL rvec%zero
    CALL rvec%assignp_cvec(cvec,1,'imag')
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assignp_cvec imag")
    CALL rvec%zero
    CALL rvec%assignp_cvec(cvec,1,'real',v1st=rvec%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar),MAXVAL(norm),"rvec%assignp_cvec real 2")
    CALL rvec%zero
    CALL rvec%assignp_cvec(cvec,cvec%nmodes,'imag',v1st=1,v2st=cvec%nqty,nq=1)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(AIMAG(cscalar),MAXVAL(norm),"rvec%assignp_cvec imag 2")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   assign to the complex 1-mode vector
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assign_rvec(rvec,'real')
    ALLOCATE(norm(cv1m%inf_norm_size))
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assign_rvec real")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assign_rvec(rvec,'imag')
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assign_rvec imag")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_rvec(rvec,'real')
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assignp_rvec real")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_rvec(rvec,'imag')
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assignp_rvec real")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_rvec(rvec,'real',v1st=cv1m%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assignp_rvec real 2")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_rvec(rvec,'imag',v1st=1,v2st=rvec%nqty,nq=1)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cv1m%assignp_rvec imag 2")
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m_out%zero()
    CALL cv1m_out%assign_cv1m(cv1m)
    CALL wrtrue(cv1m%test_if_equal(cv1m_out),"cv1m%assign_cv1m")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m_out%zero()
    CALL cv1m_out%assignp_cv1m(cv1m)
    IF (cv1m%nqty==1) THEN
      CALL wrtrue(cv1m%test_if_equal(cv1m_out),"cv1m%assignp_cv1m")
    ELSE
      CALL wrfalse(cv1m%test_if_equal(cv1m_out),"cv1m%assignp_cv1m")
    ENDIF
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m_out%zero()
    CALL cv1m_out%assignp_cv1m(cv1m,v1st=cv1m%nqty,v2st=1,nq=1)
    IF (cv1m%nqty==1) THEN
      CALL wrtrue(cv1m%test_if_equal(cv1m_out),"cv1m%assignp_cv1m 2")
    ELSE
      CALL wrfalse(cv1m%test_if_equal(cv1m_out),"cv1m%assignp_cv1m 2")
    ENDIF
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cv1m%zero()
    CALL cv1m%assign_cvec(cvec,1)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m%assign_cvec")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_cvec(cvec,cvec%nmodes)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m%assignp_cvec")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_cvec(cvec,1,v1st=cv1m%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m%assignp_cvec 2")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cv1m%zero()
    CALL cv1m%assignp_cvec(cvec,cvec%nmodes,v1st=1,v2st=cvec%nqty,nq=1)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m%assignp_cvec 3")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   assign to the complex vector
!-------------------------------------------------------------------------------
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assign_rvec(rvec,1,'real')
    ALLOCATE(norm(cvec%inf_norm_size))
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assign_rvec real")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assign_rvec(rvec,cvec%nmodes,'imag')
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assign_rvec imag")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assignp_rvec(rvec,cvec%nmodes,'real')
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assignp_rvec real")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assignp_rvec(rvec,1,'imag')
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assignp_rvec imag")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assignp_rvec(rvec,1,'real',v1st=cvec%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assignp_rvec real 2")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL cvec%zero()
    CALL cvec%assignp_rvec(rvec,cvec%nmodes,'imag',v1st=1,v2st=rvec%nqty,nq=1)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"cvec%assignp_rvec imag 2")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cvec%zero()
    CALL cvec%assign_cv1m(cv1m,1)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec%assign_cv1m")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cvec%zero()
    CALL cvec%assignp_cv1m(cv1m,cvec%nmodes)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec%assignp_cv1m")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cvec%zero()
    CALL cvec%assignp_cv1m(cv1m,1,v1st=cv1m%nqty,v2st=1,nq=1)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec%assignp_cv1m 2")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec_out%zero()
    CALL cvec_out%assign_cvec(cvec)
    CALL wrtrue(cvec%test_if_equal(cvec_out),"cvec%assign_cvec")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec_out%zero()
    CALL cvec_out%assignp_cvec(cvec)
    IF (cvec%nqty==1) THEN
      CALL wrtrue(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec")
    ELSE
      CALL wrfalse(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec")
    ENDIF
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec_out%zero()
    CALL cvec_out%assignp_cvec(cvec,v1st=cvec%nqty,v2st=1,nq=1)
    IF (cvec%nqty==1) THEN
      CALL wrtrue(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec 2")
    ELSE
      CALL wrfalse(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec 2")
    ENDIF
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec_out%zero()
    CALL cvec_out%assignp_cvec(cvec,v1st=1,v2st=cvec%nqty,nq=1)
    IF (cvec%nqty==1) THEN
      CALL wrtrue(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec 4")
    ELSE
      CALL wrfalse(cvec%test_if_equal(cvec_out),"cvec%assignp_cvec 4")
    ENDIF
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   deallocate
!-------------------------------------------------------------------------------
    CALL rvec_out%dealloc
    CALL cv1m_out%dealloc
    CALL cvec_out%dealloc
  END SUBROUTINE test_assign_vector

!-------------------------------------------------------------------------------
!* test multiplication
!-------------------------------------------------------------------------------
  SUBROUTINE test_mult(rvec,cv1m,cvec)
    USE vector_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    INTEGER(i4) :: iscalar
    REAL(r8) :: rscalar
    REAL(r8), ALLOCATABLE :: norm(:)
    COMPLEX(r8) :: cscalar

!-------------------------------------------------------------------------------
!   set scalars
!-------------------------------------------------------------------------------
    iscalar=5_i4
    rscalar=6._r8
    cscalar=CMPLX(7._r8,8._r8,r8)
!-------------------------------------------------------------------------------
!   multiply the real vector
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',REAL(iscalar,r8))
    CALL rvec%mult(iscalar)
    ALLOCATE(norm(rvec%inf_norm_size))
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(iscalar**2,r8),MAXVAL(norm),"rvec%mult_int")
    CALL rvec%mult(rscalar)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(rscalar*REAL(iscalar**2,r8),MAXVAL(norm),"rvec%mult_rsc")
    CALL rvec%mult(cscalar)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(REAL(cscalar)*rscalar*REAL(iscalar**2,r8),                     &
                 MAXVAL(norm),"rvec%mult_csc")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   multiply the complex 1-mode vector
!-------------------------------------------------------------------------------
    CALL cv1m%assign_for_testing('constant',REAL(iscalar,r8)*(1,0))
    CALL cv1m%mult(iscalar)
    ALLOCATE(norm(cv1m%inf_norm_size))
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(REAL(iscalar**2,r8),MAXVAL(norm),"cv1m%mult_int")
    CALL cv1m%mult(rscalar)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(rscalar*REAL(iscalar**2,r8),MAXVAL(norm),"cv1m%mult_rsc")
    CALL cv1m%mult(cscalar)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar*rscalar*REAL(iscalar**2,r8)),                      &
                 MAXVAL(norm),"cv1m%mult_csc")
!-------------------------------------------------------------------------------
!   multiply the complex vector
!-------------------------------------------------------------------------------
    CALL cvec%assign_for_testing('constant',REAL(iscalar,r8)*(1,0))
    CALL cvec%mult(iscalar)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(REAL(iscalar**2,r8),MAXVAL(norm),"cvec%mult_int")
    CALL cvec%mult(rscalar)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(rscalar*REAL(iscalar**2,r8),MAXVAL(norm),"cvec%mult_rsc")
    CALL cvec%mult(cscalar)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar*rscalar*REAL(iscalar**2,r8)),                      &
                 MAXVAL(norm),"cvec%mult_csc")
    DEALLOCATE(norm)
  END SUBROUTINE test_mult

!-------------------------------------------------------------------------------
!* test addition
!-------------------------------------------------------------------------------
  SUBROUTINE test_add(rvec,cv1m,cvec)
    USE vector_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    REAL(r8) :: rscalar
    REAL(r8), ALLOCATABLE :: norm(:)
    COMPLEX(r8) :: cscalar

!-------------------------------------------------------------------------------
!   set scalars
!-------------------------------------------------------------------------------
    rscalar=6._r8
    cscalar=CMPLX(7._r8,8._r8,r8)
!-------------------------------------------------------------------------------
!   add the real vector
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec%add_vec(rvec)
    ALLOCATE(norm(rvec%inf_norm_size))
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(2._r8*rscalar,MAXVAL(norm),"rvec%add_vec")
    CALL rvec%add_vec(rvec,v1st=1,v2st=rvec%nqty,nq=1,v1fac=2_i4,v2fac=3_i4)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(10._r8*rscalar,MAXVAL(norm),"rvec%add_vec option 1")
    CALL rvec%add_vec(rvec,v1fac=4_i8,v2fac=5_i8)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(90._r8*rscalar,MAXVAL(norm),"rvec%add_vec option 2")
    CALL rvec%add_vec(rvec,v1fac=0.5_r8,v2fac=0.2_r8)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(63._r8*rscalar,MAXVAL(norm),"rvec%add_vec option 3")
    CALL rvec%add_vec(rvec,v1fac=3._r4,v2fac=2._r4)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(315._r8*rscalar,MAXVAL(norm),"rvec%add_vec option 4",          &
                 tolerance=1.e-7_r8)
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   add the complex 1-mode vector
!-------------------------------------------------------------------------------
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m%add_vec(cv1m)
    ALLOCATE(norm(cv1m%inf_norm_size))
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(2._r8*ABS(cscalar),MAXVAL(norm),"cv1m%add_vec")
    CALL cv1m%add_vec(cv1m,v1st=1,v2st=cv1m%nqty,nq=1,v1fac=2_i4,v2fac=3_i4)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(10._r8*ABS(cscalar),MAXVAL(norm),"cv1m%add_vec option 1")
    CALL cv1m%add_vec(cv1m,v1fac=4_i8,v2fac=5_i8)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(90._r8*ABS(cscalar),MAXVAL(norm),"cv1m%add_vec option 2")
    CALL cv1m%add_vec(cv1m,v1fac=0.5_r8,v2fac=0.2_r8)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(63._r8*ABS(cscalar),MAXVAL(norm),"cv1m%add_vec option 3")
    CALL cv1m%add_vec(cv1m,v1fac=cscalar,v2fac=cscalar)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(126._r8*ABS(cscalar**2),MAXVAL(norm),"cv1m%add_vec option 4")
    CALL cv1m%add_vec(cv1m,v1fac=3._r4,v2fac=2._r4)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(630._r8*ABS(cscalar**2),MAXVAL(norm),"cv1m%add_vec option 5",  &
                 tolerance=1.e-7_r8)
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   add the complex vector
!-------------------------------------------------------------------------------
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec%add_vec(cvec)
    ALLOCATE(norm(cvec%inf_norm_size))
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(2._r8*ABS(cscalar),MAXVAL(norm),"cvec%add_vec")
    CALL cvec%add_vec(cvec,v1st=1,v2st=cvec%nqty,nq=1,v1fac=2_i4,v2fac=3_i4)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(10._r8*ABS(cscalar),MAXVAL(norm),"cvec%add_vec option 1")
    CALL cvec%add_vec(cvec,v1fac=4_i8,v2fac=5_i8)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(90._r8*ABS(cscalar),MAXVAL(norm),"cvec%add_vec option 2")
    CALL cvec%add_vec(cvec,v1fac=0.5_r8,v2fac=0.2_r8)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(63._r8*ABS(cscalar),MAXVAL(norm),"cvec%add_vec option 3")
    CALL cvec%add_vec(cvec,v1fac=cscalar,v2fac=cscalar)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(126._r8*ABS(cscalar**2),MAXVAL(norm),"cvec%add_vec option 4")
    CALL cvec%add_vec(cvec,v1fac=3._r4,v2fac=2._r4)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(630._r8*ABS(cscalar**2),MAXVAL(norm),"cvec%add_vec option 5",  &
                 tolerance=1.e-7_r8)
    DEALLOCATE(norm)
  END SUBROUTINE test_add

!-------------------------------------------------------------------------------
!* test the l2norm
!-------------------------------------------------------------------------------
  SUBROUTINE test_l2norm_dot(rvec,cv1m,cvec,edge)
    USE vector_mod
    USE edge_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec
    TYPE(edge_type), INTENT(IN) :: edge

    REAL(r8), PARAMETER :: rscalar = 5._r8
    COMPLEX(r8), PARAMETER :: cscalar = CMPLX(6._r8,7._r8,r8)
    REAL(r8), ALLOCATABLE :: dot(:)
    COMPLEX(r8), ALLOCATABLE :: cdot(:)
    REAL(r8), ALLOCATABLE :: norm(:)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    ALLOCATE(norm(rvec%l2_dot_size))
    ALLOCATE(dot(rvec%l2_dot_size))
    norm=0._r8
    dot=0._r8
    CALL rvec%l2_norm2(norm,edge)
    CALL rvec%dot(rvec,dot,edge)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(SUM(norm),SUM(dot),"rvec%l2_norm2 & rvec%dot")
    DEALLOCATE(norm)
    DEALLOCATE(dot)
    CALL cv1m%assign_for_testing('constant',cscalar)
    ALLOCATE(norm(cv1m%l2_dot_size))
    ALLOCATE(cdot(cv1m%l2_dot_size))
    norm=0._r8
    cdot=0._r8
    CALL cv1m%l2_norm2(norm,edge)
    CALL cv1m%dot(cv1m,cdot,edge)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(SUM(norm),SUM(REAL(cdot,8)),"cv1m%l2_norm2 & cv1m%dot")
    DEALLOCATE(norm)
    DEALLOCATE(cdot)
    CALL cvec%assign_for_testing('constant',cscalar)
    ALLOCATE(norm(cvec%l2_dot_size))
    ALLOCATE(cdot(cvec%l2_dot_size))
    norm=0._r8
    cdot=0._r8
    CALL cvec%l2_norm2(norm,edge)
    CALL cvec%dot(cvec,cdot,edge)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(SUM(norm),SUM(REAL(cdot,8)),"cvec%l2_norm2 & cvec%dot")
    DEALLOCATE(norm)
    DEALLOCATE(cdot)
  END SUBROUTINE test_l2norm_dot

!-------------------------------------------------------------------------------
!> test the assembly routines, the result of this test should be the number of
! elements that contribute to each DOF. There is no good to way to confirm
! this result in general.
!-------------------------------------------------------------------------------
  SUBROUTINE test_assemble(rvec,cv1m,cvec)
    USE vector_mod
    USE edge_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    REAL(r8), ALLOCATABLE :: rintegrand(:,:,:)
    COMPLEX(r8), ALLOCATABLE :: cintegrand1m(:,:,:),cintegrand(:,:,:,:)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    ALLOCATE(rintegrand(rvec%nqty,rvec%nel,rvec%u_ndof))
    !$acc enter data create(rintegrand) async(rvec%id) if(rvec%on_gpu)
    !$acc kernels present(rintegrand) async(rvec%id) if(rvec%on_gpu)
    rintegrand=1._r8
    !$acc end kernels
    CALL rvec%zero
    CALL rvec%assemble(rintegrand)
    !$acc exit data delete(rintegrand) finalize async(rvec%id) if(rvec%on_gpu)
    DEALLOCATE(rintegrand)
    ALLOCATE(cintegrand1m(cv1m%nqty,cv1m%nel,cv1m%u_ndof))
    !$acc enter data create(cintegrand1m) async(cv1m%id) if(cv1m%on_gpu)
    !$acc kernels present(cintegrand1m) async(cv1m%id) if(cv1m%on_gpu)
    cintegrand1m=1._r8
    !$acc end kernels
    CALL cv1m%zero
    CALL cv1m%assemble(cintegrand1m)
    !$acc exit data delete(cintegrand1m) finalize async(cv1m%id) if(cv1m%on_gpu)
    DEALLOCATE(cintegrand1m)
    ALLOCATE(cintegrand(cvec%nqty,cvec%nel,cvec%u_ndof,cvec%nmodes))
    !$acc enter data create(cintegrand) async(cvec%id) if(cvec%on_gpu)
    !$acc kernels present(cintegrand) async(cvec%id) if(cvec%on_gpu)
    cintegrand=1._r8
    !$acc end kernels
    CALL cvec%zero
    CALL cvec%assemble(cintegrand)
    !$acc exit data delete(cintegrand) finalize async(cvec%id) if(cvec%on_gpu)
    DEALLOCATE(cintegrand)
  END SUBROUTINE test_assemble

!-------------------------------------------------------------------------------
!> Test the boundary and regularity conditions
!-------------------------------------------------------------------------------
  SUBROUTINE test_bc_regularity(rvec,cv1m,cvec,edge)
    USE vector_mod
    USE matrix_mod
    USE edge_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iq,nindex(cvec%nmodes),imode
    CHARACTER(64) :: bcfl
    REAL(r8) :: rscalar = 5._r8
    COMPLEX(r8) :: cscalar = CMPLX(6._r8,7._r8,r8)
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    bcfl=" "
    IF (rvec%nqty<3) THEN
      DO iq=1,rvec%nqty
        bcfl(3*(iq-1)+1:3*iq)="sd "
      ENDDO
    ELSE
      bcfl(1:4)="3vn "
      DO iq=4,rvec%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sd "
      ENDDO
    ENDIF
    CALL rvec%dirichlet_bc(bcfl,edge)
    CALL rvec%assign_for_testing('constant',rscalar)
    bcfl=" "
    IF (rvec%nqty<3) THEN
      DO iq=1,rvec%nqty
        bcfl(3*(iq-1)+1:3*iq)="sf "
      ENDDO
    ELSE
      bcfl(1:4)="3vt "
      DO iq=4,rvec%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sf "
      ENDDO
    ENDIF
    CALL rvec%dirichlet_bc(bcfl,edge,symm="t",seam_save=.TRUE.)
    CALL rvec%regularity(edge,'all')
!-------------------------------------------------------------------------------
!   complex single-mode vectors
!-------------------------------------------------------------------------------
    CALL cv1m%assign_for_testing('constant',cscalar)
    bcfl=" "
    IF (cv1m%nqty<3) THEN
      DO iq=1,cv1m%nqty
        bcfl(3*(iq-1)+1:3*iq)="sd "
      ENDDO
    ELSE
      bcfl(1:4)="3vn "
      DO iq=4,cv1m%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sd "
      ENDDO
    ENDIF
    CALL cv1m%dirichlet_bc(bcfl,edge)
    CALL cv1m%assign_for_testing('constant',cscalar)
    bcfl=" "
    IF (cv1m%nqty<3) THEN
      DO iq=1,cv1m%nqty
        bcfl(3*(iq-1)+1:3*iq)="sf "
      ENDDO
    ELSE
      bcfl(1:4)="3vt "
      DO iq=4,cv1m%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sf "
      ENDDO
    ENDIF
    CALL cv1m%dirichlet_bc(bcfl,edge,symm="t",seam_save=.TRUE.)
    CALL cv1m%regularity(edge,'all')
!-------------------------------------------------------------------------------
!   complex vectors
!-------------------------------------------------------------------------------
    CALL cvec%assign_for_testing('constant',cscalar)
    bcfl=" "
    IF (cvec%nqty<3) THEN
      DO iq=1,cvec%nqty
        bcfl(3*(iq-1)+1:3*iq)="sd "
      ENDDO
    ELSE
      bcfl(1:4)="3vn "
      DO iq=4,cvec%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sd "
      ENDDO
    ENDIF
    CALL cvec%dirichlet_bc(bcfl,edge)
    CALL cvec%assign_for_testing('constant',cscalar)
    bcfl=" "
    IF (cvec%nqty<3) THEN
      DO iq=1,cvec%nqty
        bcfl(3*(iq-1)+1:3*iq)="sf "
      ENDDO
    ELSE
      bcfl(1:4)="3vt "
      DO iq=4,cvec%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sf "
      ENDDO
    ENDIF
    CALL cvec%dirichlet_bc(bcfl,edge,symm="t",seam_save=.TRUE.)
    DO imode=1,cvec%nmodes
      nindex(imode)=imode-1
    ENDDO
    CALL cvec%regularity(edge,nindex,'all')
  END SUBROUTINE test_bc_regularity

!-------------------------------------------------------------------------------
!> test the loading and unloading the seam
!-------------------------------------------------------------------------------
  SUBROUTINE test_load_unload(rvec,cv1m,cvec,edge)
    USE vector_mod
    USE edge_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec
    TYPE(edge_type), INTENT(INOUT) :: edge

    REAL(r8) :: rscalar = 5._r8
    COMPLEX(r8) :: cscalar = CMPLX(6._r8,7._r8,r8)
    REAL(r8), ALLOCATABLE :: norm(:)
!-------------------------------------------------------------------------------
!   real vectors
!-------------------------------------------------------------------------------
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec%edge_load_arr(edge)
    CALL simple_edge_network_real(edge)
    CALL rvec%zero()
    CALL rvec%edge_unload_arr(edge)
    ALLOCATE(norm(rvec%inf_norm_size))
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"rvec edge_(un)load_arr")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec%edge_load_arr(edge,nqty=1,n_side=0)
    CALL simple_edge_network_real(edge)
    CALL rvec%zero()
    CALL rvec%edge_unload_arr(edge)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"rvec edge_(un)load_arr options")
    rscalar=rscalar+1._r8
    CALL rvec%assign_for_testing('constant',rscalar)
    CALL rvec%edge_load_save(edge,nqty=1,n_side=0)
    CALL rvec%zero()
    CALL rvec%edge_add_save(edge,nqty=1,n_side=0)
    norm=0._r8
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(rscalar,MAXVAL(norm),"rvec edge_(un)load_arr save")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   complex single-mode vectors
!-------------------------------------------------------------------------------
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m%edge_load_arr(edge)
    CALL simple_edge_network_comp(edge)
    CALL cv1m%zero()
    CALL cv1m%edge_unload_arr(edge)
    ALLOCATE(norm(cv1m%inf_norm_size))
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m edge_(un)load_arr")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m%edge_load_arr(edge,nqty=1,n_side=0)
    CALL simple_edge_network_comp(edge)
    CALL cv1m%zero()
    CALL cv1m%edge_unload_arr(edge)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m edge_(un)load_arr options")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cv1m%assign_for_testing('constant',cscalar)
    CALL cv1m%edge_load_save(edge,nqty=1,n_side=0)
    CALL cv1m%zero()
    CALL cv1m%edge_add_save(edge,nqty=1,n_side=0)
    norm=0._r8
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cv1m edge_(un)load_arr save")
    DEALLOCATE(norm)
!-------------------------------------------------------------------------------
!   complex vectors
!-------------------------------------------------------------------------------
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec%edge_load_arr(edge)
    CALL simple_edge_network_comp(edge)
    CALL cvec%zero()
    CALL cvec%edge_unload_arr(edge)
    ALLOCATE(norm(cvec%inf_norm_size))
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec edge_(un)load_arr")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec%edge_load_arr(edge,nqty=1,n_side=0)
    CALL simple_edge_network_comp(edge)
    CALL cvec%zero()
    CALL cvec%edge_unload_arr(edge)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec edge_(un)load_arr options")
    cscalar=cscalar+CMPLX(1._r8,1._r8,r8)
    CALL cvec%assign_for_testing('constant',cscalar)
    CALL cvec%edge_load_save(edge,nqty=1,n_side=0)
    CALL cvec%zero()
    CALL cvec%edge_add_save(edge,nqty=1,n_side=0)
    norm=0._r8
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(ABS(cscalar),MAXVAL(norm),"cvec edge_(un)load_arr save")
    DEALLOCATE(norm)
  END SUBROUTINE test_load_unload

!-------------------------------------------------------------------------------
!> single-block version of edge_network for real-type testing
!-------------------------------------------------------------------------------
  SUBROUTINE simple_edge_network_real(edge)
    USE edge_mod
    IMPLICIT NONE

    TYPE(edge_type), INTENT(INOUT) :: edge

    !$acc wait if(edge%on_gpu)
    ASSOCIATE(vert_out=>edge%vert_out,vert_in=>edge%vert_in)
      vert_out=vert_in
      !$acc update device(vert_out) async(edge%id) if(edge%on_gpu)
    END ASSOCIATE
    IF (edge%nside_loaded>0) THEN
      ASSOCIATE(seg_out=>edge%seg_out,seg_in=>edge%seg_in)
        seg_out=seg_in
        !$acc update device(seg_out) async(edge%id) if(edge%on_gpu)
      END ASSOCIATE
    ENDIF
  END SUBROUTINE simple_edge_network_real

!-------------------------------------------------------------------------------
!> single-block version of edge_network for complex-type testing
!-------------------------------------------------------------------------------
  SUBROUTINE simple_edge_network_comp(edge)
    USE edge_mod
    IMPLICIT NONE

    TYPE(edge_type), INTENT(INOUT) :: edge

    !$acc wait if(edge%on_gpu)
    ASSOCIATE(vert_cout=>edge%vert_cout,vert_cin=>edge%vert_cin)
      vert_cout=vert_cin
      !$acc update device(vert_cout) async(edge%id) if(edge%on_gpu)
    END ASSOCIATE
    IF (edge%nside_loaded>0) THEN
      ASSOCIATE(seg_cout=>edge%seg_cout,seg_cin=>edge%seg_cin)
        seg_cout=seg_cin
        !$acc update device(seg_cout) async(edge%id) if(edge%on_gpu)
      END ASSOCIATE
    ENDIF
  END SUBROUTINE simple_edge_network_comp

END PROGRAM test_vector
