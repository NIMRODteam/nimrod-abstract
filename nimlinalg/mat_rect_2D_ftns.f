!-------------------------------------------------------------------------------
!< module containing shared routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing shared routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_ftns_mod
  USE timer_mod
  IMPLICIT NONE

  CHARACTER(*), PARAMETER, PRIVATE :: mod_name='mat_rect_2D_ftns_mod'
CONTAINS

!-------------------------------------------------------------------------------
!> initialize arrays that help translate element-based data.
!-------------------------------------------------------------------------------
  SUBROUTINE matrix_rbl_dof_init(dib,div,dix,diy,diq,nqc,pdc,nqd,nbd)
    USE local
    IMPLICIT NONE

    INTEGER(i4), DIMENSION(:), INTENT(OUT) :: dib,div,dix,diy,diq
    INTEGER(i4), INTENT(IN) :: nqc,pdc,nqd,nbd

    INTEGER(i4) :: ix,iy,iq,ii,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matrix_rbl_dof_init',iftn,idepth)
!-------------------------------------------------------------------------------
!   fill the dof arrays that are used to generate indices
!   when transferring information into rmat arrays.
!-------------------------------------------------------------------------------
    ii=1
    DO iy=0,1        !     4 continuous grid-centered bases
      DO ix=0,1
        DO iq=1,nqc
          dib(ii)=ix+2*iy+1
          div(ii)=iq
          dix(ii)=ix-1
          diy(ii)=iy-1
          diq(ii)=iq
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,pdc-2    !     continuous horizontal side bases
      DO iy=0,1
        DO iq=1,nqc
          dib(ii)=iy+2*id+5
          div(ii)=iq
          dix(ii)=0
          diy(ii)=iy-1
          diq(ii)=iq+nqc*id
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,pdc-2    !     continuous vertical side bases
      DO ix=0,1
        DO iq=1,nqc
          dib(ii)=ix+2*id+5+2*(pdc-1)
          div(ii)=iq
          dix(ii)=ix-1
          diy(ii)=0
          diq(ii)=iq+nqc*id
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,(pdc-1)**2-1  !  interior bases for continuous expansions
      DO iq=1,nqc
        dib(ii)=id+1+4*pdc
        div(ii)=iq
        dix(ii)=0
        diy(ii)=0
        diq(ii)=iq+nqc*id
        ii=ii+1
      ENDDO
    ENDDO
    DO id=0,nbd-1    !  bases for discontinuous expansions
      DO iq=1,nqd
        dib(ii)=id+1+MIN(nqc,1_i4)*(pdc+1)**2  ! continue interior #s
        div(ii)=iq
        dix(ii)=0
        diy(ii)=0
        diq(ii)=iq+nqd*id+nqc*(pdc-1)**2
        ii=ii+1
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matrix_rbl_dof_init

!-------------------------------------------------------------------------------
!> sets the DOF map for 2D types. This routine determines both the total number
! of DOFs and also the number of nonzeros (nnz) in the sparse matrix associated
! with each DOF.
!-------------------------------------------------------------------------------
  SUBROUTINE init_2D_dof_map(arr,arr_nnz,id,pd,edge,skip_elim_interior,         &
                             ndof_local,self_per_x,self_per_y,                  &
                             arrh,arrv,arri,arrh_nnz,arrv_nnz,arri_nnz)
    USE edge_mod
    USE local
    USE pardata_mod
    IMPLICIT NONE

    !> vertex array
    INTEGER(i4), INTENT(INOUT) :: arr(:,0:,0:)
    !> vertex array to fill with nnz
    INTEGER(i4), INTENT(INOUT) :: arr_nnz(:,0:,0:)
    !> block ID
    INTEGER(i4), INTENT(IN) :: id
    !> element polynomial degree
    INTEGER(i4), INTENT(IN) :: pd
    !> edge type associate with block
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> true if interior DOFs are eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> local number of DOFs in this block
    INTEGER(i4), INTENT(OUT) :: ndof_local
    !> if self-periodic in x
    LOGICAL, INTENT(INOUT) :: self_per_x
    !> if self-periodic in y
    LOGICAL, INTENT(INOUT) :: self_per_y
    !> horizontal array
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrh(:,:,1:,0:)
    !> vertical array
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrv(:,:,0:,1:)
    !> interior array
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arri(:,:,1:,1:)
    !> horizontal array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrh_nnz(:,:,1:,0:)
    !> vertical array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrv_nnz(:,:,0:,1:)
    !> interior array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arri_nnz(:,:,1:,1:)

    INTEGER(i4) :: nq,mx,my,iv,ix,iy,min_blk,ivp
    INTEGER(i4) :: iptr,nptr,n_side,n_int,ii,is,iq,idof
    LOGICAL :: vert_owned
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_2D_dof_map',iftn,idepth)
    nq=SIZE(arr,1)
    mx=SIZE(arr,2)-1
    my=SIZE(arr,3)-1
!-------------------------------------------------------------------------------
!   determine which nodes are considered to be owned by each block, based on the
!   global block number. Tag owned blocks with 1, otherwise tag them zero. The
!   lowest block ID owns each DOF.  also set the number of non-zeros at each
!   point in arr_nnz, arrh_nnz and arrv_nnz
!-------------------------------------------------------------------------------
    arr=1
    arr_nnz=nq*(4*(pd+1)**2-4*(pd+1)+1)
    IF (PRESENT(arrh)) THEN
      arrh=1
      arrv=1
      arrh_nnz=nq*(2*(pd+1)**2-(pd+1))
      arrv_nnz=nq*(2*(pd+1)**2-(pd+1))
      IF (skip_elim_interior) THEN
        arri=0
        arri_nnz=0
      ELSE
        arri=1
        arri_nnz=nq*(pd+1)**2
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   vertex nodes have nnz=nel*(pd+1)**2-(nel-nbdry/2)*(pd+1)+(1-nbdry/2)
!   where nel is the number of elements the node belongs to and nbdry is the
!   number of exterior boundaries for theses elements. load the local nel and
!   nbdry into the seam in preparation to determine these totals after
!   communication with neighboring blocks
!-------------------------------------------------------------------------------
    edge%nqty_loaded=2
    edge%nside_loaded=0
    edge%nmodes_loaded=0
    DO iv=1,edge%nvert
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      vert_owned=.TRUE.
      IF (SIZE(edge%vertex(iv)%ptr2(1,:))>0) THEN
        min_blk=MINVAL(edge%vertex(iv)%ptr2(1,:))
        IF (id>min_blk) vert_owned=.FALSE.
      ENDIF
      IF (.NOT.vert_owned) arr(:,ix,iy)=0
      IF ((ix==0.AND.iy==0).OR.(ix==0.AND.iy==my).OR.                           &
          (ix==mx.AND.iy==0).OR.(ix==mx.AND.iy==my)) THEN
        ! corner node
        edge%vert_in(1,iv)=1
      ELSE
        ! node on side of block
        edge%vert_in(1,iv)=2
      ENDIF
      ivp=iv+1
      IF (ivp>edge%nvert) ivp=1
      edge%vert_in(2,iv)=0
      IF (edge%exsegment(iv).OR.edge%r0segment(iv))                             &
        edge%vert_in(2,iv)=edge%vert_in(2,iv)+1
      IF (edge%exsegment(ivp).OR.edge%r0segment(ivp))                           &
        edge%vert_in(2,iv)=edge%vert_in(2,iv)+1
      edge%nqty_loaded=2
      edge%nside_loaded=0
      edge%nmodes_loaded=0
!-------------------------------------------------------------------------------
!     side nodes have all information needed to determine nnz
!-------------------------------------------------------------------------------
      IF (PRESENT(arrh)) THEN
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%exsegment(iv).OR.edge%r0segment(iv)) THEN
          ! Node touches boundary
          IF (edge%segment(iv)%h_side) THEN
            arrh_nnz(:,:,ix,iy)=nq*(pd+1)**2
          ELSE
            arrv_nnz(:,:,ix,iy)=nq*(pd+1)**2
          ENDIF
        ELSE IF (id>edge%segment(iv)%ptr(1).AND.                                &
                 edge%segment(iv)%ptr(1)/=0) THEN
          ! Node belongs to another process
          IF (edge%segment(iv)%h_side) THEN
            arrh(:,:,ix,iy)=0
            arrh_nnz(:,:,ix,iy)=0
          ELSE
            arrv(:,:,ix,iy)=0
            arrv_nnz(:,:,ix,iy)=0
          ENDIF
        ENDIF
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   special considerations for self-periodic blocks
!-------------------------------------------------------------------------------
    nptr=0
    DO iptr=1,edge%vertex(edge%nvert)%nimage
      IF (id==edge%vertex(edge%nvert)%ptr2(1,iptr)) nptr=nptr+1
    ENDDO
    IF (nptr==3) THEN
      ! periodic in x and y directions
      arr(:,mx,:)=0
      arr(:,:,my)=0
      arr_nnz(:,mx,:)=0
      arr_nnz(:,:,my)=0
      IF (PRESENT(arrh)) THEN
        arrv(:,:,mx,:)=0
        arrh(:,:,:,my)=0
        arrv_nnz(:,:,mx,:)=0
        arrh_nnz(:,:,:,my)=0
      ENDIF
      self_per_x=.TRUE.
      self_per_y=.TRUE.
    ELSEIF (nptr==1) THEN
      ! periodic in y
      arr(:,:,my)=0
      arr_nnz(:,:,my)=0
      IF (PRESENT(arrh)) THEN
        arrh(:,:,:,my)=0
        arrh_nnz(:,:,:,my)=0
      ENDIF
      self_per_y=.TRUE.
    ENDIF
!-------------------------------------------------------------------------------
!   calculate the # of DOF for this block
!-------------------------------------------------------------------------------
    ndof_local=SUM(arr)
    IF (PRESENT(arrh)) THEN
      ndof_local=ndof_local+SUM(arrh)+SUM(arrv)
      IF (.NOT.skip_elim_interior) ndof_local=ndof_local+SUM(arri)
    ENDIF
!-------------------------------------------------------------------------------
!   loop over all finite element nodes and assign local zero-index DOF indices.
!
!   the desired basis type order is numerically decreasing so that
!   the interior, vertical side, horizontal side, and grid vertex
!   bases are picked up in that order for each element.
!
!   assign non-local or eliminated DOFs a value of -1
!-------------------------------------------------------------------------------
    IF (PRESENT(arrh)) THEN
      n_side=SIZE(arrh,2)
      n_int=SIZE(arri,2)
    ELSE
      n_side=0
      n_int=0
    ENDIF
    idof=0
    IF (skip_elim_interior) arri=-1
    DO iy=0,my
      DO ix=0,mx
        IF (.NOT.skip_elim_interior) THEN
          IF (ix/=mx.AND.iy/=my) THEN
            DO ii=1,n_int
              DO iq=1,nq
                arri(iq,ii,ix+1,iy+1)=idof
                idof=idof+1
              ENDDO
            ENDDO
          ENDIF
        ENDIF
        IF (iy/=my) THEN
          DO is=1,n_side
            DO iq=1,nq
              IF (arrv(iq,is,ix,iy+1)==1) THEN
                arrv(iq,is,ix,iy+1)=idof
                idof=idof+1
              ELSE
                arrv(iq,is,ix,iy+1)=-1
              ENDIF
            ENDDO
          ENDDO
        ENDIF
        IF (ix/=mx) THEN
          DO is=1,n_side
            DO iq=1,nq
              IF (arrh(iq,is,ix+1,iy)==1) THEN
                arrh(iq,is,ix+1,iy)=idof
                idof=idof+1
              ELSE
                arrh(iq,is,ix+1,iy)=-1
              ENDIF
            ENDDO
          ENDDO
        ENDIF
        DO iq=1,nq
          IF (arr(iq,ix,iy)==1) THEN
            arr(iq,ix,iy)=idof
            idof=idof+1
          ELSE
            arr(iq,ix,iy)=-1
          ENDIF
        ENDDO
      ENDDO
    ENDDO
    IF (idof/=ndof_local) CALL par%nim_stop('Miscount in init_2D_dof_map')
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_2D_dof_map

!-------------------------------------------------------------------------------
!> Finalize the initialization of the the number of nonzeros (nnz) in the sparse
! matrix associated with each DOF. The values associated with the vertices
! need to be computed from nel and nbdry stored in the seams. Here nel is the
! number of elements the node belongs to and nbdry is the number of exterior
! boundaries for theses elements.
!-------------------------------------------------------------------------------
  SUBROUTINE init_2D_nnz_map(arr_nnz,id,pd,edge,skip_elim_interior,nnz_local,   &
                             arrh_nnz,arrv_nnz,arri_nnz)
    USE edge_mod
    USE local
    USE pardata_mod
    IMPLICIT NONE

    !> vertex array to fill with nnz
    INTEGER(i4), INTENT(INOUT) :: arr_nnz(:,0:,0:)
    !> block ID
    INTEGER(i4), INTENT(IN) :: id
    !> element polynomial degree
    INTEGER(i4), INTENT(IN) :: pd
    !> edge type associate with block
    TYPE(edge_type), INTENT(IN) :: edge
    !> true if interior DOFs are eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> local number of nonzeros in this block
    INTEGER(i4), INTENT(OUT) :: nnz_local
    !> horizontal array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrh_nnz(:,:,1:,0:)
    !> vertical array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrv_nnz(:,:,0:,1:)
    !> interior array to fill with nnz
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arri_nnz(:,:,1:,1:)

    INTEGER(i4) :: nq,mx,my,iv,ix,iy,min_blk
    INTEGER(i4) :: nel,nbdry,iptr,nptr
    LOGICAL :: vert_owned
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_2D_nnz_map',iftn,idepth)
    nq=SIZE(arr_nnz,1)
    mx=SIZE(arr_nnz,2)-1
    my=SIZE(arr_nnz,3)-1
!-------------------------------------------------------------------------------
!   vertex nodes have nnz=nel*(pd+1)**2-(nel-nbdry/2)*(pd+1)+(1-nbdry/2)
!   where nel is the number of elements the node belongs to and nbdry is the
!   number of exterior boundaries for these elements.  unload these from
!   the seam
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      vert_owned=.TRUE.
      IF (SIZE(edge%vertex(iv)%ptr2(1,:))>0) THEN
        min_blk=MINVAL(edge%vertex(iv)%ptr2(1,:))
        IF (id>min_blk) vert_owned=.FALSE.
      ENDIF
      IF (vert_owned) THEN
        nel=INT(edge%vert_out(1,iv),i4)
        nbdry=INT(edge%vert_out(2,iv),i4)
        arr_nnz(:,ix,iy)=nq*(nel*(pd+1)**2-(nel-nbdry/2)*(pd+1)+(1-nbdry/2))
      ELSE
        arr_nnz(:,ix,iy)=0
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   special considerations for self-periodic blocks
!-------------------------------------------------------------------------------
    nptr=0
    DO iptr=1,edge%vertex(edge%nvert)%nimage
      IF (id==edge%vertex(edge%nvert)%ptr2(1,iptr)) nptr=nptr+1
    ENDDO
    IF (nptr==3) THEN
      ! periodic in x and y directions
      IF (mx<=2.AND.my<=2) THEN
        arr_nnz=MAX(arr_nnz-nq*2*(2*(pd+1)-1)+nq,0_i4)
        IF (PRESENT(arrh_nnz)) THEN
          arrh_nnz=MAX(arrh_nnz-(pd+1),0_i4)
          arrv_nnz=MAX(arrv_nnz-(pd+1),0_i4)
        ENDIF
      ELSE IF (mx<=2) THEN
        arr_nnz=MAX(arr_nnz-nq*2*(pd+1)+nq,0_i4)
        IF (PRESENT(arrh_nnz)) THEN
          arrv_nnz=MAX(arrv_nnz-(pd+1),0_i4)
        ENDIF
      ELSE IF (my<=2) THEN
        arr_nnz=MAX(arr_nnz-nq*2*(pd+1)+nq,0_i4)
        IF (PRESENT(arrh_nnz)) THEN
          arrh_nnz=MAX(arrh_nnz-(pd+1),0_i4)
        ENDIF
      ENDIF
      arr_nnz(:,mx,:)=0
      arr_nnz(:,:,my)=0
      IF (PRESENT(arrh_nnz)) THEN
        arrv_nnz(:,:,mx,:)=0
        arrh_nnz(:,:,:,my)=0
      ENDIF
    ELSEIF (nptr==1) THEN
      ! periodic in y
      IF (my<=2) THEN
        arr_nnz=MAX(arr_nnz-nq*2*(pd+1)+nq,0_i4)
        DO iv=1,edge%nvert
          IF (edge%expoint(iv).OR.edge%r0point(iv)) THEN
            ix=edge%vertex(iv)%intxy(1)
            iy=edge%vertex(iv)%intxy(2)
            IF (arr_nnz(1,ix,iy)>0) THEN
              arr_nnz(:,ix,iy)=arr_nnz(:,ix,iy)+nq*pd
            ENDIF
          ENDIF
        ENDDO
        !DO ix=1,mx-1
        !  arr_nnz(:,ix,:)=MAX(arr_nnz(:,ix,:)-nq*2*(pd+1)+nq,0_i4)
        !ENDDO
        !arr_nnz(:,0,:)=MAX(arr_nnz(:,0,:)-nq*(pd+1),0_i4)
        !arr_nnz(:,mx,:)=MAX(arr_nnz(:,mx,:)-nq*(pd+1),0_i4)
        IF (PRESENT(arrh_nnz)) THEN
          arrh_nnz=MAX(arrh_nnz-nq*(pd+1),0_i4)
        ENDIF
      ENDIF
      arr_nnz(:,:,my)=0
      IF (PRESENT(arrh_nnz)) THEN
        arrh_nnz(:,:,:,my)=0
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   calculate the # of nonzeros for this block
!-------------------------------------------------------------------------------
    nnz_local=SUM(arr_nnz)
    IF (PRESENT(arrh_nnz)) THEN
      nnz_local=nnz_local+SUM(arrh_nnz)+SUM(arrv_nnz)
      IF (.NOT.skip_elim_interior) nnz_local=nnz_local+SUM(arri_nnz)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_2D_nnz_map

!-------------------------------------------------------------------------------
!* Set the values of the ptr array of the compressed matrix sparsity pattern.
!-------------------------------------------------------------------------------
  SUBROUTINE init_2D_ptr(arr_nnz,sp,mat_conv,skip_elim_interior,                &
                         arrh_nnz,arrv_nnz,arri_nnz)
    USE compressed_conversion_mod
    USE compressed_matrix_real_mod
    USE local
    USE pardata_mod
    IMPLICIT NONE

    !> vertex array with nnz per dof
    INTEGER(i4), INTENT(IN) :: arr_nnz(:,0:,0:)
    !> compressed sparse matrix sparsity pattern
    TYPE(compressed_matrix_sparsity_pattern), INTENT(INOUT) :: sp
    !> compressed conversion type for this block-matrix
    TYPE(compressed_conversion_type), INTENT(IN) :: mat_conv
    !> true if interior DOFs are eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> horizontal array with nnz per dof
    INTEGER(i4), OPTIONAL, INTENT(IN) :: arrh_nnz(:,:,1:,0:)
    !> vertical array with nnz per dof
    INTEGER(i4), OPTIONAL, INTENT(IN) :: arrv_nnz(:,:,0:,1:)
    !> interior array with nnz per dof
    INTEGER(i4), OPTIONAL, INTENT(IN) :: arri_nnz(:,:,1:,1:)

    INTEGER(i4) :: nq,mx,my,n_side,n_int
    INTEGER(i4) :: idof,iy,ix,ii,is,iq,nnz_local
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_2D_ptr',iftn,idepth)
    nq=SIZE(arr_nnz,1)
    mx=SIZE(arr_nnz,2)-1
    my=SIZE(arr_nnz,3)-1
!-------------------------------------------------------------------------------
!   set sp%ptr(idof+1) to nnz_map(idof) using the same ordering as used to set
!   dof_map
!-------------------------------------------------------------------------------
    IF (PRESENT(arrh_nnz)) THEN
      n_side=SIZE(arrh_nnz,2)
      n_int=SIZE(arri_nnz,2)
    ELSE
      n_side=0
      n_int=0
    ENDIF
    nnz_local=SUM(arr_nnz)
    IF (PRESENT(arrh_nnz)) THEN
      nnz_local=nnz_local+SUM(arrh_nnz)+SUM(arrv_nnz)
      IF (.NOT.skip_elim_interior) nnz_local=nnz_local+SUM(arri_nnz)
    ENDIF
    sp%ptr(mat_conv%firstrow)=mat_conv%firstnz
    idof=mat_conv%firstrow+1
    DO iy=0,my
      DO ix=0,mx
        IF (.NOT.skip_elim_interior) THEN
          IF (ix/=mx.AND.iy/=my) THEN
            DO ii=1,n_int
              DO iq=1,nq
                IF (arri_nnz(iq,ii,ix+1,iy+1)>0) THEN
                  sp%ptr(idof)=arri_nnz(iq,ii,ix+1,iy+1)
                  idof=idof+1
                ENDIF
              ENDDO
            ENDDO
          ENDIF
        ENDIF
        IF (iy/=my) THEN
          DO is=1,n_side
            DO iq=1,nq
              IF (arrv_nnz(iq,is,ix,iy+1)>0) THEN
                sp%ptr(idof)=arrv_nnz(iq,is,ix,iy+1)
                idof=idof+1
              ENDIF
            ENDDO
          ENDDO
        ENDIF
        IF (ix/=mx) THEN
          DO is=1,n_side
            DO iq=1,nq
              IF (arrh_nnz(iq,is,ix+1,iy)>0) THEN
                sp%ptr(idof)=arrh_nnz(iq,is,ix+1,iy)
                idof=idof+1
              ENDIF
            ENDDO
          ENDDO
        ENDIF
        DO iq=1,nq
          IF (arr_nnz(iq,ix,iy)>0) THEN
            sp%ptr(idof)=arr_nnz(iq,ix,iy)
            idof=idof+1
          ENDIF
        ENDDO
      ENDDO
    ENDDO
    IF (idof/=mat_conv%lastrow+2) CALL par%nim_stop('Miscount in init_2D_ptr')
!-------------------------------------------------------------------------------
!   Next do a prefix sum to determine ptr
!-------------------------------------------------------------------------------
    DO idof=mat_conv%firstrow+1,mat_conv%lastrow+1
      sp%ptr(idof)=sp%ptr(idof)+sp%ptr(idof-1)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_2D_ptr

!-------------------------------------------------------------------------------
!> get the number of sends for this block
!-------------------------------------------------------------------------------
  SUBROUTINE get_2D_send_counts(arr,id,edge,start_rows,mat_conv,arrh,arrv)
    USE compressed_conversion_mod
    USE edge_mod
    USE local
    USE pardata_mod
    IMPLICIT NONE

    !> vertex array
    INTEGER(i4), INTENT(INOUT) :: arr(:,0:,0:)
    !> block ID
    INTEGER(i4), INTENT(IN) :: id
    !> edge type associate with block
    TYPE(edge_type), INTENT(IN) :: edge
    !> starting rows in DOF map for each global block
    INTEGER(i4), INTENT(IN) :: start_rows(par%nbl_total)
    !> matrix conversion type to modify
    TYPE(compressed_conversion_type), INTENT(INOUT) :: mat_conv
    !> horizontal array
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrh(:,:,1:,0:)
    !> vertical array
    INTEGER(i4), OPTIONAL, INTENT(INOUT) :: arrv(:,:,0:,1:)

    INTEGER(i4) :: iv,ix,iy,min_blk
    INTEGER(i4) :: ibl,isend,irecv
    INTEGER(i4) :: send_blocks(par%nbl_total),recv_blocks(par%nbl_total)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_2D_send_counts',iftn,idepth)
    send_blocks=0_i4
    recv_blocks=0_i4
!-------------------------------------------------------------------------------
!   loop over all edge finite element nodes and look for sent and received DOF
!   indices.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      IF (arr(1,ix,iy)<mat_conv%firstrow) THEN
        min_blk=MINVAL(edge%vertex(iv)%ptr2(1,:))
        send_blocks(min_blk)=1_i4
      ENDIF
      IF (arr(1,ix,iy)>=mat_conv%firstrow) THEN
        DO ibl=1,SIZE(edge%vertex(iv)%ptr2(1,:))
          IF (edge%vertex(iv)%ptr2(1,ibl)/=id) THEN
            recv_blocks(edge%vertex(iv)%ptr2(1,ibl))=1_i4
          ENDIF
        ENDDO
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   special considerations for self-periodic blocks
!-------------------------------------------------------------------------------
    IF (mat_conv%self_per_x.OR.mat_conv%self_per_y) THEN
      send_blocks(id)=1_i4
      recv_blocks(id)=1_i4
    ENDIF
!-------------------------------------------------------------------------------
!   allocate send/recv arrays
!-------------------------------------------------------------------------------
    mat_conv%nsend=SUM(send_blocks)
    ALLOCATE(mat_conv%blk_send(mat_conv%nsend))
    ALLOCATE(mat_conv%proc_send(mat_conv%nsend))
    ALLOCATE(mat_conv%rowstart_send(mat_conv%nsend))
    ALLOCATE(mat_conv%nnz_send(mat_conv%nsend))
    mat_conv%nnz_send=0
    mat_conv%nrecv=SUM(recv_blocks)
    ALLOCATE(mat_conv%blk_recv(mat_conv%nrecv))
    ALLOCATE(mat_conv%proc_recv(mat_conv%nrecv))
    ALLOCATE(mat_conv%nnz_recv(mat_conv%nrecv))
    ALLOCATE(mat_conv%ival_recv(mat_conv%nrecv))
    isend=1
    irecv=1
    DO ibl=1,par%nbl_total
      IF (send_blocks(ibl)>0) THEN
        mat_conv%blk_send(isend)=ibl
        mat_conv%proc_send(isend)=par%block2proc(ibl)
        mat_conv%rowstart_send(isend)=start_rows(ibl)
        isend=isend+1
      ENDIF
      IF (recv_blocks(ibl)>0) THEN
        mat_conv%blk_recv(irecv)=ibl
        mat_conv%proc_recv(irecv)=par%block2proc(ibl)
        irecv=irecv+1
      ENDIF
    ENDDO
    IF (isend-1/=mat_conv%nsend)                                                &
      CALL par%nim_stop('Miscount isend in get_2D_send_counts')
    IF (irecv-1/=mat_conv%nrecv)                                                &
      CALL par%nim_stop('Miscount irecv in get_2D_send_counts')
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_2D_send_counts

END MODULE mat_rect_2D_ftns_mod
