!-------------------------------------------------------------------------------
!< module containing routines that performs standard operations on
!  real rectilinear matrices.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing routines that performs standard operations on
!  real rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_real_mod
  USE local
  USE matrix_mod
  USE pardata_mod
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: mat_rect_2D_real

  CHARACTER(*), PARAMETER :: mod_name='mat_rect_2D_real'
!-------------------------------------------------------------------------------
!* holds rblock real matrix elements.
!-------------------------------------------------------------------------------
  TYPE :: arr_6d_type
    !> arrays of size (nq_type,x0off:x1off,y0off:y1off,nq_type,ix0:mx,iy0:my)
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: arr
  END TYPE arr_6d_type

!-------------------------------------------------------------------------------
!* 2D rectangular matrix algebra implementation with a real data type
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(rmatrix) :: mat_rect_2D_real
    !> number of continuous quantites
    INTEGER(i4) :: nqcon
    !> poly degree of continuous basis (rename)
    INTEGER(i4) :: pdcon
    !> number of discontinuous quantites
    INTEGER(i4) :: nqdis
    !> number of discontinuous basis
    INTEGER(i4) :: nbdisc
    !> unit number of continuous degrees of freedom (per element for a scalar)
    INTEGER(i4) :: u_ndof_cont
    !> unit number of discontinuous degrees of freedom
    !  (per element for a scalar)
    INTEGER(i4) :: u_ndof_disc
    !> number of basis types (1: interior only or 4: vert, side, int)
    INTEGER(i4) :: nbtype
    !> number of elements in the logical x direction
    INTEGER(i4) :: mx
    !> number of elements in the logical y direction
    INTEGER(i4) :: my
    !> starting logical-x element index by basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ix0
    !> starting logical-y element index by basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: iy0
    !> ending logical-x element index by basis type for row matrix export
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ix1
    !> ending logical-y element index by basis type for row matrix export
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: iy1
    !> ending logical-x element index by basis type for column matrix export
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: jx1
    !> ending logical-y element index by basis type for column matrix export
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: jy1
    !> number of degrees of freedom for each basis type; this is nqty for
    !  vertices, nqty*nside for sides and nqty*nint for interior nodes
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nq_type
    !> number of boundary degrees of freedom for each basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nb_type
    !> matrix storage by basis type connectivity by basis type pairs
    TYPE(arr_6d_type), DIMENSION(:,:), ALLOCATABLE :: bsc
    !> arrays for indirect memory access patterns
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: dof_ib,dof_iv,dof_ix,dof_iy,dof_iq
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: den_type
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(mat) :: alloc => alloc_real
    ! Abstract class deferred functions
    PROCEDURE, PASS(mat) :: dealloc => dealloc_real
    PROCEDURE, PASS(mat) :: matvec => matvec_real
    PROCEDURE, PASS(mat) :: assemble => assemble_real
    PROCEDURE, PASS(mat) :: zero => zero_real
    PROCEDURE, PASS(mat) :: elim_inv_int => elim_inv_int_real
    PROCEDURE, PASS(mat) :: elim_presolve => elim_presolve_real
    PROCEDURE, PASS(mat) :: elim_postsolve => elim_postsolve_real
    PROCEDURE, PASS(mat) :: find_diag_scale => find_diag_scale_real
    PROCEDURE, PASS(mat) :: dirichlet_bc => dirichlet_bc_real
    PROCEDURE, PASS(mat) :: regularity => regularity_real
    PROCEDURE, PASS(mat) :: get_diag_as_vec => get_diag_as_vec_real
    PROCEDURE, PASS(mat) :: set_identity => set_identity_real
    PROCEDURE, PASS(mat) :: init_dof_map => init_dof_map_real
    PROCEDURE, PASS(mat) :: init_nnz_map => init_nnz_map_real
    PROCEDURE, PASS(mat) :: init_local_sparsity => init_local_sparsity_real
    PROCEDURE, PASS(mat) :: send_comm_sparsity => send_comm_sparsity_real
    PROCEDURE, PASS(mat) :: set_comm_sparsity => set_comm_sparsity_real
    PROCEDURE, PASS(mat) :: init_matrix_to_compressed_comm =>                   &
                              init_matrix_to_compressed_comm_real
    PROCEDURE, PASS(mat) :: send_matrix_to_compressed_comm =>                   &
                              send_matrix_to_compressed_comm_real
    PROCEDURE, PASS(mat) :: finalize_matrix_to_compressed_comm =>               &
                              finalize_matrix_to_compressed_comm_real
    PROCEDURE, PASS(mat) :: export_matrix_to_compressed =>                      &
                              export_matrix_to_compressed_real
  END TYPE mat_rect_2D_real

CONTAINS

!-------------------------------------------------------------------------------
!* allocate arrays needed for a real rblock matrix.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(mat,pdc,mx,my,nqc,id,vcomp,nqdin,nbdin)
    USE mat_rect_2D_ftns_mod
    IMPLICIT NONE

    !> matrix to allocate
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> polynomial degree (continuous basis)
    INTEGER(i4), INTENT(IN) :: pdc
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities (continuous basis)
    INTEGER(i4), INTENT(IN) :: nqc
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id
    !> character describing vector components / scalar
    CHARACTER(1), INTENT(IN) :: vcomp(:)
    !> number of quantities (discontinuous basis)
    INTEGER(i4), INTENT(IN), OPTIONAL :: nqdin
    !> number of discontinuous basis
    INTEGER(i4), INTENT(IN), OPTIONAL :: nbdin

    INTEGER(i4) :: ib,jb,ixst,iyst,jxst,jyst,x0off,x1off,y0off,y1off,nqd,nbd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
    IF (PRESENT(nqdin)) THEN
      nqd=nqdin
    ELSE
      nqd=0_i4
    ENDIF
    IF (PRESENT(nbdin)) THEN
      nbd=nbdin
    ELSE
      nbd=0_i4
    ENDIF
    mat%nqty=nqc+nqd
    mat%nqcon=nqc
    mat%pdcon=pdc
    mat%nel=mx*my
    mat%ndim=2
    mat%nqdis=nqd
    mat%nbdisc=nbd
    mat%id=id
    ALLOCATE(mat%vcomp(mat%nqty))
    mat%vcomp=vcomp
    mat%eliminated=.FALSE.
    mat%diag_scale=1._r8
    mat%essential_cond="none"
!-------------------------------------------------------------------------------
!   the mat array allows multiple basis function types (grid vertex,
!   horizontal element side, vertical element side, and interior-
!   centered).
!
!   the structures accommodate equations with nqc continuous fields
!   of polynomial degree pdc and nqd discontinuous fields with
!   nbd basis functions.  the storage for 'element interiors'
!   is used for both the interior coefficients of continuous fields
!   and for all coefficients of discontinuous fields.
!
!   if poly_degree=1, there is only one basis type.  otherwise, there
!   are 4.
!-------------------------------------------------------------------------------
    mat%u_ndof_cont=nqc*(pdc+1)**2
    mat%u_ndof_disc=nqd*nbd
    mat%u_ndof=mat%u_ndof_cont+mat%u_ndof_disc
    IF (pdc==1.AND.nqd==0) THEN
      mat%nbtype=1
    ELSE
      mat%nbtype=4
    ENDIF
    mat%mx=mx
    mat%my=my
    ALLOCATE(mat%bsc(mat%nbtype,mat%nbtype))
    ALLOCATE(mat%ix0(mat%nbtype))
    ALLOCATE(mat%iy0(mat%nbtype))
    ALLOCATE(mat%ix1(mat%nbtype))
    ALLOCATE(mat%iy1(mat%nbtype))
    ALLOCATE(mat%jx1(mat%nbtype))
    ALLOCATE(mat%jy1(mat%nbtype))
    ALLOCATE(mat%nb_type(mat%nbtype))
    ALLOCATE(mat%nq_type(mat%nbtype))
    ALLOCATE(mat%den_type(mat%nbtype))
    ALLOCATE(mat%dof_ib(mat%u_ndof))
    ALLOCATE(mat%dof_iv(mat%u_ndof))
    ALLOCATE(mat%dof_ix(mat%u_ndof))
    ALLOCATE(mat%dof_iy(mat%u_ndof))
    ALLOCATE(mat%dof_iq(mat%u_ndof))
!-------------------------------------------------------------------------------
!   logical indices for each of the basis types.
!   -  index 1 is grid vertex-centered.
!   -  index 2 is horizontal side-centered.
!   -  index 3 is vertical side-centered.
!   -  index 4 is interior-centered and all discontinuous bases.
!
!   this version has the 6D matrix array indices defined
!   (col_comp,col_x_off,col_y_off,row_comp,row_x_index,row_y_index),
!   where comp is vector component and basis for types with multiple
!   bases (vector component varying faster) and off is the offset
!   from the row index.
!-------------------------------------------------------------------------------
    DO ib=1,mat%nbtype
      SELECT CASE(ib)
      CASE(1)
        mat%ix0(ib)=0
        mat%iy0(ib)=0
        mat%nb_type(ib)=MIN(nqc,1_i4)
        mat%nq_type(ib)=nqc
        mat%den_type(ib)=4*nqc
      CASE(2)
        mat%ix0(ib)=1
        mat%iy0(ib)=0
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)
        mat%nq_type(ib)=nqc*(pdc-1)
        mat%den_type(ib)=mat%den_type(ib-1)+2*nqc*(pdc-1)
      CASE(3)
        mat%ix0(ib)=0
        mat%iy0(ib)=1
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)
        mat%nq_type(ib)=nqc*(pdc-1)
        mat%den_type(ib)=mat%den_type(ib-1)+2*nqc*(pdc-1)
      CASE(4)
        mat%ix0(ib)=1
        mat%iy0(ib)=1
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)**2+MIN(nqd,1_i4)*nbd
        mat%nq_type(ib)=nqc*(pdc-1)**2+nqd*nbd
        mat%den_type(ib)=mat%den_type(ib-1)+mat%nq_type(ib)
      END SELECT
    ENDDO
    DO ib=1,mat%nbtype
      ixst=mat%ix0(ib)
      iyst=mat%iy0(ib)
      DO jb=1,mat%nbtype
        jxst=mat%ix0(jb)
        jyst=mat%iy0(jb)
        x0off=jxst-1
        x1off=1-ixst
        y0off=jyst-1
        y1off=1-iyst
        ALLOCATE(mat%bsc(jb,ib)%arr(mat%nq_type(jb),x0off:x1off,y0off:y1off,    &
                                      mat%nq_type(ib),ixst:mx,iyst:my))
        mat%bsc(jb,ib)%arr=0
      ENDDO
    ENDDO
    CALL matrix_rbl_dof_init(mat%dof_ib,mat%dof_iv,mat%dof_ix,                  &
                             mat%dof_iy,mat%dof_iq,nqc,pdc,nqd,nbd)
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(mat%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(mat%ix0)+SIZEOF(mat%iy0)+SIZEOF(mat%nb_type)               &
             +SIZEOF(mat%ix1)+SIZEOF(mat%iy1)+SIZEOF(mat%jx1)+SIZEOF(mat%jy1)   &
             +SIZEOF(mat%nq_type)+SIZEOF(mat%dof_ib)+SIZEOF(mat%dof_ix)         &
             +SIZEOF(mat%dof_iy)+SIZEOF(mat%dof_iq)+SIZEOF(mat%den_type)        &
             +SIZEOF(mat%vcomp)+SIZEOF(mat),i4)
      CALL memlogger%update(mat%mem_id,mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* deallocate arrays needed for a real rblock matrix.
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(mat)
    IMPLICIT NONE

    !> matrix to dealloc
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: id,jd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over different basis combinations and deallocte.
!-------------------------------------------------------------------------------
    DO jd=1,SIZE(mat%bsc,2)
      DO id=1,SIZE(mat%bsc,1)
        DEALLOCATE(mat%bsc(id,jd)%arr)
      ENDDO
    ENDDO
    DEALLOCATE(mat%bsc,mat%ix0,mat%iy0,mat%ix1,mat%iy1,mat%jx1,mat%jy1)
    DEALLOCATE(mat%nb_type,mat%nq_type)
    DEALLOCATE(mat%den_type,mat%dof_ib,mat%dof_iv,                              &
               mat%dof_ix,mat%dof_iy,mat%dof_iq)
    DEALLOCATE(mat%vcomp)
    IF (ALLOCATED(mat%dof_map)) THEN
      CALL mat%dof_map%dealloc
      DEALLOCATE(mat%dof_map)
    ENDIF
    IF (ALLOCATED(mat%nnz_map)) THEN
      CALL mat%nnz_map%dealloc
      DEALLOCATE(mat%nnz_map)
    ENDIF
    IF (ALLOCATED(mat%mat_conv)) THEN
      CALL mat%mat_conv%dealloc
      DEALLOCATE(mat%mat_conv)
    ENDIF
    IF (ALLOCATED(mat%send_vals)) THEN
      DO id=1,SIZE(mat%send_vals)
        DEALLOCATE(mat%send_vals(id)%vals)
      ENDDO
      DEALLOCATE(mat%send_vals)
    ENDIF
    IF (ALLOCATED(mat%recv_vals)) THEN
      DO id=1,SIZE(mat%recv_vals)
        DEALLOCATE(mat%recv_vals(id)%vals)
      ENDDO
      DEALLOCATE(mat%recv_vals)
    ENDIF
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(mat%mem_id,mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!> multiply the real mat matrix by the real operand vector,
!  and return the product.  each matrix array contains all
!  connections between two sets of basis types, so operand and
!  product arrays have basis index lumped with quantity index.
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_real(mat,operand,output)
    USE vec_rect_2D_mod
    USE vector_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(mat_rect_2D_real), INTENT(IN) :: mat
    !> vector to multiply
    CLASS(rvector), INTENT(IN) :: operand
    !> result
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: mx,my,pd,ityp,jtyp,nbtype,typ_max,nqp,nqo
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: mat_ptr
    LOGICAL :: new_output
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   compute properties
!-------------------------------------------------------------------------------
    nbtype=mat%nbtype
    IF (nbtype>1) THEN
      pd=SUM(mat%nb_type(1:2))
    ELSE
      pd=1
    ENDIF
!-------------------------------------------------------------------------------
!   if interior nodes are eliminated, basis loops only cover grid
!   and element side centered nodes.
!-------------------------------------------------------------------------------
    IF (mat%eliminated) THEN
      typ_max=MIN(nbtype,3_i4)
    ELSE
      typ_max=nbtype
    ENDIF
!-------------------------------------------------------------------------------
!   call the structured-data matrix operation for each
!   basis type to basis type pair, and load seam array.
!   every basis-type pair now uses a separate multiplication routine
!   for optimization.
!-------------------------------------------------------------------------------
    mx=mat%mx
    my=mat%my
    SELECT TYPE(operand)
    TYPE IS(vec_rect_2D_real)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real)
        DO ityp=1,typ_max
          new_output=.true.
          nqp=mat%nq_type(ityp)
          SELECT CASE(ityp)
          CASE(1)
            DO jtyp=1,typ_max
              nqo=mat%nq_type(jtyp)
              mat_ptr=>mat%bsc(jtyp,ityp)%arr
              SELECT CASE(jtyp)
              CASE(1)
                CALL matvecgg_real_rbl(output%arr,mat_ptr,operand%arr,          &
                                       mx,my,nqp,nqo,new_output)
              CASE(2)
                CALL matvechg_real_rbl(output%arr,mat_ptr,operand%arrh,         &
                                       mx,my,nqp,nqo,new_output)
              CASE(3)
                CALL matvecvg_real_rbl(output%arr,mat_ptr,operand%arrv,         &
                                       mx,my,nqp,nqo,new_output)
              CASE(4)
                CALL matvecig_real_rbl(output%arr,mat_ptr,operand%arri,         &
                                       mx,my,nqp,nqo,new_output)
              END SELECT
              new_output=.false.
            ENDDO
          CASE(2)
            DO jtyp=1,typ_max
              nqo=mat%nq_type(jtyp)
              mat_ptr=>mat%bsc(jtyp,ityp)%arr
              SELECT CASE(jtyp)
              CASE(1)
                CALL matvecgh_real_rbl(output%arrh,mat_ptr,operand%arr,         &
                                       mx,my,nqp,nqo,new_output)
              CASE(2)
                CALL matvechh_real_rbl(output%arrh,mat_ptr,operand%arrh,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(3)
                CALL matvecvh_real_rbl(output%arrh,mat_ptr,operand%arrv,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(4)
                CALL matvecih_real_rbl(output%arrh,mat_ptr,operand%arri,        &
                                       mx,my,nqp,nqo,new_output)
              END SELECT
              new_output=.false.
            ENDDO
          CASE(3)
            DO jtyp=1,typ_max
              nqo=mat%nq_type(jtyp)
              mat_ptr=>mat%bsc(jtyp,ityp)%arr
              SELECT CASE(jtyp)
              CASE(1)
                CALL matvecgv_real_rbl(output%arrv,mat_ptr,operand%arr,         &
                                       mx,my,nqp,nqo,new_output)
              CASE(2)
                CALL matvechv_real_rbl(output%arrv,mat_ptr,operand%arrh,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(3)
                CALL matvecvv_real_rbl(output%arrv,mat_ptr,operand%arrv,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(4)
                CALL matveciv_real_rbl(output%arrv,mat_ptr,operand%arri,        &
                                       mx,my,nqp,nqo,new_output)
              END SELECT
              new_output=.false.
            ENDDO
          CASE(4)
            DO jtyp=1,typ_max
              nqo=mat%nq_type(jtyp)
              mat_ptr=>mat%bsc(jtyp,ityp)%arr
              SELECT CASE(jtyp)
              CASE(1)
                CALL matvecgi_real_rbl(output%arri,mat_ptr,operand%arr,         &
                                       mx,my,nqp,nqo,new_output)
              CASE(2)
                CALL matvechi_real_rbl(output%arri,mat_ptr,operand%arrh,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(3)
                CALL matvecvi_real_rbl(output%arri,mat_ptr,operand%arrv,        &
                                       mx,my,nqp,nqo,new_output)
              CASE(4)
                CALL matvecii_real_rbl(output%arri,mat_ptr,operand%arri,        &
                                       mx,my,nqp,nqo,new_output)
              END SELECT
              new_output=.false.
            ENDDO
          END SELECT
        ENDDO
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real::matvec_real unrecognized"//        &
                          " output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real::matvec_real unrecognized"//          &
                        " operand type")
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_real

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_real(mat,integrand)
    USE local
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> integrand array
    REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

    INTEGER(i4) :: idofst,jdofst,ityp,jtyp,ix0,iy0,ipol,jxoff,jyoff
    INTEGER(i4) :: idof,jdof,ix,iy
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element.
!   the degree-of-freedom arrays are now used with pre-computed information.
!-------------------------------------------------------------------------------
    idofst=1
    DO ityp=1,mat%nbtype
      IF (mat%nq_type(ityp)==0) CYCLE
      jdofst=1
      DO jtyp=1,mat%nbtype
        IF (mat%nq_type(jtyp)==0) CYCLE
        ASSOCIATE(arr=>mat%bsc(jtyp,ityp)%arr)
          DO ipol=1,mat%mx*mat%my
            iy0=(ipol-1)/mat%mx+1
            ix0=ipol-mat%mx*(iy0-1)
            DO idof=idofst,mat%den_type(ityp)
              iy=iy0+mat%dof_iy(idof)
              ix=ix0+mat%dof_ix(idof)
              DO jdof=jdofst,mat%den_type(jtyp)
                jxoff=mat%dof_ix(jdof)-mat%dof_ix(idof)
                jyoff=mat%dof_iy(jdof)-mat%dof_iy(idof)
                arr(mat%dof_iq(jdof),jxoff,jyoff,mat%dof_iq(idof),ix,iy)=       &
                  arr(mat%dof_iq(jdof),jxoff,jyoff,mat%dof_iq(idof),ix,iy)+     &
                  integrand(mat%dof_iv(jdof),mat%dof_iv(idof),                  &
                            ipol,mat%dof_ib(jdof),mat%dof_ib(idof))
              ENDDO
            ENDDO
          ENDDO
          jdofst=mat%den_type(jtyp)+1
        END ASSOCIATE
      ENDDO
      idofst=mat%den_type(ityp)+1
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!* set matrix to zero
!-------------------------------------------------------------------------------
  SUBROUTINE zero_real(mat)
    USE local
    IMPLICIT NONE

    !> matrix to set to zero
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: jd,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_real',iftn,idepth)
    DO jd=1,SIZE(mat%bsc,2)
      DO id=1,SIZE(mat%bsc,1)
        mat%bsc(id,jd)%arr=0._r8
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!> invert the connections within cell interiors for basis functions
! of degree 2 or more.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_inv_int_real(mat)
    USE math_tran
    IMPLICIT NONE

    !> matrix to eliminate
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: mat_ptr,int_ptr
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: xx,id
    INTEGER(i4) :: iof,jof,nqint,mxb,myb,ix,iy,iq,jq,ixo,iyo,jxo,jyo
    LOGICAL :: sing
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l1(mod_name,'elim_inv_int_real',iftn,idepth)
    nqint=mat%nq_type(4)
    mxb=SIZE(mat%bsc(4,4)%arr,5)
    myb=SIZE(mat%bsc(4,4)%arr,6)
!-------------------------------------------------------------------------------
!   invert the interior to interior matrix, A_ii.
!-------------------------------------------------------------------------------
    CALL math_invert_q1(mat%nq_type(4),mat%mx*mat%my,mat%bsc(4,4)%arr,          &
                        mat%id,mat%symmetric,sing)
    IF (sing) CALL par%nim_stop('elim_inv_int_cm1m:'//                          &
                                ' dense interior does not factor.')
!-------------------------------------------------------------------------------
!   if nq is zero, there are no bases that are continuous across
!   element borders.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) THEN
      CALL timer%end_timer_l2(iftn,idepth)
      RETURN
    ENDIF
    ALLOCATE(xx(nqint,mxb,myb))
    ALLOCATE(id(nqint,mxb,myb))
!-------------------------------------------------------------------------------
!   now create the Schur complement, A_oo-A_oi.A_ii**-1.A_io,
!   where i refers to interior data, and o refers to other data.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   start with a computation of A_ii**-1.A_io taking one grid
!   offset and column vector-component-index (but all ix and iy) at
!   a time to use matrix/vector algebra.
!-------------------------------------------------------------------------------
    DO jyo=-1,0
      DO jxo=-1,0
        DO jq=1,mat%nqcon
          mat_ptr=>mat%bsc(1,4)%arr
          int_ptr=>mat%bsc(4,4)%arr
          xx=mat_ptr(jq,jxo,jyo,:,:,:)
          CALL matvecii_real_rbl(id,int_ptr,xx,mxb,myb,                         &
                                 nqint,nqint,.true.)
!-------------------------------------------------------------------------------
!         complete the computation of A_oo-A_oi.A_ii**-1.A_io for
!         one column (but all ix and iy) of A_io.
!
!         modify the rows connecting to grid nodes (i.e. A_oo=A_gg):
!-------------------------------------------------------------------------------
          mat_ptr=>mat%bsc(1,1)%arr
          int_ptr=>mat%bsc(4,1)%arr
          DO iy=1,myb
            DO ix=1,mxb
              DO iq=1,mat%nqcon
                DO iyo=0,1
                  jof=jyo+iyo
                  DO ixo=0,1
                    iof=jxo+ixo
                    mat_ptr(jq,iof,jof,iq,ix-ixo,iy-iyo)=                       &
                      mat_ptr(jq,iof,jof,iq,ix-ixo,iy-iyo)-                     &
                        SUM(int_ptr(:,ixo,iyo,iq,ix-ixo,iy-iyo)*                &
                            id(:,ix,iy))
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
!-------------------------------------------------------------------------------
!         modify the rows connecting to horizontal side nodes
!         (i.e. A_oo=A_hg):
!-------------------------------------------------------------------------------
          mat_ptr=>mat%bsc(1,2)%arr
          int_ptr=>mat%bsc(4,2)%arr
          DO iy=1,myb
            DO ix=1,mxb
              DO iq=1,mat%nq_type(2)
                DO iyo=0,1
                  jof=jyo+iyo
                  mat_ptr(jq,jxo,jof,iq,ix,iy-iyo)=                             &
                    mat_ptr(jq,jxo,jof,iq,ix,iy-iyo)-                           &
                      SUM(int_ptr(:,0,iyo,iq,ix,iy-iyo)*id(:,ix,iy))
                ENDDO
              ENDDO
            ENDDO
          ENDDO
!-------------------------------------------------------------------------------
!         modify the rows connecting to vertical side nodes
!         (i.e. A_oo=A_vg):
!-------------------------------------------------------------------------------
          mat_ptr=>mat%bsc(1,3)%arr
          int_ptr=>mat%bsc(4,3)%arr
          DO iy=1,myb
            DO ix=1,mxb
              DO iq=1,mat%nq_type(3)
                DO ixo=0,1
                  iof=jxo+ixo
                  mat_ptr(jq,iof,jyo,iq,ix-ixo,iy)=                             &
                    mat_ptr(jq,iof,jyo,iq,ix-ixo,iy)-                           &
                      SUM(int_ptr(:,ixo,0,iq,ix-ixo,iy)*id(:,ix,iy))
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   now take one horizontal side node and vector-component-index
!   as a column element.
!-------------------------------------------------------------------------------
    DO jyo=-1,0
      DO jq=1,mat%nq_type(2)
        mat_ptr=>mat%bsc(2,4)%arr
        int_ptr=>mat%bsc(4,4)%arr
        xx=mat_ptr(jq,0,jyo,:,:,:)
        CALL matvecii_real_rbl(id,int_ptr,xx,mxb,myb,nqint,nqint,.true.)
!-------------------------------------------------------------------------------
!       complete the computation of A_oo-A_oi.A_ii**-1.A_io for
!       one column (but all ix and iy) of A_io.
!
!       modify the rows connecting to grid nodes (i.e. A_oo=A_gh):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(2,1)%arr
        int_ptr=>mat%bsc(4,1)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nqcon
              DO iyo=0,1
                jof=jyo+iyo
                DO ixo=0,1
                  mat_ptr(jq,ixo,jof,iq,ix-ixo,iy-iyo)=                         &
                    mat_ptr(jq,ixo,jof,iq,ix-ixo,iy-iyo)-                       &
                      SUM(int_ptr(:,ixo,iyo,iq,ix-ixo,iy-iyo)*                  &
                          id(:,ix,iy))
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       modify the rows connecting to horizontal side nodes
!       (i.e. A_oo=A_hh):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(2,2)%arr
        int_ptr=>mat%bsc(4,2)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nq_type(2)
              DO iyo=0,1
                jof=jyo+iyo
                mat_ptr(jq,0,jof,iq,ix,iy-iyo)=                                 &
                  mat_ptr(jq,0,jof,iq,ix,iy-iyo)-                               &
                    SUM(int_ptr(:,0,iyo,iq,ix,iy-iyo)*id(:,ix,iy))
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       modify the rows connecting to vertical side nodes
!       (i.e. A_oo=A_vh):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(2,3)%arr
        int_ptr=>mat%bsc(4,3)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nq_type(3)
              DO ixo=0,1
                mat_ptr(jq,ixo,jyo,iq,ix-ixo,iy)=                               &
                  mat_ptr(jq,ixo,jyo,iq,ix-ixo,iy)-                             &
                    SUM(int_ptr(:,ixo,0,iq,ix-ixo,iy)*id(:,ix,iy))
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   finally take one vertical side node and vector-component-index
!   as a column element.
!-------------------------------------------------------------------------------
    DO jxo=-1,0
      DO jq=1,mat%nq_type(3)
        mat_ptr=>mat%bsc(3,4)%arr
        int_ptr=>mat%bsc(4,4)%arr
        xx=mat_ptr(jq,jxo,0,:,:,:)
        CALL matvecii_real_rbl(id,int_ptr,xx,mxb,myb,                           &
                               nqint,nqint,.true.)
!-------------------------------------------------------------------------------
!       complete the computation of A_oo-A_oi.A_ii**-1.A_io for
!       one column (but all ix and iy) of A_io.
!
!       modify the rows connecting to grid nodes (i.e. A_oo=A_gv):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(3,1)%arr
        int_ptr=>mat%bsc(4,1)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nqcon
              DO iyo=0,1
                DO ixo=0,1
                  iof=jxo+ixo
                  mat_ptr(jq,iof,iyo,iq,ix-ixo,iy-iyo)=                         &
                    mat_ptr(jq,iof,iyo,iq,ix-ixo,iy-iyo)-                       &
                      SUM(int_ptr(:,ixo,iyo,iq,ix-ixo,iy-iyo)*                  &
                          id(:,ix,iy))
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       modify the rows connecting to horizontal side nodes
!       (i.e. A_oo=A_hv):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(3,2)%arr
        int_ptr=>mat%bsc(4,2)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nq_type(2)
              DO iyo=0,1
                mat_ptr(jq,jxo,iyo,iq,ix,iy-iyo)=                               &
                  mat_ptr(jq,jxo,iyo,iq,ix,iy-iyo)-                             &
                    SUM(int_ptr(:,0,iyo,iq,ix,iy-iyo)*id(:,ix,iy))
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       modify the rows connecting to vertical side nodes
!       (i.e. A_oo=A_vv):
!-------------------------------------------------------------------------------
        mat_ptr=>mat%bsc(3,3)%arr
        int_ptr=>mat%bsc(4,3)%arr
        DO iy=1,myb
          DO ix=1,mxb
            DO iq=1,mat%nq_type(3)
              DO ixo=0,1
                iof=jxo+ixo
                mat_ptr(jq,iof,0,iq,ix-ixo,iy)=                                 &
                  mat_ptr(jq,iof,0,iq,ix-ixo,iy)-                               &
                    SUM(int_ptr(:,ixo,0,iq,ix-ixo,iy)*id(:,ix,iy))
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
    ENDDO
    DEALLOCATE(id,xx)
!-------------------------------------------------------------------------------
!   set flag.
!-------------------------------------------------------------------------------
    mat%eliminated=.true.
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE elim_inv_int_real

!-------------------------------------------------------------------------------
!> for matrices partitioned into cell-interior / other, find
! A_ii**-1.b_i and b_o - A_oi.A_ii**-1.b_i (where i means interior
! and o is other).  assume that elim_inv_int_real has been
! called, so A_ii**-1 is available in the A_ii storage.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_presolve_real(mat,input,output)
    USE vector_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(IN) :: mat
    !> vector on which to eliminate interior data
    CLASS(rvector), INTENT(IN) :: input
    !> vector on which to store eliminated form
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: mxb,myb,nqi,nqh,nqv
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: mat_ptr
    REAL(r8), DIMENSION(:,:,:), POINTER :: pr_ptr
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: op_ptr4,pr_ptr4
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l2(mod_name,'elim_presolve_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   check that A_ii has been inverted.
!-------------------------------------------------------------------------------
    IF (.NOT.mat%eliminated) CALL par%nim_stop('elim_presolve_real:'//          &
                                           ' dense interior is not factored.')
!-------------------------------------------------------------------------------
!   compute and save A_ii**-1.b_i first.
!-------------------------------------------------------------------------------
    SELECT TYPE(input)
    TYPE IS(vec_rect_2D_real)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real)
        mxb=SIZE(mat%bsc(4,4)%arr,5)
        myb=SIZE(mat%bsc(4,4)%arr,6)
        nqh=mat%nq_type(2)
        nqv=mat%nq_type(3)
        nqi=mat%nq_type(4)
        mat_ptr=>mat%bsc(4,4)%arr
        CALL matvecii_real_rbl(output%arri,mat_ptr,                             &
                               input%arri,mxb,myb,nqi,nqi,.true.)
!-------------------------------------------------------------------------------
!       if nqh is zero, there are no bases that are continuous across
!       element borders.
!-------------------------------------------------------------------------------
        IF (nqh==0) THEN
          CALL timer%end_timer_l2(iftn,idepth)
          RETURN
        ENDIF
!-------------------------------------------------------------------------------
!       now find b_o-A_oi.A_ii**-1.b_i for each of the "other" types
!       of bases.
!-------------------------------------------------------------------------------
        op_ptr4=>output%arri
        pr_ptr=>output%arr
        pr_ptr=-input%arr
        mat_ptr=>mat%bsc(4,1)%arr
        CALL matvecig_real_rbl(pr_ptr,mat_ptr,op_ptr4,                          &
                               mxb,myb,mat%nqcon,nqi,.false.)
        pr_ptr=-pr_ptr
        pr_ptr4=>output%arrh
        pr_ptr4=-input%arrh
        mat_ptr=>mat%bsc(4,2)%arr
        CALL matvecih_real_rbl(pr_ptr4,mat_ptr,op_ptr4,                         &
                               mxb,myb,nqh,nqi,.false.)
        pr_ptr4=-pr_ptr4
        pr_ptr4=>output%arrv
        pr_ptr4=-input%arrv
        mat_ptr=>mat%bsc(4,3)%arr
        CALL matveciv_real_rbl(pr_ptr4,mat_ptr,op_ptr4,                         &
                               mxb,myb,nqv,nqi,.false.)
        pr_ptr4=-pr_ptr4
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real::elim_presolve_real unrecognized"// &
                      " output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real::elim_presolve_real unrecognized"//   &
                    " input type")
    END SELECT
!-------------------------------------------------------------------------------
!   set output vector to skip operations on arri
!-------------------------------------------------------------------------------
    output%skip_elim_interior=.TRUE.
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE elim_presolve_real

!-------------------------------------------------------------------------------
!> for matrices partitioned into cell-interior / other,
! subtract A_ii**-1.A_io.x_o from A_ii**-1.b_i to get x_i.
! the output should contain A_ii**-1.b_i in the interior storage on call.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_postsolve_real(mat,input,output)
    USE vector_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(IN) :: mat
    !* vector from which to restore interior data
    !  should contain A_ii**-1.b_i in the interior before this call
    CLASS(rvector), INTENT(INOUT) :: input
    !* vector to which to restore full data (i.e. find x_i)
    !  should contain x_o in the exterior before this call
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: mxb,myb,nqi,nqh,nqv
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: mat_ptr
    REAL(r8), DIMENSION(:,:,:), POINTER :: op_ptr
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: op_ptr4,pr_ptr4
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l2(mod_name,'elim_postsolve_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   compute -A_io.x_o first in the output interior
!-------------------------------------------------------------------------------
    SELECT TYPE(input)
    TYPE IS(vec_rect_2D_real)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real)
        mxb=SIZE(mat%bsc(4,4)%arr,5)
        myb=SIZE(mat%bsc(4,4)%arr,6)
        nqh=mat%nq_type(2)
        nqv=mat%nq_type(3)
        nqi=mat%nq_type(4)
        pr_ptr4=>output%arri
        mat_ptr=>mat%bsc(1,4)%arr
        op_ptr=>output%arr
        CALL matvecgi_real_rbl(pr_ptr4,mat_ptr,op_ptr,                          &
                               mxb,myb,nqi,mat%nqcon,.true.)
        mat_ptr=>mat%bsc(2,4)%arr
        op_ptr4=>output%arrh
        CALL matvechi_real_rbl(pr_ptr4,mat_ptr,op_ptr4,                         &
                               mxb,myb,nqi,nqh,.false.)
        mat_ptr=>mat%bsc(3,4)%arr
        op_ptr4=>output%arrv
        CALL matvecvi_real_rbl(pr_ptr4,mat_ptr,op_ptr4,                         &
                               mxb,myb,nqi,nqv,.false.)
        output%arri=-output%arri
!-------------------------------------------------------------------------------
!       now find x_ii = A_ii**-1.b_i-A_ii**-1.A_io.x_o.
!       A_ii**-1.b_i is in input, use this as work space
!-------------------------------------------------------------------------------
        pr_ptr4=>input%arri
        op_ptr4=>output%arri
        mat_ptr=>mat%bsc(4,4)%arr
        CALL matvecii_real_rbl(pr_ptr4,mat_ptr,op_ptr4,                         &
                               mxb,myb,nqi,nqi,.false.)
        output%arri=input%arri
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real::elim_postsolve_real unrecognized"//&
                      " output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real::elim_postsolve_real unrecognized"//  &
                    " input type")
    END SELECT
!-------------------------------------------------------------------------------
!   set output vector to do operations on arri
!-------------------------------------------------------------------------------
    output%skip_elim_interior=.FALSE.
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE elim_postsolve_real

!-------------------------------------------------------------------------------
!> determine a scaling factor for diagonal
! matrix elements that is based on grid-vertex entries.
!-------------------------------------------------------------------------------
  SUBROUTINE find_diag_scale_real(mat)
    USE local
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   the scaling factor, regularity, and boundary operations are
!   not called if there are no continuous fields.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) RETURN
    CALL timer%start_timer_l2(mod_name,'find_diag_scale_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   find the scaling factor
!-------------------------------------------------------------------------------
    mat%diag_scale=0._r8
    DO iq=1,mat%nqcon
      mat%diag_scale=MAX(mat%diag_scale,                                        &
                         MAXVAL(ABS(mat%bsc(1,1)%arr(iq,0,0,iq,:,:))))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE find_diag_scale_real

!-------------------------------------------------------------------------------
!> apply Dirichlet boundary conditions to a Cartesian operator.
! If the specified component is tangent, the resultant matrix is
! (I-tt-zz).M.(I-tt-zz)+tt+zz, where t is the surface tangent in
! the computational plane, z is the unit vector normal to the plane,
! and I is the identity matrix.  If the specified component is
! normal, the resultant matrix is (I-nn).M.(I-nn)+nn, where n is
! the surface normal.  If the input is all, then the result is
! (I-tt-zz-nn).M.(I-tt-zz-nn)+tt+zz+nn.
!
! when the end of the component parameter is 'offdiag,' the passed
! matrix structure contains only off-diagonal matrix entries, and
! there is no diagonal entry to enter after couplings are
! eliminated.
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_real(mat,component,edge,symm)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), INTENT(IN), OPTIONAL :: symm

    INTEGER(i4) :: iv,nv,jxmin,jxmax,jymin,jymax,                               &
                   jx,jy,ix,iy,ijx,ijy,mx,my,iq,jq,clim,                        &
                   imat,jmat,ivp,ix0,iy0,jx0,jy0,nqtyp,                         &
                   itype,jtype,iq0,iq1,jq0,jq1,isymm
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: rmat
    LOGICAL :: diag
    REAL(r8), DIMENSION(:), ALLOCATABLE :: proj
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: bcpmat
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: bciarr
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   check vector component range.
!-------------------------------------------------------------------------------
    clim=SIZE(mat%bsc(1,1)%arr,1)
    ALLOCATE(proj(clim),bcpmat(clim,clim),bciarr(clim,2))
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,clim,bciarr,diag)
!-------------------------------------------------------------------------------
!   save the essential-condition flag.
!-------------------------------------------------------------------------------
    mat%essential_cond=component
!-------------------------------------------------------------------------------
!   loop over all external boundary points and zero the couplings
!   to the specified components along the boundary.
!-------------------------------------------------------------------------------
    nv=edge%nvert
!-------------------------------------------------------------------------------
!   rblock basis types indices are
!   1 == grid vertex centered
!   2 == horizontal side centered
!   3 == vertical side centered
!   4 == interior centered
!   1 : 3 are affected by boundary conditions.
!   note that itype is the to basis type and jtype is the from
!   basis type.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   connections to the rblock boundary vertices.
!-------------------------------------------------------------------------------
    rb_vert: DO iv=1,nv
      ivp=iv-1
      IF (ivp==0) ivp=edge%nvert
      mx=SIZE(mat%bsc(1,1)%arr,5)-1
      my=SIZE(mat%bsc(1,1)%arr,6)-1
!-------------------------------------------------------------------------------
!     eliminate couplings from the specified components only.
!-------------------------------------------------------------------------------
      DO itype=1,mat%nbtype
        ix0=mat%ix0(itype)
        iy0=mat%iy0(itype)
        nqtyp=mat%nq_type(itype)
        DO jtype=1,MIN(mat%nbtype,3_i4)
          rmat=>mat%bsc(jtype,itype)%arr
          jx0=mat%ix0(jtype)
          jy0=mat%iy0(jtype)
          DO jmat=1,mat%nb_type(jtype)
            IF (jtype==1) THEN
              IF (.NOT.edge%expoint(iv)) CYCLE
              ix=edge%vertex(iv)%intxy(1)
              iy=edge%vertex(iv)%intxy(2)
              CALL bcdir_set(clim,bciarr,edge%excorner(iv),isymm,               &
                             edge%vert_norm(:,iv),edge%vert_tang(:,iv),bcpmat)
            ELSE
              IF (.NOT.(edge%expoint(iv).AND.                                   &
                        edge%expoint(ivp))) CYCLE
              IF (edge%segment(iv)%h_side) THEN
                IF (jtype>2) CYCLE
              ELSE
                IF (jtype<3) CYCLE
              ENDIF
              ix=edge%segment(iv)%intxys(1)
              iy=edge%segment(iv)%intxys(2)
              CALL bcdir_set(clim,bciarr,.false.,isymm,                         &
                             edge%seg_norm(:,jmat,iv),edge%seg_tang(:,jmat,iv), &
                             bcpmat)
            ENDIF
            jxmin=MAX(ix0-1,ix0-ix)
            jxmax=MIN(1-jx0,mx -ix)
            jymin=MAX(iy0-1,iy0-iy)
            jymax=MIN(1-jy0,my -iy)
!-------------------------------------------------------------------------------
!           find the vector elements of M.uu then subtract
!           M.uu from M
!-------------------------------------------------------------------------------
            jq0=(jmat-1)*clim+1
            jq1=jmat*clim
            DO jy=jymin,jymax
              ijy=iy+jy
              DO jx=jxmin,jxmax
                ijx=ix+jx
                DO iq=1,nqtyp
                  proj=                                                         &
                    MATMUL(bcpmat,rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy))
                  rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)=                             &
                    rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)-proj
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     eliminate couplings to the specified component only.
!-------------------------------------------------------------------------------
      DO itype=1,MIN(mat%nbtype,3_i4)
        ix0=mat%ix0(itype)
        iy0=mat%iy0(itype)
        DO jtype=1,mat%nbtype
          rmat=>mat%bsc(jtype,itype)%arr
          jx0=mat%ix0(jtype)
          jy0=mat%iy0(jtype)
          nqtyp=mat%nq_type(jtype)
          DO imat=1,mat%nb_type(itype)
            IF (itype==1) THEN
              IF (.NOT.edge%expoint(iv)) CYCLE
              ix=edge%vertex(iv)%intxy(1)
              iy=edge%vertex(iv)%intxy(2)
              CALL bcdir_set(clim,bciarr,edge%excorner(iv),isymm,               &
                             edge%vert_norm(:,iv),edge%vert_tang(:,iv),bcpmat)
            ELSE
              IF (.NOT.(edge%expoint(iv).AND.                                   &
                        edge%expoint(ivp))) CYCLE
              IF (edge%segment(iv)%h_side) THEN
                IF (itype>2) CYCLE
              ELSE
                IF (itype<3) CYCLE
              ENDIF
              ix=edge%segment(iv)%intxys(1)
              iy=edge%segment(iv)%intxys(2)
              CALL bcdir_set(clim,bciarr,.false.,isymm,                         &
                             edge%seg_norm(:,imat,iv),edge%seg_tang(:,imat,iv), &
                             bcpmat)
            ENDIF
            jxmin=MAX(jx0-1,jx0-ix)
            jxmax=MIN(1-ix0,mx -ix)
            jymin=MAX(jy0-1,jy0-iy)
            jymax=MIN(1-iy0,my -iy)
!-------------------------------------------------------------------------------
!           find the vector elements of uu.M then subtract
!           uu.M from M
!-------------------------------------------------------------------------------
            iq0=(imat-1)*clim+1
            iq1=imat*clim
            DO jy=jymin,jymax
              DO jx=jxmin,jxmax
                DO jq=1,nqtyp
                  proj=                                                         &
                    MATMUL(bcpmat,rmat(jq,jx,jy,iq0:iq1,ix,iy))
                  rmat(jq,jx,jy,iq0:iq1,ix,iy)=                                 &
                    rmat(jq,jx,jy,iq0:iq1,ix,iy)-proj
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           take care of uu and zz.
!-------------------------------------------------------------------------------
            IF (jtype==itype.AND.diag) THEN
              rmat(iq0:iq1,0,0,iq0:iq1,ix,iy)=                                  &
                rmat(iq0:iq1,0,0,iq0:iq1,ix,iy)+bcpmat*mat%diag_scale
            ENDIF
          ENDDO
        ENDDO
      ENDDO
    ENDDO rb_vert
    DEALLOCATE(proj,bcpmat,bciarr)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!> apply regularity conditions to R=0 points of an operator.
!  the action on the matrix is (I-uu).M.(I-uu)+uu, where u is the
!  unit vector for the components that need elimination.  the
!  vcomp list of component descriptions in the matrix structure
!  and the fcomp Fourier component index are used to decide
!  where to apply this action.
!
!  if dscale is provided, it is used to scale the added uu entry,
!  instead of determining the scaling on the fly.
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_real(mat,edge)
    USE edge_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge

    REAL(r8), DIMENSION(mat%nqcon,mat%nqcon) :: mult,multj
    INTEGER(i4) :: iv,nv,jxmin,jxmax,jymin,jymax,                               &
                   jx,jy,ix,iy,ijx,ijy,mx,my,iq,jq,clim,                        &
                   ix0,iy0,jx0,jy0,imat,jmat,ivp,nvec,ivec,iv0,iv1,             &
                   itype,jtype,iq0,iq1,jq0,jq1,jcomp
    REAL(r8), DIMENSION(mat%nqcon) :: mult_v,mult_j
    REAL(r8), DIMENSION(:,:,:,:,:,:), POINTER :: rmat
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different vector components
!   of a matrix.  the elements at R=0 are uncoupled for n>0, if the
!   matrix acts on a scalar field.  the elements at R=0 of a matrix
!   for a vector field are uncoupled for:
!   -  n=0:  r and phi
!   -  n=1:  z
!   -  n>1:  r, z, and phi
!   a covariant phi component (a_phi*r) is uncoupled for all n.  these
!   components are indicated with 'c'.
!-------------------------------------------------------------------------------
    clim=mat%nqcon
    nvec=clim/3
!-------------------------------------------------------------------------------
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!
!   the second array, mult_j, is used for the columns.
!   [preconditioning may require matrix elements with different
!   Fourier indices for rows and columns.]
!-------------------------------------------------------------------------------
    mult_v=0
    SELECT CASE(mat%fcomp)
    CASE(0)
      WHERE(mat%vcomp=='s') mult_v=1
      WHERE(mat%vcomp=='z') mult_v=1
    CASE(1)
      WHERE(mat%vcomp=='r') mult_v=1
    END SELECT
    mult_j=0
    jcomp=mat%fcomp
    SELECT CASE(ABS(jcomp))
    CASE(0)
      WHERE(mat%vcomp=='s') mult_j=1
      WHERE(mat%vcomp=='z') mult_j=1
    CASE(1)
      WHERE(mat%vcomp=='r') mult_j=1
    END SELECT
    DO jq=1,clim
      mult(jq,:)=mult_v
      multj(:,jq)=mult_j
    ENDDO
!-------------------------------------------------------------------------------
!   loop over the borders of blocks touching R=0, and decouple the
!   appropriate matrix elements.
!-------------------------------------------------------------------------------
    nv=edge%nvert
!-------------------------------------------------------------------------------
!   loop over the block boundary.
!-------------------------------------------------------------------------------
    vert: DO iv=1,nv
      ivp=iv-1
      IF (ivp==0) ivp=nv
      mx=SIZE(mat%bsc(1,1)%arr,5)-1
      my=SIZE(mat%bsc(1,1)%arr,6)-1
!-------------------------------------------------------------------------------
!     loop over couplings from a given node at R=0 for n=1
!     vectors only.
!-------------------------------------------------------------------------------
      IF (ABS(jcomp)==1.AND.MODULO(clim,3_i4)==0) THEN
        DO itype=1,mat%nbtype
          ix0=mat%ix0(itype)
          iy0=mat%iy0(itype)
          DO jtype=1,MIN(mat%nbtype,3_i4)
            rmat=>mat%bsc(jtype,itype)%arr
            jx0=mat%ix0(jtype)
            jy0=mat%iy0(jtype)
            DO jmat=1,mat%nb_type(jtype)
              IF (jtype==1) THEN
                IF (.NOT.edge%r0point(iv)) CYCLE
                ix=edge%vertex(iv)%intxy(1)
                iy=edge%vertex(iv)%intxy(2)
              ELSE
                IF (.NOT.(edge%r0point(iv).AND.edge%r0point(ivp))) CYCLE
                IF (edge%segment(iv)%h_side) THEN
                  IF (jtype>2) CYCLE
                ELSE
                  IF (jtype<3) CYCLE
                ENDIF
                ix=edge%segment(iv)%intxys(1)
                iy=edge%segment(iv)%intxys(2)
              ENDIF
              jxmin=MAX(ix0-1,ix0-ix)
              jxmax=MIN(1-jx0,mx -ix)
              jymin=MAX(iy0-1,iy0-iy)
              jymax=MIN(1-jy0,my -iy)
!-------------------------------------------------------------------------------
!             combine r and phi equations to find averages in the
!             matrix that enforce Vec_phi=i*Vec_r for n=1.  for
!             preconditioning, we may also need Vec_phi=-i*Vec_r
!             for n=-1.
!
!             mathematically, the steps are 1) change variables
!             to X1=(Vec_r+i*Vec_phi)/2 and X2=(Vec_r-i*Vec_phi)/2
!             for each vertex at R=0, 2) set X1=0 (remove
!             couplings to and from X1) giving an overdetermined
!             system of equations, 3) add -i*(the X2-equation) to
!             the X1 equation.
!
!             for the n=-1 preconditioning matrix case, we will
!             set X2 to zero and keep X1 in the r-comp location.
!-------------------------------------------------------------------------------
              DO ivec=0,nvec-1
                jq0=(jmat-1)*clim+3*ivec+1
                jq1=jq0+2
                DO jy=jymin,jymax
                  ijy=iy+jy
                  DO jx=jxmin,jxmax
                    ijx=ix+jx
                    rmat(jq0,-jx,-jy,:,ijx,ijy)=                                &
                         rmat(jq0,-jx,-jy,:,ijx,ijy)                            &
                        -rmat(jq1,-jx,-jy,:,ijx,ijy)*jcomp
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDIF
!-------------------------------------------------------------------------------
!     loop over couplings to a given node at R=0.
!-------------------------------------------------------------------------------
      DO itype=1,MIN(mat%nbtype,3_i4)
        ix0=mat%ix0(itype)
        iy0=mat%iy0(itype)
        DO jtype=1,mat%nbtype
          rmat=>mat%bsc(jtype,itype)%arr
          jx0=mat%ix0(jtype)
          jy0=mat%iy0(jtype)
          DO imat=1,mat%nb_type(itype)
            IF (itype==1) THEN
              IF (.NOT.edge%r0point(iv)) CYCLE
              ix=edge%vertex(iv)%intxy(1)
              iy=edge%vertex(iv)%intxy(2)
            ELSE
              IF (.NOT.(edge%r0point(iv).AND.edge%r0point(ivp))) CYCLE
              IF (edge%segment(iv)%h_side) THEN
                IF (itype>2) CYCLE
              ELSE
                IF (itype<3) CYCLE
              ENDIF
              ix=edge%segment(iv)%intxys(1)
              iy=edge%segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(jx0-1,jx0-ix)
            jxmax=MIN(1-ix0,mx -ix)
            jymin=MAX(jy0-1,jy0-iy)
            jymax=MIN(1-iy0,my -iy)
!-------------------------------------------------------------------------------
!           combine r and phi equations to find averages in the
!           matrix that enforce Vec_phi=i*Vec_r for n=1.  [note
!           rows always represent n>=0.]
!
!           then, eliminate couplings to this node.
!-------------------------------------------------------------------------------
            iq0=(imat-1)*clim+1
            iq1=imat*clim
            DO jy=jymin,jymax
              DO jx=jxmin,jxmax
                IF (mat%fcomp==1.AND.MODULO(clim,3_i4)==0) THEN
                  DO ivec=0,nvec-1
                    iv0=(imat-1)*clim+3*ivec+1
                    iv1=iv0+2
                    rmat(:,jx,jy,iv0,ix,iy)=                                    &
                        rmat(:,jx,jy,iv0,ix,iy)                                 &
                       -rmat(:,jx,jy,iv1,ix,iy)
                  ENDDO
                ENDIF
                DO jq=1,mat%nq_type(jtype)
                  rmat(jq,jx,jy,iq0:iq1,ix,iy)=                                 &
                      rmat(jq,jx,jy,iq0:iq1,ix,iy)*mult_v
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     again loop over couplings from a given node at R=0.
!-------------------------------------------------------------------------------
      DO itype=1,mat%nbtype
        ix0=mat%ix0(itype)
        iy0=mat%iy0(itype)
        DO jtype=1,MIN(mat%nbtype,3_i4)
          rmat=>mat%bsc(jtype,itype)%arr
          jx0=mat%ix0(jtype)
          jy0=mat%iy0(jtype)
          DO jmat=1,mat%nb_type(jtype)
            IF (jtype==1) THEN
              IF (.NOT.edge%r0point(iv)) CYCLE
              ix=edge%vertex(iv)%intxy(1)
              iy=edge%vertex(iv)%intxy(2)
            ELSE
              IF (.NOT.(edge%r0point(iv).AND.edge%r0point(ivp))) CYCLE
              IF (edge%segment(iv)%h_side) THEN
                IF (jtype>2) CYCLE
              ELSE
                IF (jtype<3) CYCLE
              ENDIF
              ix=edge%segment(iv)%intxys(1)
              iy=edge%segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(ix0-1,ix0-ix)
            jxmax=MIN(1-jx0,mx -ix)
            jymin=MAX(iy0-1,iy0-iy)
            jymax=MIN(1-jy0,my -iy)
!-------------------------------------------------------------------------------
!           eliminate couplings from the node on R=0.
!-------------------------------------------------------------------------------
            jq0=(jmat-1)*clim+1
            jq1=jmat*clim
            DO jy=jymin,jymax
              ijy=iy+jy
              DO jx=jxmin,jxmax
                ijx=ix+jx
                DO iq=1,mat%nq_type(itype)
                  rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)=                             &
                      rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)*mult_j
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           take care of uu and zz.
!-------------------------------------------------------------------------------
            IF (jtype==itype) THEN
              DO jq=0,clim-1
                rmat(jq+jq0,0,0,jq+jq0,ix,iy)=rmat(jq+jq0,0,0,jq+jq0,ix,iy)     &
                                              +(1-mult_v(jq+1))*mat%diag_scale
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDDO
    ENDDO vert
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!* Extract the diagonal as a vector
!-------------------------------------------------------------------------------
  SUBROUTINE get_diag_as_vec_real(mat,output_vec)
    USE vec_rect_2D_mod
    USE vector_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(IN) :: mat
    !> output vector
    CLASS(rvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: iq,is,ii,iqs,iqi
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_diag_as_vec_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   extract the diagonal
!-------------------------------------------------------------------------------
    SELECT TYPE(output_vec)
    TYPE IS(vec_rect_2D_real)
      IF (ASSOCIATED(output_vec%arr)) THEN
        DO iq=1,output_vec%nqty
          output_vec%arr(iq,:,:)=mat%bsc(1,1)%arr(iq,0,0,iq,:,:)
        ENDDO
      ENDIF
      IF (ASSOCIATED(output_vec%arrh)) THEN
        DO iq=1,output_vec%nqty
          DO is=1,output_vec%n_side
            iqs=iq+mat%nqcon*(is-1)
            output_vec%arrh(iq,is,:,:)=mat%bsc(2,2)%arr(iqs,0,0,iqs,:,:)
          ENDDO
        ENDDO
      ENDIF
      IF (ASSOCIATED(output_vec%arrv)) THEN
        DO iq=1,output_vec%nqty
          DO is=1,output_vec%n_side
            iqs=iq+mat%nqcon*(is-1)
            output_vec%arrv(iq,is,:,:)=mat%bsc(3,3)%arr(iqs,0,0,iqs,:,:)
          ENDDO
        ENDDO
      ENDIF
      IF (ASSOCIATED(output_vec%arri).AND.                                      &
          .NOT.output_vec%skip_elim_interior) THEN
        DO iq=1,output_vec%nqty
          DO ii=1,output_vec%n_int
            iqi=iq+mat%nqcon*(ii-1)
            output_vec%arri(iq,ii,:,:)=mat%bsc(4,4)%arr(iqi,0,0,iqi,:,:)
          ENDDO
        ENDDO
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for output_vec'              &
                        //' in get_diag_as_vec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_diag_as_vec_real

!-------------------------------------------------------------------------------
!*  Set matrix to the identity tensor for testing
!-------------------------------------------------------------------------------
  SUBROUTINE set_identity_real(mat,edge)
    USE edge_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> associated edge
    TYPE(edge_type), OPTIONAL, INTENT(IN) :: edge

    INTEGER(i4) :: id,jd,iq,iv,ix,iy
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   not called if there are no continuous fields.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) RETURN
    CALL timer%start_timer_l2(mod_name,'set_identity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   set to the identity
!-------------------------------------------------------------------------------
    DO id=1,mat%nbtype
      DO jd=1,mat%nbtype
        mat%bsc(id,jd)%arr(:,:,:,:,:,:)=0._r8
      ENDDO
      DO iq=1,mat%nq_type(id)
        mat%bsc(id,id)%arr(iq,0,0,iq,:,:)=1._r8
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   loop over boundary and set to ave_factor
!-------------------------------------------------------------------------------
    IF (PRESENT(edge)) THEN
      DO iv=1,edge%nvert
        ix=edge%vertex(iv)%intxy(1)
        iy=edge%vertex(iv)%intxy(2)
        DO iq=1,mat%nq_type(1)
          mat%bsc(1,1)%arr(iq,0,0,iq,ix,iy)=edge%vertex(iv)%ave_factor
        ENDDO
        IF (mat%nbtype>1) THEN
          ix=edge%segment(iv)%intxys(1)
          iy=edge%segment(iv)%intxys(2)
          IF (edge%segment(iv)%h_side) THEN
            DO iq=1,mat%nq_type(2)
              mat%bsc(2,2)%arr(iq,0,0,iq,ix,iy)=edge%segment(iv)%ave_factor
            ENDDO
          ELSE
            DO iq=1,mat%nq_type(3)
              mat%bsc(3,3)%arr(iq,0,0,iq,ix,iy)=edge%segment(iv)%ave_factor
            ENDDO
          ENDIF
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_identity_real

!-------------------------------------------------------------------------------
!> Allocate and initialize an integer DOF map, also allocate and begin
! initialization of an integer nnz map
!-------------------------------------------------------------------------------
  SUBROUTINE init_dof_map_real(mat,edge,skip_elim_interior,ndof)
    USE edge_mod
    USE mat_rect_2D_ftns_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> number of DOFs owned by this matrix block
    INTEGER(i4), INTENT(OUT) :: ndof

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_dof_map_real',iftn,idepth)
    ALLOCATE(mat%mat_conv)
    ALLOCATE(vec_rect_2D_int::mat%dof_map)
    SELECT TYPE(dof_map=>mat%dof_map)
    TYPE IS(vec_rect_2D_int)
      CALL dof_map%alloc(mat%pdcon,mat%mx,mat%my,mat%nqty,mat%id)
      CALL mat%dof_map%alloc_with_mold(mat%nnz_map)
      SELECT TYPE(nnz_map=>mat%nnz_map)
      TYPE IS(vec_rect_2D_int)
        IF (ASSOCIATED(dof_map%arrh)) THEN
          CALL init_2D_dof_map(dof_map%arr,nnz_map%arr,mat%id,mat%pdcon,        &
                               edge,skip_elim_interior,ndof,                    &
                               mat%mat_conv%self_per_x,                         &
                               mat%mat_conv%self_per_y,                         &
                               dof_map%arrh,dof_map%arrv,dof_map%arri,          &
                               nnz_map%arrh,nnz_map%arrv,nnz_map%arri)
        ELSE
          CALL init_2D_dof_map(dof_map%arr,nnz_map%arr,mat%id,mat%pdcon,        &
                               edge,skip_elim_interior,ndof,                    &
                               mat%mat_conv%self_per_x,                         &
                               mat%mat_conv%self_per_y)
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_int for nnz_map'                &
                          //' in init_dof_map_real')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for dof_map'                  &
                        //' in init_dof_map_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_dof_map_real

!-------------------------------------------------------------------------------
!* Finalize the initialization of the integer nnz map
!-------------------------------------------------------------------------------
  SUBROUTINE init_nnz_map_real(mat,edge,skip_elim_interior,nnz)
    USE edge_mod
    USE mat_rect_2D_ftns_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> number of nonzero matrix elements owned by this matrix block
    INTEGER(i4), INTENT(OUT) :: nnz

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_nnz_map_real',iftn,idepth)
    SELECT TYPE(nnz_map=>mat%nnz_map)
    TYPE IS(vec_rect_2D_int)
      IF (ASSOCIATED(nnz_map%arrh)) THEN
        CALL init_2D_nnz_map(nnz_map%arr,mat%id,mat%pdcon,edge,                 &
                             skip_elim_interior,nnz,                            &
                             nnz_map%arrh,nnz_map%arrv,nnz_map%arri)
      ELSE
        CALL init_2D_nnz_map(nnz_map%arr,mat%id,mat%pdcon,edge,                 &
                             skip_elim_interior,nnz)
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for nnz_map'                  &
                        //' in init_nnz_map_real')
    END SELECT
    mat%mat_conv%nnz_block=nnz
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_nnz_map_real

!-------------------------------------------------------------------------------
!* Set local sparsity pattern
!-------------------------------------------------------------------------------
  SUBROUTINE init_local_sparsity_real(mat,csm,edge,skip_elim_interior,          &
                                      start_rows)
    USE compressed_matrix_real_mod
    USE edge_mod
    USE mat_rect_2D_ftns_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
    LOGICAL, INTENT(IN) :: skip_elim_interior
    !> starting rows in DOF map for each global block
    INTEGER(i4), INTENT(IN) :: start_rows(:)

    INTEGER(i4) :: ib,ix0,iy0,ix1,iy1,iy,ix,iv,iq,iqv,ind
    INTEGER(i4) :: jb,jx0,jy0,jx1,jy1,jy,jx,jv,jq,jqv,jnd,ly,lx
    INTEGER(i4) :: idof,isend,irecv,send_bl,nalloc,inz,tag
    LOGICAL :: self_periodic
    INTEGER(i4), ALLOCATABLE :: inz_arr(:)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_local_sparsity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   Set sparsity pattern ptr array
!-------------------------------------------------------------------------------
    SELECT TYPE(dof_map=>mat%dof_map)
    TYPE IS(vec_rect_2D_int)
      SELECT TYPE(nnz_map=>mat%nnz_map)
      TYPE IS(vec_rect_2D_int)
        IF (ASSOCIATED(nnz_map%arrh)) THEN
          CALL init_2D_ptr(nnz_map%arr,csm%sp,mat%mat_conv,skip_elim_interior,  &
                        nnz_map%arrh,nnz_map%arrv,nnz_map%arri)
          CALL get_2D_send_counts(dof_map%arr,mat%id,edge,start_rows,           &
                                  mat%mat_conv,dof_map%arrh,dof_map%arrv)
        ELSE
          CALL init_2D_ptr(nnz_map%arr,csm%sp,mat%mat_conv,skip_elim_interior)
          CALL get_2D_send_counts(dof_map%arr,mat%id,edge,start_rows,           &
                                  mat%mat_conv)
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_int for nnz_map'                &
                          //' in init_local_sparsity_real')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for dof_map'                  &
                        //' in init_local_sparsity_real')
    END SELECT
!-------------------------------------------------------------------------------
!   set end indexing for self periodic elements
!-------------------------------------------------------------------------------
    mat%ix1=mat%mx
    mat%iy1=mat%my
    IF (mat%mat_conv%self_per_x) mat%ix1(1)=mat%mx-1
    IF (mat%mat_conv%self_per_y) mat%iy1(1)=mat%my-1
    IF (mat%nbtype>1) THEN
      mat%iy1(2)=mat%iy1(1)
      mat%ix1(3)=mat%ix1(1)
    ENDIF
    mat%jx1=mat%mx
    mat%jy1=mat%my
    IF (mat%mat_conv%self_per_x.AND.mat%mx==2) mat%jx1(1)=mat%mx-1
    IF (mat%mat_conv%self_per_y.AND.mat%my==2) mat%jy1(1)=mat%my-1
    IF (mat%nbtype>1) THEN
      mat%jy1(2)=mat%jy1(1)
      mat%jx1(3)=mat%jx1(1)
    ENDIF
!-------------------------------------------------------------------------------
!   Set sparsity pattern ind array
!-------------------------------------------------------------------------------
    ind=0
    jnd=0
    send_bl=-1
    self_periodic=.FALSE.
    csm%sp%ind(mat%mat_conv%firstnz:mat%mat_conv%lastnz)=-1
    SELECT TYPE(dof_map=>mat%dof_map)
    TYPE IS(vec_rect_2D_int)
      ibasis_1: DO ib=1,mat%nbtype
        IF (skip_elim_interior.AND.ib==4) CYCLE
        ix0=mat%ix0(ib)
        iy0=mat%iy0(ib)
        ix1=mat%ix1(ib)
        iy1=mat%iy1(ib)
        iy_el_1: DO iy=iy0,iy1
          ix_el_1: DO ix=ix0,ix1
            inode_1: DO iv=1,mat%nb_type(ib)
              iqty_1: DO iq=1,mat%nqty
                iqv=(iv-1)*mat%nqty+iq
                SELECT CASE(ib)
                CASE(1)
                  ind=dof_map%arr(iq,ix,iy)
                CASE(2)
                  ind=dof_map%arrh(iq,iv,ix,iy)
                CASE(3)
                  ind=dof_map%arrv(iq,iv,ix,iy)
                CASE(4)
                  ind=dof_map%arri(iq,iv,ix,iy)
                END SELECT
!-------------------------------------------------------------------------------
!               local row values
!-------------------------------------------------------------------------------
                IF (ind>=mat%mat_conv%firstrow) THEN
                  jbasis_1: DO jb=1,mat%nbtype
                    IF (skip_elim_interior.AND.jb==4) CYCLE
                    jx0=mat%ix0(jb)
                    jy0=mat%iy0(jb)
                    jx1=mat%jx1(jb)
                    jy1=mat%jy1(jb)
                    jy_el_1: DO ly=jy0-1,1-iy0
                      jy=iy+ly
                      IF (jy<jy0.OR.jy>mat%my) CYCLE
                      jx_el_1: DO lx=jx0-1,1-ix0
                        jx=ix+lx
                        IF (jx<jx0.OR.jx>mat%mx) CYCLE
                        IF (jx>jx1.OR.jy>jy1) THEN
                          ! self periodic sent column
                          DO isend=1,mat%mat_conv%nsend
                            IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                              send_bl=isend
                              EXIT
                            ENDIF
                          ENDDO
                          mat%mat_conv%nnz_send(send_bl)=                       &
                            mat%mat_conv%nnz_send(send_bl)                      &
                            +mat%nqty*mat%nb_type(jb)
                          CYCLE
                        ENDIF
                        SELECT CASE(jb)
                        CASE(1)
                          jnd=dof_map%arr(1,jx,jy)
                        CASE(2)
                          jnd=dof_map%arrh(1,1,jx,jy)
                        CASE(3)
                          jnd=dof_map%arrv(1,1,jx,jy)
                        CASE(4)
                          jnd=dof_map%arri(1,1,jx,jy)
                        END SELECT
                        irow_1: DO idof=csm%sp%ptr(ind),csm%sp%ptr(ind+1)-1
                          IF (csm%sp%ind(idof)==jnd) THEN
                            CALL par%nim_stop('Double count of local ind')
                          ELSEIF (csm%sp%ind(idof)<0) THEN
                            jnode_1: DO jv=1,mat%nb_type(jb)
                              jqty_1: DO jq=1,mat%nqty
                                jqv=(jv-1)*mat%nqty+jq
                                csm%sp%ind(idof+jqv-1)=jnd+jqv-1
                              ENDDO jqty_1
                            ENDDO jnode_1
                            EXIT ! idof loop finished
                          ELSEIF (idof==csm%sp%ptr(ind+1)-1) THEN
                            CALL par%nim_stop('Miscount of local ind')
                          ENDIF
                        ENDDO irow_1
                      ENDDO jx_el_1
                    ENDDO jy_el_1
                  ENDDO jbasis_1
                ELSE
!-------------------------------------------------------------------------------
!                 values for rows that belong to other blocks
!                 determine nnz_send
!-------------------------------------------------------------------------------
                  DO isend=1,mat%mat_conv%nsend
                    IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                      send_bl=isend
                      EXIT
                    ENDIF
                  ENDDO
                  jbasis_1_send: DO jb=1,mat%nbtype
                    IF (skip_elim_interior.AND.jb==4) CYCLE
                    jx0=mat%ix0(jb)
                    jy0=mat%iy0(jb)
                    jy_el_1_send: DO ly=jy0-1,1-iy0
                      jy=iy+ly
                      IF (jy<jy0.OR.jy>mat%my) CYCLE
                      jx_el_1_send: DO lx=jx0-1,1-ix0
                        jx=ix+lx
                        IF (jx<jx0.OR.jx>mat%mx) CYCLE
                        mat%mat_conv%nnz_send(send_bl)=                         &
                          mat%mat_conv%nnz_send(send_bl)                        &
                          +mat%nqty*mat%nb_type(jb)
                      ENDDO jx_el_1_send
                    ENDDO jy_el_1_send
                  ENDDO jbasis_1_send
                ENDIF
              ENDDO iqty_1
            ENDDO inode_1
          ENDDO ix_el_1
        ENDDO iy_el_1
      ENDDO ibasis_1
!-------------------------------------------------------------------------------
!     count self-periodic sends
!-------------------------------------------------------------------------------
      ibasis_2: DO ib=1,mat%nbtype
        IF (ib==4) CYCLE
        IF (mat%iy1(ib)==mat%my.AND.mat%ix1(ib)==mat%mx) CYCLE
        ix0=mat%ix0(ib)
        iy0=mat%iy0(ib)
        iy_el_2: DO iy=iy0,mat%my
          ix_el_2: DO ix=ix0,mat%mx
            IF (iy/=mat%iy1(ib)+1.AND.ix/=mat%ix1(ib)+1) CYCLE
            inode_2: DO iv=1,mat%nb_type(ib)
              iqty_2: DO iq=1,mat%nqty
                iqv=(iv-1)*mat%nqty+iq
                SELECT CASE(ib)
                CASE(1)
                  ind=dof_map%arr(iq,ix,iy)
                CASE(2)
                  ind=dof_map%arrh(iq,iv,ix,iy)
                CASE(3)
                  ind=dof_map%arrv(iq,iv,ix,iy)
                CASE(4)
                  ind=dof_map%arri(iq,iv,ix,iy)
                END SELECT
                DO isend=1,mat%mat_conv%nsend
                  IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                    send_bl=isend
                    EXIT
                  ENDIF
                ENDDO
                jbasis_2: DO jb=1,mat%nbtype
                  IF (skip_elim_interior.AND.jb==4) CYCLE
                  jx0=mat%ix0(jb)
                  jy0=mat%iy0(jb)
                  jy_el_2: DO ly=jy0-1,1-iy0
                    jy=iy+ly
                    IF (jy<jy0.OR.jy>mat%my) CYCLE
                    jx_el_2: DO lx=jx0-1,1-ix0
                      jx=ix+lx
                      IF (jx<jx0.OR.jx>mat%mx) CYCLE
                      mat%mat_conv%nnz_send(send_bl)=                           &
                        mat%mat_conv%nnz_send(send_bl)                          &
                        +mat%nqty*mat%nb_type(jb)
                    ENDDO jx_el_2
                  ENDDO jy_el_2
                ENDDO jbasis_2
              ENDDO iqty_2
            ENDDO inode_2
          ENDDO ix_el_2
        ENDDO iy_el_2
      ENDDO ibasis_2
!-------------------------------------------------------------------------------
!     Allocate arrays
!-------------------------------------------------------------------------------
      ALLOCATE(mat%mat_conv%send_addr(mat%mat_conv%nsend))
      ALLOCATE(mat%mat_conv%send_req(mat%mat_conv%nsend))
      ALLOCATE(mat%send_vals(mat%mat_conv%nsend))
      DO isend=1,mat%mat_conv%nsend
        nalloc=mat%mat_conv%nnz_send(isend)
        tag=mat%mat_conv%blk_send(isend)+par%nbl_total*(mat%id-1)
        CALL par%all_isend(nalloc,mat%mat_conv%proc_send(isend),                &
                           mat%mat_conv%send_req(isend),tag)
        CALL par%request_free(mat%mat_conv%send_req(isend))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_irow(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_jcol(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_iqv(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_lx(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_ly(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_ibasis(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_jqv(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_jx(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_jy(nalloc))
        ALLOCATE(mat%mat_conv%send_addr(isend)%map_jbasis(nalloc))
        ALLOCATE(mat%send_vals(isend)%vals(nalloc))
      ENDDO
      ALLOCATE(mat%mat_conv%recv_addr(mat%mat_conv%nrecv))
      ALLOCATE(mat%mat_conv%recv_req(2*mat%mat_conv%nrecv))
      ALLOCATE(mat%recv_vals(mat%mat_conv%nrecv))
      DO irecv=1,mat%mat_conv%nrecv
        tag=mat%id+par%nbl_total*(mat%mat_conv%blk_recv(irecv)-1)
        CALL par%all_irecv(mat%mat_conv%nnz_recv(irecv),                        &
                           mat%mat_conv%proc_recv(irecv),                       &
                           mat%mat_conv%recv_req(irecv),tag)
      ENDDO
!-------------------------------------------------------------------------------
!     Fill indirect addressing arrays for communication
!-------------------------------------------------------------------------------
      ALLOCATE(inz_arr(mat%mat_conv%nsend))
      inz_arr=0
      ibasis_3: DO ib=1,mat%nbtype
        IF (skip_elim_interior.AND.ib==4) CYCLE
        ix0=mat%ix0(ib)
        iy0=mat%iy0(ib)
        ix1=mat%ix1(ib)
        iy1=mat%iy1(ib)
        iy_el_3: DO iy=iy0,iy1
          ix_el_3: DO ix=ix0,ix1
            inode_3: DO iv=1,mat%nb_type(ib)
              iqty_3: DO iq=1,mat%nqty
                iqv=(iv-1)*mat%nqty+iq
                SELECT CASE(ib)
                CASE(1)
                  ind=dof_map%arr(iq,ix,iy)
                CASE(2)
                  ind=dof_map%arrh(iq,iv,ix,iy)
                CASE(3)
                  ind=dof_map%arrv(iq,iv,ix,iy)
                CASE(4)
                  ind=dof_map%arri(iq,iv,ix,iy)
                END SELECT
!-------------------------------------------------------------------------------
!               local row values
!-------------------------------------------------------------------------------
                IF (ind>=mat%mat_conv%firstrow) THEN
                  jbasis_3: DO jb=1,mat%nbtype
                    IF (skip_elim_interior.AND.jb==4) CYCLE
                    jx0=mat%ix0(jb)
                    jy0=mat%iy0(jb)
                    jx1=mat%jx1(jb)
                    jy1=mat%jy1(jb)
                    jy_el_3: DO ly=jy0-1,1-iy0
                      jy=iy+ly
                      IF (jy<jy0.OR.jy>mat%my) CYCLE
                      jx_el_3: DO lx=jx0-1,1-ix0
                        jx=ix+lx
                        IF (jx<jx0.OR.jx>mat%mx) CYCLE
                        IF (jx>jx1.OR.jy>jy1) THEN
                          ! self periodic sent column
                          DO isend=1,mat%mat_conv%nsend
                            IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                              send_bl=isend
                              EXIT
                            ENDIF
                          ENDDO
                          jnode_3: DO jv=1,mat%nb_type(jb)
                            jqty_3: DO jq=1,mat%nqty
                              SELECT CASE(jb)
                              CASE(1)
                                jnd=dof_map%arr(jq,jx,jy)
                              CASE(2)
                                jnd=dof_map%arrh(jq,jv,jx,jy)
                              CASE(3)
                                jnd=dof_map%arrv(jq,jv,jx,jy)
                              CASE(4)
                                jnd=dof_map%arri(jq,jv,jx,jy)
                              END SELECT
                              jqv=(jv-1)*mat%nqty+jq
                              inz_arr(send_bl)=inz_arr(send_bl)+1
                              inz=inz_arr(send_bl)
                              mat%mat_conv%send_addr(send_bl)%map_irow(inz)=ind
                              mat%mat_conv%send_addr(send_bl)%map_jcol(inz)=jnd
                              mat%mat_conv%send_addr(send_bl)%map_jbasis(inz)=jb
                              mat%mat_conv%send_addr(send_bl)%map_ibasis(inz)=ib
                              mat%mat_conv%send_addr(send_bl)%map_iqv(inz)=iqv
                              mat%mat_conv%send_addr(send_bl)%map_lx(inz)=-lx
                              mat%mat_conv%send_addr(send_bl)%map_ly(inz)=-ly
                              mat%mat_conv%send_addr(send_bl)%map_jqv(inz)=jqv
                              mat%mat_conv%send_addr(send_bl)%map_jx(inz)=jx
                              mat%mat_conv%send_addr(send_bl)%map_jy(inz)=jy
                            ENDDO jqty_3
                          ENDDO jnode_3
                        ENDIF
                      ENDDO jx_el_3
                    ENDDO jy_el_3
                  ENDDO jbasis_3
                ELSE
!-------------------------------------------------------------------------------
!                 non-local row values -- values for rows that belong to other
!                 blocks
!-------------------------------------------------------------------------------
                  DO isend=1,mat%mat_conv%nsend
                    IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                      send_bl=isend
                      EXIT
                    ENDIF
                  ENDDO
                  jbasis_3_send: DO jb=1,mat%nbtype
                    IF (mat%eliminated.AND.jb==4) CYCLE
                    jx0=mat%ix0(jb)
                    jy0=mat%iy0(jb)
                    jy_el_3_send: DO ly=jy0-1,1-iy0
                      jy=iy+ly
                      IF (jy<jy0.OR.jy>mat%my) CYCLE
                      jx_el_3_send: DO lx=jx0-1,1-ix0
                        jx=ix+lx
                        IF (jx<jx0.OR.jx>mat%mx) CYCLE
                        jnode_3_send: DO jv=1,mat%nb_type(jb)
                          jqty_3_send: DO jq=1,mat%nqty
                            SELECT CASE(jb)
                            CASE(1)
                              jnd=dof_map%arr(jq,jx,jy)
                            CASE(2)
                              jnd=dof_map%arrh(jq,jv,jx,jy)
                            CASE(3)
                              jnd=dof_map%arrv(jq,jv,jx,jy)
                            CASE(4)
                              jnd=dof_map%arri(jq,jv,jx,jy)
                            END SELECT
                            jqv=(jv-1)*mat%nqty+jq
                            inz_arr(send_bl)=inz_arr(send_bl)+1
                            inz=inz_arr(send_bl)
                            mat%mat_conv%send_addr(send_bl)%map_irow(inz)=ind
                            mat%mat_conv%send_addr(send_bl)%map_jcol(inz)=jnd
                            mat%mat_conv%send_addr(send_bl)%map_jbasis(inz)=jb
                            mat%mat_conv%send_addr(send_bl)%map_ibasis(inz)=ib
                            mat%mat_conv%send_addr(send_bl)%map_iqv(inz)=iqv
                            mat%mat_conv%send_addr(send_bl)%map_lx(inz)=-lx
                            mat%mat_conv%send_addr(send_bl)%map_ly(inz)=-ly
                            mat%mat_conv%send_addr(send_bl)%map_jqv(inz)=jqv
                            mat%mat_conv%send_addr(send_bl)%map_jx(inz)=jx
                            mat%mat_conv%send_addr(send_bl)%map_jy(inz)=jy
                          ENDDO jqty_3_send
                        ENDDO jnode_3_send
                      ENDDO jx_el_3_send
                    ENDDO jy_el_3_send
                  ENDDO jbasis_3_send
                ENDIF
              ENDDO iqty_3
            ENDDO inode_3
          ENDDO ix_el_3
        ENDDO iy_el_3
      ENDDO ibasis_3
!-------------------------------------------------------------------------------
!     add self-periodic sends
!-------------------------------------------------------------------------------
      ibasis_4: DO ib=1,mat%nbtype
        IF (ib==4) CYCLE
        IF (mat%iy1(ib)==mat%my.AND.mat%ix1(ib)==mat%mx) CYCLE
        ix0=mat%ix0(ib)
        iy0=mat%iy0(ib)
        iy_el_4: DO iy=iy0,mat%my
          ix_el_4: DO ix=ix0,mat%mx
            IF (iy/=mat%iy1(ib)+1.AND.ix/=mat%ix1(ib)+1) CYCLE
            inode_4: DO iv=1,mat%nb_type(ib)
              iqty_4: DO iq=1,mat%nqty
                iqv=(iv-1)*mat%nqty+iq
                SELECT CASE(ib)
                CASE(1)
                  ind=dof_map%arr(iq,ix,iy)
                CASE(2)
                  ind=dof_map%arrh(iq,iv,ix,iy)
                CASE(3)
                  ind=dof_map%arrv(iq,iv,ix,iy)
                CASE(4)
                  ind=dof_map%arri(iq,iv,ix,iy)
                END SELECT
                DO isend=1,mat%mat_conv%nsend
                  IF (ind>mat%mat_conv%rowstart_send(isend)) THEN
                    send_bl=isend
                    EXIT
                  ENDIF
                ENDDO
                jbasis_4: DO jb=1,mat%nbtype
                  IF (skip_elim_interior.AND.jb==4) CYCLE
                  jx0=mat%ix0(jb)
                  jy0=mat%iy0(jb)
                  jy_el_4: DO ly=jy0-1,1-iy0
                    jy=iy+ly
                    IF (jy<jy0.OR.jy>mat%my) CYCLE
                    jx_el_4: DO lx=jx0-1,1-ix0
                      jx=ix+lx
                      IF (jx<jx0.OR.jx>mat%mx) CYCLE
                      jnode_4: DO jv=1,mat%nb_type(jb)
                        jqty_4: DO jq=1,mat%nqty
                          SELECT CASE(jb)
                          CASE(1)
                            jnd=dof_map%arr(jq,jx,jy)
                          CASE(2)
                            jnd=dof_map%arrh(jq,jv,jx,jy)
                          CASE(3)
                            jnd=dof_map%arrv(jq,jv,jx,jy)
                          CASE(4)
                            jnd=dof_map%arri(jq,jv,jx,jy)
                          END SELECT
                          jqv=(jv-1)*mat%nqty+jq
                          inz_arr(send_bl)=inz_arr(send_bl)+1
                          inz=inz_arr(send_bl)
                          mat%mat_conv%send_addr(send_bl)%map_irow(inz)=ind
                          mat%mat_conv%send_addr(send_bl)%map_jcol(inz)=jnd
                          mat%mat_conv%send_addr(send_bl)%map_jbasis(inz)=jb
                          mat%mat_conv%send_addr(send_bl)%map_ibasis(inz)=ib
                          mat%mat_conv%send_addr(send_bl)%map_iqv(inz)=iqv
                          mat%mat_conv%send_addr(send_bl)%map_lx(inz)=-lx
                          mat%mat_conv%send_addr(send_bl)%map_ly(inz)=-ly
                          mat%mat_conv%send_addr(send_bl)%map_jqv(inz)=jqv
                          mat%mat_conv%send_addr(send_bl)%map_jx(inz)=jx
                          mat%mat_conv%send_addr(send_bl)%map_jy(inz)=jy
                        ENDDO jqty_4
                      ENDDO jnode_4
                    ENDDO jx_el_4
                  ENDDO jy_el_4
                ENDDO jbasis_4
              ENDDO iqty_4
            ENDDO inode_4
          ENDDO ix_el_4
        ENDDO iy_el_4
      ENDDO ibasis_4
      DO isend=1,mat%mat_conv%nsend
        IF (inz_arr(isend)/=mat%mat_conv%nnz_send(isend)) THEN
          CALL par%nim_stop('Miscount in send arrays'                           &
                            //' in init_local_sparsity_real')
        ENDIF
      ENDDO
      DEALLOCATE(inz_arr)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for dof_map'                  &
                        //' in init_local_sparsity_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_local_sparsity_real

!-------------------------------------------------------------------------------
!* Send communicated sparsity pattern and associated data
!-------------------------------------------------------------------------------
  SUBROUTINE send_comm_sparsity_real(mat)
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: irecv,isend,nalloc,tag
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'send_comm_sparsity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   check that communication is finished and alloc recv arrays
!-------------------------------------------------------------------------------
    CALL par%waitall(mat%mat_conv%nrecv,                                        &
                     mat%mat_conv%recv_req(1:mat%mat_conv%nrecv))
    DO irecv=1,mat%mat_conv%nrecv
      nalloc=mat%mat_conv%nnz_recv(irecv)
      ALLOCATE(mat%recv_vals(irecv)%vals(nalloc))
      ALLOCATE(mat%mat_conv%recv_addr(irecv)%map_ival(nalloc))
      ALLOCATE(mat%mat_conv%recv_addr(irecv)%map_irow(nalloc))
      ALLOCATE(mat%mat_conv%recv_addr(irecv)%map_jcol(nalloc))
      tag=mat%id+par%nbl_total*(mat%mat_conv%blk_recv(irecv)-1)
      CALL par%all_irecv(mat%mat_conv%recv_addr(irecv)%map_irow,                &
                         mat%mat_conv%nnz_recv(irecv),                          &
                         mat%mat_conv%proc_recv(irecv),                         &
                         mat%mat_conv%recv_req(irecv),tag)
      tag=tag+par%nbl_total**2
      CALL par%all_irecv(mat%mat_conv%recv_addr(irecv)%map_jcol,                &
                         mat%mat_conv%nnz_recv(irecv),                          &
                         mat%mat_conv%proc_recv(irecv),                         &
                         mat%mat_conv%recv_req(irecv+mat%mat_conv%nrecv),tag)
    ENDDO
!-------------------------------------------------------------------------------
!   post sends
!-------------------------------------------------------------------------------
    DO isend=1,mat%mat_conv%nsend
      tag=mat%mat_conv%blk_send(isend)+par%nbl_total*(mat%id-1)
      CALL par%all_isend(mat%mat_conv%send_addr(isend)%map_irow,                &
                         mat%mat_conv%nnz_send(isend),                          &
                         mat%mat_conv%proc_send(isend),                         &
                         mat%mat_conv%send_req(isend),tag)
      CALL par%request_free(mat%mat_conv%send_req(isend))
      tag=tag+par%nbl_total**2
      CALL par%all_isend(mat%mat_conv%send_addr(isend)%map_jcol,                &
                         mat%mat_conv%nnz_send(isend),                          &
                         mat%mat_conv%proc_send(isend),                         &
                         mat%mat_conv%send_req(isend),tag)
      CALL par%request_free(mat%mat_conv%send_req(isend))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE send_comm_sparsity_real

!-------------------------------------------------------------------------------
!* Set communicated sparsity pattern and associated data
!-------------------------------------------------------------------------------
  SUBROUTINE set_comm_sparsity_real(mat,csm)
    USE compressed_matrix_real_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm

    INTEGER(i4) :: isend,irecv,inz,ind,jnd,idof
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_comm_sparsity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   check that communication is finished
!-------------------------------------------------------------------------------
    CALL par%waitall(mat%mat_conv%nrecv,mat%mat_conv%recv_req)
    DEALLOCATE(mat%mat_conv%recv_req) ! resize
    ALLOCATE(mat%mat_conv%recv_req(mat%mat_conv%nrecv))
!-------------------------------------------------------------------------------
!   set associated ind array to jcol for communicated values
!-------------------------------------------------------------------------------
    DO irecv=1,mat%mat_conv%nrecv
      DO inz=1,mat%mat_conv%nnz_recv(irecv)
        ind=mat%mat_conv%recv_addr(irecv)%map_irow(inz)
        jnd=mat%mat_conv%recv_addr(irecv)%map_jcol(inz)
        DO idof=csm%sp%ptr(ind),csm%sp%ptr(ind+1)-1
          IF (csm%sp%ind(idof)==jnd) THEN
            mat%mat_conv%recv_addr(irecv)%map_ival(inz)=idof
            EXIT ! idof loop finished, entry present
          ELSEIF (csm%sp%ind(idof)<0) THEN
            csm%sp%ind(idof)=jnd
            mat%mat_conv%recv_addr(irecv)%map_ival(inz)=idof
            EXIT ! idof loop finished
          ELSEIF (idof==csm%sp%ptr(ind+1)-1) THEN
            CALL par%nim_stop('set_comm: miscount of local ind')
          ENDIF
        ENDDO
      ENDDO
    ENDDO
#ifdef DEBUG
    BLOCK
      INTEGER(i4) :: irow
      DO irow=mat%mat_conv%firstrow,mat%mat_conv%lastrow
        DO ind=csm%sp%ptr(irow),csm%sp%ptr(irow+1)-1
          IF (csm%sp%ind(ind)<0) THEN
            CALL par%nim_stop('set_comm: negative ind values')
          ENDIF
        ENDDO
      ENDDO
    END BLOCK
#endif

!-------------------------------------------------------------------------------
!   sort the ind array
!-------------------------------------------------------------------------------
    !TODO
!-------------------------------------------------------------------------------
!   deallocate no longer needed arrays
!-------------------------------------------------------------------------------
    DO isend=1,mat%mat_conv%nsend
      DEALLOCATE(mat%mat_conv%send_addr(isend)%map_irow)
      DEALLOCATE(mat%mat_conv%send_addr(isend)%map_jcol)
    ENDDO
    DO irecv=1,mat%mat_conv%nrecv
      DEALLOCATE(mat%mat_conv%recv_addr(irecv)%map_irow)
      DEALLOCATE(mat%mat_conv%recv_addr(irecv)%map_jcol)
    ENDDO
    CALL mat%nnz_map%dealloc
    DEALLOCATE(mat%nnz_map)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_comm_sparsity_real

!-------------------------------------------------------------------------------
!> Start export of matrix values to a compressed format by posting recv
!  communication and loading values
!-------------------------------------------------------------------------------
  SUBROUTINE init_matrix_to_compressed_comm_real(mat)
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: isend,irecv,inz,tag
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_matrix_to_compressed_comm_real',   &
                              iftn,idepth)
!-------------------------------------------------------------------------------
!   post async receives
!-------------------------------------------------------------------------------
    DO irecv=1,mat%mat_conv%nrecv
      tag=mat%id+par%nbl_total*(mat%mat_conv%blk_recv(irecv)-1)
      CALL par%all_irecv(mat%recv_vals(irecv)%vals,                             &
                         mat%mat_conv%nnz_recv(irecv),                          &
                         mat%mat_conv%proc_recv(irecv),                         &
                         mat%mat_conv%recv_req(irecv),tag)
    ENDDO
!-------------------------------------------------------------------------------
!   load send values
!-------------------------------------------------------------------------------
    DO isend=1,mat%mat_conv%nsend
      DO inz=1,mat%mat_conv%nnz_send(isend)
        mat%send_vals(isend)%vals(inz)=                                         &
          mat%bsc(mat%mat_conv%send_addr(isend)%map_ibasis(inz),                &
                  mat%mat_conv%send_addr(isend)%map_jbasis(inz))%arr(           &
                  mat%mat_conv%send_addr(isend)%map_iqv(inz),                   &
                  mat%mat_conv%send_addr(isend)%map_lx(inz),                    &
                  mat%mat_conv%send_addr(isend)%map_ly(inz),                    &
                  mat%mat_conv%send_addr(isend)%map_jqv(inz),                   &
                  mat%mat_conv%send_addr(isend)%map_jx(inz),                    &
                  mat%mat_conv%send_addr(isend)%map_jy(inz))
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!> Post sends of matrix data. This is a separate function from init as it
!  requires GPU synchornization before sends
!-------------------------------------------------------------------------------
  SUBROUTINE send_matrix_to_compressed_comm_real(mat)
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: isend,tag
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'send_matrix_to_compressed_comm_real',   &
                              iftn,idepth)
!-------------------------------------------------------------------------------
!   load send values, post sends
!-------------------------------------------------------------------------------
    DO isend=1,mat%mat_conv%nsend
!-------------------------------------------------------------------------------
!     post async sends
!-------------------------------------------------------------------------------
      tag=mat%mat_conv%blk_send(isend)+par%nbl_total*(mat%id-1)
      CALL par%all_isend(mat%send_vals(isend)%vals,                             &
                         mat%mat_conv%nnz_send(isend),                          &
                         mat%mat_conv%proc_send(isend),                         &
                         mat%mat_conv%send_req(isend),tag)
      CALL par%request_free(mat%mat_conv%send_req(isend))
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE send_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!> Read received values into the compressed format
!-------------------------------------------------------------------------------
  SUBROUTINE finalize_matrix_to_compressed_comm_real(mat,csm)
    USE compressed_matrix_real_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm

    INTEGER(i4) :: irecv,inz
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,                                         &
                              'finalize_matrix_to_compressed_comm_real',        &
                              iftn,idepth)
!-------------------------------------------------------------------------------
!   check that communication is finished and unload recv arrays
!-------------------------------------------------------------------------------
    CALL par%waitall(mat%mat_conv%nrecv,mat%mat_conv%recv_req)
    DO irecv=1,mat%mat_conv%nrecv
      DO inz=1,mat%mat_conv%nnz_recv(irecv)
        csm%val(mat%mat_conv%recv_addr(irecv)%map_ival(inz))=                   &
          csm%val(mat%mat_conv%recv_addr(irecv)%map_ival(inz))                  &
          +mat%recv_vals(irecv)%vals(inz)
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE finalize_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!> Export matrix values to a compressed format, post sends
!-------------------------------------------------------------------------------
  SUBROUTINE export_matrix_to_compressed_real(mat,csm)
    USE compressed_matrix_real_mod
    USE vec_rect_2D_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real), INTENT(INOUT) :: mat
    !> compressed sparse matrix
    TYPE(compressed_matrix_real), INTENT(INOUT) :: csm

    INTEGER(i4) :: ib,ix0,iy0,ix1,iy1,ix,iy,iv,ind,iq,iqv
    INTEGER(i4) :: jb,jx0,jy0,jx1,jy1,jx,jy,jv,jnd,jq,jqv
    INTEGER(i4) :: lx,ly,idof
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'export_matrix_to_compressed_real',      &
                              iftn,idepth)
!-------------------------------------------------------------------------------
!   set matrix to zero as there is a += operation for communicated values
!-------------------------------------------------------------------------------
    csm%val(mat%mat_conv%firstnz:mat%mat_conv%lastnz)=0
!-------------------------------------------------------------------------------
!   load local values
!-------------------------------------------------------------------------------
    ind=0
    jnd=0
    SELECT TYPE(dof_map=>mat%dof_map)
    TYPE IS(vec_rect_2D_int)
      ibasis: DO ib=1,mat%nbtype
        IF (mat%eliminated.AND.ib==4) CYCLE
        ix0=mat%ix0(ib)
        iy0=mat%iy0(ib)
        ix1=mat%ix1(ib)
        iy1=mat%iy1(ib)
        iy_el: DO iy=iy0,iy1
          ix_el: DO ix=ix0,ix1
            inode: DO iv=1,mat%nb_type(ib)
              iqty: DO iq=1,mat%nqty
                SELECT CASE(ib)
                CASE(1)
                  ind=dof_map%arr(iq,ix,iy)
                CASE(2)
                  ind=dof_map%arrh(iq,iv,ix,iy)
                CASE(3)
                  ind=dof_map%arrv(iq,iv,ix,iy)
                CASE(4)
                  ind=dof_map%arri(iq,iv,ix,iy)
                END SELECT
!-------------------------------------------------------------------------------
!               load local row values only
!-------------------------------------------------------------------------------
                IF (ind>=mat%mat_conv%firstrow) THEN
                  iqv=(iv-1)*mat%nqty+iq
                  jbasis: DO jb=1,mat%nbtype
                    IF (mat%eliminated.AND.jb==4) CYCLE
                    jx0=mat%ix0(jb)
                    jy0=mat%iy0(jb)
                    jx1=mat%ix1(jb)
                    jy1=mat%iy1(jb)
                    jy_el: DO ly=jy0-1,1-iy0
                      jy=iy+ly
                      IF (jy<jy0.OR.jy>jy1) CYCLE
                      jx_el: DO lx=jx0-1,1-ix0
                        jx=ix+lx
                        IF (jx<jx0.OR.jx>jx1) CYCLE
                        SELECT CASE(jb)
                        CASE(1)
                          jnd=dof_map%arr(1,jx,jy)
                        CASE(2)
                          jnd=dof_map%arrh(1,1,jx,jy)
                        CASE(3)
                          jnd=dof_map%arrv(1,1,jx,jy)
                        CASE(4)
                          jnd=dof_map%arri(1,1,jx,jy)
                        END SELECT
                        irow: DO idof=csm%sp%ptr(ind),csm%sp%ptr(ind+1)-1
                          IF (jnd==csm%sp%ind(idof)) THEN
                            jnode: DO jv=1,mat%nb_type(jb)
                              jqty: DO jq=1,mat%nqty
                                jqv=(jv-1)*mat%nqty+jq
                                csm%val(idof+jqv-1)=                            &
                                  mat%bsc(ib,jb)%arr(iqv,-lx,-ly,jqv,jx,jy)
                              ENDDO jqty
                            ENDDO jnode
                            EXIT ! idof loop finished
                          ENDIF
                        ENDDO irow
                      ENDDO jx_el
                    ENDDO jy_el
                  ENDDO jbasis
                ENDIF
              ENDDO iqty
            ENDDO inode
          ENDDO ix_el
        ENDDO iy_el
      ENDDO ibasis
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for map'                      &
                        //' in export_matrix_to_compressed')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE export_matrix_to_compressed_real

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the output in the array output.  this handles grid vertex
!  to grid vertex operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecgg_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,-1:,:,0:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecgg_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create or sum output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=                                                         &
          SUM(matrix(:,0:,0:,iq,0,0)*vector(:,:1,:1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix-1:ix+1,:1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=                                                        &
          SUM(matrix(:,:0,0:,iq,mx,0)*vector(:,mx-1:,:1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy-1:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy-1:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy-1:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=                                                        &
          SUM(matrix(:,0:,:0,iq,0,my)*vector(:,:1,my-1:my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix-1:ix+1,my-1:my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=                                                       &
          SUM(matrix(:,:0,:0,iq,mx,my)*vector(:,mx-1:,my-1:my))
      ENDDO
    ELSE res_if
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=output(iq,0,0)+                                          &
          SUM(matrix(:,0:,0:,iq,0,0)*vector(:,:1,:1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix-1:ix+1,:1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=output(iq,mx,0)+                                        &
          SUM(matrix(:,:0,0:,iq,mx,0)*vector(:,mx-1:,:1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy-1:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy-1:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy-1:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=output(iq,0,my)+                                        &
          SUM(matrix(:,0:,:0,iq,0,my)*vector(:,:1,my-1:my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix-1:ix+1,my-1:my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=output(iq,mx,my)+                                      &
          SUM(matrix(:,:0,:0,iq,mx,my)*vector(:,mx-1:,my-1:my))
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecgg_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles grid
!  vertex to horizontal side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecgh_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,-1:,:,1:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecgh_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix-1:ix,:1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy-1:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix-1:ix,my-1:my))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix-1:ix,:1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy-1:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix-1:ix,my-1:my))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecgh_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles grid vertex
!  to vertical side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecgv_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,-1:,:,0:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecgv_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy-1:iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy-1:iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy-1:iy))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy-1:iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy-1:iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy-1:iy))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecgv_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles grid vertex
!  to interior operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecgi_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,-1:,:,1:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecgi_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy-1:iy))
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy-1:iy))
          ENDDO
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecgi_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles horizontal
!  side to grid vertex operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvechg_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,-1:,:,0:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvechg_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create or sum output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=                                                         &
          SUM(matrix(:,1,0:,iq,0,0)*vector(:,1,:1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix:ix+1,:1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=                                                        &
          SUM(matrix(:,0,0:,iq,mx,0)*vector(:,mx,:1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy-1:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy-1:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy-1:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=                                                        &
          SUM(matrix(:,1,:0,iq,0,my)*vector(:,1,my-1:my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix:ix+1,my-1:my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=                                                       &
          SUM(matrix(:,0,:0,iq,mx,my)*vector(:,mx,my-1:my))
      ENDDO
    ELSE res_if
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=output(iq,0,0)+                                          &
          SUM(matrix(:,1,0:,iq,0,0)*vector(:,1,:1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix:ix+1,:1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=output(iq,mx,0)+                                        &
          SUM(matrix(:,0,0:,iq,mx,0)*vector(:,mx,:1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy-1:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy-1:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy-1:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=output(iq,0,my)+                                        &
          SUM(matrix(:,1,:0,iq,0,my)*vector(:,1,my-1:my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix:ix+1,my-1:my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=output(iq,mx,my)+                                      &
          SUM(matrix(:,0,:0,iq,mx,my)*vector(:,mx,my-1:my))
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvechg_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles horizontal
!  side to horizontal side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvechh_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,-1:,:,1:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvechh_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix:ix,:1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy-1:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix:ix,my-1:my))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,0:,iq,ix,0)*vector(:,ix:ix,:1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy-1:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,:0,iq,ix,my)*vector(:,ix:ix,my-1:my))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvechh_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles horizontal
!  side to vertical side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvechv_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,-1:,:,0:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvechv_real_rb',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy-1:iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy-1:iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy-1:iy))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy-1:iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy-1:iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy-1:iy))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvechv_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles horizontal
!  side to interior operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvechi_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,-1:,:,1:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,0:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvechi_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy-1:iy))
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy-1:iy))
          ENDDO
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvechi_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles vertical
!  side to grid vertex operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecvg_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,0:,:,0:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecvg_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create or sum output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=                                                         &
          SUM(matrix(:,0:,1,iq,0,0)*vector(:,:1,1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix-1:ix+1,1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=                                                        &
          SUM(matrix(:,:0,1,iq,mx,0)*vector(:,mx-1:,1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=                                                        &
          SUM(matrix(:,0:,0,iq,0,my)*vector(:,:1,my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix-1:ix+1,my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=                                                       &
          SUM(matrix(:,:0,0,iq,mx,my)*vector(:,mx-1:,my))
      ENDDO
    ELSE res_if
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=output(iq,0,0)+                                          &
          SUM(matrix(:,0:,1,iq,0,0)*vector(:,:1,1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix-1:ix+1,1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=output(iq,mx,0)+                                        &
          SUM(matrix(:,:0,1,iq,mx,0)*vector(:,mx-1:,1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,0:,:,iq,0,iy)*vector(:,:1,iy:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix+1,iy:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,:0,:,iq,mx,iy)*vector(:,mx-1:,iy:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=output(iq,0,my)+                                        &
          SUM(matrix(:,0:,0,iq,0,my)*vector(:,:1,my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix-1:ix+1,my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=output(iq,mx,my)+                                      &
          SUM(matrix(:,:0,0,iq,mx,my)*vector(:,mx-1:,my))
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecvg_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles vertical
!  side to horizontal side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecvh_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,0:,:,1:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecvh_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix-1:ix,1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix-1:ix,my))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix-1:ix,1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix-1:ix,iy:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix-1:ix,my))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecvh_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles vertical
!  side to vertical side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecvv_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,0:,:,0:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecvv_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,0:,0,iq,0,iy)*vector(:,:1,iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix-1:ix+1,iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,:0,0,iq,mx,iy)*vector(:,mx-1:,iy))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,0:,0,iq,0,iy)*vector(:,:1,iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix-1:ix+1,iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,:0,0,iq,mx,iy)*vector(:,mx-1:,iy))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecvv_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles vertical
!  side to interior operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecvi_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,-1:,0:,:,1:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,0:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecvi_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix-1:ix,iy))
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix-1:ix,iy))
          ENDDO
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecvi_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles interior
!  to grid vertex operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecig_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,0:,:,0:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecig_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create or sum output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=                                                         &
          SUM(matrix(:,1,1,iq,0,0)*vector(:,1,1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix:ix+1,1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=                                                        &
          SUM(matrix(:,0,1,iq,mx,0)*vector(:,mx,1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=                                                        &
          SUM(matrix(:,1,0,iq,0,my)*vector(:,1,my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix:ix+1,my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=                                                       &
          SUM(matrix(:,0,0,iq,mx,my)*vector(:,mx,my))
      ENDDO
    ELSE res_if
!-------------------------------------------------------------------------------
!     bottom
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,0)=output(iq,0,0)+                                          &
          SUM(matrix(:,1,1,iq,0,0)*vector(:,1,1))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,:,1,iq,ix,0)*vector(:,ix:ix+1,1))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,0)=output(iq,mx,0)+                                        &
          SUM(matrix(:,0,1,iq,mx,0)*vector(:,mx,1))
      ENDDO
!-------------------------------------------------------------------------------
!     interior
!-------------------------------------------------------------------------------
      DO iy=1,my-1
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,1,:,iq,0,iy)*vector(:,1,iy:iy+1))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix+1,iy:iy+1))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,0,:,iq,mx,iy)*vector(:,mx,iy:iy+1))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     top
!-------------------------------------------------------------------------------
      DO iq=1,nqr
        output(iq,0,my)=output(iq,0,my)+                                        &
          SUM(matrix(:,1,0,iq,0,my)*vector(:,1,my))
      ENDDO
      DO ix=1,mx-1
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,:,0,iq,ix,my)*vector(:,ix:ix+1,my))
        ENDDO
      ENDDO
      DO iq=1,nqr
        output(iq,mx,my)=output(iq,mx,my)+                                      &
          SUM(matrix(:,0,0,iq,mx,my)*vector(:,mx,my))
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecig_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles interior
!  to horizontal side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecih_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,0:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,0:,:,1:,0:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecih_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=                                                      &
            SUM(matrix(:,0,1,iq,ix,0)*vector(:,ix,1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=                                                     &
            SUM(matrix(:,0,0,iq,ix,my)*vector(:,ix,my))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,0)=output(iq,ix,0)+                                      &
            SUM(matrix(:,0,1,iq,ix,0)*vector(:,ix,1))
        ENDDO
      ENDDO
      DO iy=1,my-1
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,:,iq,ix,iy)*vector(:,ix:ix,iy:iy+1))
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mx
        DO iq=1,nqr
          output(iq,ix,my)=output(iq,ix,my)+                                    &
            SUM(matrix(:,0,0,iq,ix,my)*vector(:,ix,my))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecih_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles interior
!  to vertical side operations only.
!-------------------------------------------------------------------------------
  SUBROUTINE matveciv_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,0:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,0:,:,0:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matveciv_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=                                                      &
            SUM(matrix(:,1,0,iq,0,iy)*vector(:,1,iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix:ix+1,iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=                                                     &
            SUM(matrix(:,0,0,iq,mx,iy)*vector(:,mx,iy))
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO iq=1,nqr
          output(iq,0,iy)=output(iq,0,iy)+                                      &
            SUM(matrix(:,1,0,iq,0,iy)*vector(:,1,iy))
        ENDDO
        DO ix=1,mx-1
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,:,0,iq,ix,iy)*vector(:,ix:ix+1,iy))
          ENDDO
        ENDDO
        DO iq=1,nqr
          output(iq,mx,iy)=output(iq,mx,iy)+                                    &
            SUM(matrix(:,0,0,iq,mx,iy)*vector(:,mx,iy))
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matveciv_real_rbl

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication for an rblock and
!  return the product in the array output.  this handles interior
!  to interior only.
!-------------------------------------------------------------------------------
  SUBROUTINE matvecii_real_rbl(output,matrix,vector,mx,my,nqr,nqv,new_output)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: mx,my,nqr,nqv
    REAL(r8), DIMENSION(nqr,1:mx,1:*), INTENT(INOUT) :: output
    REAL(r8), DIMENSION(:,0:,0:,:,1:,1:), INTENT(IN) :: matrix
    REAL(r8), DIMENSION(nqv,1:mx,1:*), INTENT(IN) :: vector
    LOGICAL, INTENT(IN) :: new_output

    INTEGER(i4) :: ix,iy,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvecii_real_rbl',iftn,idepth)
!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    res_if: IF (new_output) THEN
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=                                                   &
              SUM(matrix(:,0,0,iq,ix,iy)*vector(:,ix,iy))
          ENDDO
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     sum output vector.
!-------------------------------------------------------------------------------
    ELSE res_if
      DO iy=1,my
        DO ix=1,mx
          DO iq=1,nqr
            output(iq,ix,iy)=output(iq,ix,iy)+                                  &
              SUM(matrix(:,0,0,iq,ix,iy)*vector(:,ix,iy))
          ENDDO
        ENDDO
      ENDDO
    ENDIF res_if
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvecii_real_rbl

END MODULE mat_rect_2D_real_mod
