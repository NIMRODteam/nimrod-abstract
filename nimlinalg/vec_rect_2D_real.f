!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_real included in vec_rect_2D.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a real vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(rvec,poly_degree,mx,my,nqty,id)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    rvec%nqty=nqty
    rvec%mx=mx
    rvec%my=my
    rvec%n_side=poly_degree-1
    rvec%n_int=(poly_degree-1)**2
    rvec%u_ndof=(poly_degree+1)**2
    rvec%pd=poly_degree
    rvec%nel=mx*my
    rvec%ndim=2
    rvec%id=id
    rvec%inf_norm_size=4
    rvec%l2_dot_size=7
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(rvec%arr(nqty,0:mx,0:my))
      NULLIFY(rvec%arri,rvec%arrh,rvec%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(rvec%arr(nqty,0:mx,0:my))
      ALLOCATE(rvec%arrh(nqty,poly_degree-1,1:mx,0:my))
      ALLOCATE(rvec%arrv(nqty,poly_degree-1,0:mx,1:my))
      ALLOCATE(rvec%arri(nqty,(poly_degree-1)**2,1:mx,1:my))
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(rvec%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(rvec%arr)+SIZEOF(rvec%arrh)+SIZEOF(rvec%arrv)              &
             +SIZEOF(rvec%arri)+SIZEOF(rvec),i4)
      CALL memlogger%update(rvec%mem_id,'rvec'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(rvec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) THEN
      DEALLOCATE(rvec%arr)
      NULLIFY(rvec%arr)
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      DEALLOCATE(rvec%arrh)
      NULLIFY(rvec%arrh)
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      DEALLOCATE(rvec%arrv)
      NULLIFY(rvec%arrv)
    ENDIF
    IF (ASSOCIATED(rvec%arri)) THEN
      DEALLOCATE(rvec%arri)
      NULLIFY(rvec%arri)
    ENDIF
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(rvec%mem_id,'rvec'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_real(rvec,new_rvec,nqty,pd)
    IMPLICIT NONE

    !> reference vector to mold
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> new vector to be created
    CLASS(rvector), ALLOCATABLE, INTENT(OUT) :: new_rvec
    !> number of quantities, rvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, rvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_real',iftn,idepth)
    new_nqty=rvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=rvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the base allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_real::new_rvec)
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_rvec%skip_elim_interior=rvec%skip_elim_interior
!-------------------------------------------------------------------------------
!   call the allocation routine
!-------------------------------------------------------------------------------
    SELECT TYPE (new_rvec)
    TYPE IS (vec_rect_2D_real)
      CALL new_rvec%alloc(new_pd,rvec%mx,rvec%my,new_nqty,rvec%id)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_real

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_real(rvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) THEN
      ASSOCIATE (arr=>rvec%arr)
        arr=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      ASSOCIATE (arrh=>rvec%arrh)
        arrh=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      ASSOCIATE (arrv=>rvec%arrv)
        arrv=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arri)) THEN
      ASSOCIATE (arri=>rvec%arri)
        arri=0.0_r8
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_real(rvec,rvec2)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_real',iftn,idepth)
    rvec%skip_elim_interior=rvec2%skip_elim_interior
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real)
      IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=rvec2%arr(:,:,:)
      IF (ASSOCIATED(rvec%arrh)) rvec%arrh(:,:,:,:)=rvec2%arrh(:,:,:,:)
      IF (ASSOCIATED(rvec%arrv)) rvec%arrv(:,:,:,:)=rvec2%arrv(:,:,:,:)
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)               &
        rvec%arri(:,:,:,:)=rvec2%arri(:,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2 '//                 &
                        'in assign_rvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_real

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_real(rvec,rvec2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_real',iftn,idepth)
    rvec%skip_elim_interior=rvec2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real)
      IF (ASSOCIATED(rvec%arr))  rvec%arr(v1s:v1e,:,:)=rvec2%arr(v2s:v2e,:,:)
      IF (ASSOCIATED(rvec%arrh)) rvec%arrh(v1s:v1e,:,:,:)=                      &
        rvec2%arrh(v2s:v2e,:,:,:)
      IF (ASSOCIATED(rvec%arrv)) rvec%arrv(v1s:v1e,:,:,:)=                      &
        rvec2%arrv(v2s:v2e,:,:,:)
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)               &
        rvec%arri(v1s:v1e,:,:,:)=rvec2%arri(v2s:v2e,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2'//                  &
                        ' in assignp_rvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_real

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_real(rvec,cv1m,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_real',iftn,idepth)
    rvec%skip_elim_interior=cv1m%skip_elim_interior
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=REAL(cv1m%arr(:,:,:))
        IF (ASSOCIATED(rvec%arrh)) rvec%arrh(:,:,:,:)=REAL(cv1m%arrh(:,:,:,:))
        IF (ASSOCIATED(rvec%arrv)) rvec%arrv(:,:,:,:)=REAL(cv1m%arrv(:,:,:,:))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(:,:,:,:)=REAL(cv1m%arri(:,:,:,:))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=AIMAG(cv1m%arr(:,:,:))
        IF (ASSOCIATED(rvec%arrh)) rvec%arrh(:,:,:,:)=AIMAG(cv1m%arrh(:,:,:,:))
        IF (ASSOCIATED(rvec%arrv)) rvec%arrv(:,:,:,:)=AIMAG(cv1m%arrv(:,:,:,:))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(:,:,:,:)=AIMAG(cv1m%arri(:,:,:,:))
      CASE DEFAULT
        CALL par%nim_stop('assign_cv1m_real '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m in assign_cv1m_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_real

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_real(rvec,cv1m,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cv1m_real',iftn,idepth)
    rvec%skip_elim_interior=cv1m%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(rvec%arr))                                               &
          rvec%arr(v1s:v1e,:,:)=REAL(cv1m%arr(v2s:v2e,:,:))
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(v1s:v1e,:,:,:)=REAL(cv1m%arrh(v2s:v2e,:,:,:))
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(v1s:v1e,:,:,:)=REAL(cv1m%arrv(v2s:v2e,:,:,:))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(v1s:v1e,:,:,:)=REAL(cv1m%arri(v2s:v2e,:,:,:))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(rvec%arr))                                               &
          rvec%arr(v1s:v1e,:,:)=AIMAG(cv1m%arr(v2s:v2e,:,:))
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(v1s:v1e,:,:,:)=AIMAG(cv1m%arrh(v2s:v2e,:,:,:))
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(v1s:v1e,:,:,:)=AIMAG(cv1m%arrv(v2s:v2e,:,:,:))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(v1s:v1e,:,:,:)=AIMAG(cv1m%arri(v2s:v2e,:,:,:))
      CASE DEFAULT
        CALL par%nim_stop('assignp_cv1m_real '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m in assignp_cv1m_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_real

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_real(rvec,cvec,imode,r_i)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_real',iftn,idepth)
    rvec%skip_elim_interior=cvec%skip_elim_interior
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=REAL(cvec%arr(:,:,:,imode))
        IF (ASSOCIATED(rvec%arrh)) rvec%arrh(:,:,:,:)=                          &
          REAL(cvec%arrh(:,:,:,:,imode))
        IF (ASSOCIATED(rvec%arrv)) rvec%arrv(:,:,:,:)=                          &
          REAL(cvec%arrv(:,:,:,:,imode))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(:,:,:,:)=REAL(cvec%arri(:,:,:,:,imode))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=AIMAG(cvec%arr(:,:,:,imode))
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(:,:,:,:)=AIMAG(cvec%arrh(:,:,:,:,imode))
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(:,:,:,:)=AIMAG(cvec%arrv(:,:,:,:,imode))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(:,:,:,:)=AIMAG(cvec%arri(:,:,:,:,imode))
      CASE DEFAULT
        CALL par%nim_stop('assign_cvec_real '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in assign_cvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_real

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_real(rvec,cvec,imode,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_real',iftn,idepth)
    rvec%skip_elim_interior=cvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(rvec%arr))                                               &
          rvec%arr(v1s:v1e,:,:)=REAL(cvec%arr(v2s:v2e,:,:,imode))
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(v1s:v1e,:,:,:)=REAL(cvec%arrh(v2s:v2e,:,:,:,imode))
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(v1s:v1e,:,:,:)=REAL(cvec%arrv(v2s:v2e,:,:,:,imode))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(v1s:v1e,:,:,:)=REAL(cvec%arri(v2s:v2e,:,:,:,imode))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(rvec%arr))                                               &
          rvec%arr(v1s:v1e,:,:)=AIMAG(cvec%arr(v2s:v2e,:,:,imode))
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(v1s:v1e,:,:,:)=AIMAG(cvec%arrh(v2s:v2e,:,:,:,imode))
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(v1s:v1e,:,:,:)=AIMAG(cvec%arrv(v2s:v2e,:,:,:,imode))
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(v1s:v1e,:,:,:)=AIMAG(cvec%arri(v2s:v2e,:,:,:,imode))
      CASE DEFAULT
        CALL par%nim_stop('assignp_cvec_real '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec'//                  &
                        ' in assignp_cvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_real

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_ivec_real(rvec,ivec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(ivector), INTENT(IN) :: ivec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_ivec_real',iftn,idepth)
    rvec%skip_elim_interior=ivec%skip_elim_interior
    SELECT TYPE (ivec)
    TYPE IS (vec_rect_2D_int)
      IF (ASSOCIATED(rvec%arr))  rvec%arr(:,:,:)=ivec%arr(:,:,:)
      IF (ASSOCIATED(rvec%arrh)) rvec%arrh(:,:,:,:)=ivec%arrh(:,:,:,:)
      IF (ASSOCIATED(rvec%arrv)) rvec%arrv(:,:,:,:)=ivec%arrv(:,:,:,:)
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)               &
        rvec%arri(:,:,:,:)=ivec%arri(:,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for ivec '//                  &
                        'in assign_rvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_ivec_real

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_real(rvec,rvec2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    REAL(r8) :: v1f,v2f
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'add_vec_real',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_real_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_real_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real)
      IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
        IF (ASSOCIATED(rvec%arr))  rvec%arr=v1f*rvec%arr+v2f*rvec2%arr
        IF (ASSOCIATED(rvec%arrh)) rvec%arrh=v1f*rvec%arrh+v2f*rvec2%arrh
        IF (ASSOCIATED(rvec%arrv)) rvec%arrv=v1f*rvec%arrv+v2f*rvec2%arrv
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri=v1f*rvec%arri+v2f*rvec2%arri
      ELSE
        IF (ASSOCIATED(rvec%arr))                                               &
          rvec%arr(v1s:v1e,:,:)=                                                &
            v1f*rvec%arr(v1s:v1e,:,:)+v2f*rvec2%arr(v2s:v2e,:,:)
        IF (ASSOCIATED(rvec%arrh))                                              &
          rvec%arrh(v1s:v1e,:,:,:)=                                             &
            v1f*rvec%arrh(v1s:v1e,:,:,:)+v2f*rvec2%arrh(v2s:v2e,:,:,:)
        IF (ASSOCIATED(rvec%arrv))                                              &
          rvec%arrv(v1s:v1e,:,:,:)=                                             &
            v1f*rvec%arrv(v1s:v1e,:,:,:)+v2f*rvec2%arrv(v2s:v2e,:,:,:)
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)             &
          rvec%arri(v1s:v1e,:,:,:)=                                             &
            v1f*rvec%arri(v1s:v1e,:,:,:)+v2f*rvec2%arri(v2s:v2e,:,:,:)
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for cvec in add_vec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_real

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_real(rvec,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) rvec%arr=rvec%arr*rscalar
    IF (ASSOCIATED(rvec%arrh)) rvec%arrh=rvec%arrh*rscalar
    IF (ASSOCIATED(rvec%arrv)) rvec%arrv=rvec%arrv*rscalar
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)                 &
      rvec%arri=rvec%arri*rscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_real

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_real(rvec,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) rvec%arr=rvec%arr*REAL(cscalar)
    IF (ASSOCIATED(rvec%arrh)) rvec%arrh=rvec%arrh*REAL(cscalar)
    IF (ASSOCIATED(rvec%arrv)) rvec%arrv=rvec%arrv*REAL(cscalar)
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)                 &
      rvec%arri=rvec%arri*REAL(cscalar)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_real

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_real(rvec,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) rvec%arr=rvec%arr*iscalar
    IF (ASSOCIATED(rvec%arrh)) rvec%arrh=rvec%arrh*iscalar
    IF (ASSOCIATED(rvec%arrv)) rvec%arrv=rvec%arrv*iscalar
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior)                 &
      rvec%arri=rvec%arri*iscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_real

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_real(rvec,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> inf norm contributions from the vector (norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) THEN
      ASSOCIATE (arr=>rvec%arr)
        infnorm(1)=MAXVAL(ABS(arr))
      END ASSOCIATE
    ELSE
      infnorm(1)=0.
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      ASSOCIATE (arrh=>rvec%arrh)
        infnorm(2)=MAXVAL(ABS(arrh))
      END ASSOCIATE
    ELSE
      infnorm(2)=0.
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      ASSOCIATE (arrv=>rvec%arrv)
        infnorm(3)=MAXVAL(ABS(arrv))
      END ASSOCIATE
    ELSE
      infnorm(3)=0.
    ENDIF
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
      ASSOCIATE (arri=>rvec%arri)
        infnorm(4)=MAXVAL(ABS(arri))
      END ASSOCIATE
    ELSE
      infnorm(4)=0.
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_real

!-------------------------------------------------------------------------------
!* compute the L2 norm squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_real(rvec,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> L2 norm contribution from the vector (additive to prior value)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(rvec%arr)) THEN
      ASSOCIATE (arr=>rvec%arr)
        l2norm(1)=SUM(arr*arr)
      END ASSOCIATE
      ASSOCIATE (arr=>rvec%arr,vertex=>edge%vertex)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(arr(:,ix,iy)*arr(:,ix,iy))
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      ASSOCIATE (arrh=>rvec%arrh)
        l2norm(3)=SUM(arrh*arrh)
      END ASSOCIATE
      ASSOCIATE (arrh=>rvec%arrh,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(4)=l2norm(4)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(arrh(:,:,ix,iy)*arrh(:,:,ix,iy))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      ASSOCIATE (arrv=>rvec%arrv)
        l2norm(5)=SUM(arrv*arrv)
      END ASSOCIATE
      ASSOCIATE (arrv=>rvec%arrv,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (.NOT.segment(iv)%h_side) THEN
            l2norm(6)=l2norm(6)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(arrv(:,:,ix,iy)*arrv(:,:,ix,iy))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
      ASSOCIATE (arri=>rvec%arri)
        l2norm(7)=SUM(arri*arri)
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_real

!-------------------------------------------------------------------------------
!*  compute the dot product of vec and vec2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_real(rvec,rvec2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> vector to dot
    CLASS(rvector), INTENT(IN) :: rvec2
    !> dot contribution from the vector (additive to prior value)
    REAL(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real)
      IF (ASSOCIATED(rvec%arr)) THEN
        ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr)
          dot(1)=SUM(arr*arr2)
        END ASSOCIATE
        ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr,vertex=>edge%vertex)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                         SUM(arr(:,ix,iy)*arr2(:,ix,iy))
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        ASSOCIATE (arrh=>rvec%arrh,arrh2=>rvec2%arrh)
          dot(3)=SUM(arrh*arrh2)
        END ASSOCIATE
        ASSOCIATE (arrh=>rvec%arrh,arrh2=>rvec2%arrh,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (edge%segment(iv)%h_side) THEN
              dot(4)=dot(4)+(segment(iv)%ave_factor-1._r8)*                     &
                         SUM(arrh(:,:,ix,iy)*arrh2(:,:,ix,iy))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        ASSOCIATE (arrv=>rvec%arrv,arrv2=>rvec2%arrv)
          dot(5)=SUM(arrv*arrv2)
        END ASSOCIATE
        ASSOCIATE (arrv=>rvec%arrv,arrv2=>rvec2%arrv,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (.NOT.edge%segment(iv)%h_side) THEN
              dot(6)=dot(6)+(segment(iv)%ave_factor-1._r8)*                     &
                         SUM(arrv(:,:,ix,iy)*arrv2(:,:,ix,iy))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>rvec%arri,arri2=>rvec2%arri)
          dot(7)=SUM(arri*arri2)
        END ASSOCIATE
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2 in dot_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_real

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_real(rvec,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> integrand array
    REAL(r8), INTENT(IN) :: integrand(:,:,:)

    INTEGER(i4) :: iq,iel,iv,iy,ix,is,ii,start_horz,start_vert,start_int
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_real',iftn,idepth)
    start_horz=5
    start_vert=5+2*rvec%n_side
    start_int=5+4*rvec%n_side
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(rvec%arr)) THEN
      ASSOCIATE (arr=>rvec%arr)
        DO iq=1,rvec%nqty
          DO iy=0,rvec%my-1
            DO ix=0,rvec%mx-1
              iel=1+ix+iy*rvec%mx
              arr(iq,ix,iy)=arr(iq,ix,iy)+integrand(iq,iel,1)
            ENDDO
          ENDDO
          DO iy=0,rvec%my-1
            DO ix=0,rvec%mx-1
              iel=1+ix+iy*rvec%mx
              arr(iq,ix+1,iy)=arr(iq,ix+1,iy)+integrand(iq,iel,2)
            ENDDO
          ENDDO
          DO iy=0,rvec%my-1
            DO ix=0,rvec%mx-1
              iel=1+ix+iy*rvec%mx
              arr(iq,ix,iy+1)=arr(iq,ix,iy+1)+integrand(iq,iel,3)
            ENDDO
          ENDDO
          DO iy=0,rvec%my-1
            DO ix=0,rvec%mx-1
              iel=1+ix+iy*rvec%mx
              arr(iq,ix+1,iy+1)=arr(iq,ix+1,iy+1)+integrand(iq,iel,4)
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      ASSOCIATE (arrh=>rvec%arrh)
        DO iq=1,rvec%nqty
          DO is=1,rvec%n_side
            iv=start_horz+2*(is-1)
            DO iy=0,rvec%my-1
              DO ix=1,rvec%mx
                iel=ix+iy*rvec%mx
                arrh(:,is,ix,iy)=arrh(:,is,ix,iy)+integrand(:,iel,iv)
              ENDDO
            ENDDO
            DO iy=0,rvec%my-1
              DO ix=1,rvec%mx
                iel=ix+iy*rvec%mx
                arrh(:,is,ix,iy+1)=arrh(:,is,ix,iy+1)+integrand(:,iel,iv+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      ASSOCIATE (arrv=>rvec%arrv)
        DO iq=1,rvec%nqty
          DO is=1,rvec%n_side
            iv=start_vert+2*(is-1)
            DO iy=1,rvec%my
              DO ix=0,rvec%mx-1
                iel=1+ix+(iy-1)*rvec%mx
                arrv(:,is,ix,iy)=arrv(:,is,ix,iy)+integrand(:,iel,iv)
              ENDDO
            ENDDO
            DO iy=1,rvec%my
              DO ix=0,rvec%mx-1
                iel=1+ix+(iy-1)*rvec%mx
                arrv(:,is,ix+1,iy)=arrv(:,is,ix+1,iy)+integrand(:,iel,iv+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arri)) THEN
      ASSOCIATE (arri=>rvec%arri)
        DO iq=1,rvec%nqty
          DO ii=1,rvec%n_int
            iv=start_int+ii-1
            DO iy=1,rvec%my
              DO ix=1,rvec%mx
                iel=ix+(iy-1)*rvec%mx
                arri(:,ii,ix,iy)=arri(:,ii,ix,iy)+integrand(:,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_real(rvec,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    REAL(r8), DIMENSION(rvec%nqty) :: proj
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: arr4d
    REAL(r8), DIMENSION(rvec%nqty,rvec%nqty) :: bcpmat
    INTEGER(i4), DIMENSION(rvec%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,ivp,is,isymm,iq
    !> save the existing boundary data in seam_save
    LOGICAL :: seam_save_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,rvec%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.  mesh vertices
!   are treated first.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%expoint(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      CALL bcdir_set(rvec%nqty,bciarr,edge%excorner(iv),isymm,                  &
                     edge%vert_norm(:,iv),edge%vert_tang(:,iv),bcpmat)
      IF (seam_save_loc) THEN
        proj=MATMUL(bcpmat,rvec%arr(:,ix,iy))
        rvec%arr(:,ix,iy)=rvec%arr(:,ix,iy)-proj
        edge%vert_save(1:rvec%nqty,iv)=edge%vert_save(1:rvec%nqty,iv)+proj
      ELSE
        proj=MATMUL(bcpmat,rvec%arr(:,ix,iy))
        rvec%arr(:,ix,iy)=rvec%arr(:,ix,iy)-proj
      ENDIF
    ENDDO vert
!-------------------------------------------------------------------------------
!   side-centered nodes.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(rvec%arrh)) THEN
      seg: DO iv=1,edge%nvert
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.(edge%expoint(iv).AND.edge%expoint(ivp))) CYCLE seg
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr4d=>rvec%arrh
        ELSE
          arr4d=>rvec%arrv
        ENDIF
        IF (seam_save_loc) THEN
          iq=1
          DO is=1,rvec%n_side
            CALL bcdir_set(rvec%nqty,bciarr,.false.,isymm,                      &
                           edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),bcpmat)
            proj=MATMUL(bcpmat,arr4d(:,is,ix,iy))
            arr4d(:,is,ix,iy)=arr4d(:,is,ix,iy)-proj
            edge%seg_save(1:rvec%nqty,is,iv)=                                   &
              edge%seg_save(1:rvec%nqty,is,iv)+proj
            iq=iq+rvec%nqty
          ENDDO
        ELSE
          DO is=1,rvec%n_side
            CALL bcdir_set(rvec%nqty,bciarr,.false.,isymm,                      &
                           edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),bcpmat)
            proj=MATMUL(bcpmat,arr4d(:,is,ix,iy))
            arr4d(:,is,ix,iy)=arr4d(:,is,ix,iy)-proj
          ENDDO
        ENDIF
      ENDDO seg
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_real(rvec,edge,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ivp,ix,iy,iside,ivec,nvec
    REAL(r8), DIMENSION(rvec%nqty,1) :: mult
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: arr4d
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!-------------------------------------------------------------------------------
    mult=0
    SELECT CASE(rvec%nqty)
    CASE(1,2,4,5)          !   scalars
      IF (rvec%fcomp==0) THEN
        mult(:,1)=1
      ENDIF
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=rvec%nqty/3
      DO ivec=0,nvec-1
        IF (rvec%fcomp==0) THEN
          mult(3*ivec+2,1)=1
        ELSEIF (rvec%fcomp==1) THEN
          mult(3*ivec+1,1)=1
          IF (flag=='init_cyl_vec') mult(3*ivec+3,1)=1
        ENDIF
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_real"//                        &
                        " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%r0point(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      rvec%arr(:,ix,iy)=rvec%arr(:,ix,iy)*mult(:,1)
      IF (ASSOCIATED(rvec%arrh)) THEN
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.edge%r0point(ivp)) CYCLE
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr4d=>rvec%arrh
        ELSE
          arr4d=>rvec%arrv
        ENDIF
        DO iside=1,rvec%n_side
          arr4d(:,iside,ix,iy)=arr4d(:,iside,ix,iy)*mult(:,1)
        ENDDO
      ENDIF
    ENDDO vert
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_real(rvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_real',iftn,idepth)
    DO iv=1,rvec%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=rvec%mx+1,rvec%mx+rvec%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=rvec%mx+rvec%my+1,2*rvec%mx+rvec%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*rvec%mx+rvec%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_real

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_real(rvec,edge,nqty,n_side,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%nmodes_loaded=0_i4
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_in(1:nq,iv)=                                                    &
        rvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  load side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_in(iq:nq+iq-1,iv)=rvec%arrh(1:nq,is,ix,iy)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_in(iq:nq+iq-1,iv)=rvec%arrv(1:nq,is,ix,iy)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   apply average factor
!-------------------------------------------------------------------------------
    IF (do_avg_loc) THEN
      DO iv=1,edge%nvert
        edge%vert_in(1:nq,iv)=                                                  &
          edge%vert_in(1:nq,iv)*edge%vertex(iv)%ave_factor
        IF (ns/=0) THEN
          edge%seg_in(1:nq*ns,iv)=                                              &
            edge%seg_in(1:nq*ns,iv)*edge%segment(iv)%ave_factor
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_real

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_real(rvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      rvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))=         &
        edge%vert_out(1:nq,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  unload side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            rvec%arrh(1:nq,is,ix,iy)=edge%seg_out(iq:nq+iq-1,iv)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            rvec%arrv(1:nq,is,ix,iy)=edge%seg_out(iq:nq+iq-1,iv)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_real

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_real(rvec,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_save(1:nq,iv)=                                                  &
        rvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          edge%seg_save(1:nq,:,iv)=rvec%arrh(1:nq,:,ix,iy)
        ELSE
          edge%seg_save(1:nq,:,iv)=rvec%arrv(1:nq,:,ix,iy)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_real

!-------------------------------------------------------------------------------
!* unload a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_real(rvec,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      rvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))=         &
        rvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))        &
        +edge%vert_save(1:nq,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          rvec%arrh(1:nq,:,ix,iy)=rvec%arrh(1:nq,:,ix,iy)+                      &
            edge%seg_save(1:nq,:,iv)
        ELSE
          rvec%arrv(1:nq,:,ix,iy)=rvec%arrv(1:nq,:,ix,iy)+                      &
            edge%seg_save(1:nq,:,iv)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_real

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_real(rvec,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to multiply
    CLASS(rvector), INTENT(IN) :: input_vec
    !> output vector = rvec (matrix diagonal) dot input_vec
    CLASS(rvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_real',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_real)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_real)
        IF (ASSOCIATED(rvec%arr)) THEN
          output_vec%arr=rvec%arr*input_vec%arr
        ENDIF
        IF (ASSOCIATED(rvec%arrh)) THEN
          output_vec%arrh=rvec%arrh*input_vec%arrh
        ENDIF
        IF (ASSOCIATED(rvec%arrv)) THEN
          output_vec%arrv=rvec%arrv*input_vec%arrv
        ENDIF
        IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
          output_vec%arri=rvec%arri*input_vec%arri
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_real for output_vec'            &
                          //' in apply_diag_matvec_real')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for input_vec'               &
                        //' in apply_diag_matvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_real

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_real(rvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) THEN
      rvec%arr=1._r8/rvec%arr
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      rvec%arrh=1._r8/rvec%arrh
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      rvec%arrv=1._r8/rvec%arrv
    ENDIF
    IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
      rvec%arri=1._r8/rvec%arri
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_real

!-------------------------------------------------------------------------------
!* Import vector dofs from a flat-array storage version
!-------------------------------------------------------------------------------
  SUBROUTINE export_to_flat_real(rvec,rvec_flat,dof_map)
    USE compressed_matrix_real_mod
    IMPLICIT NONE

    !> vector to copy to
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> vector to copy from
    TYPE(flat_vector_real), INTENT(INOUT) :: rvec_flat
    !> dof map to use in copy
    CLASS(ivector), INTENT(IN) :: dof_map

    INTEGER(i4) :: iy,ix,ii,iq,is,idof
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'export_to_flat',iftn,idepth)
    SELECT TYPE(dof_map)
    TYPE IS(vec_rect_2D_int)
      DO iy=0,rvec%my
        DO ix=0,rvec%mx
          IF (.NOT.rvec%skip_elim_interior) THEN
            IF (ix/=rvec%mx.AND.iy/=rvec%my) THEN
              DO ii=1,rvec%n_int
                DO iq=1,rvec%nqty
                  idof=dof_map%arri(iq,ii,ix+1,iy+1)
                  rvec_flat%val(idof)=rvec%arri(iq,ii,ix+1,iy+1)
                ENDDO
              ENDDO
            ENDIF
          ENDIF
          IF (iy/=rvec%my) THEN
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                idof=dof_map%arrv(iq,is,ix,iy+1)
                rvec_flat%val(idof)=rvec%arrv(iq,is,ix,iy+1)
              ENDDO
            ENDDO
          ENDIF
          IF (ix/=rvec%mx) THEN
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                idof=dof_map%arrh(iq,is,ix+1,iy)
                rvec_flat%val(idof)=rvec%arrh(iq,is,ix+1,iy)
              ENDDO
            ENDDO
          ENDIF
          DO iq=1,rvec%nqty
            idof=dof_map%arr(iq,ix,iy)
            rvec_flat%val(idof)=rvec%arr(iq,ix,iy)
          ENDDO
        ENDDO
      ENDDO
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for dof_map'                  &
                        //' in export_to_flat')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE export_to_flat_real

!-------------------------------------------------------------------------------
!* Export vector dofs to a flat-array storage version
!-------------------------------------------------------------------------------
  SUBROUTINE import_from_flat_real(rvec,rvec_flat,dof_map)
    USE compressed_matrix_real_mod
    IMPLICIT NONE

    !> vector to copy from
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> vector to copy to
    TYPE(flat_vector_real), INTENT(IN) :: rvec_flat
    !> dof map to use in copy
    CLASS(ivector), INTENT(IN) :: dof_map

    INTEGER(i4) :: iy,ix,ii,iq,is,idof
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'import_from_flat_real',iftn,idepth)
    SELECT TYPE(dof_map)
    TYPE IS(vec_rect_2D_int)
      DO iy=0,rvec%my
        DO ix=0,rvec%mx
          IF (.NOT.rvec%skip_elim_interior) THEN
            IF (ix/=rvec%mx.AND.iy/=rvec%my) THEN
              DO ii=1,rvec%n_int
                DO iq=1,rvec%nqty
                  idof=dof_map%arri(iq,ii,ix+1,iy+1)
                  rvec%arri(iq,ii,ix+1,iy+1)=rvec_flat%val(idof)
                ENDDO
              ENDDO
            ENDIF
          ENDIF
          IF (iy/=rvec%my) THEN
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                idof=dof_map%arrv(iq,is,ix,iy+1)
                rvec%arrv(iq,is,ix,iy+1)=rvec_flat%val(idof)
              ENDDO
            ENDDO
          ENDIF
          IF (ix/=rvec%mx) THEN
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                idof=dof_map%arrh(iq,is,ix+1,iy)
                rvec%arrh(iq,is,ix+1,iy)=rvec_flat%val(idof)
              ENDDO
            ENDDO
          ENDIF
          DO iq=1,rvec%nqty
            idof=dof_map%arr(iq,ix,iy)
            rvec%arr(iq,ix,iy)=rvec_flat%val(idof)
          ENDDO
        ENDDO
      ENDDO
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_int for dof_map'                  &
                        //' in import_from_flat')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE import_from_flat_real

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_real(rvec,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real), INTENT(INOUT) :: rvec
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    REAL(r8), INTENT(IN), OPTIONAL :: const

    REAL(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ind
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_real',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(rvec%arr)) THEN
        ASSOCIATE (arr=>rvec%arr)
          arr=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        ASSOCIATE (arrh=>rvec%arrh)
          arrh=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        ASSOCIATE (arrv=>rvec%arrv)
          arrv=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>rvec%arri)
          arri=fac
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(rvec%arr)) THEN
        ASSOCIATE (arr=>rvec%arr)
          DO ii1=LBOUND(arr,1),UBOUND(arr,1)
            DO ii2=LBOUND(arr,2),UBOUND(arr,2)
              DO ii3=LBOUND(arr,3),UBOUND(arr,3)
                ind=ii1*SIZE(arr,2)*SIZE(arr,3)+ii2*SIZE(arr,3)+ii3
                arr(ii1,ii2,ii3)=ind
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        ASSOCIATE (arrh=>rvec%arrh)
          DO ii1=LBOUND(arrh,1),UBOUND(arrh,1)
            DO ii2=LBOUND(arrh,2),UBOUND(arrh,2)
              DO ii3=LBOUND(arrh,3),UBOUND(arrh,3)
                DO ii4=LBOUND(arrh,4),UBOUND(arrh,4)
                  ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)+               &
                      ii2*SIZE(arrh,3)*SIZE(arrh,4)+ii3*SIZE(arrh,4)+ii4
                  arrh(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        ASSOCIATE (arrv=>rvec%arrv)
          DO ii1=LBOUND(arrv,1),UBOUND(arrv,1)
            DO ii2=LBOUND(arrv,2),UBOUND(arrv,2)
              DO ii3=LBOUND(arrv,3),UBOUND(arrv,3)
                DO ii4=LBOUND(arrv,4),UBOUND(arrv,4)
                  ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)+               &
                      ii2*SIZE(arrv,3)*SIZE(arrv,4)+ii3*SIZE(arrv,4)+ii4
                  arrv(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>rvec%arri)
          DO ii1=LBOUND(arri,1),UBOUND(arri,1)
            DO ii2=LBOUND(arri,2),UBOUND(arri,2)
              DO ii3=LBOUND(arri,3),UBOUND(arri,3)
                DO ii4=LBOUND(arri,4),UBOUND(arri,4)
                  ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)+               &
                      ii2*SIZE(arri,3)*SIZE(arri,4)+ii3*SIZE(arri,4)+ii4
                  arri(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                        //' in assign_for_testing_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_real

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_real(rvec,rvec2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real), INTENT(IN) :: rvec
    !> vector to test against
    CLASS(rvector), INTENT(IN) :: rvec2

    INTEGER(i4) :: shape3a(3),shape3b(3),shape4a(4),shape4b(4)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_real',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real)
      IF (ASSOCIATED(rvec%arr)) THEN
        shape3a=SHAPE(rvec%arr)
        shape3b=SHAPE(rvec2%arr)
        IF (.NOT.ALL(shape3a == shape3b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arr == rvec2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        shape4a=SHAPE(rvec%arrh)
        shape4b=SHAPE(rvec2%arrh)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arrh == rvec2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        shape4a=SHAPE(rvec%arrv)
        shape4b=SHAPE(rvec2%arrv)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arrv == rvec2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        shape4a=SHAPE(rvec%arri)
        shape4b=SHAPE(rvec2%arri)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arri == rvec2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2'//                  &
                        ' in test_if_equal_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_real
