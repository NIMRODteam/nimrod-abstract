!-------------------------------------------------------------------------------
!! Defines the compressed sparse storage matrix storage for real types
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the compressed sparse storage matrix storage types for storing
!  block-coalesced data with compressed sparse row (CSR) or compressed sparse
!  column (CSC) format. Note that a matrix stored in CSR format is the transpose
!  of the same matrix stored in CSC format.
!
!  Zero indexing is used to be compliant with external solvers.
!-------------------------------------------------------------------------------
MODULE compressed_matrix_real_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: flat_vector_real

  PUBLIC :: compressed_matrix_sparsity_pattern

  PUBLIC :: compressed_matrix_real

  CHARACTER(*), PARAMETER :: mod_name='compressed_matrix_real_mod'
!-------------------------------------------------------------------------------
!> Vector storage as a flat array for a real data type.
!-------------------------------------------------------------------------------
  TYPE :: flat_vector_real
    !> Number of rows (and columns as all matrices are square)
    INTEGER(i4) :: nrow=0
    !> Number of rows across all processes
    INTEGER(i4) :: nrow_total=0
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
    !> vector value array
    REAL(r8), ALLOCATABLE :: val(:)
  CONTAINS

    PROCEDURE, PASS(vec) :: alloc => alloc_vec_real
    PROCEDURE, PASS(vec) :: dealloc => dealloc_vec_real
    PROCEDURE, PASS(vec) :: zero => zero_vec_real
  END TYPE flat_vector_real

!-------------------------------------------------------------------------------
!> Sparsity pattern information storage or distributed compressed sparse
! storage.
!-------------------------------------------------------------------------------
  TYPE :: compressed_matrix_sparsity_pattern
    !> Number of rows (and columns as all matrices are square)
    INTEGER(i4) :: nrow=0
    !> Number of rows across all processes
    INTEGER(i4) :: nrow_total=0
    !> first row in global DOF indexing
    INTEGER(i4) :: firstrow=0
    !> last row in global DOF indexing
    INTEGER(i4) :: lastrow=0
    !> Number of local nonzeros
    INTEGER(i4) :: nnz=0
    !> first nonzero in global indexing
    INTEGER(i4) :: firstnz=0
    !> last nonzero in global indexing
    INTEGER(i4) :: lastnz=0
    !> Number of nonzeros across all processes
    INTEGER(i4) :: nnz_total=0
    !> True if CSR format, otherwise CSC
    LOGICAL :: csr_format=.FALSE.
    !> index of each value (row for CSR, column for CSC), size nnz
    INTEGER(i4), ALLOCATABLE :: ind(:)
    !> pointer to each row/column start for CSR/CSC, respectively, size nrow
    INTEGER(i4), ALLOCATABLE :: ptr(:)
    !> external solvers may modify the index array, save a copy
    INTEGER(i4), ALLOCATABLE :: ind_save(:)
    !> external solvers may modify the pointer array, save a copy
    INTEGER(i4), ALLOCATABLE :: ptr_save(:)
  END TYPE compressed_matrix_sparsity_pattern

!-------------------------------------------------------------------------------
!> Distributed compressed sparse storage of a matrix with a real data type.
!-------------------------------------------------------------------------------
  TYPE :: compressed_matrix_real
    !> Sparsity pattern and associated information
    TYPE(compressed_matrix_sparsity_pattern) :: sp
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
    !> matrix values, size nnz
    REAL(r8), ALLOCATABLE :: val(:)
  CONTAINS

    PROCEDURE, PASS(mat) :: alloc => alloc_mat_real
    PROCEDURE, PASS(mat) :: dealloc => dealloc_mat_real
    PROCEDURE, PASS(mat) :: zero => zero_mat_real
    PROCEDURE, PASS(mat) :: matvec => matvec_real
  END TYPE compressed_matrix_real

CONTAINS

!-------------------------------------------------------------------------------
!* Allocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_vec_real(vec,nrow,id)
    USE pardata_mod
    IMPLICIT NONE

    !> vector to allocate
    CLASS(flat_vector_real), INTENT(INOUT) :: vec
    !> Number of rows (local to this process)
    INTEGER(i4), INTENT(IN) :: nrow
    !> ID for parallel streaming
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_vec_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   throw an error if allocated
!-------------------------------------------------------------------------------
    IF (ALLOCATED(vec%val)) THEN
      CALL par%nim_stop("flat_vector_real tried to reallocate")
    ENDIF
!-------------------------------------------------------------------------------
!   do allocation and set internal variables
!-------------------------------------------------------------------------------
    vec%nrow=nrow
    vec%id=id
    ALLOCATE(vec%val(0:nrow-1))
    CALL par%layer_sum(nrow,vec%nrow_total)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_vec_real

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_vec_real(vec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(flat_vector_real), INTENT(INOUT) :: vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_vec_real',iftn,idepth)
    IF (ALLOCATED(vec%val)) DEALLOCATE(vec%val)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_vec_real

!-------------------------------------------------------------------------------
!* Zero the vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_vec_real(vec)
    IMPLICIT NONE

    !> vector to zero
    CLASS(flat_vector_real), INTENT(INOUT) :: vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_vec_real',iftn,idepth)
    vec%val=0._r8
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_vec_real

!-------------------------------------------------------------------------------
!* Allocate the matrix quantities that depend on nrow
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_mat_real(mat,nrow,firstrow,lastrow,nnz,firstnz,lastnz,id)
    USE pardata_mod
    IMPLICIT NONE

    !> matrix to allocate
    CLASS(compressed_matrix_real), INTENT(INOUT) :: mat
    !> Number of rows (local to this process)
    INTEGER(i4), INTENT(IN) :: nrow
    !> first row in global DOF indexing
    INTEGER(i4), INTENT(IN) :: firstrow
    !> last row in global DOF indexing
    INTEGER(i4), INTENT(IN) :: lastrow
    !> Number of non-zeros (local to this process)
    INTEGER(i4), INTENT(IN) :: nnz
    !> first non-zero with global indexing
    INTEGER(i4), INTENT(IN) :: firstnz
    !> last non-zero with global indexing
    INTEGER(i4), INTENT(IN) :: lastnz
    !> ID for parallel streaming
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_mat_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   throw an error if allocated
!-------------------------------------------------------------------------------
    IF (ALLOCATED(mat%sp%ind)) THEN
      CALL par%nim_stop("compressed_matrix_real tried to reallocate")
    ENDIF
!-------------------------------------------------------------------------------
!   do allocation and set internal variables
!-------------------------------------------------------------------------------
    mat%sp%nrow=nrow
    mat%sp%firstrow=firstrow
    mat%sp%lastrow=lastrow
    mat%sp%nnz=nnz
    mat%sp%firstnz=firstnz
    mat%sp%lastnz=lastnz
    mat%id=id
    ALLOCATE(mat%sp%ptr(firstrow:lastrow+1),mat%sp%ptr_save(firstrow:lastrow+1))
    ALLOCATE(mat%sp%ind(firstnz:lastnz),mat%sp%ind_save(firstnz:lastnz))
    ALLOCATE(mat%val(firstnz:lastnz))
    CALL par%layer_sum(nrow,mat%sp%nrow_total)
    CALL par%layer_sum(nnz,mat%sp%nnz_total)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_mat_real

!-------------------------------------------------------------------------------
!* Deallocate the matrix
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_mat_real(mat)
    IMPLICIT NONE

    !> matrix to deallocate
    CLASS(compressed_matrix_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_mat_real',iftn,idepth)
    IF (ALLOCATED(mat%val)) DEALLOCATE(mat%val)
    IF (ALLOCATED(mat%sp%ind)) DEALLOCATE(mat%sp%ind)
    IF (ALLOCATED(mat%sp%ptr)) DEALLOCATE(mat%sp%ptr)
    IF (ALLOCATED(mat%sp%ind_save)) DEALLOCATE(mat%sp%ind_save)
    IF (ALLOCATED(mat%sp%ptr_save)) DEALLOCATE(mat%sp%ptr_save)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_mat_real

!-------------------------------------------------------------------------------
!* Zero the matrix
!-------------------------------------------------------------------------------
  SUBROUTINE zero_mat_real(mat)
    IMPLICIT NONE

    !> mattor to zero
    CLASS(compressed_matrix_real), INTENT(INOUT) :: mat

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_mat_real',iftn,idepth)
    mat%val=0._r8
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_mat_real

!-------------------------------------------------------------------------------
!* matrix vector product
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_real(mat,operand,output)
    USE pardata_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(compressed_matrix_real), INTENT(IN) :: mat
    !> vector to multiply
    CLASS(flat_vector_real), INTENT(IN) :: operand
    !> result
    CLASS(flat_vector_real), INTENT(INOUT) :: output

    INTEGER(i4) :: irow,inz,inz_col,icol,inz_row
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_real',iftn,idepth)
#ifdef DEBUG
    IF (mat%sp%nrow/=operand%nrow.OR.mat%sp%nrow/=output%nrow)                  &
      CALL par%nim_stop('Incompatible sizes in compressed_matrix_mod'           &
                        //' matvec_real')
#endif
    CALL output%zero
    inz=0_i4
    IF (mat%sp%csr_format) THEN
      DO irow=0,mat%sp%nrow-1
        DO inz_col=mat%sp%ptr(irow),mat%sp%ptr(irow+1)-1
          icol=mat%sp%ind(inz_col)
          output%val(irow)=output%val(irow)+mat%val(inz)*operand%val(icol)
          inz=inz+1
        ENDDO
      ENDDO
    ELSE ! CSC
      DO icol=0,mat%sp%nrow-1
        DO inz_row=mat%sp%ptr(icol),mat%sp%ptr(icol+1)-1
          irow=mat%sp%ind(inz_row)
          output%val(irow)=output%val(irow)+mat%val(inz)*operand%val(icol)
          inz=inz+1
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_real

END MODULE compressed_matrix_real_mod
