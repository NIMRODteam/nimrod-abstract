!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_comp_acc included in vec_rect_2D_acc.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a complex vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_comp(cvec,poly_degree,mx,my,nqty,nmodes,id,on_gpu)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> number of Fourier modes
    INTEGER(i4), INTENT(IN) :: nmodes
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id
    !> true if data on GPU
    LOGICAL, INTENT(IN) :: on_gpu

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    cvec%nqty=nqty
    cvec%mx=mx
    cvec%my=my
    cvec%n_side=poly_degree-1
    cvec%n_int=(poly_degree-1)**2
    cvec%u_ndof=(poly_degree+1)**2
    cvec%pd=poly_degree
    cvec%nel=mx*my
    cvec%ndim=2
    cvec%nmodes=nmodes
    cvec%id=id
    cvec%on_gpu=on_gpu
    cvec%inf_norm_size=4
    cvec%l2_dot_size=5
    !$acc enter data copyin(cvec) async(cvec%id) if(cvec%on_gpu)
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!   pad all basis type arrays, meaningful element bounds are
!   arr(0:mx,0:my) arrh(1:mx,0:my) arrv(0:mx,1:my) and arri(1:mx,1:my)
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(cvec%arr(nqty,-1:mx+1,-1:my+1,nmodes))
      !$acc enter data create(cvec%arr) async(cvec%id) if(cvec%on_gpu)
      NULLIFY(cvec%arri,cvec%arrh,cvec%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(cvec%arr(nqty,-1:mx+1,-1:my+1,nmodes))
      !$acc enter data create(cvec%arr) async(cvec%id) if(cvec%on_gpu)
      ALLOCATE(cvec%arrh(nqty,poly_degree-1,-1:mx+1,-1:my+1,nmodes))
      !$acc enter data create(cvec%arrh) async(cvec%id) if(cvec%on_gpu)
      ALLOCATE(cvec%arrv(nqty,poly_degree-1,-1:mx+1,-1:my+1,nmodes))
      !$acc enter data create(cvec%arrv) async(cvec%id) if(cvec%on_gpu)
      ALLOCATE(cvec%arri(nqty,(poly_degree-1)**2,-1:mx+1,-1:my+1,nmodes))
      !$acc enter data create(cvec%arri) async(cvec%id) if(cvec%on_gpu)
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(cvec%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(cvec%arr)+SIZEOF(cvec%arrh)+SIZEOF(cvec%arrv)              &
             +SIZEOF(cvec%arri)+SIZEOF(cvec),i4)
      CALL memlogger%update(cvec%mem_id,'cvec'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_comp

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_comp(cvec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) THEN
      !$acc exit data delete(cvec%arr) async(cvec%id) finalize if(cvec%on_gpu)
      DEALLOCATE(cvec%arr)
      NULLIFY(cvec%arr)
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      !$acc exit data delete(cvec%arrh) async(cvec%id) finalize if(cvec%on_gpu)
      DEALLOCATE(cvec%arrh)
      NULLIFY(cvec%arrh)
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      !$acc exit data delete(cvec%arrv) async(cvec%id) finalize if(cvec%on_gpu)
      DEALLOCATE(cvec%arrv)
      NULLIFY(cvec%arrv)
    ENDIF
    IF (ASSOCIATED(cvec%arri)) THEN
      !$acc exit data delete(cvec%arri) async(cvec%id) finalize if(cvec%on_gpu)
      DEALLOCATE(cvec%arri)
      NULLIFY(cvec%arri)
    ENDIF
    !$acc exit data delete(cvec) finalize async(cvec%id) if(cvec%on_gpu)
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(cvec%mem_id,'cvec'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_comp(cvec,new_cvec,nqty,pd,nmodes)
    IMPLICIT NONE

    !> reference cvector to mold
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> new cvector to be created
    CLASS(cvector), ALLOCATABLE, INTENT(OUT) :: new_cvec
    !> number of quantities, cvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    !> number of Fourier components, cvec%nmodes is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nmodes

    INTEGER(i4) :: new_nqty,new_pd,new_nmodes
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_comp',iftn,idepth)
    new_nqty=cvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
    new_nmodes=cvec%nmodes
    IF (PRESENT(nmodes)) THEN
      new_nmodes=nmodes
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_comp_acc::new_cvec)
    SELECT TYPE (new_cvec)
    TYPE IS (vec_rect_2D_comp_acc)
      CALL new_cvec%alloc(new_pd,cvec%mx,cvec%my,new_nqty,new_nmodes,           &
                          cvec%id,cvec%on_gpu)
    END SELECT
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cvec%skip_elim_interior=cvec%skip_elim_interior
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_comp

!-------------------------------------------------------------------------------
!* Create a new cvector1m from this cvector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_cv1m_comp(cvec,new_cv1m,nqty,pd)
    IMPLICIT NONE

    !> reference cvector to mold
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> new cvector1m to be created
    CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
    !> number of quantities, cvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd

    new_nqty=cvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_cv1m_acc::new_cv1m)
    SELECT TYPE (new_cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      CALL new_cv1m%alloc(new_pd,cvec%mx,cvec%my,new_nqty,cvec%id,cvec%on_gpu)
    END SELECT
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cv1m%skip_elim_interior=cvec%skip_elim_interior
  END SUBROUTINE alloc_with_mold_cv1m_comp

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_comp(cvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_comp',iftn,idepth)
    IF (cvec%pd>1) THEN
      ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,arri=>cvec%arri)
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) async(cvec%id) if(cvec%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) async(cvec%id) if(cvec%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          arri=0.0_r8
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>cvec%arr)
        !$acc kernels present(arr) async(cvec%id) if(cvec%on_gpu)
        arr=0.0_r8
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_comp

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_comp(cvec,rvec,imode,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_comp',iftn,idepth)
    cvec%skip_elim_interior=rvec%skip_elim_interior
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>rvec%arr,                                   &
                arrh=>cvec%arrh,arrh2=>rvec%arrh,                               &
                arrv=>cvec%arrv,arrv2=>rvec%arrv,                               &
                arri=>cvec%arri,arri2=>rvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr(:,:,:,imode)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:,imode))
              arrh(:,:,:,:,imode)=arrh2(:,:,:,:)+(0,1)*AIMAG(arrh(:,:,:,:,imode))
              arrv(:,:,:,:,imode)=arrv2(:,:,:,:)+(0,1)*AIMAG(arrv(:,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr(:,:,:,imode)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:,imode))
              arrh(:,:,:,:,imode)=arrh2(:,:,:,:)+(0,1)*AIMAG(arrh(:,:,:,:,imode))
              arrv(:,:,:,:,imode)=arrv2(:,:,:,:)+(0,1)*AIMAG(arrv(:,:,:,:,imode))
              arri(:,:,:,:,imode)=arri2(:,:,:,:)+(0,1)*AIMAG(arri(:,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(cvec%id) if(cvec%on_gpu)
            arr(:,:,:,imode)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr(:,:,:,imode)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:,imode))
              arrh(:,:,:,:,imode)=(0,1)*arrh2(:,:,:,:)+REAL(arrh(:,:,:,:,imode))
              arrv(:,:,:,:,imode)=(0,1)*arrv2(:,:,:,:)+REAL(arrv(:,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr(:,:,:,imode)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:,imode))
              arrh(:,:,:,:,imode)=(0,1)*arrh2(:,:,:,:)+REAL(arrh(:,:,:,:,imode))
              arrv(:,:,:,:,imode)=(0,1)*arrv2(:,:,:,:)+REAL(arrv(:,:,:,:,imode))
              arri(:,:,:,:,imode)=(0,1)*arri2(:,:,:,:)+REAL(arri(:,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(cvec%id) if(cvec%on_gpu)
            arr(:,:,:,imode)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assign_rvec_cvec: '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in assign_rvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_comp

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_comp(cvec,rvec,imode,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_comp',iftn,idepth)
    cvec%skip_elim_interior=rvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>rvec%arr,                                   &
                arrh=>cvec%arrh,arrh2=>rvec%arrh,                               &
                arrv=>cvec%arrv,arrv2=>rvec%arrv,                               &
                arri=>cvec%arri,arri2=>rvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(imode) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,imode)=                                           &
                arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:,imode))
              arrh(v1s:v1e,:,:,:,imode)=                                        &
                arrh2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrh(v1s:v1e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:,imode)=                                        &
                arrv2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrv(v1s:v1e,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(imode) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,imode)=                                           &
                arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:,imode))
              arrh(v1s:v1e,:,:,:,imode)=                                        &
                arrh2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrh(v1s:v1e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:,imode)=                                        &
                arrv2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrv(v1s:v1e,:,:,:,imode))
              arri(v1s:v1e,:,:,:,imode)=                                        &
                arri2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arri(v1s:v1e,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(imode)                       &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,imode)=                                             &
              arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,imode)=                                           &
                (0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:,imode))
              arrh(v1s:v1e,:,:,:,imode)=                                        &
                (0,1)*arrh2(v2s:v2e,:,:,:)+REAL(arrh(v1s:v1e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:,imode)=                                        &
                (0,1)*arrv2(v2s:v2e,:,:,:)+REAL(arrv(v1s:v1e,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,imode)=                                           &
                (0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:,imode))
              arrh(v1s:v1e,:,:,:,imode)=                                        &
                (0,1)*arrh2(v2s:v2e,:,:,:)+REAL(arrh(v1s:v1e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:,imode)=                                        &
                (0,1)*arrv2(v2s:v2e,:,:,:)+REAL(arrv(v1s:v1e,:,:,:,imode))
              arri(v1s:v1e,:,:,:,imode)=                                        &
                (0,1)*arri2(v2s:v2e,:,:,:)+REAL(arri(v1s:v1e,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e,imode)       &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,imode)=                                             &
              (0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assignp_rvec_cvec: '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in'//                &
                        ' assignp_rvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_comp

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_comp(cvec,cv1m,imode)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_comp',iftn,idepth)
    cvec%skip_elim_interior=cv1m%skip_elim_interior
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>cv1m%arr,                                   &
                arrh=>cvec%arrh,arrh2=>cv1m%arrh,                               &
                arrv=>cvec%arrv,arrv2=>cv1m%arrv,                               &
                arri=>cvec%arri,arri2=>cv1m%arri)
        IF (cvec%pd>1) THEN
          IF (cvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(imode) async(cvec%id) if(cvec%on_gpu)
            arr(:,:,:,imode)=arr2(:,:,:)
            arrh(:,:,:,:,imode)=arrh2(:,:,:,:)
            arrv(:,:,:,:,imode)=arrv2(:,:,:,:)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(imode) async(cvec%id) if(cvec%on_gpu)
            arr(:,:,:,imode)=arr2(:,:,:)
            arrh(:,:,:,:,imode)=arrh2(:,:,:,:)
            arrv(:,:,:,:,imode)=arrv2(:,:,:,:)
            arri(:,:,:,:,imode)=arri2(:,:,:,:)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(imode)                         &
          !$acc async(cvec%id) if(cvec%on_gpu)
          arr(:,:,:,imode)=arr2(:,:,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec in assign_cv1m_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_comp

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_comp(cvec,cv1m,imode,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cv1m_comp',iftn,idepth)
    cvec%skip_elim_interior=cv1m%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>cv1m%arr,                                   &
                arrh=>cvec%arrh,arrh2=>cv1m%arrh,                               &
                arrv=>cvec%arrv,arrv2=>cv1m%arrv,                               &
                arri=>cvec%arri,arri2=>cv1m%arri)
        IF (cvec%pd>1) THEN
          IF (cvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,imode)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:,imode)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:,imode)=arrv2(v2s:v2e,:,:,:)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,imode)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:,imode)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:,imode)=arrv2(v2s:v2e,:,:,:)
            arri(v1s:v1e,:,:,:,imode)=arri2(v2s:v2e,:,:,:)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)               &
          !$acc copyin(imode) async(cvec%id) if(cvec%on_gpu)
          arr(v1s:v1e,:,:,imode)=arr2(v2s:v2e,:,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec in'//                &
                        ' assignp_cv1m_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_comp

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_comp(cvec,cvec2)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_comp',iftn,idepth)
    cvec%skip_elim_interior=cvec2%skip_elim_interior
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>cvec2%arr,                                  &
                arrh=>cvec%arrh,arrh2=>cvec2%arrh,                              &
                arrv=>cvec%arrv,arrv2=>cvec2%arrv,                              &
                arri=>cvec%arri,arri2=>cvec2%arri)
        IF (cvec%pd>1) THEN
          IF (cvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            arri=arri2
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) async(cvec%id) if(cvec%on_gpu)
          arr=arr2
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assign_cvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_comp

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_comp(cvec,cvec2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_comp',iftn,idepth)
    cvec%skip_elim_interior=cvec2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>cvec2%arr,                                  &
                arrh=>cvec%arrh,arrh2=>cvec2%arrh,                              &
                arrv=>cvec%arrv,arrv2=>cvec2%arrv,                              &
                arri=>cvec%arri,arri2=>cvec2%arri)
        IF (cvec%pd>1) THEN
          IF (cvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(v1s,v1e,v2s,v2e) async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,:)=arr2(v2s:v2e,:,:,:)
            arrh(v1s:v1e,:,:,:,:)=arrh2(v2s:v2e,:,:,:,:)
            arrv(v1s:v1e,:,:,:,:)=arrv2(v2s:v2e,:,:,:,:)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(v1s,v1e,v2s,v2e) async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,:)=arr2(v2s:v2e,:,:,:)
            arrh(v1s:v1e,:,:,:,:)=arrh2(v2s:v2e,:,:,:,:)
            arrv(v1s:v1e,:,:,:,:)=arrv2(v2s:v2e,:,:,:,:)
            arri(v1s:v1e,:,:,:,:)=arri2(v2s:v2e,:,:,:,:)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)               &
          !$acc async(cvec%id) if(cvec%on_gpu)
          arr(v1s:v1e,:,:,:)=arr2(v2s:v2e,:,:,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assignp_cvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_comp

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_comp(cvec,cvec2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    COMPLEX(r8) :: v1f,v2f
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'add_vec_comp',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_cmplx_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_cmplx_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>cvec%arr,arr2=>cvec2%arr,                                  &
                arrh=>cvec%arrh,arrh2=>cvec2%arrh,                              &
                arrv=>cvec%arrv,arrv2=>cvec2%arrv,                              &
                arri=>cvec%arri,arri2=>cvec2%arri)
        IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f) async(cvec%id) if(cvec%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f) async(cvec%id) if(cvec%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              arri=v1f*arri+v2f*arri2
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1f,v2f)                     &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr=v1f*arr+v2f*arr2
            !$acc end kernels
          ENDIF
        ELSE
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f,v1s,v1e,v2s,v2e) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,:)=v1f*arr(v1s:v1e,:,:,:)+v2f*arr2(v2s:v2e,:,:,:)
              arrh(v1s:v1e,:,:,:,:)=                                            &
                v1f*arrh(v1s:v1e,:,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:,:)
              arrv(v1s:v1e,:,:,:,:)=                                            &
                v1f*arrv(v1s:v1e,:,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:,:)
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f,v1s,v1e,v2s,v2e) async(cvec%id) if(cvec%on_gpu)
              arr(v1s:v1e,:,:,:)=v1f*arr(v1s:v1e,:,:,:)+v2f*arr2(v2s:v2e,:,:,:)
              arrh(v1s:v1e,:,:,:,:)=                                            &
                v1f*arrh(v1s:v1e,:,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:,:)
              arrv(v1s:v1e,:,:,:,:)=                                            &
                v1f*arrv(v1s:v1e,:,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:,:)
              arri(v1s:v1e,:,:,:,:)=                                            &
                v1f*arri(v1s:v1e,:,:,:,:)+v2f*arri2(v2s:v2e,:,:,:,:)
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1f,v2f,v1s,v1e,v2s,v2e)     &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr(v1s:v1e,:,:,:)=v1f*arr(v1s:v1e,:,:,:)+v2f*arr2(v2s:v2e,:,:,:)
            !$acc end kernels
          ENDIF
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec2 in add_vec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_comp(cvec,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_comp',iftn,idepth)
    ASSOCIATE(arr=>cvec%arr,arrh=>cvec%arrh,                                    &
              arrv=>cvec%arrv,arri=>cvec%arri)
      IF (cvec%pd>1) THEN
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(rscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(rscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          arri=arri*rscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(rscalar) async(cvec%id) if(cvec%on_gpu)
        arr=arr*rscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_comp(cvec,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_comp',iftn,idepth)
    ASSOCIATE(arr=>cvec%arr,arrh=>cvec%arrh,                                    &
              arrv=>cvec%arrv,arri=>cvec%arri)
      IF (cvec%pd>1) THEN
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(cscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*cscalar
          arrh=arrh*cscalar
          arrv=arrv*cscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(cscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*cscalar
          arrh=arrh*cscalar
          arrv=arrv*cscalar
          arri=arri*cscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(cscalar) async(cvec%id) if(cvec%on_gpu)
        arr=arr*cscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_comp(cvec,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_comp',iftn,idepth)
    ASSOCIATE(arr=>cvec%arr,arrh=>cvec%arrh,                                    &
              arrv=>cvec%arrv,arri=>cvec%arri)
      IF (cvec%pd>1) THEN
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(iscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(iscalar) async(cvec%id) if(cvec%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          arri=arri*iscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(iscalar) async(cvec%id) if(cvec%on_gpu)
        arr=arr*iscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_comp

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_comp(cvec,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> inf norm contributions from the vector (size vec%inf_norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_comp',iftn,idepth)
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,                                   &
               arrv=>cvec%arrv,arri=>cvec%arri)
      IF (cvec%pd>1) THEN
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(infnorm(1:3))            &
          !$acc async(cvec%id) if(cvec%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          !$acc end kernels
          infnorm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(infnorm(1:4))       &
          !$acc async(cvec%id) if(cvec%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          infnorm(4)=MAXVAL(ABS(arri))
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyout(infnorm(1))                          &
        !$acc async(cvec%id) if(cvec%on_gpu)
        infnorm(1)=MAXVAL(ABS(arr))
        !$acc end kernels
        infnorm(2:4)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_comp

!-------------------------------------------------------------------------------
!* compute the L2 norm squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_comp(cvec,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> L2 norm contributions from the vector (size vec%l2_dot_size)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_comp',iftn,idepth)
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,                                   &
               arrv=>cvec%arrv,arri=>cvec%arri,                                 &
               vertex=>edge%vertex,segment=>edge%segment)
      IF (cvec%pd>1) THEN
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(l2norm(1:3))             &
          !$acc async(cvec%id) if(cvec%on_gpu)
          l2norm(1)=SUM(REAL(CONJG(arr)*arr))
          l2norm(2)=SUM(REAL(CONJG(arrh)*arrh))
          l2norm(3)=SUM(REAL(CONJG(arrv)*arrv))
          !$acc end kernels
          l2norm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(l2norm(1:4))        &
          !$acc async(cvec%id) if(cvec%on_gpu)
          l2norm(1)=SUM(REAL(CONJG(arr)*arr))
          l2norm(2)=SUM(REAL(CONJG(arrh)*arrh))
          l2norm(3)=SUM(REAL(CONJG(arrv)*arrv))
          l2norm(4)=SUM(REAL(CONJG(arri)*arri))
          !$acc end kernels
        ENDIF
!-------------------------------------------------------------------------------
!       boundary points must be divided by the number of internal
!       representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)               &
        !$acc copy(l2norm(5)) reduction(+:l2norm(5)) async(cvec%id) if(cvec%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(5)=l2norm(5)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(REAL(CONJG(arr(:,ix,iy,:))*arr(:,ix,iy,:)))
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrh(:,:,ix,iy,:))*arrh(:,:,ix,iy,:)))
          ELSE
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrv(:,:,ix,iy,:))*arrv(:,:,ix,iy,:)))
          ENDIF
        ENDDO
        !$acc end parallel
      ELSE
        !$acc kernels present(arr) copyout(l2norm(1))                           &
        !$acc async(cvec%id) if(cvec%on_gpu)
        l2norm(1)=SUM(REAL(CONJG(arr)*arr))
        !$acc end kernels
        !$acc parallel present(arr,edge,vertex) copy(l2norm(2))                 &
        !$acc reduction(+:l2norm(2)) async(cvec%id) if(cvec%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(REAL(CONJG(arr(:,ix,iy,:))*arr(:,ix,iy,:)))
        ENDDO
        !$acc end parallel
        l2norm(3:5)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_comp

!-------------------------------------------------------------------------------
!*  compute the dot product of cvec and cvec2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_comp(cvec,cvec2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> vector to dot
    CLASS(cvector), INTENT(IN) :: cvec2
    !> dot contributions from the vector (size vec%l2_dot_size)
    COMPLEX(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_comp',iftn,idepth)
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,                                 &
                 arrv=>cvec%arrv,arri=>cvec%arri,                               &
                 arr2=>cvec2%arr,arrh2=>cvec2%arrh,                             &
                 arrv2=>cvec2%arrv,arri2=>cvec2%arri,                           &
                 vertex=>edge%vertex,segment=>edge%segment)
        IF (cvec%pd>1) THEN
          IF (cvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyout(dot(1:3)) async(cvec%id) if(cvec%on_gpu)
            dot(1)=SUM(CONJG(arr)*arr2)
            dot(2)=SUM(CONJG(arrh)*arrh2)
            dot(3)=SUM(CONJG(arrv)*arrv2)
            !$acc end kernels
            dot(4)=0._r8
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyout(dot(1:4)) async(cvec%id) if(cvec%on_gpu)
            dot(1)=SUM(CONJG(arr)*arr2)
            dot(2)=SUM(CONJG(arrh)*arrh2)
            dot(3)=SUM(CONJG(arrv)*arrv2)
            dot(4)=SUM(CONJG(arri)*arri2)
            !$acc end kernels
          ENDIF
!-------------------------------------------------------------------------------
!         boundary points must be divided by the number of internal
!         representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
          !$acc parallel present(arr,arrh,arrv,arr2,arrh2,arrv2)                &
          !$acc present(edge,vertex,segment) copy(dot(5))                       &
          !$acc reduction(+:dot(5)) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(5)=dot(5)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(CONJG(arr(:,ix,iy,:))*arr2(:,ix,iy,:))
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrh(:,:,ix,iy,:))*arrh2(:,:,ix,iy,:))
            ELSE
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrv(:,:,ix,iy,:))*arrv2(:,:,ix,iy,:))
            ENDIF
          ENDDO
          !$acc end parallel
        ELSE
          !$acc kernels present(arr) copyout(dot(1))                            &
          !$acc async(cvec%id) if(cvec%on_gpu)
          dot(1)=SUM(CONJG(arr)*arr2)
          !$acc end kernels
          !$acc parallel present(arr,edge,vertex) copy(dot(2))                  &
          !$acc reduction(+:dot(2)) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(CONJG(arr(:,ix,iy,:))*arr2(:,ix,iy,:))
          ENDDO
          !$acc end parallel
          dot(3:5)=0._r8
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for vec2 in dot_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_comp

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_comp(cvec,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> integrand array
    COMPLEX(r8), INTENT(IN) :: integrand(:,:,:,:)

    INTEGER(i4) :: iq,iel,iv,iy,ix,is,ii,start_horz,start_vert,start_int,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_comp',iftn,idepth)
    start_horz=5
    start_vert=5+2*cvec%n_side
    start_int=5+4*cvec%n_side
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cvec%arr)) THEN
      ASSOCIATE (arr=>cvec%arr)
        id=par%get_stream(cvec%id,1,4)
        !$acc parallel present(arr,cvec,integrand)                              &
        !$acc wait(cvec%id) async(id) if(cvec%on_gpu)
        !$acc loop gang
        DO iq=1,cvec%nqty
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cvec%my-1
            DO ix=0,cvec%mx-1
              iel=1+ix+iy*cvec%mx
              arr(iq,ix,iy,:)=arr(iq,ix,iy,:)+integrand(iq,iel,1,:)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cvec%my-1
            DO ix=0,cvec%mx-1
              iel=1+ix+iy*cvec%mx
              arr(iq,ix+1,iy,:)=arr(iq,ix+1,iy,:)+integrand(iq,iel,2,:)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cvec%my-1
            DO ix=0,cvec%mx-1
              iel=1+ix+iy*cvec%mx
              arr(iq,ix,iy+1,:)=arr(iq,ix,iy+1,:)+integrand(iq,iel,3,:)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cvec%my-1
            DO ix=0,cvec%mx-1
              iel=1+ix+iy*cvec%mx
              arr(iq,ix+1,iy+1,:)=arr(iq,ix+1,iy+1,:)+integrand(iq,iel,4,:)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      ASSOCIATE (arrh=>cvec%arrh)
        id=par%get_stream(cvec%id,2,4)
        !$acc parallel present(arrh,cvec,integrand)                             &
        !$acc copyin(start_horz) wait(cvec%id) async(id) if(cvec%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cvec%nqty
          DO is=1,cvec%n_side
            iv=start_horz+2*(is-1)
            !$acc loop vector collapse(2) private(iel)
            DO iy=0,cvec%my-1
              DO ix=1,cvec%mx
                iel=ix+iy*cvec%mx
                arrh(iq,is,ix,iy,:)=arrh(iq,is,ix,iy,:)+integrand(iq,iel,iv,:)
              ENDDO
            ENDDO
            !$acc loop vector collapse(2) private(iel)
            DO iy=0,cvec%my-1
              DO ix=1,cvec%mx
                iel=ix+iy*cvec%mx
                arrh(iq,is,ix,iy+1,:)=arrh(iq,is,ix,iy+1,:)                     &
                                     +integrand(iq,iel,iv+1,:)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      ASSOCIATE (arrv=>cvec%arrv)
        id=par%get_stream(cvec%id,3,4)
        !$acc parallel present(arrv,cvec,integrand)                             &
        !$acc copyin(start_vert) wait(cvec%id) async(id) if(cvec%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cvec%nqty
          DO is=1,cvec%n_side
            iv=start_vert+2*(is-1)
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cvec%my
              DO ix=0,cvec%mx-1
                iel=1+ix+(iy-1)*cvec%mx
                arrv(iq,is,ix,iy,:)=arrv(iq,is,ix,iy,:)+integrand(iq,iel,iv,:)
              ENDDO
            ENDDO
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cvec%my
              DO ix=0,cvec%mx-1
                iel=1+ix+(iy-1)*cvec%mx
                arrv(iq,is,ix+1,iy,:)=arrv(iq,is,ix+1,iy,:)                     &
                                     +integrand(iq,iel,iv+1,:)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arri)) THEN
      ASSOCIATE (arri=>cvec%arri)
        id=par%get_stream(cvec%id,4,4)
        !$acc parallel present(arri,cvec,integrand)                             &
        !$acc copyin(start_int) wait(cvec%id) async(id) if(cvec%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cvec%nqty
          DO ii=1,cvec%n_int
            iv=start_int+ii-1
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cvec%my
              DO ix=1,cvec%mx
                iel=ix+(iy-1)*cvec%mx
                arri(iq,ii,ix,iy,:)=arri(iq,ii,ix,iy,:)+integrand(iq,iel,iv,:)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (cvec%on_gpu) CALL par%wait_streams(cvec%id,4)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_comp

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_comp(cvec,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    COMPLEX(r8) :: cproj
    REAL(r8), DIMENSION(cvec%nqty,cvec%nqty,edge%nvert) :: bcpmat
    REAL(r8), DIMENSION(cvec%nqty,cvec%nqty,cvec%n_side,edge%nvert) :: bcpmats
    INTEGER(i4), DIMENSION(cvec%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,imode,is,isymm,iq1,iq2
    LOGICAL :: seam_save_loc,do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,cvec%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(cvec%arrh).OR.ASSOCIATED(cvec%arrv)) do_segment=.TRUE.
    DO iv=1,edge%nvert
      IF (edge%expoint(iv)) THEN
        ix=edge%vertex(iv)%intxy(1)
        iy=edge%vertex(iv)%intxy(2)
        CALL bcdir_set(cvec%nqty,bciarr,edge%excorner(iv),isymm,                &
                       edge%vert_norm(:,iv),edge%vert_tang(:,iv),               &
                       bcpmat(:,:,iv))
      ENDIF
      IF (do_segment) THEN
        IF (edge%exsegment(iv)) THEN
          DO is=1,cvec%n_side
            CALL bcdir_set(cvec%nqty,bciarr,.false.,isymm,                      &
                           edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),       &
                           bcpmats(:,:,is,iv))
          ENDDO
        ENDIF
      ENDIF
    ENDDO
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               expoint=>edge%expoint,exsegment=>edge%exsegment,                 &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,cvec,edge,vertex,segment)            &
      !$acc present(expoint,exsegment,vert_csave,seg_csave)                     &
      !$acc copyin(bcpmat,bcpmats,seam_save_loc,do_segment)                     &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (expoint(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          !$acc loop vector collapse(2) private(cproj)
          DO imode=1,cvec%nmodes
            DO iq1=1,cvec%nqty
              cproj=0.0
              !$acc loop seq
              DO iq2=1,cvec%nqty
                cproj=cproj+bcpmat(iq1,iq2,iv)*arr(iq2,ix,iy,imode)
              ENDDO
              arr(iq1,ix,iy,imode)=arr(iq1,ix,iy,imode)-cproj
              IF (seam_save_loc)                                                &
                vert_csave(iq1,imode,iv)=vert_csave(iq1,imode,iv)+cproj
            ENDDO
          ENDDO
        ENDIF
        IF (do_segment) THEN
          IF (exsegment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              !$acc loop vector collapse(3) private(cproj)
              DO imode=1,cvec%nmodes
                DO is=1,cvec%n_side
                  DO iq1=1,cvec%nqty
                    cproj=0.0
                    !$acc loop seq
                    DO iq2=1,cvec%nqty
                      cproj=cproj                                               &
                            +bcpmats(iq1,iq2,is,iv)*arrh(iq2,is,ix,iy,imode)
                    ENDDO
                    arrh(iq1,is,ix,iy,imode)=arrh(iq1,is,ix,iy,imode)-cproj
                    IF (seam_save_loc)                                          &
                      seg_csave(iq1,is,imode,iv)=                               &
                        seg_csave(iq1,is,imode,iv)+cproj
                  ENDDO
                ENDDO
              ENDDO
            ELSE
              !$acc loop vector collapse(3) private(cproj)
              DO imode=1,cvec%nmodes
                DO is=1,cvec%n_side
                  DO iq1=1,cvec%nqty
                    cproj=0.0
                    !$acc loop seq
                    DO iq2=1,cvec%nqty
                      cproj=cproj                                               &
                            +bcpmats(iq1,iq2,is,iv)*arrv(iq2,is,ix,iy,imode)
                    ENDDO
                    arrv(iq1,is,ix,iy,imode)=arrv(iq1,is,ix,iy,imode)-cproj
                    IF (seam_save_loc)                                          &
                      seg_csave(iq1,is,imode,iv)=                               &
                        seg_csave(iq1,is,imode,iv)+cproj
                  ENDDO
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_comp

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_comp(cvec,edge,nindex,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> mode number array associated with vector
    INTEGER(i4), INTENT(IN) :: nindex(cvec%nmodes)
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ix,iy,imode,imn1,iside,ivec,nvec
    REAL(r8), DIMENSION(cvec%nqty,cvec%nmodes) :: mult
    LOGICAL :: do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!
!   For n=1 vectors the r and phi components are degenerate, transfer the
!   phi component coefficient to r and zero phi
!-------------------------------------------------------------------------------
    mult=0
    imn1=0
    nvec=0
    SELECT CASE(cvec%nqty)
    CASE(1,2,4,5)          !   scalars
      DO imode=1,cvec%nmodes
        IF (nindex(imode)==0) THEN
          mult(:,imode)=1
          EXIT
        ENDIF
      ENDDO
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=cvec%nqty/3
      DO ivec=0,nvec-1
        DO imode=1,cvec%nmodes
          IF (nindex(imode)==0) THEN
            mult(3*ivec+2,imode)=1
            CYCLE
          ENDIF
          IF (nindex(imode)==1) THEN
            mult(3*ivec+1,imode)=1
            IF (flag=='cyl_vec') imn1=imode
            IF (flag=='init_cyl_vec') mult(3*ivec+3,imode)=1
          ENDIF
        ENDDO
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_cvec"//                        &
                    " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(cvec%arrh).OR.ASSOCIATED(cvec%arrv)) do_segment=.TRUE.
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               r0point=>edge%r0point,r0segment=>edge%r0segment)
      !$acc parallel present(arr,arrh,arrv,cvec,edge,vertex,segment)            &
      !$acc present(r0point,r0segment) copyin(mult,imn1,nvec,do_segment)        &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (r0point(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          IF (imn1/=0) THEN
            DO ivec=0,nvec-1
              arr(3*ivec+1,ix,iy,imn1)=arr(3*ivec+1,ix,iy,imn1)                 &
                                       -(0,1)*arr(3*ivec+3,ix,iy,imn1)
            ENDDO
          ENDIF
          !$acc loop worker
          DO imode=1,cvec%nmodes
            arr(:,ix,iy,imode)=arr(:,ix,iy,imode)*mult(:,imode)
          ENDDO
        ENDIF
        IF (do_segment) THEN
          IF (r0segment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              IF (imn1/=0) THEN
                !$acc loop worker
                DO iside=1,cvec%n_side
                  DO ivec=0,nvec-1
                    arrh(3*ivec+1,iside,ix,iy,imn1)=                            &
                      arrh(3*ivec+1,iside,ix,iy,imn1)                           &
                      -(0,1)*arrh(3*ivec+3,iside,ix,iy,imn1)
                  ENDDO
                ENDDO
              ENDIF
              !$acc loop worker collapse(2)
              DO imode=1,cvec%nmodes
                DO iside=1,cvec%n_side
                  arrh(:,iside,ix,iy,imode)=                                    &
                    arrh(:,iside,ix,iy,imode)*mult(:,imode)
                ENDDO
              ENDDO
            ELSE
              IF (imn1/=0) THEN
                !$acc loop worker
                DO iside=1,cvec%n_side
                  DO ivec=0,nvec-1
                    arrv(3*ivec+1,iside,ix,iy,imn1)=                            &
                      arrv(3*ivec+1,iside,ix,iy,imn1)                           &
                      -(0,1)*arrv(3*ivec+3,iside,ix,iy,imn1)
                  ENDDO
                ENDDO
              ENDIF
              !$acc loop worker collapse(2)
              DO imode=1,cvec%nmodes
                DO iside=1,cvec%n_side
                  arrv(:,iside,ix,iy,imode)=                                    &
                    arrv(:,iside,ix,iy,imode)*mult(:,imode)
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_comp

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_comp(cvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_comp',iftn,idepth)
    DO iv=1,cvec%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cvec%mx+1,cvec%mx+cvec%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cvec%mx+cvec%my+1,2*cvec%mx+cvec%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*cvec%mx+cvec%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    !$acc update device(edge%segment) async(edge%id) if(cvec%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_comp

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_comp(cvec,edge,nqty,n_side,ifs,ife,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,nm,im,ims,ime,ivs,ld
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
    nm=ime-ims+1
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%imode_start=ims
    edge%nmodes_loaded=nm
    edge%on_gpu=.TRUE.
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_cin=>edge%vert_cin,seg_cin=>edge%seg_cin)
      !$acc parallel present(arr,arrh,arrv,cvec,edge,vertex,segment)            &
      !$acc present(vert_cin,seg_cin) copyin(nq,ns,nm,ims,ime,do_avg_loc)       &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        !$acc loop worker private(ivs)
        DO im=ims,ime
          ivs=(im-ims)*nq+1
          vert_cin(ivs:ivs+nq-1,iv)=arr(1:nq,ix,iy,im)
        ENDDO
!-------------------------------------------------------------------------------
!       element side-centered data.  load side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker collapse(2) private(ivs)
            DO im=ims,ime
              DO is=1,ns
                ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq*nm+(im-ims)*nq+1
                seg_cin(ivs:ivs+nq-1,iv)=arrh(1:nq,is,ix,iy,im)
              ENDDO
            ENDDO
          ELSE
            !$acc loop worker collapse(2) private(ivs)
            DO im=ims,ime
              DO is=1,ns
                ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq*nm+(im-ims)*nq+1
                seg_cin(ivs:ivs+nq-1,iv)=arrv(1:nq,is,ix,iy,im)
              ENDDO
            ENDDO
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       apply average factor
!-------------------------------------------------------------------------------
        IF (do_avg_loc) THEN
          vert_cin(1:nq*nm,iv)=vert_cin(1:nq*nm,iv)*vertex(iv)%ave_factor
          IF (ns/=0) THEN
            seg_cin(1:nq*nm*ns,iv)=seg_cin(1:nq*nm*ns,iv)*segment(iv)%ave_factor
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
!-------------------------------------------------------------------------------
!   copy data to CPU, wait/transfer in seam%edge_network
!   TODO: use GPU RDMA in seam%edge_network
!-------------------------------------------------------------------------------
    ASSOCIATE (vert_cin=>edge%vert_cin,seg_cin=>edge%seg_cin)
      !$acc update self(vert_cin) async(cvec%id) if(cvec%on_gpu)
      IF (ns/=0) THEN
        !$acc update self(seg_cin) async(cvec%id) if(cvec%on_gpu)
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_comp

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_comp(cvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,nm,im,ims,ime,ivs,ld
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
    ims=edge%imode_start
    nm=edge%nmodes_loaded
    ime=nm+ims-1_i4
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_cout=>edge%vert_cout,seg_cout=>edge%seg_cout)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_cout,seg_cout) copyin(nq,nm,ns,ims,ime)                &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        !$acc loop worker private(ivs)
        DO im=ims,ime
          ivs=(im-ims)*nq+1
          arr(1:nq,ix,iy,im)=vert_cout(ivs:ivs+nq-1,iv)
        ENDDO
!-------------------------------------------------------------------------------
!       element side-centered data.  unload side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker collapse(2) private(ivs)
            DO im=ims,ime
              DO is=1,ns
                ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq*nm+(im-ims)*nq+1
                arrh(1:nq,is,ix,iy,im)=seg_cout(ivs:ivs+nq-1,iv)
              ENDDO
            ENDDO
          ELSE
            !$acc loop worker collapse(2) private(ivs)
            DO im=ims,ime
              DO is=1,ns
                ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq*nm+(im-ims)*nq+1
                arrv(1:nq,is,ix,iy,im)=seg_cout(ivs:ivs+nq-1,iv)
              ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_comp

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_comp(cvec,edge,nqty,n_side,ifs,ife)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,im,ims,ime
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_csave,seg_csave) copyin(nq,ns,ims,ime)                 &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        !$acc loop worker
        DO im=ims,ime
          vert_csave(1:nq,im,iv)=arr(1:nq,ix,iy,im)
        ENDDO
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker collapse(2)
            DO im=ims,ime
              DO is=1,ns
                seg_csave(1:nq,is,im,iv)=arrh(1:nq,is,ix,iy,im)
              ENDDO
            ENDDO
          ELSE
            !$acc loop worker collapse(2)
            DO im=ims,ime
              DO is=1,ns
                seg_csave(1:nq,is,im,iv)=arrv(1:nq,is,ix,iy,im)
              ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_comp

!-------------------------------------------------------------------------------
!* unload a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_comp(cvec,edge,nqty,n_side,ifs,ife)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,im,ims,ime
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
    ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_csave,seg_csave) copyin(nq,ns,ims,ime)                 &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        !$acc loop worker
        DO im=ims,ime
          arr(1:nq,ix,iy,im)=arr(1:nq,ix,iy,im)+vert_csave(1:nq,im,iv)
        ENDDO
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker collapse(2)
            DO im=ims,ime
              DO is=1,ns
                arrh(1:nq,is,ix,iy,im)=arrh(1:nq,is,ix,iy,im)                   &
                                       +seg_csave(1:nq,is,im,iv)
              ENDDO
            ENDDO
          ELSE
            !$acc loop worker collapse(2)
            DO im=ims,ime
              DO is=1,ns
                arrv(1:nq,is,ix,iy,im)=arrv(1:nq,is,ix,iy,im)                   &
                                       +seg_csave(1:nq,is,im,iv)
              ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_comp

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_comp(cvec,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> vector to multiply
    CLASS(cvector), INTENT(IN) :: input_vec
    !> output vector = cvec (matrix diagonal) dot input_vec
    CLASS(cvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_comp',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_comp_acc)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_comp_acc)
        ASSOCIATE (arr_mat=>cvec%arr,arrh_mat=>cvec%arrh,                       &
                   arrv_mat=>cvec%arrv,arri_mat=>cvec%arri,                     &
                   arr_in=>input_vec%arr,arrh_in=>input_vec%arrh,               &
                   arrv_in=>input_vec%arrv,arri_in=>input_vec%arri,             &
                   arr_out=>output_vec%arr,arrh_out=>output_vec%arrh,           &
                   arrv_out=>output_vec%arrv,arri_out=>output_vec%arri)
          IF (cvec%pd>1) THEN
            IF (cvec%skip_elim_interior) THEN
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat)                  &
              !$acc present(arr_in,arrh_in,arrv_in)                             &
              !$acc present(arr_out,arrh_out,arrv_out)                          &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              !$acc end kernels
            ELSE
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat,arri_mat)         &
              !$acc present(arr_in,arrh_in,arrv_in,arri_in)                     &
              !$acc present(arr_out,arrh_out,arrv_out,arri_out)                 &
              !$acc async(cvec%id) if(cvec%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              arri_out=arri_mat*arri_in
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr_mat,arr_in,arr_out)                       &
            !$acc async(cvec%id) if(cvec%on_gpu)
            arr_out=arr_mat*arr_in
            !$acc end kernels
          ENDIF
        END ASSOCIATE
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_comp_acc for output_vec'        &
                          //' in apply_diag_matvec_comp')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp_acc for input_vec'           &
                        //' in apply_diag_matvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_comp

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_comp(cvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_comp',iftn,idepth)
    IF (cvec%pd>1) THEN
      ASSOCIATE (arr=>cvec%arr,arrh=>cvec%arrh,arrv=>cvec%arrv,arri=>cvec%arri)
        IF (cvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv,cvec) async(cvec%id) if(cvec%on_gpu)
          arr(:,0:cvec%mx,0:cvec%my,:)=1._r8/arr(:,0:cvec%mx,0:cvec%my,:)
          arrh(:,:,1:cvec%mx,0:cvec%my,:)=1._r8/arrh(:,:,1:cvec%mx,0:cvec%my,:)
          arrv(:,:,0:cvec%mx,1:cvec%my,:)=1._r8/arrv(:,:,0:cvec%mx,1:cvec%my,:)
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri,cvec)                        &
          !$acc async(cvec%id) if(cvec%on_gpu)
          arr(:,0:cvec%mx,0:cvec%my,:)=1._r8/arr(:,0:cvec%mx,0:cvec%my,:)
          arrh(:,:,1:cvec%mx,0:cvec%my,:)=1._r8/arrh(:,:,1:cvec%mx,0:cvec%my,:)
          arrv(:,:,0:cvec%mx,1:cvec%my,:)=1._r8/arrv(:,:,0:cvec%mx,1:cvec%my,:)
          arri(:,:,1:cvec%mx,1:cvec%my,:)=1._r8/arri(:,:,1:cvec%mx,1:cvec%my,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>cvec%arr)
        !$acc kernels present(arr,cvec) async(cvec%id) if(cvec%on_gpu)
        arr(:,0:cvec%mx,0:cvec%my,:)=1._r8/arr(:,0:cvec%mx,0:cvec%my,:)
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_comp

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_comp(cvec,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp_acc), INTENT(INOUT) :: cvec
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    COMPLEX(r8), INTENT(IN), OPTIONAL :: const

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    COMPLEX(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ii5,ind

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_comp',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    CALL cvec%zero
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(cvec%arr)) THEN
        ASSOCIATE (arr=>cvec%arr)
          !$acc kernels copyin(fac) present(arr,cvec) async(cvec%id) if(cvec%on_gpu)
          arr(:,0:cvec%mx,0:cvec%my,:)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        ASSOCIATE (arrh=>cvec%arrh)
          !$acc kernels copyin(fac) present(arrh,cvec) async(cvec%id) if(cvec%on_gpu)
          arrh(:,:,1:cvec%mx,0:cvec%my,:)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        ASSOCIATE (arrv=>cvec%arrv)
          !$acc kernels copyin(fac) present(arrv,cvec) async(cvec%id) if(cvec%on_gpu)
          arrv(:,:,0:cvec%mx,1:cvec%my,:)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>cvec%arri)
          !$acc kernels copyin(fac) present(arri,cvec) async(cvec%id) if(cvec%on_gpu)
          arri(:,:,1:cvec%mx,1:cvec%my,:)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(cvec%arr)) THEN
        ASSOCIATE (arr=>cvec%arr)
          !$acc parallel present(cvec,arr) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,cvec%nqty
            DO ii2=0,cvec%mx
              DO ii3=0,cvec%my
                DO ii4=1,cvec%nmodes
                  ind=ii1*SIZE(arr,2)*SIZE(arr,3)*SIZE(arr,4)+                  &
                      ii2*SIZE(arr,3)*SIZE(arr,4)+ii3*SIZE(arr,4)+ii4
                  arr(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        ASSOCIATE (arrh=>cvec%arrh)
          !$acc parallel present(cvec,arrh) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector collapse(5) private(ind)
          DO ii1=1,cvec%nqty
            DO ii2=1,cvec%n_side
              DO ii3=1,cvec%mx
                DO ii4=0,cvec%my
                  DO ii5=1,cvec%nmodes
                    ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)*SIZE(arrh,5)+&
                        ii2*SIZE(arrh,3)*SIZE(arrh,4)*SIZE(arrh,5)+             &
                        ii3*SIZE(arrh,4)*SIZE(arrh,5)+ii4*SIZE(arrh,5)+ii5
                    arrh(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        ASSOCIATE (arrv=>cvec%arrv)
          !$acc parallel present(cvec,arrv) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector collapse(5) private(ind)
          DO ii1=1,cvec%nqty
            DO ii2=1,cvec%n_side
              DO ii3=0,cvec%mx
                DO ii4=1,cvec%my
                  DO ii5=1,cvec%nmodes
                    ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)*SIZE(arrv,5)+&
                        ii2*SIZE(arrv,3)*SIZE(arrv,4)*SIZE(arrv,5)+             &
                        ii3*SIZE(arrv,4)*SIZE(arrv,5)+ii4*SIZE(arrv,5)+ii5
                    arrv(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>cvec%arri)
          !$acc parallel present(cvec,arri) async(cvec%id) if(cvec%on_gpu)
          !$acc loop gang vector collapse(5) private(ind)
          DO ii1=1,cvec%nqty
            DO ii2=1,cvec%n_int
              DO ii3=1,cvec%mx
                DO ii4=1,cvec%my
                  DO ii5=1,cvec%nmodes
                    ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)*SIZE(arri,5)+&
                        ii2*SIZE(arri,3)*SIZE(arri,4)*SIZE(arri,5)+             &
                        ii3*SIZE(arri,4)*SIZE(arri,5)+ii4*SIZE(arri,5)+ii5
                    arri(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                        //' in assign_for_testing_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_comp

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_comp(cvec,cvec2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp_acc), INTENT(IN) :: cvec
    !> vector to test against
    CLASS(cvector), INTENT(IN) :: cvec2

    INTEGER(i4) :: shape4a(4),shape4b(4),shape5a(5),shape5b(5)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_comp',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp_acc)
      !$acc update self(cvec%arr,cvec%arrh,cvec%arrv,cvec%arri)                 &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc update self(cvec2%arr,cvec2%arrh,cvec2%arrv,cvec2%arri)             &
      !$acc async(cvec%id) if(cvec%on_gpu)
      !$acc wait(cvec%id) if(cvec%on_gpu)
      IF (ASSOCIATED(cvec%arr)) THEN
        shape4a=SHAPE(cvec%arr)
        shape4b=SHAPE(cvec2%arr)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arr == cvec2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        shape5a=SHAPE(cvec%arrh)
        shape5b=SHAPE(cvec2%arrh)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arrh == cvec2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        shape5a=SHAPE(cvec%arrv)
        shape5b=SHAPE(cvec2%arrv)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arrv == cvec2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        shape5a=SHAPE(cvec%arri)
        shape5b=SHAPE(cvec2%arri)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arri == cvec2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec2 in'//              &
                        ' test_if_equal_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_comp
