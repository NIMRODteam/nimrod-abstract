!-------------------------------------------------------------------------------
!! Defines the abstract interface for matrix data storage conversion
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the abstract interface for tools used with matrix data conversion
!  from the internal NIMROD data format to a compressed sparse storage data
!  format using either compressed sparse row (CSR) or compressed sparse column
!  (CSC) as appropriate for export to an external preconditioner package.
!  This operation coalesces data from all local blocks, but the maps to do this
!  are defined on a block-wise basis.
!
!  Note that offsets related to nqty are computed on the fly, so that only
!  a single copy of this type is required for a given problem geometry.
!-------------------------------------------------------------------------------
MODULE compressed_conversion_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: compressed_conversion_type, comm_vals_real, comm_vals_comp

  CHARACTER(*), PARAMETER, PRIVATE :: mod_name='compressed_conversion_mod'
!-------------------------------------------------------------------------------
!> stores indirect-addressing arrays for gather operations associated with
!  conversion from NIMROD's linear algebra formats to a compressed sparse
!  storage formats for matrices and a dense format for vectors.
!
!  Some arrays may be unused depending on the problem geometry and
!  implementation.
!-------------------------------------------------------------------------------
  TYPE :: indirect_address_type
    !> map from ival to global index in value/ind array
    INTEGER(i4), ALLOCATABLE :: map_ival(:)
    !> map from ival to row in global dof indexing
    INTEGER(i4), ALLOCATABLE :: map_irow(:)
    !> map from ival to column in global dof indexing
    INTEGER(i4), ALLOCATABLE :: map_jcol(:)
    !> map from ival matrix element iqv (combined nqty and side/int)
    INTEGER(i4), ALLOCATABLE :: map_iqv(:)
    !> map from ival matrix element lx index (relative offset)
    INTEGER(i4), ALLOCATABLE :: map_lx(:)
    !> map from ival matrix element ly index (relative offset)
    INTEGER(i4), ALLOCATABLE :: map_ly(:)
    !> map from ival matrix element lz index (relative offset)
    INTEGER(i4), ALLOCATABLE :: map_lz(:)
    !> map from ival matrix element ibasis
    INTEGER(i4), ALLOCATABLE :: map_ibasis(:)
    !> map from ival matrix element jqv (combined nqty and side/int)
    INTEGER(i4), ALLOCATABLE :: map_jqv(:)
    !> map from ival matrix element jx index
    INTEGER(i4), ALLOCATABLE :: map_jx(:)
    !> map from ival  matrix element jy index
    INTEGER(i4), ALLOCATABLE :: map_jy(:)
    !> map from ival matrix element jz index
    INTEGER(i4), ALLOCATABLE :: map_jz(:)
    !> map from ival matrix element jbasis
    INTEGER(i4), ALLOCATABLE :: map_jbasis(:)
  END TYPE indirect_address_type

!-------------------------------------------------------------------------------
!> stores indirect-addressing information for gather operations associated with
!  conversion from NIMROD's linear algebra formats to a compressed sparse
!  storage formats for matrices and a dense format for vectors. The operation
!  is split into a gather of block-local matrix element data, and a seaming-type
!  operation where cross-block information is communicated. There is no
!  optimization for multiple blocks local to a process.
!
!  Some arrays may be unused depending on the problem geometry and
!  implementation.
!
!  This type is initialized using a call to matrix%init_compressed_conversion.
!-------------------------------------------------------------------------------
  TYPE :: compressed_conversion_type
    !> number of local matrix non-zeros associated with this block
    INTEGER(i4) :: nnz_block
    !> first non-zero in global nnz indexing
    INTEGER(i4) :: firstnz
    !> last non-zero in global nnz indexing
    INTEGER(i4) :: lastnz
    !> first row in global DOF indexing
    INTEGER(i4) :: firstrow
    !> last row in global DOF indexing
    INTEGER(i4) :: lastrow
    !> indirect addressing for local matrix non-zero values, may be unused
    TYPE(indirect_address_type) :: map_bl_local
    !> number of sends associated with this block
    INTEGER(i4) :: nsend
    !> blocks to send to, from this block, size nsend
    INTEGER(i4), ALLOCATABLE :: blk_send(:)
    !> processors to send to, from this block, size nsend
    INTEGER(i4), ALLOCATABLE :: proc_send(:)
    !> starting row for block to send to, from this block, size nsend
    INTEGER(i4), ALLOCATABLE :: rowstart_send(:)
    !> number of sent matrix non-zeros associated with this block, size nsend
    INTEGER(i4), ALLOCATABLE :: nnz_send(:)
    !> mpi requests for asynchronous recv operations
    TYPE(mpi_request), ALLOCATABLE :: send_req(:)
    !> indirect addressing information for sent matrix values, size nsend
    !  the contained arrays are of size nnz_send
    TYPE(indirect_address_type), ALLOCATABLE :: send_addr(:)
    !> number of receives associated with this block
    INTEGER(i4) :: nrecv
    !> blocks to recv from, to this block, size nrecv
    INTEGER(i4), ALLOCATABLE :: blk_recv(:)
    !> processors to recv from, to this block, size nrecv
    INTEGER(i4), ALLOCATABLE :: proc_recv(:)
    !> number of received matrix non-zeros associated with this block, size nrecv
    INTEGER(i4), ALLOCATABLE :: nnz_recv(:)
    !> mpi requests for asynchronous recv operations
    TYPE(mpi_request), ALLOCATABLE :: recv_req(:)
    !> indirect address of received values for a scatter (+=) operation
    INTEGER(i4), ALLOCATABLE :: ival_recv(:)
    !> indirect addressing information for sent matrix values, size nsend
    !  the contained arrays are of size nnz_recv
    TYPE(indirect_address_type), ALLOCATABLE :: recv_addr(:)
    !> True if self-periodic in x
    LOGICAL :: self_per_x=.FALSE.
    !> True if self-periodic in y
    LOGICAL :: self_per_y=.FALSE.
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE, PASS(cct) :: dealloc => dealloc_compressed_conversion
  END TYPE compressed_conversion_type

!-------------------------------------------------------------------------------
! used to store values communicated decomposed into processor groupings
!-------------------------------------------------------------------------------
  TYPE :: comm_vals_real
    !> values to be communicated to a particular processor
    REAL(r8), ALLOCATABLE :: vals(:)
  END TYPE comm_vals_real

!-------------------------------------------------------------------------------
! used to store values communicated decomposed into processor groupings
!-------------------------------------------------------------------------------
  TYPE :: comm_vals_comp
    !> values to be communicated to a particular processor
    COMPLEX(r8), ALLOCATABLE :: vals(:)
  END TYPE comm_vals_comp

CONTAINS

!-------------------------------------------------------------------------------
!* Deallocate the compressed conversion type
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_compressed_conversion(cct)
    USE timer_mod
    IMPLICIT NONE

    !> compressed conversion type to deallocate
    CLASS(compressed_conversion_type), INTENT(INOUT) :: cct

    INTEGER(i4) :: isend,irecv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_compressed_conversion',         &
                              iftn,idepth)
    IF (ALLOCATED(cct%blk_send)) DEALLOCATE(cct%blk_send)
    IF (ALLOCATED(cct%proc_send)) DEALLOCATE(cct%proc_send)
    IF (ALLOCATED(cct%rowstart_send)) DEALLOCATE(cct%rowstart_send)
    IF (ALLOCATED(cct%nnz_send)) DEALLOCATE(cct%nnz_send)
    IF (ALLOCATED(cct%send_req)) DEALLOCATE(cct%send_req)
    IF (ALLOCATED(cct%send_addr)) THEN
      DO isend=1,cct%nsend
        ASSOCIATE(send_addr=>cct%send_addr(isend))
          IF (ALLOCATED(send_addr%map_ival)) DEALLOCATE(send_addr%map_ival)
          IF (ALLOCATED(send_addr%map_irow)) DEALLOCATE(send_addr%map_irow)
          IF (ALLOCATED(send_addr%map_jcol)) DEALLOCATE(send_addr%map_jcol)
          IF (ALLOCATED(send_addr%map_iqv)) DEALLOCATE(send_addr%map_iqv)
          IF (ALLOCATED(send_addr%map_lx)) DEALLOCATE(send_addr%map_lx)
          IF (ALLOCATED(send_addr%map_ly)) DEALLOCATE(send_addr%map_ly)
          IF (ALLOCATED(send_addr%map_lz)) DEALLOCATE(send_addr%map_lz)
          IF (ALLOCATED(send_addr%map_ibasis)) DEALLOCATE(send_addr%map_ibasis)
          IF (ALLOCATED(send_addr%map_jqv)) DEALLOCATE(send_addr%map_jqv)
          IF (ALLOCATED(send_addr%map_jx)) DEALLOCATE(send_addr%map_jx)
          IF (ALLOCATED(send_addr%map_jy)) DEALLOCATE(send_addr%map_jy)
          IF (ALLOCATED(send_addr%map_jz)) DEALLOCATE(send_addr%map_jz)
          IF (ALLOCATED(send_addr%map_jbasis)) DEALLOCATE(send_addr%map_jbasis)
        END ASSOCIATE
      ENDDO
      DEALLOCATE(cct%send_addr)
    ENDIF
    IF (ALLOCATED(cct%blk_recv)) DEALLOCATE(cct%blk_recv)
    IF (ALLOCATED(cct%proc_recv)) DEALLOCATE(cct%proc_recv)
    IF (ALLOCATED(cct%nnz_recv)) DEALLOCATE(cct%nnz_recv)
    IF (ALLOCATED(cct%recv_req)) DEALLOCATE(cct%recv_req)
    IF (ALLOCATED(cct%ival_recv)) DEALLOCATE(cct%ival_recv)
    IF (ALLOCATED(cct%recv_addr)) THEN
      DO irecv=1,cct%nrecv
        ASSOCIATE(recv_addr=>cct%recv_addr(irecv))
          IF (ALLOCATED(recv_addr%map_ival)) DEALLOCATE(recv_addr%map_ival)
          IF (ALLOCATED(recv_addr%map_irow)) DEALLOCATE(recv_addr%map_irow)
          IF (ALLOCATED(recv_addr%map_jcol)) DEALLOCATE(recv_addr%map_jcol)
          IF (ALLOCATED(recv_addr%map_iqv)) DEALLOCATE(recv_addr%map_iqv)
          IF (ALLOCATED(recv_addr%map_lx)) DEALLOCATE(recv_addr%map_lx)
          IF (ALLOCATED(recv_addr%map_ly)) DEALLOCATE(recv_addr%map_ly)
          IF (ALLOCATED(recv_addr%map_lz)) DEALLOCATE(recv_addr%map_lz)
          IF (ALLOCATED(recv_addr%map_ibasis)) DEALLOCATE(recv_addr%map_ibasis)
          IF (ALLOCATED(recv_addr%map_jqv)) DEALLOCATE(recv_addr%map_jqv)
          IF (ALLOCATED(recv_addr%map_jx)) DEALLOCATE(recv_addr%map_jx)
          IF (ALLOCATED(recv_addr%map_jy)) DEALLOCATE(recv_addr%map_jy)
          IF (ALLOCATED(recv_addr%map_jz)) DEALLOCATE(recv_addr%map_jz)
          IF (ALLOCATED(recv_addr%map_jbasis)) DEALLOCATE(recv_addr%map_jbasis)
        END ASSOCIATE
      ENDDO
      DEALLOCATE(cct%recv_addr)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_compressed_conversion

END MODULE compressed_conversion_mod
