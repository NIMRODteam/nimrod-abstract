!-------------------------------------------------------------------------------
!< Routines for evaluating continuous Lagrange finite elements on blocks
!  of structured quadrilaterals.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the types for continuous Lagrange finite elements on blocks
!  of structured quadrilaterals and their associated procedures.
!-------------------------------------------------------------------------------
MODULE h1_rect_2D_mod
  USE h1_rect_2D_real_mod
  USE h1_rect_2D_real_acc_mod
  USE h1_rect_2D_comp_mod
  USE h1_rect_2D_comp_acc_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: h1_rect_2D_real, h1_rect_2D_real_acc

  PUBLIC :: h1_rect_2D_comp, h1_rect_2D_comp_acc

END MODULE h1_rect_2D_mod
