!-------------------------------------------------------------------------------
!< Routines for evaluating continuous H(curl)-conforming finite elements on
!  blocks of structured quadrilaterals.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the types for continuous H(curl)-conforming finite elements on blocks
!  of structured quadrilaterals and their associated procedures.
!-------------------------------------------------------------------------------
MODULE hcurl_rect_2D_real_mod
  USE function_mod, ONLY : abstract_function_real
  USE local
  USE nodal_mod
  USE pardata_mod
  USE poly_mod, ONLY: gauleg, lobleg
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: hcurl_rect_2D_real

  CHARACTER(*), PARAMETER :: mod_name='hcurl_rect_2D_real'
!-------------------------------------------------------------------------------
!> Type for the structured blocks of H(curl) conforming quadrilateral real
!  finite elements. This represents the in-plane directional field only.
!
!  @note
!  See the abstract base type [[nodal_field_real(type)]] for
!  type-bound procedure interface details.
!  @endnote
!
!  Basis starting indices and centering in logical space are contained
!  in ix0 and iy0. Note that the centering are for H(curl) lagrange
!  polynomial bases.
!
!  There is no Grid-vertex data so fs is not an array.
!
!  Horizontal side data has grid indices running
!  fsh(1:nqty,1:mx,0:my,1:n_side), so ix0=1 and iy0=0.
!
!  Vertical side data has grid indices running
!  fsv(1:nqty,0:mx,1:my,1:n_side), so ix0=0 and iy0=1.
!
!  Interior side data has grid indices running
!  fsi(1:nqty,1:mx,1:my,1:n_int), so ix0=1 and iy0=1.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(nodal_field_real) :: hcurl_rect_2D_real
    !* number of elements within this block in the logical x direction
    INTEGER(i4) :: mx=0
    !* number of elements within this block in the logical y direction
    INTEGER(i4) :: my=0
    !> number of degrees of freedom per element in each side basis
    !  function nodal array
    INTEGER(i4) :: n_side=0
    !> number of degrees of freedom per element in each interior basis
    !  function nodal array
    INTEGER(i4) :: n_int=0
    !* basis starting indices and centering in logical space.
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ix0,iy0
    !* the relative positions within a cell for the different bases.
    REAL(r8), DIMENSION(:), ALLOCATABLE :: dx,dy
    !* Grid horizontal (x-dir) side basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsh
    !* Grid vertical (y-dir) side basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsv
    !* Grid interior basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsi
    !* Class for evaluating 1D Gauss-Legendre cardinal functions
    TYPE(gauleg) :: gl
    !* Class for evaluating 1D Lobatto-Legendre cardinal functions
    TYPE(lobleg) :: ll
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(field) :: alloc => alloc_real_2D
    ! Abstract class deferred functions
    PROCEDURE, PASS(field) :: dealloc => dealloc_real_2D
    PROCEDURE, PASS(field) :: alloc_with_mold => alloc_with_mold_real_2D
    PROCEDURE, PASS(field) :: assign_field => assign_field_real_2D
    PROCEDURE, PASS(field) :: init_basis_ftn => init_basis_ftn_real_2D
    PROCEDURE, PASS(field) :: init_block_basis_ftn =>                           &
                                init_block_basis_ftn_real_2D
    PROCEDURE, PASS(field) :: eval_val => eval_val_real_2D
    PROCEDURE, PASS(field) :: eval_many => eval_many_real_2D
    PROCEDURE, PASS(field) :: collocation => collocation_real_2D
    PROCEDURE, PASS(field) :: qp_update => qp_update_real_2D
    PROCEDURE, PASS(field) :: get_logical => get_logical_real_2D
    PROCEDURE, PASS(field) :: set_field => set_field_real_2D
    PROCEDURE, PASS(field) :: get_field => get_field_real_2D
    PROCEDURE, PASS(field) :: h5_read => h5_read_real_2D
    PROCEDURE, PASS(field) :: h5_dump => h5_dump_real_2D
    PROCEDURE, PASS(field) :: bcast => bcast_real_2D
    ! Private functions
    PROCEDURE, PASS(field), PRIVATE :: get_element_dofs
    PROCEDURE, PASS(field), PRIVATE :: hcurl_rect_2D_bases_val
    PROCEDURE, PASS(field), PRIVATE :: hcurl_rect_2D_bases_many
    GENERIC, PRIVATE :: hcurl_rect_2D_bases => hcurl_rect_2D_bases_val,         &
                                               hcurl_rect_2D_bases_many
  END TYPE hcurl_rect_2D_real

CONTAINS

!-------------------------------------------------------------------------------
!> allocates space for hcurl_rect_2D_real.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real_2D(field,mx,my,nqty,pd,id,name,title)
    IMPLICIT NONE

    !> field to allocate
    CLASS(hcurl_rect_2D_real), INTENT(OUT) :: field
    !> number of elements within this block in the logical x direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements within this block in the logical y direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities stored at DoF
    INTEGER(i4), INTENT(IN) :: nqty
    !> degree of Lobatto-Legendre polynomials used
    INTEGER(i4), INTENT(IN) :: pd
    !> ID for parallel streaming
    INTEGER(i4), INTENT(IN) :: id
    !> field name
    CHARACTER(*), INTENT(IN), OPTIONAL :: name
    !> component names
    CHARACTER(*), DIMENSION(:), INTENT(IN), OPTIONAL :: title

    INTEGER(i4) :: ib, ip, ix, iy
    REAL(r8), DIMENSION(0:pd) :: x_ll
    REAL(r8), DIMENSION(1:pd) :: x_gl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   throw an error if allocated
!-------------------------------------------------------------------------------
    IF (ALLOCATED(field%fsh)) THEN
      CALL par%nim_stop("alloc_real_2D tried to reallocate"//TRIM(title(1)))
    ENDIF
!-------------------------------------------------------------------------------
!   store grid and vector dimensions, and set the
!   number of side and interior basis functions.
!-------------------------------------------------------------------------------
    field%nqty = nqty
    field%fqty = 2
    field%dfqty = 1
    field%mx = mx
    field%my = my
    field%n_side = pd
    field%n_int = 2*pd*(pd - 1)
    field%nbasis = 2*pd*(pd + 1)
    field%nel = mx*my
    field%ndof = field%n_side*mx*(my+1) + field%n_side*(mx+1)*my                &
                + field%n_int*mx*my
    field%ndim = 2
    field%pd = pd
    field%id=id
    field%deriv_type = "rot2"
    field%field_type="hcurl_rect_2D_real"
!-------------------------------------------------------------------------------
!   initialize the 1D basis functions to be used
!-------------------------------------------------------------------------------
    CALL field%ll%init(pd)
    CALL field%gl%init(pd-1)
!-------------------------------------------------------------------------------
!   allocate space.
!-------------------------------------------------------------------------------
    ALLOCATE(field%fsh(field%n_side,nqty,1:mx,0:my))
    ALLOCATE(field%fsv(field%n_side,nqty,0:mx,1:my))
    IF (pd > 1) THEN
      ALLOCATE(field%fsi(field%n_int,nqty,1:mx,1:my))
    ENDIF
    ALLOCATE(field%title(nqty))
!-------------------------------------------------------------------------------
!   character descriptors, if present in input.
!-------------------------------------------------------------------------------
    IF (PRESENT(name)) field%name=name
    IF (PRESENT(title)) THEN
      IF (SIZE(title)==nqty) THEN
        field%title=title
      ELSE
        field%title=title(1)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   compute basis starting indices and centering in logical space.
!   note that the centering are for standard lagrange polynomial
!   bases.
!-------------------------------------------------------------------------------
    ALLOCATE(field%ix0(2*field%n_side + field%n_int))
    ALLOCATE(field%iy0(2*field%n_side + field%n_int))
    ALLOCATE(field%dx(2*field%n_side + field%n_int))
    ALLOCATE(field%dy(2*field%n_side + field%n_int))
    CALL field%gl%getnodes(x_gl)
    CALL field%ll%getnodes(x_ll)
    DO ib = 1, 2*field%n_side + field%n_int
      IF (ib <= pd) THEN
        field%ix0(ib) = 1
        field%iy0(ib) = 0
        field%dx(ib) = x_gl(ib)
        field%dy(ib) = 0._r8
      ELSEIF (ib <= 2*pd) THEN
        field%ix0(ib) = 0
        field%iy0(ib) = 1
        field%dx(ib) = 0._r8
        iy = ib - pd
        field%dy(ib) = x_gl(iy)
      ELSEIF (ib <= pd*(pd + 1)) THEN
        field%ix0(ib) = 1
        field%iy0(ib) = 1
        ip = ib - 2*pd - 1
        ix = MODULO(ip, pd) + 1
        iy = ip/pd + 1
        field%dx(ib) = x_gl(ix)
        field%dy(ib) = x_ll(iy)
      ELSE
        field%ix0(ib) = 1
        field%iy0(ib) = 1
        ip = ib - pd*(pd + 1) - 1
        ix = MODULO(ip, pd - 1) + 1
        iy = ip/(pd - 1) + 1
        field%dx(ib) = x_ll(ix)
        field%dy(ib) = x_gl(iy)
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(field%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      CHARACTER(16) :: obj_name
      sz= INT(SIZEOF(field%fsh)+SIZEOF(field%fsv)                               &
             +SIZEOF(field%fsi)+SIZEOF(field%ix0)+SIZEOF(field%iy0)             &
             +SIZEOF(field%dx)+SIZEOF(field%dy)                                 &
             +SIZEOF(field)+SIZEOF(field%title),i4)
      obj_name='unknown'
      IF (PRESENT(name)) obj_name=name
      CALL memlogger%update(field%mem_id,mod_name,obj_name,sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real_2D

!-------------------------------------------------------------------------------
!* deallocates space for hcurl_rect_2D_real.
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real_2D(field)
    IMPLICIT NONE

    !> field to dealloc
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   deallocate space.
!-------------------------------------------------------------------------------
    IF (ALLOCATED(field%fsh)) DEALLOCATE(field%fsh)
    IF (ALLOCATED(field%fsv)) DEALLOCATE(field%fsv)
    IF (ALLOCATED(field%fsi)) DEALLOCATE(field%fsi)
    IF (ALLOCATED(field%ix0)) DEALLOCATE(field%ix0)
    IF (ALLOCATED(field%iy0)) DEALLOCATE(field%iy0)
    IF (ALLOCATED(field%dx)) DEALLOCATE(field%dx)
    IF (ALLOCATED(field%dy)) DEALLOCATE(field%dy)
    IF (ALLOCATED(field%title)) DEALLOCATE(field%title)
    IF (ALLOCATED(field%qa%alf)) DEALLOCATE(field%qa%alf)
    IF (ALLOCATED(field%qa%aldf)) DEALLOCATE(field%qa%aldf)
    IF (ALLOCATED(field%qab%alf)) DEALLOCATE(field%qab%alf)
    IF (ALLOCATED(field%qab%aldf)) DEALLOCATE(field%qab%aldf)
    CALL field%gl%dealloc()
    CALL field%ll%dealloc()
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(field%mem_id,mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real_2D

!-------------------------------------------------------------------------------
!* Create a new nodal field of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_real_2D(field,new_field,nqty,pd)
    IMPLICIT NONE

    !> reference field to mold
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> new field to be created
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(OUT) :: new_field
    !> number of quantities, field%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, field%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_real_2D',iftn,idepth)
    new_nqty=field%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=field%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(hcurl_rect_2D_real::new_field)
    SELECT TYPE (new_field)
    TYPE IS (hcurl_rect_2D_real)
      CALL new_field%alloc(field%mx,field%my,new_nqty,new_pd,field%id,          &
                           field%name)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_real_2D

!-------------------------------------------------------------------------------
!* Assign nodal_field_real to a nodal_field_real structure
!-------------------------------------------------------------------------------
  SUBROUTINE assign_field_real_2D(field,field2)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> field value to assign
    CLASS(nodal_field_real), INTENT(IN) :: field2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_field_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   removing the select type would be ideal
!-------------------------------------------------------------------------------
    SELECT TYPE (field2)
    TYPE IS (hcurl_rect_2D_real)
#ifdef DEBUG
      IF (field%nqty /= field2%nqty .OR. field%mx   /= field2%mx   .OR.         &
          field%my   /= field2%my   .OR. field%pd   /= field2%pd   .OR.         &
          field%ndim /= field2%ndim) THEN
        CALL par%nim_stop('Incompatible fields in assign_field_real_2D')
      ENDIF
#endif
      field%fsh=field2%fsh
      field%fsv=field2%fsv
      IF (ALLOCATED(field%fsi)) THEN
        field%fsi=field2%fsi
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected hcurl_rect_2D_real for field2'//              &
                        ' in assign_field_real_2D')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_field_real_2D

!-------------------------------------------------------------------------------
!> Evaluate the basis functions at the quadrature points
!  If the basis function are used for vector integrations, then wjac is the
!  the full weighted jacobian. If the basis functions are used for matrix
!  integration then wjac is only the sqaure root of the weighted jacobian.
!  If the basis functions are used for interpolation `wjac` is not passed.
!-------------------------------------------------------------------------------
  SUBROUTINE init_basis_ftn_real_2D(field,xg_vec)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> coordinates of the quadrature points (ng,ndim)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xg_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_basis_ftn_real_2D',iftn,idepth)
    !calls the _many overload
    CALL field%hcurl_rect_2D_bases(xg_vec,dorder=1,alpha=field%qa)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_basis_ftn_real_2D

!-------------------------------------------------------------------------------
!> Initialize the basis function alpha over full block for field including
!  a transformation from logical to coordinate space.
!-------------------------------------------------------------------------------
  SUBROUTINE init_block_basis_ftn_real_2D(field,jac,ng)
    USE quadrature_mod
    IMPLICIT NONE

    !> field to compute basis function for
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> the Jacobian information
    TYPE(qjac_real), INTENT(IN) :: jac
    !> number of gaussian quadrature points per element
    INTEGER(i4), INTENT(IN) :: ng

    INTEGER(i4) :: nel,nbasis,iel,ib,ig
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_block_basis_ftn_real_2D',          &
                              iftn,idepth)
    nel=field%nel
    nbasis=field%nbasis
    ALLOCATE(field%qab%alf(ng,nbasis,nel,field%fqty),                           &
             field%qab%aldf(ng,nbasis,nel,field%dfqty))
!-------------------------------------------------------------------------------
!   The coordinate transformation is trivial for the alpha so the reference
!   element evaluations can be copied out. For the derivative, this is the
!   canonical differential operation to perform on the H1 space. The coordinate
!   transformation uses the reciprocal basis vectors of the logical coordinates
!   in the orthonormal coordinates to get the components.
!-------------------------------------------------------------------------------
    DO iel=1,field%nel
      DO ig=1,ng
        DO ib = 1, 2*field%n_side
          field%qab%alf(ig,ib,iel,:) = jac%ijac(ig,iel,:,1)*field%qa%alf(ig,ib)
        ENDDO
        DO ib = 2*field%n_side + 1, 4*field%n_side
          field%qab%alf(ig,ib,iel,:) = jac%ijac(ig,iel,:,2)*field%qa%alf(ig,ib)
        ENDDO
        DO ib = 4*field%n_side + 1, 4*field%n_side + field%n_int/2
          field%qab%alf(ig,ib,iel,:) = jac%ijac(ig,iel,:,1)*field%qa%alf(ig,ib)
        ENDDO
        DO ib = 4*field%n_side + field%n_int/2, 4*field%n_side + field%n_int
          field%qab%alf(ig,ib,iel,:) = jac%ijac(ig,iel,:,2)*field%qa%alf(ig,ib)
        ENDDO
        field%qab%aldf(ig,:,iel,1) = field%qa%aldf(ig,:,1)/jac%detj(ig,iel)
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_block_basis_ftn_real_2D

!-------------------------------------------------------------------------------
!> Evaluate the field at an arbitrary point.
!-------------------------------------------------------------------------------
  RECURSIVE SUBROUTINE eval_val_real_2D(field,xi,iel,out_val,dorder,transform,  &
                                        coord)
    USE quadrature_mod, ONLY: interp_real,alpha_real
    IMPLICIT NONE

    !> field that is being interpolated
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> logical coordinate within the element where interpolation occurs
    REAL(r8), DIMENSION(:), INTENT(IN) :: xi
    !> block element number in which interpolation is being done
    INTEGER(i4), INTENT(IN) :: iel
    !> interpolated output value; this is assumed to be allocated
    TYPE(interp_real), INTENT(INOUT) :: out_val
    !> order to which to evaluate derivatives of the field
    INTEGER(i4), INTENT(IN) :: dorder
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> coordinate field
    CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord

    TYPE(alpha_real) :: al
    TYPE(interp_real) :: rz
    REAL(r8) :: f_el(field%nbasis,field%nqty)
    REAL(r8), DIMENSION(2,2) :: ijac
    REAL(r8) :: detj
    INTEGER(i4) :: ib,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'eval_val_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the output dorder
!-------------------------------------------------------------------------------
    out_val%dorder=dorder
!-------------------------------------------------------------------------------
!   gather the alpha values and the element-wise degrees of freedom
!-------------------------------------------------------------------------------
    CALL field%hcurl_rect_2D_bases(xi,dorder,al)
    CALL field%get_element_dofs(iel,f_el)
!-------------------------------------------------------------------------------
!   sum over bases
!-------------------------------------------------------------------------------
    out_val%f = 0._r8
    IF (transform) THEN
      ALLOCATE(rz%f(2), rz%df(4))
      CALL coord%eval(xi, iel, rz, dorder=1, transform=.FALSE.)
      detj = rz%df(1)*rz%df(4)-rz%df(3)*rz%df(2)
      ijac = RESHAPE([rz%df(4), -rz%df(3), -rz%df(2), rz%df(1)]/detj, [2,2])
      DO ib = 1, 2*field%n_side
        DO iq = 1, field%nqty
          out_val%f(2*iq-1:2*iq) = out_val%f(2*iq-1:2*iq)                       &
                                  + ijac(:,1)*al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 2*field%n_side + 1, 4*field%n_side
        DO iq = 1, field%nqty
          out_val%f(2*iq-1:2*iq) = out_val%f(2*iq-1:2*iq)                       &
                                  + ijac(:,2)*al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + 1, 4*field%n_side + field%n_int/2
        DO iq = 1, field%nqty
          out_val%f(2*iq-1:2*iq) = out_val%f(2*iq-1:2*iq)                       &
                                  + ijac(:,1)*al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + field%n_int/2 + 1, 4*field%n_side + field%n_int
        DO iq = 1, field%nqty
          out_val%f(2*iq-1:2*iq) = out_val%f(2*iq-1:2*iq)                       &
                                  + ijac(:,2)*al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      IF (dorder > 0) THEN
        DO iq = 1, field%nqty
          out_val%df(iq) = SUM(al%aldf(:,1)*f_el(:,iq))/detj
        ENDDO
      ENDIF
    ELSE
      DO ib = 1, 2*field%n_side
        DO iq = 1, field%nqty
          out_val%f(2*iq-1) = out_val%f(2*iq-1) + al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 2*field%n_side + 1, 4*field%n_side
        DO iq = 1, field%nqty
          out_val%f(2*iq) = out_val%f(2*iq) + al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + 1, 4*field%n_side + field%n_int/2
        DO iq = 1, field%nqty
          out_val%f(2*iq-1) = out_val%f(2*iq-1) + al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + field%n_int/2 + 1, 4*field%n_side + field%n_int
        DO iq = 1, field%nqty
          out_val%f(2*iq) = out_val%f(2*iq) + al%alf(ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      IF (dorder > 0) THEN
        DO iq = 1, field%nqty
          out_val%df(iq) = SUM(al%aldf(:,1)*f_el(:,iq))
        ENDDO
      ENDIF
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE eval_val_real_2D

!-------------------------------------------------------------------------------
!>  Evaluate the field at a collection of arbitrary points in a single element.
!-------------------------------------------------------------------------------
  RECURSIVE SUBROUTINE eval_many_real_2D(field,xi,iel,out_many,dorder,          &
                                         transform,coord)
    USE quadrature_mod, ONLY: batch_interp_real,qalpha_real
    IMPLICIT NONE

    !> field that is being interpolated
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> logical coordinates within the element where interpolation occurs
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
    !> block element number in which interpolation is being done
    INTEGER(i4), INTENT(IN) :: iel
    !> interpolated output value (assumed to be allocated)
    TYPE(batch_interp_real), INTENT(INOUT) :: out_many
    !> order to which to evaluate derivatives of the field
    INTEGER(i4), INTENT(IN) :: dorder
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> Jacobian information at xi in iel
    CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord

    TYPE(qalpha_real) :: al
    TYPE(batch_interp_real) :: rz
    REAL(r8), DIMENSION(field%nbasis,field%nqty) :: f_el
    REAL(r8), DIMENSION(SIZE(out_many%f,1),2,2) :: ijac
    REAL(r8), DIMENSION(SIZE(out_many%f,1)) :: detj
    INTEGER(i4) :: npoints,ib,iq,ip
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'eval_many_real_2D',iftn,idepth)
    npoints=SIZE(out_many%f,1)
!-------------------------------------------------------------------------------
!   set the output dorder
!-------------------------------------------------------------------------------
    out_many%dorder=dorder
!-------------------------------------------------------------------------------
!   gather the alpha values and the element-wise degrees of freedom
!-------------------------------------------------------------------------------
    CALL field%hcurl_rect_2D_bases(xi,dorder,al)
    CALL field%get_element_dofs(iel,f_el)
!-------------------------------------------------------------------------------
!   sum over bases
!-------------------------------------------------------------------------------
    out_many%f = 0._r8
    IF (transform) THEN
      ALLOCATE(rz%f(npoints,2), rz%df(npoints,4))
      CALL coord%eval(xi, iel, rz, dorder=1, transform=.FALSE.)
      detj = rz%df(:,1)*rz%df(:,4)-rz%df(:,3)*rz%df(:,2)
      ijac(:,1,1) = rz%df(:,4)/detj
      ijac(:,2,1) = -rz%df(:,3)/detj
      ijac(:,1,2) = -rz%df(:,2)/detj
      ijac(:,2,2) = rz%df(:,1)/detj
      DO ib = 1, 2*field%n_side
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1)                           &
                               + ijac(:,1,1)*al%alf(:,ib)*f_el(ib,iq)
          out_many%f(:,2*iq) = out_many%f(:,2*iq)                               &
                             + ijac(:,2,1)*al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 2*field%n_side + 1, 4*field%n_side
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1)                           &
                               + ijac(:,1,2)*al%alf(:,ib)*f_el(ib,iq)
          out_many%f(:,2*iq) = out_many%f(:,2*iq)                               &
                             + ijac(:,2,2)*al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + 1, 4*field%n_side + field%n_int/2
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1)                           &
                               + ijac(:,1,1)*al%alf(:,ib)*f_el(ib,iq)
          out_many%f(:,2*iq) = out_many%f(:,2*iq)                               &
                             + ijac(:,2,1)*al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + field%n_int/2 + 1, 4*field%n_side + field%n_int
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1)                           &
                               + ijac(:,1,2)*al%alf(:,ib)*f_el(ib,iq)
          out_many%f(:,2*iq) = out_many%f(:,2*iq)                               &
                             + ijac(:,2,2)*al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      IF (dorder > 0) THEN
        DO iq = 1, field%nqty
          DO ip = 1, npoints
            out_many%df(ip,iq) = SUM(al%aldf(ip,:,1)*f_el(:,iq))/detj(ip)
          ENDDO
        ENDDO
      ENDIF
    ELSE
      DO ib = 1, 2*field%n_side
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1) + al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 2*field%n_side + 1, 4*field%n_side
        DO iq = 1, field%nqty
          out_many%f(:,2*iq) = out_many%f(:,2*iq) + al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + 1, 4*field%n_side + field%n_int/2
        DO iq = 1, field%nqty
          out_many%f(:,2*iq-1) = out_many%f(:,2*iq-1) + al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      DO ib = 4*field%n_side + field%n_int/2 + 1, 4*field%n_side + field%n_int
        DO iq = 1, field%nqty
          out_many%f(:,2*iq) = out_many%f(:,2*iq) + al%alf(:,ib)*f_el(ib,iq)
        ENDDO
      ENDDO
      IF (dorder > 0) THEN
        DO iq = 1, field%nqty
          out_many%df(:,iq) = MATMUL(al%aldf(:,:,1), f_el(:,iq))
        ENDDO
      ENDIF
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE eval_many_real_2D

!-------------------------------------------------------------------------------
!*  Complete the collocation of a function onto nodal_field_real
!-------------------------------------------------------------------------------
  SUBROUTINE collocation_real_2D(field,func,coord)
    USE quadrature_mod, ONLY: interp_real
    IMPLICIT NONE

    !> field being set as a collocation of the function
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> function to be projected onto the field
    CLASS(abstract_function_real), INTENT(IN) :: func
    !> coordinate field corresponding to the same block as this field
    CLASS(nodal_field_real), INTENT(IN) :: coord

    REAL(r8), DIMENSION(0:field%pd) :: ll_nodes
    REAL(r8), DIMENSION(1:field%pd) :: gl_nodes
    REAL(r8), DIMENSION(field%nqty*field%fqty) :: fvec
    REAL(r8), DIMENSION(coord%dfqty) :: dRdx, dRdy
    TYPE(interp_real) :: rz
    INTEGER(i4) :: ix,iy,i,j,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'collocation_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   get the Labatto-Legendre and Gauss-Legendre node locations
!-------------------------------------------------------------------------------
    CALL field%ll%getnodes(ll_nodes)
    CALL field%gl%getnodes(gl_nodes)
!-------------------------------------------------------------------------------
!   allocate space for the spatial evaluation
!-------------------------------------------------------------------------------
    ALLOCATE(rz%f(2), rz%df(4))
!-------------------------------------------------------------------------------
!   evaluate the bottom edge
!-------------------------------------------------------------------------------
    DO ix = 1, field%mx
      DO i = 1, field%n_side
        CALL coord%eval([gl_nodes(i), ll_nodes(0)], ix, rz,                     &
                        dorder=1, transform=.FALSE.)
        CALL func%eval(rz%f, fvec)
        dRdx = [rz%df(1), rz%df(2)]
        DO iq = 1, field%nqty
          field%fsh(i,iq,ix,0) = SUM(fvec(2*iq-1:2*iq)*dRdx)
        ENDDO
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   evaluate the left edge
!-------------------------------------------------------------------------------
    DO iy = 1, field%my
      DO j = 1, field%n_side
        CALL coord%eval([ll_nodes(0), gl_nodes(j)], field%mx*(iy-1)+1, rz,      &
                        dorder=1, transform=.FALSE.)
        CALL func%eval(rz%f, fvec)
        dRdy = [rz%df(3), rz%df(4)]
        DO iq = 1, field%nqty
          field%fsv(j,iq,0,iy) = SUM(fvec(2*iq-1:2*iq)*dRdy)
        ENDDO
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   loop through all elements
!-------------------------------------------------------------------------------
    DO ix = 1, field%mx
      DO iy = 1, field%my
!-------------------------------------------------------------------------------
!       interior x-oriented points of each element
!-------------------------------------------------------------------------------
        DO j = 1, field%pd - 1
          DO i = 1, field%pd
            CALL coord%eval([gl_nodes(i), ll_nodes(j)], field%mx*(iy-1)+ix, rz, &
                            dorder=1, transform=.FALSE.)
            CALL func%eval(rz%f, fvec)
            dRdx = [rz%df(1), rz%df(2)]
            DO iq = 1, field%nqty
              field%fsi(field%pd*(j-1)+i,iq,ix,iy) = SUM(fvec(2*iq-1:2*iq)*dRdx)
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       interior y-oriented points of each element
!-------------------------------------------------------------------------------
        DO i = 1, field%pd - 1
          DO j = 1, field%pd
            CALL coord%eval([ll_nodes(i), gl_nodes(j)], field%mx*(iy-1)+ix, rz, &
                            dorder=1, transform=.FALSE.)
            CALL func%eval(rz%f, fvec)
            dRdy = [rz%df(3), rz%df(4)]
            DO iq = 1, field%nqty
              field%fsi(field%pd*(i-1)+j+field%n_int/2,iq,ix,iy)                &
                                                   = SUM(fvec(2*iq-1:2*iq)*dRdy)
            ENDDO
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       top edge of each element
!-------------------------------------------------------------------------------
        DO i = 1, field%n_side
          CALL coord%eval([gl_nodes(i), ll_nodes(field%pd)], field%mx*(iy-1)+ix,&
                          rz, dorder=1, transform=.FALSE.)
          CALL func%eval(rz%f, fvec)
          dRdx = [rz%df(1), rz%df(2)]
          DO iq = 1, field%nqty
            field%fsh(i,iq,ix,iy) = SUM(fvec(2*iq-1:2*iq)*dRdx)
          ENDDO
        ENDDO
!-------------------------------------------------------------------------------
!       right vertical edge of each element
!-------------------------------------------------------------------------------
        DO j = 1, field%n_side
          CALL coord%eval([ll_nodes(field%pd), gl_nodes(j)], field%mx*(iy-1)+ix,&
                          rz, dorder=1, transform=.FALSE.)
          CALL func%eval(rz%f, fvec)
          dRdy = [rz%df(3), rz%df(4)]
          DO iq = 1, field%nqty
            field%fsv(j,iq,ix,iy) = SUM(fvec(2*iq-1:2*iq)*dRdy)
          ENDDO
        ENDDO
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE collocation_real_2D

!-------------------------------------------------------------------------------
!> Updates quadrature point data for the H(curl) field.
!
!  If the `metric` is provided then the quadrature point data is evaluated in
!  the orthogonal coordinate system basis. Otherwise the quadrature point data
!  is evaluated the logical coordinate basis of the reference element.
!-------------------------------------------------------------------------------
  SUBROUTINE qp_update_real_2D(field,qpout,transform,metric)
    USE quadrature_mod, ONLY: qp_real,qjac_real
    IMPLICIT NONE

    !> field that is updating quadrature data
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> quadrature data being updated
    TYPE(qp_real), INTENT(INOUT) :: qpout
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> array of jacobian information within each element
    TYPE(qjac_real), OPTIONAL, INTENT(IN) :: metric

    INTEGER(i4) :: ng, iel, iq, ig, k, i, j
    REAL(r8), DIMENSION(field%nbasis, field%nqty) :: f_el
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'qp_update_real_2D',iftn,idepth)
    ng = SIZE(qpout%qpf,1)
    qpout%qpf=0._r8
    DO iel = 1, field%nel
      CALL field%get_element_dofs(iel, f_el)
      DO iq = 1, field%nqty
        IF (transform) THEN
          DO ig = 1, ng
            k = 1
            DO i = 1, 2*field%n_side
              qpout%qpf(ig,iel,:,iq) = qpout%qpf(ig,iel,:,iq)                   &
                        + metric%ijac(ig,iel,:,1)*field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO j = 1, 2*field%n_side
              qpout%qpf(ig,iel,:,iq) = qpout%qpf(ig,iel,:,iq)                   &
                        + metric%ijac(ig,iel,:,2)*field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO i = 1, field%n_int/2
              qpout%qpf(ig,iel,:,iq) = qpout%qpf(ig,iel,:,iq)                   &
                        + metric%ijac(ig,iel,:,1)*field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO j = 1, field%n_int/2
              qpout%qpf(ig,iel,:,iq) = qpout%qpf(ig,iel,:,iq)                   &
                        + metric%ijac(ig,iel,:,2)*field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            qpout%qpdf(ig,iel,1,iq) = SUM(field%qa%aldf(ig,:,1)*f_el(:,iq))     &
                                      /metric%detj(ig,iel)
          ENDDO
        ELSE
          DO ig = 1, ng
            k = 1
            DO i = 1, 2*field%n_side
              qpout%qpf(ig,iel,1,iq) = qpout%qpf(ig,iel,1,iq)                   &
                                    + field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO j = 1, 2*field%n_side
              qpout%qpf(ig,iel,2,iq) = qpout%qpf(ig,iel,2,iq)                   &
                                    + field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO i = 1, field%n_int/2
              qpout%qpf(ig,iel,1,iq) = qpout%qpf(ig,iel,1,iq)                   &
                                    + field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
            DO j = 1, field%n_int/2
              qpout%qpf(ig,iel,2,iq) = qpout%qpf(ig,iel,2,iq)                   &
                                    + field%qa%alf(ig,k)*f_el(k,iq)
              k = k + 1
            ENDDO
          ENDDO
          qpout%qpdf(:,iel,1,iq) = MATMUL(field%qa%aldf(:,:,1),f_el(:,iq))
        ENDIF
      ENDDO
    ENDDO
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE qp_update_real_2D

!-------------------------------------------------------------------------------
!> get the logical coordinates associated with this element as a flat
!  array. As these coordinates are not needed during routine finite
!  element operations, they are generated on the fly.
!-------------------------------------------------------------------------------
  SUBROUTINE get_logical_real_2D(field,lcoord)
    IMPLICIT NONE

    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: lcoord

    INTEGER(i4) :: idof,ibase,iy,ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_logical_real_2D',iftn,idepth)
    idof=1
    DO iy = 0, field%my
      DO ix = 1, field%mx
        DO ibase = 1, field%n_side
          lcoord(idof,1) = ix - field%ix0(ibase) + field%dx(ibase)
          lcoord(idof,2) = iy - field%iy0(ibase) + field%dy(ibase)
          idof=idof+1
        ENDDO
      ENDDO
    ENDDO
    DO iy = 0, field%my
      DO ix = 1, field%mx
        DO ibase = field%n_side + 1, 2*field%n_side
          lcoord(idof,1) = ix - field%ix0(ibase) + field%dx(ibase)
          lcoord(idof,2) = iy - field%iy0(ibase) + field%dy(ibase)
          idof=idof+1
        ENDDO
      ENDDO
    ENDDO
    IF (ALLOCATED(field%fsi)) THEN
      DO iy = 1, field%my
        DO ix = 1, field%mx
          DO ibase = 2*field%n_side + 1, 2*field%n_side + field%n_int
            lcoord(idof,1) = ix - field%ix0(ibase) + field%dx(ibase)
            lcoord(idof,2) = iy - field%iy0(ibase) + field%dy(ibase)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_logical_real_2D

!-------------------------------------------------------------------------------
!> Set the field value with an array ordered as given by the return
!  value of `get_logical_real_2D`
!-------------------------------------------------------------------------------
  SUBROUTINE set_field_real_2D(field,fvalue)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> field value,fvalue(nqty,ndof)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: fvalue

    INTEGER(i4) :: idof, iqty, iy, ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_logical_real_2D',iftn,idepth)
    DO iqty = 1, field%nqty
      idof = 1
      DO iy = 0, field%my
        DO ix = 1, field%mx
          field%fsh(:, iqty, ix, iy) = fvalue(iqty,idof:idof+field%n_side-1)
          idof = idof + field%n_side
        ENDDO
      ENDDO
      DO iy = 1, field%my
        DO ix = 0, field%mx
          field%fsv(:, iqty, ix, iy) = fvalue(iqty,idof:idof+field%n_side-1)
          idof = idof + field%n_side
        ENDDO
      ENDDO
      IF (ALLOCATED(field%fsi)) THEN
        DO iy = 1, field%my
          DO ix = 1, field%mx
            field%fsi(:, iqty, ix, iy) = fvalue(iqty,idof:idof+field%n_int-1)
            idof = idof + field%n_int
          ENDDO
        ENDDO
      ENDIF
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_field_real_2D

!-------------------------------------------------------------------------------
!> Get the field value with an array ordered as given by the return
!  value of `get_logical_real_2D`
!-------------------------------------------------------------------------------
  SUBROUTINE get_field_real_2D(field,fvalue)
    IMPLICIT NONE

    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: fvalue

    INTEGER(i4) :: idof, iqty, iy, ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_field_real_2D',iftn,idepth)
    DO iqty = 1, field%nqty
      idof = 1
      DO iy = 0, field%my
        DO ix = 1, field%mx
          fvalue(iqty,idof:idof+field%n_side-1) = field%fsh(:, iqty, ix, iy)
          idof = idof + field%n_side
        ENDDO
      ENDDO
      DO iy = 1, field%my
        DO ix = 0, field%mx
          fvalue(iqty,idof:idof+field%n_side-1) = field%fsv(:, iqty, ix, iy)
          idof = idof + field%n_side
        ENDDO
      ENDDO
      IF (ALLOCATED(field%fsi)) THEN
        DO iy = 1, field%my
          DO ix = 1, field%mx
            fvalue(iqty,idof:idof+field%n_int-1) = field%fsi(:, iqty, ix, iy)
            idof = idof + field%n_int
          ENDDO
        ENDDO
      ENDIF
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_field_real_2D

!-------------------------------------------------------------------------------
!> read field data from h5 file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read_real_2D(field,fname,gid,id,cid)
    USE io
    IMPLICIT NONE

    !> field to read
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> field name
    CHARACTER(*), INTENT(IN) :: fname
    !> hdf5 group ID
    INTEGER(HID_T), INTENT(IN) :: gid
    !> block id
    INTEGER(i4), INTENT(IN) :: id
    !> block id (character format)
    CHARACTER(*), INTENT(IN) :: cid

    INTEGER(i4) :: nqty,mx,my,pd
    INTEGER(HID_T) :: dsetid
    INTEGER :: error
    INTEGER(HSIZE_T), DIMENSION(4) :: dims
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read_real_2D',iftn,idepth)
    IF (.NOT.obj_exists(gid,TRIM(fname)//TRIM(cid),h5err)) THEN
      CALL par%nim_stop('h5_read_real_2D field '//TRIM(fname)//TRIM(cid)        &
                        //' not found')
    ENDIF
    CALL h5dopen_f(gid,TRIM(fname)//TRIM(cid),dsetid,error)
    CALL read_dims(dsetid,dims,h5err)
    CALL read_attribute(dsetid,"poly_degree",pd,h5in,h5err)
    CALL h5dclose_f(dsetid,error)
    nqty=INT(dims(2),KIND(i4))
    mx=INT(dims(3),KIND(i4))
    my=INT(dims(4)-1,KIND(i4))
    CALL field%alloc(mx,my,nqty,pd,id,TRIM(fname))
    CALL read_h5(gid,TRIM(fname)//TRIM(cid),field%fsh,h5in,h5err)
    CALL read_h5(gid,TRIM(fname)//'fsv'//TRIM(cid),field%fsv,h5in,h5err)
    IF (pd>1)                                                                   &
      CALL read_h5(gid,TRIM(fname)//'fsi'//TRIM(cid),field%fsi,h5in,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read_real_2D

!-------------------------------------------------------------------------------
!> write data to an h5 file as a single array to enable plotting of the
!  results.
!-------------------------------------------------------------------------------
  SUBROUTINE h5_dump_real_2D(field,fname,gid,cid)
    USE io
    IMPLICIT NONE

    !> field to write
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> field name
    CHARACTER(*), INTENT(IN) :: fname
    !> block id (character format)
    CHARACTER(*), INTENT(IN) :: cid
    !> hdf5 group ID
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(HID_T) :: dsetid
    INTEGER :: error
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_dump_real_2D',iftn,idepth)
    h5in%vsMD=TRIM(fname)
!-------------------------------------------------------------------------------
!   write arrays separately, append metadata to fsh and don't label that array
!-------------------------------------------------------------------------------
    CALL dump_h5(gid,TRIM(fname)//TRIM(cid),field%fsh,h5in,h5err)
    CALL dump_h5(gid,TRIM(fname)//'fsv'//TRIM(cid),field%fsv,h5in,h5err)
    IF (field%pd>1)                                                             &
      CALL dump_h5(gid,TRIM(fname)//'fsi'//TRIM(cid),field%fsi,h5in,h5err)
    CALL h5dopen_f(gid,TRIM(fname)//TRIM(cid),dsetid,error)
    CALL write_attribute(dsetid,"field_type","hcurl_rect_2D_real",h5in,h5err)
    CALL write_attribute(dsetid,"poly_degree",field%pd,h5in,h5err)
    CALL h5dclose_f(dsetid,error)
    h5in%vsMD=" "
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_dump_real_2D

!-------------------------------------------------------------------------------
!> broadcast field data to all modes
!-------------------------------------------------------------------------------
  SUBROUTINE bcast_real_2D(field,inode)
    USE pardata_mod
    IMPLICIT NONE

    !> field to broadcast
    CLASS(hcurl_rect_2D_real), INTENT(INOUT) :: field
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    INTEGER(i4) :: mx,my,nq,pd,id,tsize
    CHARACTER(LEN(field%name)) :: name
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'bcast_real_2D',iftn,idepth)
    IF (par%ilayer==inode) THEN
      mx=field%mx
      my=field%my
      nq=field%nqty
      pd=field%pd
      id=field%id
      name=field%name
      tsize=SIZE(field%title)
    ENDIF
    CALL par%mode_bcast(mx,inode)
    CALL par%mode_bcast(my,inode)
    CALL par%mode_bcast(nq,inode)
    CALL par%mode_bcast(pd,inode)
    CALL par%mode_bcast(id,inode)
    CALL par%mode_bcast(name,LEN(name),inode)
    CALL par%mode_bcast(tsize,inode)
    IF (par%ilayer/=inode)                                                      &
      CALL field%alloc(mx,my,nq,pd,id,name)
    CALL par%mode_bcast(field%fsh,SIZE(field%fsh),inode)
    CALL par%mode_bcast(field%fsv,SIZE(field%fsv),inode)
    IF (field%pd > 1) THEN
      CALL par%mode_bcast(field%fsi,SIZE(field%fsi),inode)
    ENDIF
    CALL par%mode_bcast(field%title(1),LEN(field%title(1))*tsize,inode)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE bcast_real_2D

!-------------------------------------------------------------------------------
!> Gather the element local degree of freedom values
!-------------------------------------------------------------------------------
  SUBROUTINE get_element_dofs(field,iel,fel)
    IMPLICIT NONE

    !> field to get degrees of freedom from
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> element number for requested degrees of freedom
    INTEGER(i4), INTENT(IN) :: iel
    !> degrees of freedom ordered by local basis number (same as alphas)
    REAL(r8), DIMENSION(field%nbasis,field%nqty), INTENT(OUT) :: fel

    INTEGER(i4) :: ix, iy, k, i
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_element_dofs',iftn,idepth)
    iy = (iel-1)/field%mx + 1
    ix = MODULO(iel-1,field%mx) + 1
!-------------------------------------------------------------------------------
!   Horizontal side centered dofs are first
!-------------------------------------------------------------------------------
    k = 1
    DO i = 1, field%n_side
      fel(k,:) = field%fsh(i,:,ix,iy-1)
      k=k+1
    ENDDO
    DO i = 1, field%n_side
      fel(k,:) = field%fsh(i,:,ix,iy)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Vertical side centered dofs are next
!-------------------------------------------------------------------------------
    DO i = 1, field%n_side
      fel(k,:) = field%fsv(i,:,ix-1,iy)
      k=k+1
    ENDDO
    DO i = 1, field%n_side
      fel(k,:) = field%fsv(i,:,ix,iy)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Interior centered dofs are next
!-------------------------------------------------------------------------------
    DO i = 1, field%n_int
      fel(k,:) = field%fsi(i,:,ix,iy)
      k=k+1
    END DO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_element_dofs

!-------------------------------------------------------------------------------
!> This subroutine evaluates the H(curl) basis functions and their curl at point
!  `(x, y)`.
!-------------------------------------------------------------------------------
  SUBROUTINE hcurl_rect_2D_bases_val(field,xi,dorder,alpha)
    USE quadrature_mod, ONLY: alpha_real
    IMPLICIT NONE

    !> field for which basis functions are being evaluated
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> logical coordinate at which to evaluate all basis functions
    REAL(r8), DIMENSION(:), INTENT(IN) :: xi
    !> derivative order for basis function evaluation
    INTEGER(i4), INTENT(IN) :: dorder
    !> evaluation of the reference element basis functions at point xi
    !  (expects alpha to be unallocated)
    TYPE(alpha_real), INTENT(OUT) :: alpha

    REAL(r8), DIMENSION(0:field%pd) :: alx_ll, aly_ll, dalx_ll, daly_ll
    REAL(r8), DIMENSION(1:field%pd) :: alx_gl, aly_gl
    INTEGER(i4) :: k, i, j
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'hcurl_rect_2D_bases_val',iftn,idepth)
!-------------------------------------------------------------------------------
!   Allocate alpha storage
!-------------------------------------------------------------------------------
    ALLOCATE(alpha%alf(field%nbasis))
!-------------------------------------------------------------------------------
!   Evaluate 1D cardinal basis functions for the quadrature points
!-------------------------------------------------------------------------------
    CALL field%gl%eval(xi(1), alx_gl)
    CALL field%ll%eval(xi(2), aly_ll)
    CALL field%ll%eval(xi(1), alx_ll)
    CALL field%gl%eval(xi(2), aly_gl)
!-------------------------------------------------------------------------------
!   Evaluates logical coordinate representation of basis functions. For
!-------------------------------------------------------------------------------
    k=1
    ! Bottom horizontal
    DO i = 1, field%n_side
      alpha%alf(k) = alx_gl(i)*aly_ll(0)
      k = k + 1
    ENDDO
    ! Top horizontal
    DO i = 1, field%n_side
      alpha%alf(k) = alx_gl(i)*aly_ll(field%pd)
      k = k + 1
    ENDDO
    ! Left vertical
    DO j = 1, field%n_side
      alpha%alf(k) = alx_ll(0)*aly_gl(j)
      k = k + 1
    ENDDO
    ! Right vertical
    DO j = 1, field%n_side
      alpha%alf(k) = alx_ll(field%pd)*aly_gl(j)
      k = k + 1
    ENDDO
    ! x-oriented interior alpha
    DO j = 1, field%pd - 1
      DO i = 1, field%pd
        alpha%alf(k) = alx_gl(i)*aly_ll(j)
        k = k + 1
      ENDDO
    ENDDO
    ! y-oriented interior alpha
    DO i = 1, field%pd - 1
      DO j = 1, field%pd
        alpha%alf(k) = alx_ll(i)*aly_gl(j)
        k = k + 1
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   Evaluates logical coordinate representation of curl of basis functions. For
!   2D H(curl) elements this maps to a scalar which represents the out of plane
!   component.
!-------------------------------------------------------------------------------
    IF (dorder > 0) THEN
!-------------------------------------------------------------------------------
!     Allocate alpha storage
!-------------------------------------------------------------------------------
      ALLOCATE(alpha%aldf(field%nbasis,1))
!-------------------------------------------------------------------------------
!     Evaluate 1D Labatto-Legendre basis function derivatives
!-------------------------------------------------------------------------------
      CALL field%ll%eval_d(xi(1), dalx_ll)
      CALL field%ll%eval_d(xi(2), daly_ll)
      k=1
      ! Bottom horizontal
      DO i = 1, field%pd
        alpha%aldf(k,1) = -alx_gl(i)*daly_ll(0)
        k = k + 1
      ENDDO
      ! Top horizontal
      DO i = 1, field%pd
        alpha%aldf(k,1) = -alx_gl(i)*daly_ll(field%pd)
        k = k + 1
      ENDDO
      ! Left vertical
      DO j = 1, field%pd
        alpha%aldf(k,1) = dalx_ll(0)*aly_gl(j)
        k = k + 1
      ENDDO
      ! Right vertical
      DO j = 1, field%pd
        alpha%aldf(k,1) = dalx_ll(field%pd)*aly_gl(j)
        k = k + 1
      ENDDO
      ! x-oriented interior alpha
      DO j = 1, field%pd - 1
        DO i = 1, field%pd
          alpha%aldf(k,1) = -alx_gl(i)*daly_ll(j)
          k = k + 1
        ENDDO
      ENDDO
      ! y-oriented interior alpha
      DO i = 1, field%pd - 1
        DO j = 1, field%pd
          alpha%aldf(k,1) = dalx_ll(i)*aly_gl(j)
          k = k + 1
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE hcurl_rect_2D_bases_val

!-------------------------------------------------------------------------------
!> This subroutine evaluates the H(curl) basis functions and their curl at point
!  `(x, y)`.
!-------------------------------------------------------------------------------
  SUBROUTINE hcurl_rect_2D_bases_many(field, xi, dorder, alpha)
    USE quadrature_mod, ONLY: qalpha_real
    IMPLICIT NONE

    !> field for which basis functions are being evaluated
    CLASS(hcurl_rect_2D_real), INTENT(IN) :: field
    !> logical coordinates at which to evaluate all basis functions
    !  (the first index is the point ordering and the second is the coordinate)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
    !> derivative order for basis function evaluation
    INTEGER(i4), INTENT(IN) :: dorder
    !> evaluation of the reference element basis functions at all points in xi
    !  (expects alpha to be unallocated)
    TYPE(qalpha_real), INTENT(OUT) :: alpha

    INTEGER(i4) :: i, j, k
    REAL(r8), DIMENSION(SIZE(xi,1),0:field%pd) :: alx_ll, dalx_ll
    REAL(r8), DIMENSION(SIZE(xi,1),0:field%pd) :: aly_ll, daly_ll
    REAL(r8), DIMENSION(SIZE(xi,1),1:field%pd) :: alx_gl
    REAL(r8), DIMENSION(SIZE(xi,1),1:field%pd) :: aly_gl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'hcurl_rect_2D_bases_many',iftn,idepth)
    CALL field%gl%eval(xi(:,1), alx_gl)
    CALL field%ll%eval(xi(:,2), aly_ll)
    CALL field%ll%eval(xi(:,1), alx_ll)
    CALL field%gl%eval(xi(:,2), aly_gl)
!-------------------------------------------------------------------------------
!   Allocate alpha storage
!-------------------------------------------------------------------------------
    ALLOCATE(alpha%alf(SIZE(xi,1),field%nbasis))
    k=1
    ! Bottom horizontal
    DO i = 1, field%n_side
      alpha%alf(:, k) = alx_gl(:,i)*aly_ll(:,0)
      k = k + 1
    ENDDO
    ! Top horizontal
    DO i = 1, field%n_side
      alpha%alf(:, k) = alx_gl(:, i)*aly_ll(:, field%pd)
      k = k + 1
    ENDDO
    ! Left vertical
    DO j = 1, field%n_side
      alpha%alf(:, k) = alx_ll(:, 0)*aly_gl(:, j)
      k = k + 1
    ENDDO
    ! Right vertical
    DO j = 1, field%n_side
      alpha%alf(:, k) = alx_ll(:, field%pd)*aly_gl(:, j)
      k = k + 1
    ENDDO
    ! x-oriented interior alpha
    DO j = 1, field%pd - 1
      DO i = 1, field%pd
        alpha%alf(:, k) = alx_gl(:, i)*aly_ll(:, j)
        k = k + 1
      ENDDO
    ENDDO
    ! y-oriented interior alpha
    DO i = 1, field%pd - 1
      DO j = 1, field%pd
        alpha%alf(:, k) = alx_ll(:, i)*aly_gl(:, j)
        k = k + 1
      ENDDO
    ENDDO
    IF (dorder > 0) THEN
!-------------------------------------------------------------------------------
!     Allocate alpha storage
!-------------------------------------------------------------------------------
      ALLOCATE(alpha%aldf(SIZE(xi,1),field%nbasis,1))
!-------------------------------------------------------------------------------
!     Evaluate 1D Labatto-Legendre basis function derivatives
!-------------------------------------------------------------------------------
      CALL field%ll%eval_d(xi(:,2), daly_ll)
      CALL field%ll%eval_d(xi(:,1), dalx_ll)
      k=1
      ! Bottom horizontal
      DO i = 1, field%pd
        alpha%aldf(:, k, 1) = -alx_gl(:, i)*daly_ll(:, 0)
        k = k + 1
      ENDDO
      ! Top horizontal
      DO i = 1, field%pd
        alpha%aldf(:, k, 1) = -alx_gl(:, i)*daly_ll(:, field%pd)
        k = k + 1
      ENDDO
      ! Left vertical
      DO j = 1, field%pd
        alpha%aldf(:, k, 1) = dalx_ll(:, 0)*aly_gl(:, j)
        k = k + 1
      ENDDO
      ! Right vertical
      DO j = 1, field%pd
        alpha%aldf(:, k, 1) = dalx_ll(:, field%pd)*aly_gl(:, j)
        k = k + 1
      ENDDO
      ! x-oriented interior alpha
      DO j = 1, field%pd - 1
        DO i = 1, field%pd
          alpha%aldf(:, k, 1) = -alx_gl(:, i)*daly_ll(:, j)
          k = k + 1
        ENDDO
      ENDDO
      ! y-oriented interior alpha
      DO i = 1, field%pd - 1
        DO j = 1, field%pd
          alpha%aldf(:, k, 1) = dalx_ll(:, i)*aly_gl(:, j)
          k = k + 1
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE hcurl_rect_2D_bases_many

END MODULE hcurl_rect_2D_real_mod
