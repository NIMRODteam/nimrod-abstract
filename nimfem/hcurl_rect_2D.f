!-------------------------------------------------------------------------------
!< Routines for evaluating Hcurl finite elements on blocks
!  of structured quadrilaterals.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the types for Hcurl finite elements on blocks
!  of structured quadrilaterals and their associated procedures.
!-------------------------------------------------------------------------------
MODULE hcurl_rect_2D_mod
  USE hcurl_rect_2D_real_mod
  !USE hcurl_rect_2D_comp_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: hcurl_rect_2D_real

  !PUBLIC :: hcurl_rect_2D_comp
END MODULE hcurl_rect_2D_mod
