!-------------------------------------------------------------------------------
!< Defines the abstract interfaces to blocks of real and complex nodal
!  data types and associated procedures.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the abstract interfaces to blocks of real and complex nodal
!  data types and associated procedures.
!
!  Operations performed are assignment, write to file, and evaluation.
!
!  Naming conventions for derived types should use the format
!  {desc}\_{shape}\_{dim}\_{type} where, for example, desc is h1 or hcurl,
!  shape is rect or tri, dim is 1D or 2D, and type is real or comp.
!-------------------------------------------------------------------------------
MODULE nodal_mod
  USE local
  USE timer_mod
  USE quadrature_mod, ONLY: qalpha_real, qalpha_block_real
  USE function_mod, ONLY: abstract_function_real, abstract_function_comp
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: nodal_field_real, nodal_field_comp

  PUBLIC :: rfem_storage, cfem_storage

  CHARACTER(*), PARAMETER :: mod_name='nodal'
!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  blocks of finite-element with a real data type.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: nodal_field_real
    !> number of quantities stored at DoF
    INTEGER(i4) :: nqty=0
    !> number of quantities for interpolation
    INTEGER(i4) :: fqty=0
    !> number of quantities for interpolation of the
    !  differential operator on the field
    INTEGER(i4) :: dfqty=0
    !> number of basis functions per element
    INTEGER(i4) :: nbasis=0
    !> number of degrees of freedom associated with a scalar quantity
    INTEGER(i4) :: ndof=0
    !> number of elements in field
    INTEGER(i4) :: nel=0
    !> FE dimensionality of field
    INTEGER(i4) :: ndim=0
    !> polynomial degree of field
    INTEGER(i4) :: pd=0
    !> derivative type that is produced by the field
    CHARACTER(8) :: deriv_type="none"
    !> component names
    CHARACTER(8), DIMENSION(:), ALLOCATABLE :: title
    !> field name
    CHARACTER(64) :: name="unknown"
    !> field type
    CHARACTER(64) :: field_type
    !> associated alpha storage for this field
    TYPE(qalpha_real) :: qa
    !> associated alpha storage for this field for the full block with
    !  jacobian transformation
    TYPE(qalpha_block_real) :: qab
    !> ID for parallel streaming
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    ! Deferred routines
    PROCEDURE(dealloc_real), PASS(field), DEFERRED :: dealloc
    PROCEDURE(alloc_with_mold_real), PASS(field), DEFERRED :: alloc_with_mold
    PROCEDURE(assign_real_field), PASS(field), DEFERRED :: assign_field
    PROCEDURE(init_basis_ftn_real), PASS(field), DEFERRED :: init_basis_ftn
    PROCEDURE(init_block_basis_ftn_real), PASS(field), DEFERRED ::              &
      init_block_basis_ftn
    PROCEDURE(eval_val_real), PASS(field), DEFERRED :: eval_val
    PROCEDURE(eval_many_real), PASS(field), DEFERRED :: eval_many
    PROCEDURE(collocation_real), PASS(field), DEFERRED :: collocation
    PROCEDURE(qp_update_real), PASS(field), DEFERRED :: qp_update
    PROCEDURE(get_logical_real), PASS(field), DEFERRED :: get_logical
    PROCEDURE(set_field_real), PASS(field), DEFERRED :: set_field
    PROCEDURE(get_field_real), PASS(field), DEFERRED :: get_field
    PROCEDURE(h5_read_real), PASS(field), DEFERRED :: h5_read
    PROCEDURE(h5_dump_real), PASS(field), DEFERRED :: h5_dump
    PROCEDURE(bcast_real), PASS(field), DEFERRED :: bcast
    GENERIC :: ASSIGNMENT (=) => assign_field
    GENERIC :: eval => eval_val, eval_many
    ! Defined routines
    PROCEDURE, PASS(field) :: move_field_to => move_field_to_real
    PROCEDURE, PASS(field) :: qp_alloc => qp_alloc_real
    PROCEDURE, PASS(field) :: qp_dealloc => qp_dealloc_real
  END TYPE nodal_field_real

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  blocks of finite-element with a complex data type.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: nodal_field_comp
    !> Number of quantities stored at DoF
    INTEGER(i4) :: nqty=0
    !> Number of quantities for interpolation
    INTEGER(i4) :: fqty=0
    !> Number of quantities for interpolation of the
    !  differential operator on the field
    INTEGER(i4) :: dfqty=0
    !> Number of basis functions per element
    INTEGER(i4) :: nbasis=0
    !> Number of degrees of freedom associated with a scalar quantity
    INTEGER(i4) :: ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> FE dimensionality of field
    INTEGER(i4) :: ndim=0
    !> polynomial degree of field
    INTEGER(i4) :: pd=0
    !> Number of Fourier components
    INTEGER(i4) :: nfour=0
    !> derivative type that is produced by the field
    CHARACTER(8) :: deriv_type="none"
    !> component names
    CHARACTER(8), DIMENSION(:), ALLOCATABLE :: title
    !> field name
    CHARACTER(64) :: name="unknown"
    !> field type
    CHARACTER(64) :: field_type
    !> associated alpha storage for this field
    TYPE(qalpha_real) :: qa
    !> associated alpha storage for this field for the full block with
    !  jacobian transformation
    TYPE(qalpha_block_real) :: qab
    !> ID for parallel streaming
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    ! Deferred routines
    PROCEDURE(dealloc_comp), PASS(field), DEFERRED :: dealloc
    PROCEDURE(alloc_with_mold_comp), PASS(field), DEFERRED :: alloc_with_mold
    PROCEDURE(assign_comp_field), PASS(field), DEFERRED :: assign_field
    PROCEDURE(init_basis_ftn_comp), PASS(field), DEFERRED :: init_basis_ftn
    PROCEDURE(init_block_basis_ftn_comp), PASS(field), DEFERRED ::              &
      init_block_basis_ftn
    PROCEDURE(eval_val_comp), PASS(field), DEFERRED :: eval_val
    PROCEDURE(eval_many_comp), PASS(field), DEFERRED :: eval_many
    PROCEDURE(collocation_comp), PASS(field), DEFERRED :: collocation
    PROCEDURE(qp_update_comp), PASS(field), DEFERRED :: qp_update
    PROCEDURE(get_logical_comp), PASS(field), DEFERRED :: get_logical
    PROCEDURE(set_field_comp), PASS(field), DEFERRED :: set_field
    PROCEDURE(get_field_comp), PASS(field), DEFERRED :: get_field
    PROCEDURE(h5_read_comp), PASS(field), DEFERRED :: h5_read
    PROCEDURE(h5_dump_comp), PASS(field), DEFERRED :: h5_dump
    PROCEDURE(send_and_trim_comp), PASS(field), DEFERRED :: send_and_trim
    PROCEDURE(collect_comp), PASS(field), DEFERRED :: collect
    GENERIC :: ASSIGNMENT (=) => assign_field
    GENERIC :: eval => eval_val, eval_many
    ! Defined routines
    PROCEDURE, PASS(field) :: move_field_to => move_field_to_comp
    PROCEDURE, PASS(field) :: qp_alloc => qp_alloc_comp
    PROCEDURE, PASS(field) :: qp_dealloc => qp_dealloc_comp
  END TYPE nodal_field_comp

!-------------------------------------------------------------------------------
!* container for nodal_field_real
!-------------------------------------------------------------------------------
  TYPE :: rfem_storage
    CLASS(nodal_field_real), ALLOCATABLE :: f
  END TYPE rfem_storage

!-------------------------------------------------------------------------------
!* container for nodal_field_comp
!-------------------------------------------------------------------------------
  TYPE :: cfem_storage
    CLASS(nodal_field_comp), ALLOCATABLE :: f
  END TYPE cfem_storage

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  Deallocate the nodal field
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_real(field)
      USE local
      IMPORT nodal_field_real
      !> field to dealloc
      CLASS(nodal_field_real), INTENT(INOUT) :: field
    END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!*  Create a new nodal field of the same type
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_real(field,new_field,nqty,pd)
      USE local
      IMPORT nodal_field_real
      !> reference field to mold
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> new field to be created
      CLASS(nodal_field_real), ALLOCATABLE, INTENT(OUT) :: new_field
      !> number of quantities, field%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, field%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    END SUBROUTINE alloc_with_mold_real

!-------------------------------------------------------------------------------
!*  Assign nodal_field_real to a nodal_field_real structure
!-------------------------------------------------------------------------------
    SUBROUTINE assign_real_field(field,field2)
      USE local
      IMPORT nodal_field_real
      !> field on which to operate
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> field value to assign
      CLASS(nodal_field_real), INTENT(IN) :: field2
    END SUBROUTINE assign_real_field

!-------------------------------------------------------------------------------
!>  Initialize the basis function alpha. This value of the basis fuction
!   and it's derivitaves are calculated and stored at the quadrature points
!-------------------------------------------------------------------------------
    SUBROUTINE init_basis_ftn_real(field,xg_vec)
      USE local
      USE quadrature_mod
      IMPORT nodal_field_real
      !> field on which to operate
      CLASS(nodal_field_real),INTENT(INOUT) :: field
      !> coordinates of the quadrature points (ng,ndim)
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: xg_vec
    END SUBROUTINE init_basis_ftn_real

!-------------------------------------------------------------------------------
!>  Initialize the basis function alpha over full block for field including
!   a tranformation from logical to coordinate space.
!-------------------------------------------------------------------------------
    SUBROUTINE init_block_basis_ftn_real(field,jac,ng)
      USE local
      USE quadrature_mod
      IMPORT nodal_field_real
      !> field to compute basis function for
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> the Jacobian information
      TYPE(qjac_real), INTENT(IN) :: jac
      !> number of gaussian quadrature points per element
      INTEGER(i4), INTENT(IN) :: ng
    END SUBROUTINE init_block_basis_ftn_real

!-------------------------------------------------------------------------------
!>  Evaluate the field at an arbitrary point.
!-------------------------------------------------------------------------------
    RECURSIVE SUBROUTINE eval_val_real(field,xi,iel,out_val,dorder,transform,   &
                                       coord)
      USE local
      USE quadrature_mod, ONLY: interp_real
      IMPORT nodal_field_real
      !> field that is being interpolated
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> logical coordinates within the element where interpolation occurs
      REAL(r8), DIMENSION(:), INTENT(IN) :: xi
      !> block element number in which interpolation is being done
      INTEGER(i4), INTENT(IN) :: iel
      !> interpolated output value; this is assumed to be allocated
      TYPE(interp_real), INTENT(INOUT) :: out_val
      !> order to which to evaluate derivatives of the field
      INTEGER(i4), INTENT(IN) :: dorder
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> Jacobian information at xi in iel
      CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord
    END SUBROUTINE eval_val_real

!-------------------------------------------------------------------------------
!>  Evaluate the field at a collection of arbitrary points in a single element.
!-------------------------------------------------------------------------------
    RECURSIVE SUBROUTINE eval_many_real(field,xi,iel,out_many,dorder,transform, &
                                        coord)
      USE local
      USE quadrature_mod, ONLY: batch_interp_real
      IMPORT nodal_field_real
      !> field that is being interpolated
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> logical coordinates within the element where interpolation occurs
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
      !> block element number in which interpolation is being done
      INTEGER(i4), INTENT(IN) :: iel
      !> interpolated output value (assumed to be allocated)
      TYPE(batch_interp_real), INTENT(INOUT) :: out_many
      !> order to which to evaluate derivatives of the field
      INTEGER(i4), INTENT(IN) :: dorder
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> Jacobian information at xi in iel
      CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord
    END SUBROUTINE eval_many_real

!-------------------------------------------------------------------------------
!*  Complete the collocation of a function onto nodal_field_real
!-------------------------------------------------------------------------------
    SUBROUTINE collocation_real(field,func,coord)
      USE local
      USE function_mod, ONLY: abstract_function_real
      IMPORT nodal_field_real
      !> field which gets projected onto
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> function to project onto the field
      CLASS(abstract_function_real), INTENT(IN) :: func
      !> field representing the mesh geometry of the field
      CLASS(nodal_field_real), INTENT(IN) :: coord
    END SUBROUTINE collocation_real

!-------------------------------------------------------------------------------
!>  Updates quadrature point storage for the field, optionally using precomputed
!   element Jacobian information.
!
!   Including the `metric` input is the usual quadrature point data
!   representation in NIMROD where values and derivatives are with respect to
!   the orthogonal spatial coordinate system.
!   The alternative is to have the quadrature point data representing values and
!   derivatives with respect to the reference element coordinates.
!-------------------------------------------------------------------------------
    SUBROUTINE qp_update_real(field,qpout,transform,metric)
      USE local
      USE quadrature_mod, ONLY: qjac_real, qp_real
      IMPORT nodal_field_real
      !> field that is updating quadrature data
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> quadrature data being updated
      TYPE(qp_real), INTENT(INOUT) :: qpout
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> array of jacobian information within each element
      TYPE(qjac_real), OPTIONAL, INTENT(IN) :: metric
    END SUBROUTINE qp_update_real

!-------------------------------------------------------------------------------
!>  get the logical coordinates associated with this element as a flat
!   array. As these coordinates are not needed during routine finite
!   element operations, they are generated on the fly.
!-------------------------------------------------------------------------------
    SUBROUTINE get_logical_real(field,lcoord)
      USE local
      IMPORT nodal_field_real
      !> field on which to operate
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> logical coordinate array, lcood(ndim,ndof)
      REAL(r8), DIMENSION(:,:), INTENT(OUT) :: lcoord
    END SUBROUTINE get_logical_real

!-------------------------------------------------------------------------------
!>  Set the field value with an array ordered as given by the return
!   value of `get_logical_real`
!-------------------------------------------------------------------------------
    SUBROUTINE set_field_real(field,fvalue)
      USE local
      IMPORT nodal_field_real
      !> field on which to operate
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> field value, fvalue(nqty,ndof)
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: fvalue
    END SUBROUTINE set_field_real

!-------------------------------------------------------------------------------
!>  Get the field value with an array ordered as given by the return
!   value of `get_logical_real`
!-------------------------------------------------------------------------------
    SUBROUTINE get_field_real(field,fvalue)
      USE local
      IMPORT nodal_field_real
      !> field on which to operate
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> field value, fvalue(nqty,ndof)
      REAL(r8), DIMENSION(:,:), INTENT(OUT) :: fvalue
    END SUBROUTINE get_field_real

!-------------------------------------------------------------------------------
!*  read field data from h5 file
!-------------------------------------------------------------------------------
    SUBROUTINE h5_read_real(field,fname,gid,id,cid)
      USE local
      USE io
      IMPORT nodal_field_real
      !> field to read
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> field name
      CHARACTER(*), INTENT(IN) :: fname
      !> hdf5 group ID
      INTEGER(HID_T), INTENT(IN) :: gid
      !> block id
      INTEGER(i4), INTENT(IN) :: id
      !> block id (character format)
      CHARACTER(*), INTENT(IN) :: cid
    END SUBROUTINE h5_read_real

!-------------------------------------------------------------------------------
!>  write data to an h5 file as a single array to enable plotting of the
!   results.
!-------------------------------------------------------------------------------
    SUBROUTINE h5_dump_real(field,fname,gid,cid)
      USE io
      IMPORT nodal_field_real
      !> field to write
      CLASS(nodal_field_real), INTENT(IN) :: field
      !> field name
      CHARACTER(*), INTENT(IN) :: fname
      !> hdf5 group ID
      INTEGER(HID_T), INTENT(IN) :: gid
      !> block id (character format)
      CHARACTER(*), INTENT(IN) :: cid
    END SUBROUTINE h5_dump_real

!-------------------------------------------------------------------------------
!>  broadcast field data to all layers
!-------------------------------------------------------------------------------
    SUBROUTINE bcast_real(field,inode)
      USE local
      IMPORT nodal_field_real
      !> field to broadcast
      CLASS(nodal_field_real), INTENT(INOUT) :: field
      !> mpi process to broadcast from
      INTEGER(i4), INTENT(IN) :: inode
    END SUBROUTINE bcast_real

!-------------------------------------------------------------------------------
!*  Deallocate nodal field
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_comp(field)
      IMPORT nodal_field_comp
      !> field to dealloc
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
    END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!*  Create a new nodal field of the same type
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_comp(field,new_field,nqty,pd,nfour)
      USE local
      IMPORT nodal_field_comp
      !> reference field to mold
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> new field to be created
      CLASS(nodal_field_comp), ALLOCATABLE, INTENT(OUT) :: new_field
      !> number of quantities, field%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, field%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
      !> number of Fourier components, field%nfour is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nfour
    END SUBROUTINE alloc_with_mold_comp

!-------------------------------------------------------------------------------
!*  Assign nodal_field_comp to a nodal_field_comp structure
!-------------------------------------------------------------------------------
    SUBROUTINE assign_comp_field(field,field2)
      USE local
      IMPORT nodal_field_comp
      !> field on which to operate
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> field value to assign
      CLASS(nodal_field_comp), INTENT(IN) :: field2
    END SUBROUTINE assign_comp_field

!-------------------------------------------------------------------------------
!>  Initialize the basis function alpha. This value of the basis fuction
!   and it's derivitaves are calculated and stored at the quadrature points
!-------------------------------------------------------------------------------
    SUBROUTINE init_basis_ftn_comp(field,xg_vec)
      USE local
      USE quadrature_mod
      IMPORT nodal_field_comp
      !> field on which to operate
      CLASS(nodal_field_comp),INTENT(INOUT) :: field
      !> coordinates of the quadrature points (ng,ndim)
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: xg_vec
    END SUBROUTINE init_basis_ftn_comp

!-------------------------------------------------------------------------------
!>  Initialize the basis function alpha over full block for field including
!   a tranformation from logical to coordinate space.
!-------------------------------------------------------------------------------
    SUBROUTINE init_block_basis_ftn_comp(field,jac,ng)
      USE local
      USE quadrature_mod
      IMPORT nodal_field_comp
      !> field to compute basis function for
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> the Jacobian information
      TYPE(qjac_real), INTENT(IN) :: jac
      !> number of gaussian quadrature points per element
      INTEGER(i4), INTENT(IN) :: ng
    END SUBROUTINE init_block_basis_ftn_comp

!-------------------------------------------------------------------------------
!>  Evaluate the field at an arbitrary point.
!-------------------------------------------------------------------------------
    RECURSIVE SUBROUTINE eval_val_comp(field,xi,iel,out_val,dorder,transform,   &
                                       coord)
      USE local
      USE quadrature_mod, ONLY: interp_comp
      IMPORT nodal_field_real, nodal_field_comp
      !> field that is being interpolated
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> logical coordinates within the element where interpolation occurs
      REAL(r8), DIMENSION(:), INTENT(IN) :: xi
      !> block element number in which interpolation is being done
      INTEGER(i4), INTENT(IN) :: iel
      !> interpolated output value; this is assumed to be allocated
      TYPE(interp_comp), INTENT(INOUT) :: out_val
      !> order to which to evaluate derivatives of the field
      INTEGER(i4), INTENT(IN) :: dorder
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> Jacobian information at xi in iel
      CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord
    END SUBROUTINE eval_val_comp

!-------------------------------------------------------------------------------
!>  Evaluate the field at a collection of arbitrary points in a single element.
!-------------------------------------------------------------------------------
    RECURSIVE SUBROUTINE eval_many_comp(field,xi,iel,out_many,dorder,transform, &
                                        coord)
      USE local
      USE quadrature_mod, ONLY: batch_interp_comp
      IMPORT nodal_field_real, nodal_field_comp
      !> field that is being interpolated
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> logical coordinates within the element where interpolation occurs
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
      !> block element number in which interpolation is being done
      INTEGER(i4), INTENT(IN) :: iel
      !> interpolated output value (assumed to be allocated)
      TYPE(batch_interp_comp), INTENT(INOUT) :: out_many
      !> order to which to evaluate derivatives of the field
      INTEGER(i4), INTENT(IN) :: dorder
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> Jacobian information at xi in iel
      CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord
    END SUBROUTINE eval_many_comp

!-------------------------------------------------------------------------------
!*  Complete the collocation of a function onto nodal_field_real
!-------------------------------------------------------------------------------
    SUBROUTINE collocation_comp(field,func,coord)
      USE local
      USE function_mod, ONLY: abstract_function_comp
      IMPORT nodal_field_real, nodal_field_comp
      !> field which gets projected onto
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> function to project onto the field
      CLASS(abstract_function_comp), INTENT(IN) :: func
      !> field representing the mesh geometry of the field
      CLASS(nodal_field_real), INTENT(IN) :: coord
    END SUBROUTINE collocation_comp

!-------------------------------------------------------------------------------
!>  Updates quadrature point storage for the field, optionally using precomputed
!   element Jacobian information.
!
!   Including the `metric` input is the usual quadrature point data
!   representation in NIMROD where values and derivatives are with respect to
!   the orthogonal spatial coordinate system.
!   The alternative is to have the quadrature point data representing values and
!   derivatives with respect to the reference element coordinates.
!-------------------------------------------------------------------------------
    SUBROUTINE qp_update_comp(field,qpout,transform,metric)
      USE local
      USE quadrature_mod, ONLY: qjac_real, qp_comp
      IMPORT nodal_field_comp
      !> field that is updating quadrature data
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> quadrature data being updated
      TYPE(qp_comp), INTENT(INOUT) :: qpout
      !> flag to indicate transformation from logical to coordinate space
      LOGICAL, INTENT(IN) :: transform
      !> array of jacobian information within each element
      TYPE(qjac_real), OPTIONAL, INTENT(IN) :: metric
    END SUBROUTINE qp_update_comp

!-------------------------------------------------------------------------------
!>  get the logical coordinates associated with this element as a flat
!   array. As these coordinates are not needed during routine finite
!   element operations, they are generated on the fly.
!-------------------------------------------------------------------------------
    SUBROUTINE get_logical_comp(field,lcoord)
      USE local
      IMPORT nodal_field_comp
      !> field on which to operate
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> logical coordinate array, lcood(ndim,ndof)
      REAL(r8), DIMENSION(:,:), INTENT(OUT) :: lcoord
    END SUBROUTINE get_logical_comp

!-------------------------------------------------------------------------------
!>  Set the field value with an array ordered as given by the return
!   value of `get_logical_comp`
!-------------------------------------------------------------------------------
    SUBROUTINE set_field_comp(field,fvalue)
      USE local
      IMPORT nodal_field_comp
      !> field on which to operate
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> field value, fvalue(nqty,ndof,nfour)
      COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: fvalue
    END SUBROUTINE set_field_comp

!-------------------------------------------------------------------------------
!>  Get the field value with an array ordered as given by the return
!   value of `get_logical_comp`
!-------------------------------------------------------------------------------
    SUBROUTINE get_field_comp(field,fvalue)
      USE local
      IMPORT nodal_field_comp
      !> field on which to operate
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> field value, fvalue(nqty,ndof,nfour)
      COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: fvalue
    END SUBROUTINE get_field_comp

!-------------------------------------------------------------------------------
!*  read field data from h5 file
!-------------------------------------------------------------------------------
    SUBROUTINE h5_read_comp(field,fname,gid,id,cid)
      USE local
      USE io
      IMPORT nodal_field_comp
      !> field to read
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> field name
      CHARACTER(*), INTENT(IN) :: fname
      !> hdf5 group ID
      INTEGER(HID_T), INTENT(IN) :: gid
      !> block id
      INTEGER(i4), INTENT(IN) :: id
      !> block id (character format)
      CHARACTER(*), INTENT(IN) :: cid
    END SUBROUTINE h5_read_comp

!-------------------------------------------------------------------------------
!>  write data to an h5 file as a single array to enable plotting of the
!   results.
!-------------------------------------------------------------------------------
    SUBROUTINE h5_dump_comp(field,fname,gid,cid)
      USE io
      IMPORT nodal_field_comp
      !> field to write
      CLASS(nodal_field_comp), INTENT(IN) :: field
      !> field name
      CHARACTER(*), INTENT(IN) :: fname
      !> hdf5 group ID
      INTEGER(HID_T), INTENT(IN) :: gid
      !> block id (character format)
      CHARACTER(*), INTENT(IN) :: cid
    END SUBROUTINE h5_dump_comp

!-------------------------------------------------------------------------------
!>  send field data to all layers and trim local data to owned modes
!-------------------------------------------------------------------------------
    SUBROUTINE send_and_trim_comp(field,inode)
      USE local
      IMPORT nodal_field_comp
      !> field to broadcast
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> mpi process to broadcast from
      INTEGER(i4), INTENT(IN) :: inode
    END SUBROUTINE send_and_trim_comp

!-------------------------------------------------------------------------------
!>  collect field data from all layers and output nodal type with full mode data
!-------------------------------------------------------------------------------
    SUBROUTINE collect_comp(field,field_full,inode)
      USE local
      IMPORT nodal_field_comp
      !> fields to collect mode data from
      CLASS(nodal_field_comp), INTENT(INOUT) :: field
      !> new field to collect data to on mpi proc inode
      CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: field_full
      !> mpi process collect data on
      INTEGER(i4), INTENT(IN) :: inode
    END SUBROUTINE collect_comp
  END INTERFACE

CONTAINS

!-------------------------------------------------------------------------------
!* Move field (dealloc original)
!-------------------------------------------------------------------------------
  SUBROUTINE move_field_to_real(field,new_field)
    USE timer_mod
    IMPLICIT NONE

    !> field to move and deallocate
    CLASS(nodal_field_real), INTENT(INOUT) :: field
    !> field to allocate and move to
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(INOUT) :: new_field

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'move_field_to_real',iftn,idepth)
    CALL field%alloc_with_mold(new_field)
    new_field=field
    CALL field%dealloc
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE move_field_to_real

!-------------------------------------------------------------------------------
!> allocate quadrature point data storage. sizes are (ng,nel,fqty,nqty) and
!  (ng,nel,dfqty,nqty)
!-------------------------------------------------------------------------------
  SUBROUTINE qp_alloc_real(field,qpout,ng)
    USE quadrature_mod, ONLY: qp_real
    IMPLICIT NONE

    !> field that is allocating associated quadrature data
    CLASS(nodal_field_real), INTENT(IN) :: field
    !> quadrature data being allocated
    TYPE(qp_real), INTENT(OUT) :: qpout
    !> number of quadrature points used by the quadrature rule for this block
    INTEGER(i4), INTENT(IN) :: ng

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'qp_alloc_real',iftn,idepth)
    ALLOCATE(qpout%qpf(ng,field%nel,field%fqty,field%nqty),                     &
             qpout%qpdf(ng,field%nel,field%dfqty,field%nqty))
    !$acc enter data create(qpout%qpf,qpout%qpdf)                               &
    !$acc async(field%id) if(field%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE qp_alloc_real

!-------------------------------------------------------------------------------
!> deallocate quadrature point data storage.
!-------------------------------------------------------------------------------
  SUBROUTINE qp_dealloc_real(field,qpout)
    USE quadrature_mod, ONLY: qp_real
    IMPLICIT NONE

    !> field that is deallocating associated quadrature data
    CLASS(nodal_field_real), INTENT(IN) :: field
    !> quadrature data being deallocated
    TYPE(qp_real), INTENT(INOUT) :: qpout

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'qp_dealloc_real',iftn,idepth)
    IF (ALLOCATED(qpout%qpf)) THEN
      !$acc exit data delete(qpout%qpf,qpout%qpdf) finalize                     &
      !$acc async(field%id) if(field%on_gpu)
      DEALLOCATE(qpout%qpf,qpout%qpdf)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE qp_dealloc_real

!-------------------------------------------------------------------------------
!* Move field (dealloc original)
!-------------------------------------------------------------------------------
  SUBROUTINE move_field_to_comp(field,new_field)
    USE timer_mod
    IMPLICIT NONE

    !> field to move and deallocate
    CLASS(nodal_field_comp), INTENT(INOUT) :: field
    !> field to allocate and move to
    CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: new_field

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'move_field_to_comp',iftn,idepth)
    CALL field%alloc_with_mold(new_field)
    new_field=field
    CALL field%dealloc
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE move_field_to_comp

!-------------------------------------------------------------------------------
!> allocate quadrature point data storage. sizes are (ng,nel,nfour,fqty,nqty)
!  and (ng,nel,nfour,dfqty,nqty)
!-------------------------------------------------------------------------------
  SUBROUTINE qp_alloc_comp(field,qpout,ng)
    USE quadrature_mod, ONLY: qp_comp
    IMPLICIT NONE

    !> field that is allocating associated quadrature data
    CLASS(nodal_field_comp), INTENT(IN) :: field
    !> quadrature data being allocated
    TYPE(qp_comp), INTENT(OUT) :: qpout
    !> number of quadrature points used by the quadrature rule for this block
    INTEGER(i4), INTENT(IN) :: ng

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'qp_alloc_comp',iftn,idepth)
    ALLOCATE(qpout%qpf(ng,field%nel,field%nfour,field%fqty,field%nqty),         &
             qpout%qpdf(ng,field%nel,field%nfour,field%dfqty,field%nqty))
    !$acc enter data create(qpout%qpf,qpout%qpdf)                               &
    !$acc async(field%id) if(field%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE qp_alloc_comp

!-------------------------------------------------------------------------------
!> deallocate quadrature point data storage.
!-------------------------------------------------------------------------------
  SUBROUTINE qp_dealloc_comp(field,qpout)
    USE quadrature_mod, ONLY: qp_comp
    IMPLICIT NONE

    !> field that is deallocating associated quadrature data
    CLASS(nodal_field_comp), INTENT(IN) :: field
    !> quadrature data being deallocated
    TYPE(qp_comp), INTENT(INOUT) :: qpout

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'qp_dealloc_comp',iftn,idepth)
    IF (ALLOCATED(qpout%qpf)) THEN
      !$acc exit data delete(qpout%qpf,qpout%qpdf) finalize                     &
      !$acc async(field%id) if(field%on_gpu)
      DEALLOCATE(qpout%qpf,qpout%qpdf)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE qp_dealloc_comp

END MODULE nodal_mod
