!-------------------------------------------------------------------------------
!! Routines for simple mesh initializations
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Routines for simple mesh initializations
!-------------------------------------------------------------------------------
MODULE simple_mesh_mod
  USE local
  USE h1_rect_2D_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: simple_rect_2D_mesh

!-------------------------------------------------------------------------------
!* Type to initialize a mesh of blocks of structured quadrilaterals
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(h1_rect_2D_real) :: simple_rect_2D_mesh
  CONTAINS

    PROCEDURE, PASS(mesh) :: setup_rect => setup_rect_2D
    PROCEDURE, PASS(mesh) :: transform_rt_rz => transform_rt_rz_2D
    PROCEDURE, PASS(mesh) :: setup_leaf => setup_leaf_cyl_2D
  END TYPE simple_rect_2D_mesh

CONTAINS

!-------------------------------------------------------------------------------
!* Compute node locations a structured mesh
!-------------------------------------------------------------------------------
  SUBROUTINE setup_rect_2D(mesh,nxbl,nybl,ixbl,iybl,xmin,xmax,ymin,ymax)
    IMPLICIT NONE

    !> real nodal field containing RZ coordinates of the mesh
    CLASS(simple_rect_2D_mesh), INTENT(INOUT) :: mesh
    !> number of logical x-directed blocks (default 1)
    INTEGER(i4), INTENT(IN), OPTIONAL :: nxbl
    !> number of logical y-directed blocks (default 1)
    INTEGER(i4), INTENT(IN), OPTIONAL :: nybl
    !> index of this block in the logical x direction (default 1)
    INTEGER(i4), INTENT(IN), OPTIONAL :: ixbl
    !> index of this block in the logical y direction (default 1)
    INTEGER(i4), INTENT(IN), OPTIONAL :: iybl
    !> minimum R value to be applied in the x direction
    REAL(r8), INTENT(IN), OPTIONAL :: xmin
    !> maximum R value to be applied in the x direction
    REAL(r8), INTENT(IN), OPTIONAL :: xmax
    !> minimum Z value to be applied in the y direction
    REAL(r8), INTENT(IN), OPTIONAL :: ymin
    !> maximum Z value to be applied in the y direction
    REAL(r8), INTENT(IN), OPTIONAL :: ymax

    INTEGER(i4) :: ix,iy,k,j
    REAL(r8), DIMENSION(0:mesh%pd) :: x,y
    REAL(r8) :: xoff,yoff,xfac,yfac

    IF (PRESENT(nxbl).AND.PRESENT(nybl).AND.PRESENT(ixbl).AND.PRESENT(iybl))    &
        THEN
      xoff=REAL(ixbl-1,r8)/nxbl
      yoff=REAL(iybl-1,r8)/nybl
      xfac=1._r8/nxbl
      yfac=1._r8/nybl
    ELSE
      xoff=0._r8
      yoff=0._r8
      xfac=1._r8
      yfac=1._r8
    ENDIF
    DO ix=0,mesh%mx
      DO iy=0,mesh%my
        mesh%fs(:,ix,iy)=                                                       &
          [xoff+xfac*REAL(ix,r8)/mesh%mx,yoff+yfac*REAL(iy,r8)/mesh%my]
      ENDDO
    ENDDO
    IF (ALLOCATED(mesh%fsh)) THEN
      DO ix=1,mesh%mx
        DO iy=0,mesh%my
          CALL mesh%ll%getnodes(x,xoff+xfac*REAL(ix-1,r8)/mesh%mx,              &
                                  xoff+xfac*REAL(ix,r8)/mesh%mx)
          mesh%fsh(1,:,ix,iy)=x(1:mesh%n_side)
          mesh%fsh(2,:,ix,iy)=yoff+yfac*REAL(iy,r8)/mesh%my
        ENDDO
      ENDDO
      DO ix=0,mesh%mx
        DO iy=1,mesh%my
          CALL mesh%ll%getnodes(y,yoff+yfac*REAL(iy-1,r8)/mesh%my,              &
                                  yoff+yfac*REAL(iy,r8)/mesh%my)
          mesh%fsv(1,:,ix,iy)=xoff+xfac*REAL(ix,r8)/mesh%mx
          mesh%fsv(2,:,ix,iy)=y(1:mesh%n_side)
        ENDDO
      ENDDO
      DO ix=1,mesh%mx
        CALL mesh%ll%getnodes(x,xoff+xfac*REAL(ix-1,r8)/mesh%mx,                &
                                xoff+xfac*REAL(ix,r8)/mesh%mx)
        DO iy=1,mesh%my
          CALL mesh%ll%getnodes(y,yoff+yfac*REAL(iy-1,r8)/mesh%my,              &
                                  yoff+yfac*REAL(iy,r8)/mesh%my)
          j=1
          DO k=1,mesh%n_int,mesh%n_side
            mesh%fsi(1,k:k+mesh%n_side-1,ix,iy)=x(1:mesh%n_side)
            mesh%fsi(2,k:k+mesh%n_side-1,ix,iy)=y(j)
            j=j + 1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    IF (PRESENT(xmax).AND.PRESENT(xmin)) THEN
      mesh%fs(1,:,:)=mesh%fs(1,:,:)*(xmax-xmin)+xmin
      IF (ALLOCATED(mesh%fsh)) THEN
        mesh%fsh(1,:,:,:)=mesh%fsh(1,:,:,:)*(xmax-xmin)+xmin
        mesh%fsv(1,:,:,:)=mesh%fsv(1,:,:,:)*(xmax-xmin)+xmin
        mesh%fsi(1,:,:,:)=mesh%fsi(1,:,:,:)*(xmax-xmin)+xmin
      ENDIF
    ENDIF
    IF (PRESENT(ymax).AND.PRESENT(ymin)) THEN
      mesh%fs(2,:,:)=mesh%fs(2,:,:)*(ymax-ymin)+ymin
      IF (ALLOCATED(mesh%fsh)) THEN
        mesh%fsh(2,:,:,:)=mesh%fsh(2,:,:,:)*(ymax-ymin)+ymin
        mesh%fsv(2,:,:,:)=mesh%fsv(2,:,:,:)*(ymax-ymin)+ymin
        mesh%fsi(2,:,:,:)=mesh%fsi(2,:,:,:)*(ymax-ymin)+ymin
      ENDIF
    ENDIF
  END SUBROUTINE setup_rect_2D

!-------------------------------------------------------------------------------
!* Transform r, theta coordinates to rz
!-------------------------------------------------------------------------------
  SUBROUTINE transform_rt_rz_2D(mesh,xo,yo)
    IMPLICIT NONE

    !> real nodal field containing RZ coordinates of the mesh
    CLASS(simple_rect_2D_mesh), INTENT(INOUT) :: mesh
    !> R0 offset
    REAL(r8), INTENT(IN) :: xo
    !> Z0 offset
    REAL(r8), INTENT(IN) :: yo

    INTEGER(i4) :: ix,iy,is,ii

    DO ix=0,mesh%mx
      DO iy=0,mesh%my
        mesh%fs(:,ix,iy)=[xo+mesh%fs(1,ix,iy)*COS(mesh%fs(2,ix,iy)),            &
                          yo+mesh%fs(1,ix,iy)*SIN(mesh%fs(2,ix,iy))]
      ENDDO
    ENDDO
    IF (ALLOCATED(mesh%fsh)) THEN
      DO ix=1,mesh%mx
        DO iy=0,mesh%my
          DO is=1,mesh%n_side
            mesh%fsh(:,is,ix,iy)=                                               &
              [xo+mesh%fsh(1,is,ix,iy)*COS(mesh%fsh(2,is,ix,iy)),               &
               yo+mesh%fsh(1,is,ix,iy)*SIN(mesh%fsh(2,is,ix,iy))]
          ENDDO
        ENDDO
      ENDDO
      DO ix=0,mesh%mx
        DO iy=1,mesh%my
          DO is=1,mesh%n_side
            mesh%fsv(:,is,ix,iy)=                                               &
              [xo+mesh%fsv(1,is,ix,iy)*COS(mesh%fsv(2,is,ix,iy)),               &
               yo+mesh%fsv(1,is,ix,iy)*SIN(mesh%fsv(2,is,ix,iy))]
          ENDDO
        ENDDO
      ENDDO
      DO ix=1,mesh%mx
        DO iy=1,mesh%my
          DO ii=1,mesh%n_int
            mesh%fsi(:,ii,ix,iy)=                                               &
              [xo+mesh%fsi(1,ii,ix,iy)*COS(mesh%fsi(2,ii,ix,iy)),               &
               yo+mesh%fsi(1,ii,ix,iy)*SIN(mesh%fsi(2,ii,ix,iy))]
          ENDDO
        ENDDO
      ENDDO
    ENDIF
  END SUBROUTINE transform_rt_rz_2D

!-------------------------------------------------------------------------------
!* Compute node locations in leaf blocks in unstructured polar mesh
!-------------------------------------------------------------------------------
  SUBROUTINE setup_leaf_cyl_2D(mesh,nrbl,ntbl,irbl,itbl,xcore,ycore,mxc,myc,    &
                               scale,xo,yo)
    IMPLICIT NONE

    !> real nodal field containing RZ coordinates of the mesh
    CLASS(simple_rect_2D_mesh), INTENT(INOUT) :: mesh
    !> number of leaf blocks in the radial direction
    INTEGER(i4), INTENT(IN) :: nrbl
    !> number of leaf blocks in the theta direction
    INTEGER(i4), INTENT(IN) :: ntbl
    !> radial index of leaf block
    INTEGER(i4), INTENT(IN) :: irbl
    !> theta index of leaf block
    INTEGER(i4), INTENT(IN) :: itbl
    !> core block normalized x extent (0 to 1)
    REAL(r8), INTENT(IN) :: xcore
    !> core block normalized y extent (0 to 1)
    REAL(r8), INTENT(IN) :: ycore
    !> number of horizontal (x-dir) elements in the core blocks
    INTEGER(i4), INTENT(IN) :: mxc
    !> number of vertical (y-dir) elements in the core blocks
    INTEGER(i4), INTENT(IN) :: myc
    !> scale factor (minor radius) to stretch mesh
    REAL(r8), INTENT(IN) :: scale
    !> R0 offset
    REAL(r8), INTENT(IN) :: xo
    !> Z0 offset
    REAL(r8), INTENT(IN) :: yo

    INTEGER(i4) :: mr,mt,mt0,mttot,ttheta
    INTEGER(i4) :: itheta,ir,ix,iy,j,k,is
    REAL(r8), DIMENSION(0:mesh%pd) :: x,y
    REAL(r8) :: xfac,yfac,rtheta
    REAL(r8) :: zeta,ns_dtheta,ew_dtheta,tzeta
    REAL(r8) :: startx,starty,endx,endy,scale_r1,scale_r2,delta

!-------------------------------------------------------------------------------
!   Compute node spacing along sides of inner "core" block
!-------------------------------------------------------------------------------
    mr=mesh%mx
    mt=mesh%my
    xfac=2.0_r8*xcore/REAL(mxc,r8)
    yfac=2.0_r8*ycore/REAL(myc,r8)
!-------------------------------------------------------------------------------
!   Compute node spacing along outer radius
!   Want equal spacing in theta along N,S,E,W arcs
!   Arcs defined by straight ray from corner or side of center block
!-------------------------------------------------------------------------------
    zeta=atan2(ycore,xcore)
    ew_dtheta=2_r8*zeta/myc
    ns_dtheta=(pi-2_r8*zeta)/mxc
    mt0=(itbl-1)*mt
    mttot=2*(mxc+myc)
    DO itheta=0, mesh%my
      ttheta=itheta + mt0
      IF (ttheta < mxc) THEN
        ! south
        startx=-xcore+xfac*REAL(ttheta,r8)
        starty=-ycore
        tzeta=pi+zeta+ttheta*ns_dtheta
      ELSE IF (ttheta < mxc + myc) THEN
        ! east
        startx=xcore
        starty=-ycore+yfac*REAL(ttheta-mxc)
        tzeta=2_r8*pi-zeta+(ttheta-mxc)*ew_dtheta
      ELSE IF (ttheta < 2 * mxc + myc) THEN
        ! north
        startx=xcore-xfac*REAL(ttheta-mxc-myc,r8)
        starty=ycore
        tzeta=zeta+(ttheta-mxc-myc)*ns_dtheta
      ELSE
        ! west
        startx=-xcore
        starty=ycore-yfac*REAL(ttheta-2*mxc-myc,r8)
        tzeta=pi-zeta+(ttheta-2*mxc-myc)*ew_dtheta
      ENDIF
      endx=COS(tzeta)
      endy=SIN(tzeta)
      scale_r1=REAL(irbl-1,r8)/REAL(nrbl,r8)
      scale_r2=REAL(irbl,r8)/REAL(nrbl,r8)
      delta=endx-startx
      endx=startx+delta*scale_r2
      startx=startx+delta*scale_r1
      delta=endy-starty
      endy=starty+delta*scale_r2
      starty=starty+delta*scale_r1
      mesh%fs(:,0,itheta)=[startx,starty]
      mesh%fs(:,mr,itheta)=[endx,endy]
      DO ir=1,mesh%mx-1
        mesh%fs(1,ir,itheta)=startx+(endx-startx)*REAL(ir,r8)/REAL(mr,r8)
        mesh%fs(2,ir,itheta)=starty+(endy-starty)*REAL(ir,r8)/REAL(mr,r8)
      ENDDO
    ENDDO
    IF (ALLOCATED(mesh%fsh)) THEN
      DO ix=1, mesh%mx
        DO iy=0, mesh%my
          CALL mesh%ll%getnodes(x,mesh%fs(1,ix-1,iy),mesh%fs(1,ix,iy))
          CALL mesh%ll%getnodes(y,mesh%fs(2,ix-1,iy),mesh%fs(2,ix,iy))
          mesh%fsh(1,:,ix,iy)=x(1:mesh%n_side)
          mesh%fsh(2,:,ix,iy)=y(1:mesh%n_side)
        ENDDO
      ENDDO
!-------------------------------------------------------------------------------
!     Add vertical nodes that conform to theta arcs
!-------------------------------------------------------------------------------
      CALL mesh%ll%getnodes(x,0._r8,1._r8)
      DO itheta=1, mesh%my
        DO is=1,mesh%n_side
          ttheta=itheta - 1 + mt0
          rtheta=REAL(ttheta,r8)+x(is)
          IF (ttheta < mxc) THEN
            ! south
            startx=-xcore+xfac*rtheta
            starty=-ycore
            tzeta=pi+zeta+rtheta*ns_dtheta
          ELSE IF (ttheta < mxc + myc) THEN
            ! east
            startx=xcore
            starty=-ycore+yfac*REAL(rtheta-mxc)
            tzeta=2_r8*pi-zeta+(rtheta-mxc)*ew_dtheta
          ELSE IF (ttheta < 2 * mxc + myc) THEN
            ! north
            startx=xcore-xfac*REAL(rtheta-mxc-myc,r8)
            starty=ycore
            tzeta=zeta+(rtheta-mxc-myc)*ns_dtheta
          ELSE
            ! west
            startx=-xcore
            starty=ycore-yfac*REAL(rtheta-2*mxc-myc,r8)
            tzeta=pi-zeta+(rtheta-2*mxc-myc)*ew_dtheta
          ENDIF
          endx=COS(tzeta)
          endy=SIN(tzeta)
          scale_r1=REAL(irbl-1,r8)/REAL(nrbl,r8)
          scale_r2=REAL(irbl,r8)/REAL(nrbl,r8)
          delta=endx-startx
          endx=startx+delta*scale_r2
          startx=startx+delta*scale_r1
          delta=endy-starty
          endy=starty+delta*scale_r2
          starty=starty+delta*scale_r1
          mesh%fsv(:,is,0,itheta)=[startx,starty]
          mesh%fsv(:,is,mr,itheta)=[endx,endy]
          DO ir=1,mesh%mx-1
            mesh%fsv(1,is,ir,itheta)=startx+(endx-startx)*REAL(ir,r8)/REAL(mr,r8)
            mesh%fsv(2,is,ir,itheta)=starty+(endy-starty)*REAL(ir,r8)/REAL(mr,r8)
          ENDDO
        ENDDO
      ENDDO
      DO ix=1, mesh%mx
        DO iy=1, mesh%my
          CALL mesh%ll%getnodes(x,mesh%fs(1,ix-1,iy-1),mesh%fs(1,ix,iy))
          CALL mesh%ll%getnodes(y,mesh%fs(2,ix-1,iy-1),mesh%fs(2,ix,iy))
          DO j=1,mesh%n_side
            k=1+(j-1)*mesh%n_side
            CALL mesh%ll%getnodes(x,mesh%fsv(1,j,ix-1,iy),mesh%fsv(1,j,ix,iy))
            CALL mesh%ll%getnodes(y,mesh%fsv(2,j,ix-1,iy),mesh%fsv(2,j,ix,iy))
            mesh%fsi(1,k:k+mesh%n_side-1,ix,iy)=x(1:mesh%n_side)
            mesh%fsi(2,k:k+mesh%n_side-1,ix,iy)=y(1:mesh%n_side)
          ENDDO
        ENDDO
      ENDDO
    END IF
    mesh%fs(:,:,:)=mesh%fs(:,:,:)*scale
    mesh%fs(1,:,:)=mesh%fs(1,:,:)+xo
    mesh%fs(2,:,:)=mesh%fs(2,:,:)+yo
    IF (ALLOCATED(mesh%fsh)) THEN
      mesh%fsh(:,:,:,:)=mesh%fsh(:,:,:,:)*scale
      mesh%fsh(1,:,:,:)=mesh%fsh(1,:,:,:)+xo
      mesh%fsh(2,:,:,:)=mesh%fsh(2,:,:,:)+yo
      mesh%fsv(:,:,:,:)=mesh%fsv(:,:,:,:)*scale
      mesh%fsv(1,:,:,:)=mesh%fsv(1,:,:,:)+xo
      mesh%fsv(2,:,:,:)=mesh%fsv(2,:,:,:)+yo
      mesh%fsi(:,:,:,:)=mesh%fsi(:,:,:,:)*scale
      mesh%fsi(1,:,:,:)=mesh%fsi(1,:,:,:)+xo
      mesh%fsi(2,:,:,:)=mesh%fsi(2,:,:,:)+yo
    ENDIF
  END SUBROUTINE setup_leaf_cyl_2D

END MODULE simple_mesh_mod
