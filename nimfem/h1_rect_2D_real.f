!-------------------------------------------------------------------------------
!< Routines for evaluating continuous Lagrange finite elements on blocks
!  of structured quadrilaterals.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the types for continuous Lagrange finite elements on blocks
!  of structured quadrilaterals and their associated procedures.
!-------------------------------------------------------------------------------
MODULE h1_rect_2D_real_mod
  USE function_mod, ONLY: abstract_function_real
  USE local
  USE nodal_mod
  USE pardata_mod
  USE poly_mod, ONLY: lobleg
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: h1_rect_2D_real

  CHARACTER(*), PARAMETER :: mod_name='h1_rect_2D_real'
!-------------------------------------------------------------------------------
!> Type for structured blocks of Lagrangian quadrilateral real finite
!  elements.
!
!  @note
!  See the abstract base type [[nodal_field_real(type)]] for
!  type-bound procedure interface details.
!  @endnote
!
!  Basis starting indices and centering in logical space are contained
!  in ix0 and iy0. Note that the centering are for standard lagrange
!  polynomial bases.
!
!  Grid-vertex data has grid indices running fs(1:nqty,0:mx,0:my), so
!  ix0=0 and iy0=0.
!
!  Horizontal side data has grid indices running
!  fsh(1:nqty,1:mx,0:my,1:n_side), so ix0=1 and iy0=0.
!
!  Vertical side data has grid indices running
!  fsv(1:nqty,0:mx,1:my,1:n_side), so ix0=0 and iy0=1.
!
!  Interior side data has grid indices running
!  fsi(1:nqty,1:mx,1:my,1:n_int), so ix0=1 and iy0=1.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(nodal_field_real) :: h1_rect_2D_real
    !> number of elements within this block in the logical x direction
    INTEGER(i4) :: mx=0
    !> number of elements within this block in the logical y direction
    INTEGER(i4) :: my=0
    !> number of degrees of freedom per element in each side basis
    !  function nodal array
    INTEGER(i4) :: n_side=0
    !> number of degrees of freedom per element in each interior basis
    !  function nodal array
    INTEGER(i4) :: n_int=0
    !> basis starting indices and centering in logical space.
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ix0,iy0
    !> the relative positions within a cell for the different bases.
    REAL(r8), DIMENSION(:), ALLOCATABLE :: dx,dy
    !> Grid vertex basis function nodal array
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: fs
    !> Grid horizontal (x-dir) side basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsh
    !> Grid vertical (y-dir) side basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsv
    !> Grid interior basis function nodal array
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: fsi
    !> Locally used polynomial evaluator
    TYPE(lobleg) :: ll
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(field) :: alloc => alloc_real_2D
    ! Abstract class deferred functions
    PROCEDURE, PASS(field) :: dealloc => dealloc_real_2D
    PROCEDURE, PASS(field) :: alloc_with_mold => alloc_with_mold_real_2D
    PROCEDURE, PASS(field) :: assign_field => assign_field_real_2D
    PROCEDURE, PASS(field) :: init_basis_ftn => init_basis_ftn_real_2D
    PROCEDURE, PASS(field) :: init_block_basis_ftn =>                           &
                                init_block_basis_ftn_real_2D
    PROCEDURE, PASS(field) :: eval_val => eval_val_real_2D
    PROCEDURE, PASS(field) :: eval_many => eval_many_real_2D
    PROCEDURE, PASS(field) :: collocation => collocation_real_2D
    PROCEDURE, PASS(field) :: qp_update => qp_update_real_2D
    PROCEDURE, PASS(field) :: get_logical => get_logical_real_2D
    PROCEDURE, PASS(field) :: set_field => set_field_real_2D
    PROCEDURE, PASS(field) :: get_field => get_field_real_2D
    PROCEDURE, PASS(field) :: h5_read => h5_read_real_2D
    PROCEDURE, PASS(field) :: h5_dump => h5_dump_real_2D
    PROCEDURE, PASS(field) :: bcast => bcast_real_2D
    ! Private functions
    PROCEDURE, PASS(field), PRIVATE :: get_element_dofs
    PROCEDURE, PASS(field), PRIVATE :: h1_rect_2D_bases_val
    PROCEDURE, PASS(field), PRIVATE :: h1_rect_2D_bases_many
    GENERIC, PRIVATE :: h1_rect_2D_bases => h1_rect_2D_bases_val,               &
                                           h1_rect_2D_bases_many
  END TYPE h1_rect_2D_real

CONTAINS

!-------------------------------------------------------------------------------
!* allocates space for h1_rect_2D_real.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real_2D(field,mx,my,nqty,poly_degree,id,name,title)
    IMPLICIT NONE

    !> field to allocate
    CLASS(h1_rect_2D_real), INTENT(OUT) :: field
    !> number of elements within this block in the logical x direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements within this block in the logical y direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities stored at DoF
    INTEGER(i4), INTENT(IN) :: nqty
    !> polynomial degree of field
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> ID for parallel streaming
    INTEGER(i4), INTENT(IN) :: id
    !> field name
    CHARACTER(*), INTENT(IN), OPTIONAL :: name
    !> component names
    CHARACTER(*), DIMENSION(:), INTENT(IN), OPTIONAL :: title

    INTEGER(i4) :: ix,iy,ib,ip,pd1
    REAL(r8), DIMENSION(0:poly_degree) :: x_node
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   throw an error if allocated
!-------------------------------------------------------------------------------
    IF (ALLOCATED(field%fs)) THEN
      CALL par%nim_stop("alloc_real_2D tried to reallocate "//TRIM(title(1)))
    ENDIF
!-------------------------------------------------------------------------------
!   store grid and vector dimensions, and set the
!   number of side and interior basis functions.
!-------------------------------------------------------------------------------
    field%mx=mx
    field%my=my
    field%nqty=nqty
    field%fqty=1
    field%dfqty=2
    field%nbasis=(poly_degree+1)**2
    field%n_side=poly_degree-1
    field%n_int=(poly_degree-1)**2
    field%nel=mx*my
    field%ndof=(mx+1)*(my+1)+field%n_side*(mx+1)*my                             &
               +field%n_side*mx*(my+1)+field%n_int*mx*my
    field%ndim=2
    field%pd=poly_degree
    field%id=id
    field%deriv_type="grad"
    field%field_type="h1_rect_2D_real"
    CALL field%ll%init(poly_degree)
    IF (PRESENT(name)) field%name=name
!-------------------------------------------------------------------------------
!   allocate space.
!-------------------------------------------------------------------------------
    ALLOCATE(field%fs(nqty,0:mx,0:my))
    IF (poly_degree>1) THEN
      ALLOCATE(field%fsh(nqty,field%n_side,1:mx,0:my))
      ALLOCATE(field%fsv(nqty,field%n_side,0:mx,1:my))
      ALLOCATE(field%fsi(nqty,field%n_int,1:mx,1:my))
    ENDIF
!-------------------------------------------------------------------------------
!   title if present in input.
!-------------------------------------------------------------------------------
    ALLOCATE(field%title(nqty))
    IF (PRESENT(title)) THEN
      IF (SIZE(title)==nqty) THEN
        field%title=title
      ELSE
        field%title=title(1)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   compute basis starting indices and centering in logical space.
!   note that the centering are for standard lagrange polynomial
!   bases.
!
!   grid-vertex data has grid indices running (0:mx,0:my), so
!   ix0=0 and iy0=0.
!   horizontal side data has grid indices running (1:mx,0:my), so
!   ix0=1 and iy0=0.
!   vertical side data has grid indices running (0:mx,1:my), so
!   ix0=0 and iy0=1.
!   interior data has grid indices running (1:mx,1:my), so
!   ix0=1 and iy0=1.
!-------------------------------------------------------------------------------
    pd1=poly_degree-1
    ALLOCATE(field%ix0(poly_degree**2))
    ALLOCATE(field%iy0(poly_degree**2))
    ALLOCATE(field%dx(poly_degree**2))
    ALLOCATE(field%dy(poly_degree**2))
    CALL field%ll%getnodes(x_node)
    DO ib=1,poly_degree**2
      IF (ib==1) THEN
        field%ix0(ib)=0
        field%iy0(ib)=0
        field%dx(ib)=0
        field%dy(ib)=0
      ELSE IF (ib<=poly_degree) THEN
        field%ix0(ib)=1
        field%iy0(ib)=0
        field%dx(ib)=x_node(ib-1_i4)
        field%dy(ib)=0
      ELSE IF (ib<2*poly_degree) THEN
        field%ix0(ib)=0
        field%iy0(ib)=1
        field%dx(ib)=0
        field%dy(ib)=x_node(ib-poly_degree)
      ELSE
        field%ix0(ib)=1
        field%iy0(ib)=1
        ip=ib-2*poly_degree
        ix=MODULO(ip,pd1)+1
        iy=ip/pd1+1
        field%dx(ib)=x_node(ix)
        field%dy(ib)=x_node(iy)
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(field%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      CHARACTER(16) :: obj_name
      sz= INT(SIZEOF(field%fs)+SIZEOF(field%fsh)+SIZEOF(field%fsv)              &
             +SIZEOF(field%fsi)+SIZEOF(field%ix0)+SIZEOF(field%iy0)             &
             +SIZEOF(field%dx)+SIZEOF(field%dy)                                 &
             +SIZEOF(field)+SIZEOF(field%title),i4)
      obj_name='unknown'
      IF (PRESENT(name)) obj_name=name
      CALL memlogger%update(field%mem_id,mod_name,obj_name,sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real_2D

!-------------------------------------------------------------------------------
!* deallocate h1_rect_2D_real.
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real_2D(field)
    IMPLICIT NONE

    !> field to dealloc
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   deallocate space.
!-------------------------------------------------------------------------------
    IF (ALLOCATED(field%fs))  DEALLOCATE(field%fs)
    IF (ALLOCATED(field%fsh)) DEALLOCATE(field%fsh)
    IF (ALLOCATED(field%fsv)) DEALLOCATE(field%fsv)
    IF (ALLOCATED(field%fsi)) DEALLOCATE(field%fsi)
    IF (ALLOCATED(field%ix0)) DEALLOCATE(field%ix0)
    IF (ALLOCATED(field%iy0)) DEALLOCATE(field%iy0)
    IF (ALLOCATED(field%dx)) DEALLOCATE(field%dx)
    IF (ALLOCATED(field%dy)) DEALLOCATE(field%dy)
    IF (ALLOCATED(field%title)) DEALLOCATE(field%title)
    IF (ALLOCATED(field%qa%alf)) DEALLOCATE(field%qa%alf)
    IF (ALLOCATED(field%qa%aldf)) DEALLOCATE(field%qa%aldf)
    IF (ALLOCATED(field%qab%alf)) DEALLOCATE(field%qab%alf)
    IF (ALLOCATED(field%qab%aldf)) DEALLOCATE(field%qab%aldf)
    CALL field%ll%dealloc
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(field%mem_id,mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real_2D

!-------------------------------------------------------------------------------
!* Create a new nodal field of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_real_2D(field,new_field,nqty,pd)
    IMPLICIT NONE

    !> reference field to mold
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> new field to be created
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(OUT) :: new_field
    !> number of quantities,field%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree,field%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_real_2D',iftn,idepth)
    new_nqty=field%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=field%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(h1_rect_2D_real::new_field)
    SELECT TYPE (new_field)
    TYPE IS (h1_rect_2D_real)
      CALL new_field%alloc(field%mx,field%my,new_nqty,new_pd,field%id,          &
                           field%name)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_real_2D

!-------------------------------------------------------------------------------
!* Assign nodal_field_real to a nodal_field_real structure
!-------------------------------------------------------------------------------
  SUBROUTINE assign_field_real_2D(field,field2)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> field value to assign
    CLASS(nodal_field_real), INTENT(IN) :: field2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_field_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   removing the select type would be ideal
!-------------------------------------------------------------------------------
    SELECT TYPE (field2)
    TYPE IS (h1_rect_2D_real)
#ifdef DEBUG
      IF (field%nqty /= field2%nqty .OR. field%mx   /= field2%mx   .OR.         &
          field%my   /= field2%my   .OR. field%pd   /= field2%pd   .OR.         &
          field%ndim /= field2%ndim) THEN
        CALL par%nim_stop('Incompatible fields in assign_field_real_2D')
      ENDIF
#endif
      field%fs=field2%fs
      IF (ALLOCATED(field%fsh)) THEN
        field%fsh=field2%fsh
        field%fsv=field2%fsv
        field%fsi=field2%fsi
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected h1_rect_2D_real for field2'//                 &
                        ' in assign_field_real_2D')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_field_real_2D

!-------------------------------------------------------------------------------
!> Initialize the basis function alpha. This value of the basis fuction
!  and it's derivatives are calculated and stored at the quadrature points
!-------------------------------------------------------------------------------
  SUBROUTINE init_basis_ftn_real_2D(field,xg_vec)
    USE quadrature_mod
    IMPLICIT NONE

    !> field on which to operate
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> coordinates of the quadrature points (ng,ndim)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xg_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_basis_ftn_real_2D',iftn,idepth)
    CALL field%h1_rect_2D_bases(xg_vec,dorder=1,alpha=field%qa)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_basis_ftn_real_2D

!-------------------------------------------------------------------------------
!> Initialize the basis function alpha over full block for field including
!  a transformation from logical to coordinate space.
!-------------------------------------------------------------------------------
  SUBROUTINE init_block_basis_ftn_real_2D(field,jac,ng)
    USE quadrature_mod
    IMPLICIT NONE

    !> field to compute basis function for
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> the Jacobian information
    TYPE(qjac_real), INTENT(IN) :: jac
    !> number of gaussian quadrature points per element
    INTEGER(i4), INTENT(IN) :: ng

    INTEGER(i4) :: nel,nbasis,iel,ib,ig
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init_block_basis_ftn_real_2D',          &
                              iftn,idepth)
    nel=field%nel
    nbasis=field%nbasis
    ALLOCATE(field%qab%alf(ng,nbasis,nel,field%fqty),                           &
             field%qab%aldf(ng,nbasis,nel,field%dfqty))
!-------------------------------------------------------------------------------
!   The coordinate transformation is trivial for the alpha so the reference
!   element evaluations can be copied out. For the derivative, this is the
!   canonical differential operation to perform on the H1 space. The coordinate
!   transformation uses the reciprocal basis vectors of the logical coordinates
!   in the orthonormal coordinates to get the components.
!-------------------------------------------------------------------------------
    DO iel=1,field%nel
      field%qab%alf(:,:,iel,1)=field%qa%alf(:,:)
      DO ib=1,field%nbasis
        DO ig=1,ng
          field%qab%aldf(ig,ib,iel,:)=                                          &
            MATMUL(jac%ijac(ig,iel,:,:),field%qa%aldf(ig,ib,:))
        ENDDO
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init_block_basis_ftn_real_2D

!-------------------------------------------------------------------------------
!> Evaluate the field at an arbitrary point.
!-------------------------------------------------------------------------------
  RECURSIVE SUBROUTINE eval_val_real_2D(field,xi,iel,out_val,dorder,transform,  &
                                        coord)
    USE quadrature_mod, ONLY: interp_real,alpha_real
    IMPLICIT NONE

    !> field that is being interpolated
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> logical coordinate within the element where interpolation occurs
    REAL(r8), DIMENSION(:), INTENT(IN) :: xi
    !> block element number in which interpolation is being done
    INTEGER(i4), INTENT(IN) :: iel
    !> interpolated output value; this is assumed to be allocated
    TYPE(interp_real), INTENT(INOUT) :: out_val
    !> order to which to evaluate derivatives of the field
    INTEGER(i4), INTENT(IN) :: dorder
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> coordinate field
    CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord

    TYPE(alpha_real) :: al
    TYPE(interp_real) :: rz
    REAL(r8) :: f_el(field%nbasis,field%nqty)
    REAL(r8), DIMENSION(2,2) :: ijac
    REAL(r8) :: detj
    INTEGER(i4) :: ib,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'eval_val_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the output dorder
!-------------------------------------------------------------------------------
    out_val%dorder=dorder
!-------------------------------------------------------------------------------
!   gather the alpha values and the element-wise degrees of freedom
!-------------------------------------------------------------------------------
    CALL field%h1_rect_2D_bases(xi,dorder,al)
    CALL field%get_element_dofs(iel,f_el)
!-------------------------------------------------------------------------------
!   sum over bases
!-------------------------------------------------------------------------------
    out_val%f=0._r8
    DO ib=1,field%nbasis
      out_val%f=out_val%f+al%alf(ib)*f_el(ib,:)
    ENDDO
    IF (dorder > 0) THEN
      out_val%df=0._r8
      IF (transform) THEN
        ALLOCATE(rz%f(2),rz%df(4))
        CALL coord%eval(xi,iel,rz,dorder=1,transform=.FALSE.)
        detj=rz%df(1)*rz%df(4)-rz%df(3)*rz%df(2)
        ijac=RESHAPE([rz%df(4),-rz%df(3),-rz%df(2),rz%df(1)]/detj,[2,2])
        DO ib=1,field%nbasis
          DO iq=1,field%nqty
            out_val%df(2*iq-1:2*iq)=out_val%df(2*iq-1:2*iq)                     &
                                   +MATMUL(ijac,al%aldf(ib,:))*f_el(ib,iq)
          ENDDO
        ENDDO
      ELSE
        DO ib=1,field%nbasis
          DO iq=1,field%nqty
            out_val%df(2*iq-1:2*iq)=out_val%df(2*iq-1:2*iq)                     &
                                    +al%aldf(ib,:)*f_el(ib,iq)
          ENDDO
        ENDDO
      ENDIF
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE eval_val_real_2D

!-------------------------------------------------------------------------------
!>  Evaluate the field at a collection of arbitrary points in a single element.
!-------------------------------------------------------------------------------
  RECURSIVE SUBROUTINE eval_many_real_2D(field,xi,iel,out_many,dorder,          &
                                         transform,coord)
    USE quadrature_mod, ONLY: batch_interp_real,qalpha_real
    IMPLICIT NONE

    !> field that is being interpolated
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> logical coordinates within the element where interpolation occurs
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
    !> block element number in which interpolation is being done
    INTEGER(i4), INTENT(IN) :: iel
    !> interpolated output value (assumed to be allocated)
    TYPE(batch_interp_real), INTENT(INOUT) :: out_many
    !> order to which to evaluate derivatives of the field
    INTEGER(i4), INTENT(IN) :: dorder
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> Jacobian information at xi in iel
    CLASS(nodal_field_real), OPTIONAL, INTENT(IN) :: coord

    TYPE(qalpha_real) :: al
    TYPE(batch_interp_real) :: rz
    REAL(r8), DIMENSION(field%nbasis,field%nqty) :: f_el
    REAL(r8), DIMENSION(SIZE(out_many%f,1),2,2) :: ijac
    REAL(r8), DIMENSION(SIZE(out_many%f,1)) :: detj
    INTEGER(i4) :: npoints,ib,iq,ip
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'eval_many_real_2D',iftn,idepth)
    npoints=SIZE(out_many%f,1)
!-------------------------------------------------------------------------------
!   set the output dorder
!-------------------------------------------------------------------------------
    out_many%dorder=dorder
!-------------------------------------------------------------------------------
!   gather the alpha values and the element-wise degrees of freedom
!-------------------------------------------------------------------------------
    CALL field%h1_rect_2D_bases(xi,dorder,al)
    CALL field%get_element_dofs(iel,f_el)
!-------------------------------------------------------------------------------
!   sum over bases
!-------------------------------------------------------------------------------
    out_many%f=0._r8
    DO ib=1,field%nbasis
      DO ip=1,npoints
        out_many%f(ip,:)=out_many%f(ip,:)+al%alf(ip,ib)*f_el(ib,:)
      ENDDO
    ENDDO
    IF (dorder > 0) THEN
      ALLOCATE(rz%f(npoints,2),rz%df(npoints,4))
      out_many%df=0._r8
      IF (transform) THEN
        CALL coord%eval(xi,iel,rz,dorder=1,transform=.FALSE.)
        detj=rz%df(:,1)*rz%df(:,4)-rz%df(:,3)*rz%df(:,2)
        ijac(:,1,1)=rz%df(:,4)/detj
        ijac(:,2,1)=-rz%df(:,3)/detj
        ijac(:,1,2)=-rz%df(:,2)/detj
        ijac(:,2,2)=rz%df(:,1)/detj
        DO ib=1,field%nbasis
          DO iq=1,field%nqty
            DO ip=1,npoints
              out_many%df(ip,2*iq-1:2*iq)=out_many%df(ip,2*iq-1:2*iq)           &
                            +MATMUL(ijac(ip,:,:),al%aldf(ip,ib,:))*f_el(ib,iq)
            ENDDO
          ENDDO
        ENDDO
      ELSE
        DO ib=1,field%nbasis
          DO iq=1,field%nqty
            DO ip=1,npoints
              out_many%df(ip,2*iq-1:2*iq)=out_many%df(ip,2*iq-1:2*iq)           &
                                          +al%aldf(ip,ib,:)*f_el(ib,iq)
            ENDDO
          ENDDO
        ENDDO
      ENDIF
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE eval_many_real_2D

!-------------------------------------------------------------------------------
!*  Complete the collocation of a function onto nodal_field_real
!-------------------------------------------------------------------------------
  SUBROUTINE collocation_real_2D(field,func,coord)
    USE quadrature_mod, ONLY: interp_real
    IMPLICIT NONE

    !> field being set as a collocation of the function
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> function to be projected onto the field
    CLASS(abstract_function_real), INTENT(IN) :: func
    !> coordinate field corresponding to the same block as this field
    CLASS(nodal_field_real), INTENT(IN) :: coord

    REAL(r8), DIMENSION(0:field%pd) :: ll_nodes
    TYPE(interp_real) :: rz
    INTEGER(i4) :: ix,iy,i,j
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'collocation_real_2D',iftn,idepth)
!-------------------------------------------------------------------------------
!   get the Labatto-Legendre node locations
!-------------------------------------------------------------------------------
    CALL field%ll%getnodes(ll_nodes)
!-------------------------------------------------------------------------------
!   allocate space for the spatial evaluation
!-------------------------------------------------------------------------------
    ALLOCATE(rz%f(2))
!-------------------------------------------------------------------------------
!   evaluate the bottom left vertex
!-------------------------------------------------------------------------------
    CALL coord%eval([ll_nodes(0),ll_nodes(0)],1,rz,dorder=0,transform=.FALSE.)
    CALL func%eval(rz%f,field%fs(:,0,0))
!-------------------------------------------------------------------------------
!   evaluate the bottom edge
!-------------------------------------------------------------------------------
    DO ix=1,field%mx
      DO i=1,field%n_side
        CALL coord%eval([ll_nodes(i),ll_nodes(0)],ix,rz,                        &
                        dorder=0,transform=.FALSE.)
        CALL func%eval(rz%f,field%fsh(:,i,ix,0))
      ENDDO
      CALL coord%eval([ll_nodes(field%pd),ll_nodes(0)],ix,rz,                   &
                      dorder=0,transform=.FALSE.)
      CALL func%eval(rz%f,field%fs(:,ix,0))
    ENDDO
!-------------------------------------------------------------------------------
!   evaluate the left edge
!-------------------------------------------------------------------------------
    DO iy=1,field%my
      DO j=1,field%n_side
        CALL coord%eval([ll_nodes(0),ll_nodes(j)],field%mx*(iy-1)+1,            &
                        rz,dorder=0,transform=.FALSE.)
        CALL func%eval(rz%f,field%fsv(:,j,0,iy))
      ENDDO
      CALL coord%eval([ll_nodes(0),ll_nodes(field%pd)],field%mx*(iy-1)+1,rz,    &
                      dorder=0,transform=.FALSE.)
      CALL func%eval(rz%f,field%fs(:,0,iy))
    ENDDO
!-------------------------------------------------------------------------------
!   loop through all elements
!-------------------------------------------------------------------------------
    DO ix=1,field%mx
      DO iy=1,field%my
!-------------------------------------------------------------------------------
!       interior points of each element
!-------------------------------------------------------------------------------
        DO j=1,field%n_side
          DO i=1,field%n_side
            CALL coord%eval([ll_nodes(i),ll_nodes(j)],field%mx*(iy-1)+ix,rz,    &
                            dorder=0,transform=.FALSE.)
            CALL func%eval(rz%f,field%fsi(:,field%n_side*(j-1)+i,ix,iy))
          ENDDO
!-------------------------------------------------------------------------------
!         right vertical edge of each element
!-------------------------------------------------------------------------------
          CALL coord%eval([ll_nodes(field%pd),ll_nodes(j)],field%mx*(iy-1)+ix,  &
                          rz,dorder=0,transform=.FALSE.)
          CALL func%eval(rz%f,field%fsv(:,j,ix,iy))
        ENDDO
!-------------------------------------------------------------------------------
!       top edge of each element
!-------------------------------------------------------------------------------
        DO i=1,field%n_side
          CALL coord%eval([ll_nodes(i),ll_nodes(field%pd)],field%mx*(iy-1)+ix,  &
                          rz,dorder=0,transform=.FALSE.)
          CALL func%eval(rz%f,field%fsh(:,i,ix,iy))
        ENDDO
!-------------------------------------------------------------------------------
!       upper right corner of each element
!-------------------------------------------------------------------------------
        CALL coord%eval([ll_nodes(field%pd),ll_nodes(field%pd)],                &
                        field%mx*(iy-1)+ix,rz,dorder=0,transform=.FALSE.)
        CALL func%eval(rz%f,field%fs(:,ix,iy))
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE collocation_real_2D

!-------------------------------------------------------------------------------
!> Updates quadrature point data for the H1 field.
!
!  If the `metric` is provided then the quadrature point data is evaluated in
!  the orthogonal coordinate system basis. Otherwise the quadrature point data
!  is evaluated the logical coordinate basis of the reference element.
!-------------------------------------------------------------------------------
  SUBROUTINE qp_update_real_2D(field,qpout,transform,metric)
    USE quadrature_mod, ONLY: qp_real,qjac_real
    IMPLICIT NONE

    !> field that is updating quadrature data
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> quadrature data being updated
    TYPE(qp_real), INTENT(INOUT) :: qpout
    !> flag to indicate transformation from logical to coordinate space
    LOGICAL, INTENT(IN) :: transform
    !> array of jacobian information within each element
    TYPE(qjac_real), OPTIONAL, INTENT(IN) :: metric

    INTEGER(i4) :: ng,iel,iq,ig,ii,is,ix,iy,base_st0,base_st1
    REAL(r8) :: fsall(field%nbasis,field%nqty,field%nel)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'qp_update_real_2D',iftn,idepth)
    ng=SIZE(qpout%qpf,1)
!-------------------------------------------------------------------------------
!   gather block data into a unified basis format in fsall
!-------------------------------------------------------------------------------
    ASSOCIATE(fs=>field%fs,fsh=>field%fsh,fsv=>field%fsv,fsi=>field%fsi,        &
              qpf=>qpout%qpf,qpdf=>qpout%qpdf,ijac=>metric%ijac,                &
              alf=>field%qa%alf,aldf=>field%qa%aldf)
      DO iel=1,field%nel
        iy=(iel-1)/field%mx+1
        ix=MODULO(iel-1,field%mx)+1
        DO iq=1,field%nqty
!-------------------------------------------------------------------------------
!         First gather the vertex centered dofs
!-------------------------------------------------------------------------------
          fsall(1,iq,iel)=fs(iq,ix-1,iy-1)
          fsall(2,iq,iel)=fs(iq,ix,iy-1)
          fsall(3,iq,iel)=fs(iq,ix-1,iy)
          fsall(4,iq,iel)=fs(iq,ix,iy)
!-------------------------------------------------------------------------------
!         Horizontal side centered dofs are next
!-------------------------------------------------------------------------------
          base_st0=3
          base_st1=4
          DO is=1,field%n_side
            fsall(base_st0+2*is,iq,iel)=fsh(iq,is,ix,iy-1)
            fsall(base_st1+2*is,iq,iel)=fsh(iq,is,ix,iy)
          ENDDO
!-------------------------------------------------------------------------------
!         Vertical side centered dofs are next
!-------------------------------------------------------------------------------
          base_st0=3+2*field%n_side
          base_st1=4+2*field%n_side
          DO is=1,field%n_side
            fsall(base_st0+2*is,iq,iel)=fsv(iq,is,ix-1,iy)
            fsall(base_st1+2*is,iq,iel)=fsv(iq,is,ix,iy)
          ENDDO
!-------------------------------------------------------------------------------
!         Interior centered dofs are last
!-------------------------------------------------------------------------------
          base_st0=4+4*field%n_side
          DO ii=1,field%n_int
            fsall(base_st0+ii,iq,iel)=fsi(iq,ii,ix,iy)
          ENDDO
        ENDDO
      ENDDO
      IF (transform) THEN
        DO iel=1,field%nel
          DO iq=1,field%nqty
            DO ig=1,ng
              qpf(ig,iel,1,iq)=SUM(alf(ig,:)*fsall(:,iq,iel))
              qpdf(ig,iel,1,iq)=SUM((ijac(ig,iel,1,1)*aldf(ig,:,1)              &
                                    +ijac(ig,iel,1,2)*aldf(ig,:,2))             &
                                                     *fsall(:,iq,iel))
              qpdf(ig,iel,2,iq)=SUM((ijac(ig,iel,2,1)*aldf(ig,:,1)              &
                                    +ijac(ig,iel,2,2)*aldf(ig,:,2))             &
                                                     *fsall(:,iq,iel))
            ENDDO
          ENDDO
        ENDDO
      ELSE
        DO iel=1,field%nel
          DO iq=1,field%nqty
            DO ig=1,ng
              qpf(ig,iel,1,iq)=SUM(alf(ig,:)*fsall(:,iq,iel))
              qpdf(ig,iel,1,iq)=SUM(aldf(ig,:,1)*fsall(:,iq,iel))
              qpdf(ig,iel,2,iq)=SUM(aldf(ig,:,2)*fsall(:,iq,iel))
            ENDDO
          ENDDO
        ENDDO
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE qp_update_real_2D

!-------------------------------------------------------------------------------
!> get the logical coordinates associated with this element as a flat
!  array. As these coordinates are not needed during routine finite
!  element operations, they are generated on the fly.
!-------------------------------------------------------------------------------
  SUBROUTINE get_logical_real_2D(field,lcoord)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> logical coordinate array,lcood(ndim,ndof)
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: lcoord

    INTEGER(i4) :: idof,ibase,iy,ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_logical_real_2D',iftn,idepth)
    idof=1
    DO ibase=1,SIZE(field%ix0)
      DO iy=field%iy0(ibase),field%my
        DO ix=field%ix0(ibase),field%mx
          lcoord(idof,1)=ix-field%ix0(ibase)+field%dx(ibase)
          lcoord(idof,2)=iy-field%iy0(ibase)+field%dy(ibase)
          idof=idof+1
        ENDDO
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_logical_real_2D

!-------------------------------------------------------------------------------
!> Set the field value with an array ordered as given by the return
!  value of `get_logical_real_2D`
!-------------------------------------------------------------------------------
  SUBROUTINE set_field_real_2D(field,fvalue)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> field value,fvalue(nqty,ndof)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: fvalue

    INTEGER(i4) :: idof,ibase,ibasis,iy,ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_field_real_2D',iftn,idepth)
    idof=1
    ibase=1
    DO iy=field%iy0(ibase),field%my
      DO ix=field%ix0(ibase),field%mx
        field%fs(:,ix,iy)=fvalue(:,idof)
        idof=idof+1
      ENDDO
    ENDDO
    IF (ALLOCATED(field%fsh)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_side
        DO iy=0,field%my
          DO ix=1,field%mx
            field%fsh(:,ibasis,ix,iy)=fvalue(:,idof)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    IF (ALLOCATED(field%fsv)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_side
        DO iy=1,field%my
          DO ix=0,field%mx
            field%fsv(:,ibasis,ix,iy)=fvalue(:,idof)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    IF (ALLOCATED(field%fsi)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_int
        DO iy=1,field%my
          DO ix=1,field%mx
            field%fsi(:,ibasis,ix,iy)=fvalue(:,idof)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_field_real_2D

!-------------------------------------------------------------------------------
!> Get the field value with an array ordered as given by the return
!  value of `get_logical_real_2D`
!-------------------------------------------------------------------------------
  SUBROUTINE get_field_real_2D(field,fvalue)
    IMPLICIT NONE

    !> field on which to operate
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> field value,fvalue(nqty,ndof)
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: fvalue

    INTEGER(i4) :: idof,ibase,ibasis,iy,ix
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_field_real_2D',iftn,idepth)
    idof=1
    ibase=1
    DO iy=field%iy0(ibase),field%my
      DO ix=field%ix0(ibase),field%mx
        fvalue(:,idof)=field%fs(:,ix,iy)
        idof=idof+1
      ENDDO
    ENDDO
    IF (ALLOCATED(field%fsh)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_side
        DO iy=0,field%my
          DO ix=1,field%mx
            fvalue(:,idof)=field%fsh(:,ibasis,ix,iy)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    IF (ALLOCATED(field%fsv)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_side
        DO iy=1,field%my
          DO ix=0,field%mx
            fvalue(:,idof)=field%fsv(:,ibasis,ix,iy)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    IF (ALLOCATED(field%fsi)) THEN
      ibase=ibase+1
      DO ibasis=1,field%n_int
        DO iy=1,field%my
          DO ix=1,field%mx
            fvalue(:,idof)=field%fsi(:,ibasis,ix,iy)
            idof=idof+1
          ENDDO
        ENDDO
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_field_real_2D

!-------------------------------------------------------------------------------
!> read field data from h5 file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read_real_2D(field,fname,gid,id,cid)
    USE io
    IMPLICIT NONE

    !> field to read
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> field name
    CHARACTER(*), INTENT(IN) :: fname
    !> hdf5 group ID
    INTEGER(HID_T), INTENT(IN) :: gid
    !> block id
    INTEGER(i4), INTENT(IN) :: id
    !> block id (character format)
    CHARACTER(*), INTENT(IN) :: cid

    INTEGER(i4) :: dn,nx,ny,iq,is,ins,ine,ii,nqty,mx,my,pd
    REAL(r8), ALLOCATABLE :: tarr(:,:,:),ttarr(:,:,:)
    INTEGER(HID_T) :: dsetid
    INTEGER :: error
    INTEGER(HSIZE_T), DIMENSION(3) :: dims
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read_real_2D',iftn,idepth)
    IF (.NOT.obj_exists(gid,TRIM(fname)//TRIM(cid),h5err)) THEN
      CALL par%nim_stop('h5_read_real_2D field '//TRIM(fname)//TRIM(cid)        &
                        //' not found')
    ENDIF
    CALL h5dopen_f(gid,TRIM(fname)//TRIM(cid),dsetid,error)
    CALL read_dims(dsetid,dims,h5err)
    CALL read_attribute(dsetid,"poly_degree",pd,h5in,h5err)
    CALL h5dclose_f(dsetid,error)
    nqty=INT(dims(1),KIND(i4))
    mx=INT((dims(2)-1)/pd,KIND(i4))
    my=INT((dims(3)-1)/pd,KIND(i4))
    CALL field%alloc(mx,my,nqty,pd,id,TRIM(fname))
    ii=len(fname)
    IF (ii>=6) THEN
      field%name=TRIM(fname(1:6))
    ELSE
      field%name=TRIM(fname)
    ENDIF
    dn=field%n_side+1
    nx=field%mx*dn
    ny=field%my*dn
    ALLOCATE(tarr(0:nx,0:ny,field%nqty),ttarr(field%nqty,0:nx,0:ny))
    CALL read_h5(gid,TRIM(fname)//TRIM(cid),ttarr,h5in,h5err)
    DO iq=1,field%nqty
      tarr(0:nx,0:ny,iq)=ttarr(iq,0:nx,0:ny)
    ENDDO
    DO iq=1,field%nqty
      field%fs(iq,:,:)=tarr(0:nx:dn,0:ny:dn,iq)
      IF (ALLOCATED(field%fsh)) THEN
        DO is=1,field%n_side
          field%fsh(iq,is,:,:)=tarr(is:nx:dn,0:ny:dn,iq)
          field%fsv(iq,is,:,:)=tarr(0:nx:dn,is:ny:dn,iq)
          ins=(is-1)*field%n_side+1; ine=is*field%n_side
          DO ii=ins,ine
            field%fsi(iq,ii,:,:)=tarr(ii-ins+1:nx:dn,is:ny:dn,iq)
          ENDDO
        ENDDO
      ENDIF
    ENDDO
    DEALLOCATE(tarr,ttarr)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read_real_2D

!-------------------------------------------------------------------------------
!> write data to an h5 file as a single array to enable plotting of the
!  results.
!-------------------------------------------------------------------------------
  SUBROUTINE h5_dump_real_2D(field,fname,gid,cid)
    USE io
    IMPLICIT NONE

    !> field to write
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> field name
    CHARACTER(*), INTENT(IN) :: fname
    !> block id (character format)
    CHARACTER(*), INTENT(IN) :: cid
    !> hdf5 group ID
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: dn,nx,ny,iq,is,ins,ine,ii
    REAL(r8), ALLOCATABLE :: tarr(:,:,:),ttarr(:,:,:)
    INTEGER(HID_T) :: dsetid
    INTEGER :: error
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_dump_real_2D',iftn,idepth)
    h5in%vsMD=TRIM(fname)
    dn=field%n_side+1
    nx=field%mx*dn
    ny=field%my*dn
    ALLOCATE(tarr(0:nx,0:ny,field%nqty))
    ALLOCATE(ttarr(field%nqty,0:nx,0:ny))
    DO iq=1,field%nqty
      tarr(0:nx:dn,0:ny:dn,iq)=field%fs(iq,:,:)
      IF (ALLOCATED(field%fsh)) THEN
        DO is=1,field%n_side
          tarr(is:nx:dn,0:ny:dn,iq)=field%fsh(iq,is,:,:)
          tarr(0:nx:dn,is:ny:dn,iq)=field%fsv(iq,is,:,:)
          ins=(is-1)*field%n_side+1; ine=is*field%n_side
          DO ii=ins,ine
            tarr(ii-ins+1:nx:dn,is:ny:dn,iq)=field%fsi(iq,ii,:,:)
          ENDDO
        ENDDO
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   Reorder indices as compMajorF crashes visit
!   (really this reordering is compMinorF, but visit displays
!   correctly with the default compMinorC).
!-------------------------------------------------------------------------------
    DO iq=1,field%nqty
      ttarr(iq,0:nx,0:ny)=tarr(0:nx,0:ny,iq)
    ENDDO
    CALL dump_h5(gid,TRIM(fname)//TRIM(cid),ttarr,h5in,h5err)
    CALL h5dopen_f(gid,TRIM(fname)//TRIM(cid),dsetid,error)
    CALL write_attribute(dsetid,"field_type","h1_rect_2D_real",h5in,h5err)
    CALL write_attribute(dsetid,"poly_degree",field%pd,h5in,h5err)
    CALL h5dclose_f(dsetid,error)
    DEALLOCATE(tarr,ttarr)
    h5in%vsMD=" "
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_dump_real_2D

!-------------------------------------------------------------------------------
!> broadcast field data to all modes
!-------------------------------------------------------------------------------
  SUBROUTINE bcast_real_2D(field,inode)
    USE pardata_mod
    IMPLICIT NONE

    !> field to broadcast
    CLASS(h1_rect_2D_real), INTENT(INOUT) :: field
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    INTEGER(i4) :: mx,my,nq,pd,id,tsize
    CHARACTER(LEN(field%name)) :: name
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'bcast_real_2D',iftn,idepth)
    IF (par%ilayer==inode) THEN
      mx=field%mx
      my=field%my
      nq=field%nqty
      pd=field%pd
      id=field%id
      name=field%name
      tsize=SIZE(field%title)
    ENDIF
    CALL par%mode_bcast(mx,inode)
    CALL par%mode_bcast(my,inode)
    CALL par%mode_bcast(nq,inode)
    CALL par%mode_bcast(pd,inode)
    CALL par%mode_bcast(id,inode)
    CALL par%mode_bcast(name,LEN(name),inode)
    CALL par%mode_bcast(tsize,inode)
    IF (par%ilayer/=inode)                                                      &
      CALL field%alloc(mx,my,nq,pd,id,name)
    CALL par%mode_bcast(field%fs,SIZE(field%fs),inode)
    IF (ALLOCATED(field%fsh)) THEN
      CALL par%mode_bcast(field%fsh,SIZE(field%fsh),inode)
      CALL par%mode_bcast(field%fsv,SIZE(field%fsv),inode)
      CALL par%mode_bcast(field%fsi,SIZE(field%fsi),inode)
    ENDIF
    CALL par%mode_bcast(field%title(1),LEN(field%title(1))*tsize,inode)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE bcast_real_2D

!-------------------------------------------------------------------------------
!> Gather the element local degree of freedom values
!-------------------------------------------------------------------------------
  SUBROUTINE get_element_dofs(field,iel,fel)
    IMPLICIT NONE

    !> field to get degrees of freedom from
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> element number for requested degrees of freedom
    INTEGER(i4), INTENT(IN) :: iel
    !> degrees of freedom ordered by local basis number (same as alphas)
    REAL(r8), INTENT(OUT) :: fel(field%nbasis,field%nqty)

    INTEGER(i4) :: ix,iy,k,i
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_element_dofs',iftn,idepth)
    iy=(iel-1)/field%mx+1
    ix=MODULO(iel-1,field%mx)+1
!-------------------------------------------------------------------------------
!   First gather the vertex centered dofs
!-------------------------------------------------------------------------------
    fel(1,:)=field%fs(:,ix-1,iy-1)
    fel(2,:)=field%fs(:,ix,iy-1)
    fel(3,:)=field%fs(:,ix-1,iy)
    fel(4,:)=field%fs(:,ix,iy)
!-------------------------------------------------------------------------------
!   Horizontal side centered dofs are next
!-------------------------------------------------------------------------------
    k=5
    DO i=1,field%n_side
      fel(k,:)=field%fsh(:,i,ix,iy-1)
      k=k+1
      fel(k,:)=field%fsh(:,i,ix,iy)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Vertical side centered dofs are next
!-------------------------------------------------------------------------------
    DO i=1,field%n_side
      fel(k,:)=field%fsv(:,i,ix-1,iy)
      k=k+1
      fel(k,:)=field%fsv(:,i,ix,iy)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Interior centered dofs are next
!-------------------------------------------------------------------------------
    DO i=1,field%n_int
      fel(k,:)=field%fsi(:,i,ix,iy)
      k=k+1
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_element_dofs

!-------------------------------------------------------------------------------
!> Evaluate the H1 element basis functions at logical coordinate xi
!-------------------------------------------------------------------------------
  SUBROUTINE h1_rect_2D_bases_val(field,xi,dorder,alpha)
    USE quadrature_mod, ONLY: alpha_real
    IMPLICIT NONE

    !> field for which basis functions are being evaluated
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> logical coordinate at which to evaluate all basis functions
    REAL(r8), DIMENSION(:), INTENT(IN) :: xi
    !> derivative order for basis function evaluation
    INTEGER(i4), INTENT(IN) :: dorder
    !> evaluation of the reference element basis functions at point xi
    !  (expects alpha to be unallocated)
    TYPE(alpha_real), INTENT(OUT) :: alpha

    REAL(r8), DIMENSION(:), ALLOCATABLE :: alx,aly,dalx,daly
    INTEGER(i4) :: k,i,j
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h1_rect_2D_bases_val',iftn,idepth)
!-------------------------------------------------------------------------------
!   Allocate alpha storage
!-------------------------------------------------------------------------------
    ALLOCATE(alpha%alf(field%nbasis))
    ALLOCATE(alx(0:field%pd),aly(0:field%pd))
!-------------------------------------------------------------------------------
!   Evaluate 1D Labatto-Legendre basis functions for the quadrature points
!-------------------------------------------------------------------------------
    CALL field%ll%eval(xi(1),alx)
    CALL field%ll%eval(xi(2),aly)
!-------------------------------------------------------------------------------
!   First evaluate the vertex centered bases
!-------------------------------------------------------------------------------
    alpha%alf(1)=alx(0)*aly(0)
    alpha%alf(2)=alx(field%pd)*aly(0)
    alpha%alf(3)=alx(0)*aly(field%pd)
    alpha%alf(4)=alx(field%pd)*aly(field%pd)
!-------------------------------------------------------------------------------
!   Horizontal side centered bases are next
!-------------------------------------------------------------------------------
    k=5
    DO i=1,field%n_side
      alpha%alf(k)=alx(i)*aly(0)
      k=k+1
      alpha%alf(k)=alx(i)*aly(field%pd)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Vertical side centered bases are next
!-------------------------------------------------------------------------------
    DO j=1,field%n_side
      alpha%alf(k)=alx(0)*aly(j)
      k=k+1
      alpha%alf(k)=alx(field%pd)*aly(j)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Interior centered bases are next
!-------------------------------------------------------------------------------
    DO j=1,field%n_side
      DO i=1,field%n_side
        alpha%alf(k)=alx(i)*aly(j)
        k=k+1
      ENDDO
    ENDDO
    IF (dorder > 0) THEN
!-------------------------------------------------------------------------------
!     Allocate alpha storage
!-------------------------------------------------------------------------------
      ALLOCATE(alpha%aldf(field%nbasis,2))
      alpha%aldf=0._r8
      ALLOCATE(dalx(0:field%pd),daly(0:field%pd))
!-------------------------------------------------------------------------------
!     Evaluate 1D Labatto-Legendre basis function derivatives
!-------------------------------------------------------------------------------
      CALL field%ll%eval_d(xi(1),dalx)
      CALL field%ll%eval_d(xi(2),daly)
!-------------------------------------------------------------------------------
!     First evaluate the vertex centered basis derivatives
!-------------------------------------------------------------------------------
      alpha%aldf(1,1)=dalx(0)*aly(0)
      alpha%aldf(2,1)=dalx(field%pd)*aly(0)
      alpha%aldf(3,1)=dalx(0)*aly(field%pd)
      alpha%aldf(4,1)=dalx(field%pd)*aly(field%pd)
      alpha%aldf(1,2)=alx(0)*daly(0)
      alpha%aldf(2,2)=alx(field%pd)*daly(0)
      alpha%aldf(3,2)=alx(0)*daly(field%pd)
      alpha%aldf(4,2)=alx(field%pd)*daly(field%pd)
!-------------------------------------------------------------------------------
!     Horizontal side centered basis derivatives are next
!-------------------------------------------------------------------------------
      k=5
      DO i=1,field%n_side
        alpha%aldf(k,1)=dalx(i)*aly(0)
        alpha%aldf(k,2)=alx(i)*daly(0)
        k=k+1
        alpha%aldf(k,1)=dalx(i)*aly(field%pd)
        alpha%aldf(k,2)=alx(i)*daly(field%pd)
        k=k+1
      ENDDO
!-------------------------------------------------------------------------------
!     Vertical side centered basis derivatives are next
!-------------------------------------------------------------------------------
      DO j=1,field%n_side
        alpha%aldf(k,1)=dalx(0)*aly(j)
        alpha%aldf(k,2)=alx(0)*daly(j)
        k=k+1
        alpha%aldf(k,1)=dalx(field%pd)*aly(j)
        alpha%aldf(k,2)=alx(field%pd)*daly(j)
        k=k+1
      ENDDO
!-------------------------------------------------------------------------------
!     Interior centered basis derivatives are next
!-------------------------------------------------------------------------------
      DO j=1,field%n_side
        DO i=1,field%n_side
          alpha%aldf(k,1)=dalx(i)*aly(j)
          alpha%aldf(k,2)=alx(i)*daly(j)
          k=k+1
        ENDDO
      ENDDO
      DEALLOCATE(dalx,daly)
    ENDIF
    DEALLOCATE(alx,aly)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h1_rect_2D_bases_val

!-------------------------------------------------------------------------------
!> Evaluate the H1 element basis functions at all logical coordinates in xi
!-------------------------------------------------------------------------------
  SUBROUTINE h1_rect_2D_bases_many(field,xi,dorder,alpha)
    USE quadrature_mod, ONLY: qalpha_real
    IMPLICIT NONE

    !> field for which basis functions are being evaluated
    CLASS(h1_rect_2D_real), INTENT(IN) :: field
    !> logical coordinates at which to evaluate all basis functions
    !  (the first index is the point ordering and the second is the coordinate)
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: xi
    !> derivative order for basis function evaluation
    INTEGER(i4), INTENT(IN) :: dorder
    !> evaluation of the reference element basis functions at all points in xi
    !  (expects alpha to be unallocated)
    TYPE(qalpha_real), INTENT(OUT) :: alpha

    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: alx,aly,dalx,daly
    INTEGER(i4) :: ng,k,i,j
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h1_rect_2D_bases_many',iftn,idepth)
!-------------------------------------------------------------------------------
!   Allocate alpha storage
!-------------------------------------------------------------------------------
    ng=SIZE(xi,1)
    ALLOCATE(alpha%alf(ng,field%nbasis))
    alpha%alf=0._r8
    ALLOCATE(alx(ng,0:field%pd),aly(ng,0:field%pd))
!-------------------------------------------------------------------------------
!   Evaluate 1D Labatto-Legendre basis functions for the quadrature points
!-------------------------------------------------------------------------------
    CALL field%ll%eval(xi(:,1),alx)
    CALL field%ll%eval(xi(:,2),aly)
!-------------------------------------------------------------------------------
!   First evaluate the vertex centered bases and their derivatives
!-------------------------------------------------------------------------------
    alpha%alf(:,1)=alx(:,0)*aly(:,0)
    alpha%alf(:,2)=alx(:,field%pd)*aly(:,0)
    alpha%alf(:,3)=alx(:,0)*aly(:,field%pd)
    alpha%alf(:,4)=alx(:,field%pd)*aly(:,field%pd)
!-------------------------------------------------------------------------------
!   Horizontal side centered bases and their derivatives are next
!-------------------------------------------------------------------------------
    k=5
    DO i=1,field%n_side
      alpha%alf(:,k)=alx(:,i)*aly(:,0)
      k=k+1
      alpha%alf(:,k)=alx(:,i)*aly(:,field%pd)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Vertical side centered bases and their derivatives are next
!-------------------------------------------------------------------------------
    DO j=1,field%n_side
      alpha%alf(:,k)=alx(:,0)*aly(:,j)
      k=k+1
      alpha%alf(:,k)=alx(:,field%pd)*aly(:,j)
      k=k+1
    ENDDO
!-------------------------------------------------------------------------------
!   Interior centered bases and their derivatives are next
!-------------------------------------------------------------------------------
    DO j=1,field%n_side
      DO i=1,field%n_side
        alpha%alf(:,k)=alx(:,i)*aly(:,j)
        k=k+1
      ENDDO
    ENDDO
    IF (dorder > 0) THEN
      ALLOCATE(alpha%aldf(ng,field%nbasis,2))
      alpha%aldf=0._r8
      ALLOCATE(dalx(ng,0:field%pd),daly(ng,0:field%pd))
!-------------------------------------------------------------------------------
!     Evaluate 1D Labatto-Legendre basis function derivatives
!-------------------------------------------------------------------------------
      CALL field%ll%eval_d(xi(:,1),dalx)
      CALL field%ll%eval_d(xi(:,2),daly)
!-------------------------------------------------------------------------------
!     First evaluate the vertex centered basis derivatives
!-------------------------------------------------------------------------------
      alpha%aldf(:,1,1)=dalx(:,0)*aly(:,0)
      alpha%aldf(:,2,1)=dalx(:,field%pd)*aly(:,0)
      alpha%aldf(:,3,1)=dalx(:,0)*aly(:,field%pd)
      alpha%aldf(:,4,1)=dalx(:,field%pd)*aly(:,field%pd)
      alpha%aldf(:,1,2)=alx(:,0)*daly(:,0)
      alpha%aldf(:,2,2)=alx(:,field%pd)*daly(:,0)
      alpha%aldf(:,3,2)=alx(:,0)*daly(:,field%pd)
      alpha%aldf(:,4,2)=alx(:,field%pd)*daly(:,field%pd)
!-------------------------------------------------------------------------------
!     Horizontal side centered bases and their derivatives are next
!-------------------------------------------------------------------------------
      k=5
      DO i=1,field%n_side
        alpha%aldf(:,k,1)=dalx(:,i)*aly(:,0)
        alpha%aldf(:,k,2)=alx(:,i)*daly(:,0)
        k=k+1
        alpha%aldf(:,k,1)=dalx(:,i)*aly(:,field%pd)
        alpha%aldf(:,k,2)=alx(:,i)*daly(:,field%pd)
        k=k+1
      ENDDO
!-------------------------------------------------------------------------------
!     Vertical side centered basis derivatives are next
!-------------------------------------------------------------------------------
      DO j=1,field%n_side
        alpha%aldf(:,k,1)=dalx(:,0)*aly(:,j)
        alpha%aldf(:,k,2)=alx(:,0)*daly(:,j)
        k=k+1
        alpha%aldf(:,k,1)=dalx(:,field%pd)*aly(:,j)
        alpha%aldf(:,k,2)=alx(:,field%pd)*daly(:,j)
        k=k+1
      ENDDO
!-------------------------------------------------------------------------------
!     Interior centered basis derivatives are next
!-------------------------------------------------------------------------------
      DO j=1,field%n_side
        DO i=1,field%n_side
          alpha%aldf(:,k,1)=dalx(:,i)*aly(:,j)
          alpha%aldf(:,k,2)=alx(:,i)*daly(:,j)
          k=k+1
        ENDDO
      ENDDO
      DEALLOCATE(dalx,daly)
    ENDIF
    DEALLOCATE(alx,aly)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h1_rect_2D_bases_many

END MODULE h1_rect_2D_real_mod
