## Read the [Documentation](https://nimrodteam.gitlab.io/open/nimrod-aai/page/index.html)

## Visit the [NIMROD Team website](https://nimrodteam.org)

## Join us on [Slack](https://join.slack.com/t/nimrod-mhdcode/signup)

![pipeline status](https://gitlab.com/NIMRODteam/open/nimrod-aai/badges/main/pipeline.svg)
![code coverage](https://gitlab.com/NIMRODteam/open/nimrod-aai/badges/main/coverage.svg)
![gcc warnings](https://gitlab.com/NIMRODteam/open/nimrod-aai/-/jobs/artifacts/main/raw/build-cov/gccwarn.svg?job=coverage)
