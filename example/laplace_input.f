!-------------------------------------------------------------------------------
!! define laplace input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module that defines laplace input
!-------------------------------------------------------------------------------
MODULE laplace_input
  USE local
  IMPLICIT NONE

  PUBLIC

!-------------------------------------------------------------------------------
!===============================================================================
!: namelist laplace_input
!===============================================================================
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* number of time steps
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nstep=10
!-------------------------------------------------------------------------------
!* time step length, units of time
!-------------------------------------------------------------------------------
  REAL(r8) :: dt=1._r8
!-------------------------------------------------------------------------------
!* diffusion coefficient; units of length^2/time
!-------------------------------------------------------------------------------
  REAL(r8) :: diff=1._r8
!-------------------------------------------------------------------------------
!> if true apply a dirichlet boundary condition, otherwise a Neumann natural
! boundary condition is applied that restricts the normal component of the
! gradient of the field to zero at the boundary.
!-------------------------------------------------------------------------------
  LOGICAL :: dirichlet_boundary_condition=.FALSE.
!-------------------------------------------------------------------------------
!* initial dump file name
!-------------------------------------------------------------------------------
  CHARACTER(64) :: dump_file='dump.00000.h5'
!-------------------------------------------------------------------------------
!* implicit centering parameter for the diffusive term
!-------------------------------------------------------------------------------
  REAL(r8) :: theta=0.5_r8
!-------------------------------------------------------------------------------
!> The integration_formula lets you choose the type of numerical integration,
! either Gaussian (no end-point abscissas) or Lobatto (abscissas located at the
! GLL nodes if ngr=2, including end points).
!-------------------------------------------------------------------------------
  CHARACTER(8) :: int_formula='gaussian' ! or 'lobatto'
!-------------------------------------------------------------------------------
!> `ngr` sets the number of Gaussian quadrature points used in each direction of
! a rectangular cell. The number of quadrature points is set by the function:
! `#_rblock_quad_pts_per_dir=ngr+poly_degree-1`.
! The number of quadrature points automatically increases with the polynomial
! degree of the expansions for the dependent variables. `ngr<2` may produce
! singular matrices.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: ngr=2
!-------------------------------------------------------------------------------
!* if true, eliminate element interior DOF before solving the system.
!-------------------------------------------------------------------------------
  LOGICAL :: static_condensation=.FALSE.
!-------------------------------------------------------------------------------
!> if true apply the jacobian transformation to test/trial functions on-the-fly,
! otherwise precompute it.
!-------------------------------------------------------------------------------
  LOGICAL :: compute_integrand_alpha=.FALSE.
!-------------------------------------------------------------------------------
!* if true, force all blocks to use GPU-enabled OpenACC types, if available.
!-------------------------------------------------------------------------------
  LOGICAL :: on_gpu=.FALSE.
!-------------------------------------------------------------------------------
!* wait on GPU when timing, better kernel timing but more overhead.
!-------------------------------------------------------------------------------
  LOGICAL :: timer_directive_wait=.FALSE.
!-------------------------------------------------------------------------------
!> if true, reset timers after the first time step to avoid initialization
! costs.
!-------------------------------------------------------------------------------
  LOGICAL :: reset_timers_at_step2=.FALSE.
!-------------------------------------------------------------------------------
! TODO: move to solver_input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* maximum iterations per time step
!-------------------------------------------------------------------------------
  INTEGER(i4) :: maxit=100
!-------------------------------------------------------------------------------
!* solver tolerance
!-------------------------------------------------------------------------------
  REAL(r8) :: tol=1.e-8_r8
!-------------------------------------------------------------------------------
!* preconditioner strategy, other option is none
!-------------------------------------------------------------------------------
  CHARACTER(64) :: precon_type='jacobi'
!-------------------------------------------------------------------------------
!===============================================================================
!: computed quantities
!===============================================================================
!-------------------------------------------------------------------------------
CONTAINS

!-------------------------------------------------------------------------------
!> compute secondary parameters and check input
!-------------------------------------------------------------------------------
  SUBROUTINE laplace_compute
    USE pardata_mod
    IMPLICIT NONE

    ! nothing to do
  END SUBROUTINE laplace_compute

!-------------------------------------------------------------------------------
!> open and read the namelist input.
!-------------------------------------------------------------------------------
  SUBROUTINE laplace_read_namelist(infile)
    USE local
    USE fourier_input_mod
    USE grid_input_mod
    USE read_namelist_mod
    USE parallel_input_mod
    IMPLICIT NONE

    !> input file name
    CHARACTER(*), INTENT(IN) :: infile

    INTEGER(i4) :: read_stat
    CHARACTER(64) :: tempfile
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
    NAMELIST/laplace_input/nstep,dt,diff,dirichlet_boundary_condition,dump_file,&
          theta,int_formula,ngr,static_condensation,compute_integrand_alpha,    &
          on_gpu,timer_directive_wait,reset_timers_at_step2,maxit,tol,          &
          precon_type
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   Open and remove comments from input file and put into temporary file.
!-------------------------------------------------------------------------------
    tempfile='temp'//ADJUSTL(infile)
    CALL rmcomment(infile,tempfile)
    OPEN(UNIT=in_unit,FILE=tempfile,STATUS='OLD',POSITION='REWIND')
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
    CALL position_at_namelist('laplace_input', read_stat)
    IF (read_stat==0) THEN
      READ(UNIT=in_unit,NML=laplace_input,IOSTAT=read_stat)
      IF (read_stat/=0) THEN
        CALL print_namelist_error('laplace_input')
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   close and delete temporary input file.
!-------------------------------------------------------------------------------
    CLOSE(in_unit,STATUS='DELETE')
!-------------------------------------------------------------------------------
!   compute secondary parameters.
!-------------------------------------------------------------------------------
    CALL laplace_compute
!-------------------------------------------------------------------------------
!   read other namelists.
!-------------------------------------------------------------------------------
    CALL fourier_in%read_namelist(infile)
    CALL grid_in%read_namelist(infile)
    CALL parallel_in%read_namelist(infile)
  END SUBROUTINE laplace_read_namelist

!-------------------------------------------------------------------------------
!> read h5 namelist from dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE laplace_h5read
    USE io
    IMPLICIT NONE

    INTEGER(HID_T) :: gid

    CALL open_group(rootgid,"laplace",gid,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL read_attribute(gid,"nstep",nstep,h5in,h5err)
    CALL read_attribute(gid,"dt",dt,h5in,h5err)
    CALL read_attribute(gid,"diff",diff,h5in,h5err)
    CALL read_attribute(gid,"dirichlet_boundary_condition",                     &
                        dirichlet_boundary_condition,h5in,h5err)
    CALL read_attribute(gid,"dump_file",dump_file,h5in,h5err)
    CALL read_attribute(gid,"theta",theta,h5in,h5err)
    CALL read_attribute(gid,"int_formula",int_formula,h5in,h5err)
    CALL read_attribute(gid,"ngr",ngr,h5in,h5err)
    CALL read_attribute(gid,"static_condensation",                              &
                        static_condensation,h5in,h5err)
    CALL read_attribute(gid,"compute_integrand_alpha",                          &
                        compute_integrand_alpha,h5in,h5err)
    CALL read_attribute(gid,"on_gpu",on_gpu,h5in,h5err)
    CALL read_attribute(gid,"timer_directive_wait",                             &
                        timer_directive_wait,h5in,h5err)
    CALL read_attribute(gid,"reset_timers_at_step2",                            &
                        reset_timers_at_step2,h5in,h5err)
    CALL read_attribute(gid,"maxit",maxit,h5in,h5err)
    CALL read_attribute(gid,"tol",tol,h5in,h5err)
    CALL read_attribute(gid,"precon_type",precon_type,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL close_group("laplace",gid,h5err)
  END SUBROUTINE laplace_h5read

!-------------------------------------------------------------------------------
!> write h5 namelist to dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE laplace_h5write
    USE io
    IMPLICIT NONE

    INTEGER(HID_T) :: gid

    CALL make_group(rootgid,"laplace",gid,h5in,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL write_attribute(gid,"nstep",nstep,h5in,h5err)
    CALL write_attribute(gid,"dt",dt,h5in,h5err)
    CALL write_attribute(gid,"diff",diff,h5in,h5err)
    CALL write_attribute(gid,"dirichlet_boundary_condition",                    &
                         dirichlet_boundary_condition,h5in,h5err)
    CALL write_attribute(gid,"dump_file",dump_file,h5in,h5err)
    CALL write_attribute(gid,"theta",theta,h5in,h5err)
    CALL write_attribute(gid,"int_formula",int_formula,h5in,h5err)
    CALL write_attribute(gid,"ngr",ngr,h5in,h5err)
    CALL write_attribute(gid,"static_condensation",                             &
                         static_condensation,h5in,h5err)
    CALL write_attribute(gid,"compute_integrand_alpha",                         &
                         compute_integrand_alpha,h5in,h5err)
    CALL write_attribute(gid,"on_gpu",on_gpu,h5in,h5err)
    CALL write_attribute(gid,"timer_directive_wait",                            &
                         timer_directive_wait,h5in,h5err)
    CALL write_attribute(gid,"reset_timers_at_step2",                           &
                         reset_timers_at_step2,h5in,h5err)
    CALL write_attribute(gid,"maxit",maxit,h5in,h5err)
    CALL write_attribute(gid,"tol",tol,h5in,h5err)
    CALL write_attribute(gid,"precon_type",precon_type,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL close_group("laplace",gid,h5err)
  END SUBROUTINE laplace_h5write

!-------------------------------------------------------------------------------
!> broadcast info read by proc 0 out of nimrod.in (namelist reads) to all
! processors. every quantity in input module is broadcast in case it was read.
!-------------------------------------------------------------------------------
  SUBROUTINE laplace_bcast_input
    USE fourier_input_mod
    USE grid_input_mod
    USE pardata_mod
    USE parallel_input_mod
    IMPLICIT NONE

    INTEGER(i4) :: nn
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   laplace_input
!-------------------------------------------------------------------------------
    CALL par%all_bcast(nstep,0)
    CALL par%all_bcast(dt,0)
    CALL par%all_bcast(diff,0)
    CALL par%all_bcast(dirichlet_boundary_condition,0)
    nn=LEN(dump_file)
    CALL par%all_bcast(dump_file,nn,0)
    CALL par%all_bcast(theta,0)
    nn=LEN(int_formula)
    CALL par%all_bcast(int_formula,nn,0)
    CALL par%all_bcast(ngr,0)
    CALL par%all_bcast(static_condensation,0)
    CALL par%all_bcast(compute_integrand_alpha,0)
    CALL par%all_bcast(on_gpu,0)
    CALL par%all_bcast(timer_directive_wait,0)
    CALL par%all_bcast(reset_timers_at_step2,0)
    CALL par%all_bcast(maxit,0)
    CALL par%all_bcast(tol,0)
    nn=LEN(precon_type)
    CALL par%all_bcast(precon_type,nn,0)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
    CALL laplace_compute
!-------------------------------------------------------------------------------
!   broadcast other namelists.
!-------------------------------------------------------------------------------
    CALL fourier_in%bcast_input
    CALL grid_in%bcast_input
    CALL parallel_in%bcast_input
  END SUBROUTINE laplace_bcast_input

END MODULE laplace_input
