!-------------------------------------------------------------------------------
!* Function definition for an initial condition
!-------------------------------------------------------------------------------
MODULE init_function_mod
  USE function_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: init_function_real,init_function_comp

  TYPE, EXTENDS(abstract_function_real) :: init_function_real
    REAL(r8) :: kx = twopi
    REAL(r8) :: ky = twopi
    INTEGER(i4) :: bessel_order = 0
    CHARACTER(64) :: init_type = "waves"
  CONTAINS

    PROCEDURE, PASS(func) :: eval => eval_real
  END TYPE init_function_real

  TYPE, EXTENDS(abstract_function_comp) :: init_function_comp
    REAL(r8), ALLOCATABLE :: kx(:)
    REAL(r8) :: ky = twopi
    INTEGER(i4) :: bessel_order = 0
    CHARACTER(64) :: init_type = "waves"
  CONTAINS

    PROCEDURE, PASS(func) :: eval => eval_comp
  END TYPE init_function_comp

CONTAINS

  SUBROUTINE eval_real(func, in_var, out_var)
    IMPLICIT NONE

    CLASS(init_function_real), INTENT(IN) :: func
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    REAL(r8), DIMENSION(:), INTENT(OUT) :: out_var

    REAL(r8), PARAMETER :: sqrt2=SQRT(2._r8)
    REAL(r8) :: rminor=1._r8,theta=0._r8

    SELECT CASE(func%init_type)
    CASE ("one")
      out_var=1_i4
    CASE ("waves")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        out_var=SIN(func%kx*in_var(1))
      CASE (2)
        out_var=SIN(func%kx*in_var(1))*COS(func%ky*in_var(2))
      END SELECT
    CASE ("sine_waves")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        out_var=SIN(func%kx*in_var(1))
      CASE (2)
        out_var=SIN(func%kx*in_var(1))*SIN(func%ky*in_var(2))
      END SELECT
    CASE ("bessel_cosine_rect")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        out_var=BESSEL_JN(func%bessel_order,func%kx*in_var(1))
      CASE (2)
        out_var=BESSEL_JN(func%bessel_order,func%kx*in_var(1))                  &
                *COS(func%ky*in_var(2))
      END SELECT
    CASE ("bessel_cosine_circ")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        rminor=in_var(1)
        theta=pi/2._r8
      CASE (2)
        rminor=SQRT(in_var(1)**2+in_var(2)**2)
        theta=ATAN2(in_var(2),in_var(1))
      END SELECT
      out_var=BESSEL_JN(func%bessel_order,func%kx*rminor)                       &
              *COS(func%ky*theta/twopi)
    CASE DEFAULT
      CALL par%nim_stop('Unrecognized init_type')
    END SELECT
  END SUBROUTINE eval_real

  SUBROUTINE eval_comp(func, in_var, out_var)
    IMPLICIT NONE

    CLASS(init_function_comp), INTENT(IN) :: func
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    COMPLEX(r8), DIMENSION(:,:), INTENT(OUT) :: out_var

    REAL(r8) :: rminor=1._r8,theta=0._r8
    INTEGER(i4) :: imode

    SELECT CASE(func%init_type)
    CASE ("one")
      out_var=1_i4
    CASE ("waves")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        out_var=(1,1)*SIN(func%kx(1)*in_var(1))
      CASE (2)
        out_var=(1,1)*SIN(func%kx(1)*in_var(1))*COS(func%ky*in_var(2))
      END SELECT
    CASE ("sine_waves")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        out_var=(1,1)*SIN(func%kx(1)*in_var(1))
      CASE (2)
        out_var=(1,1)*SIN(func%kx(1)*in_var(1))*SIN(func%ky*in_var(2))
      END SELECT
    CASE ("bessel_cosine_rect")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        DO imode=1,par%nmodes
          out_var(:,imode)=                                                     &
            (1,1)*BESSEL_JN(par%nindex(imode),func%kx(imode)*in_var(1))
        ENDDO
      CASE (2)
        DO imode=1,par%nmodes
          out_var(:,imode)=                                                     &
            (1,1)*BESSEL_JN(par%nindex(imode),func%kx(imode)*in_var(1))         &
                 *COS(func%ky*in_var(2))
        ENDDO
      END SELECT
    CASE ("bessel_cosine_circ")
      SELECT CASE (SIZE(in_var))
      CASE (1)
        rminor=in_var(1)
        theta=pi/2._r8
      CASE (2)
        rminor=SQRT(in_var(1)**2+in_var(2)**2)
        theta=ATAN2(in_var(2),in_var(1))
      END SELECT
      out_var=(1,1)*BESSEL_JN(func%bessel_order,func%kx(1)*rminor)              &
                   *COS(func%ky*theta/twopi)
    CASE DEFAULT
      CALL par%nim_stop('Unrecognized init_type')
    END SELECT
  END SUBROUTINE eval_comp

END MODULE init_function_mod
