! Decay of a Bessel function in the x-directed radial direction with radial
! Dirichlet BCs on an unstructured circular mesh in linear geometry.
!
! This big example is for performance testing and runs with many features
! of the code.
!
! This case is not converging -- need a better meshing algorithm!
&grid_input
  gridshape='circ'
  torgeom=F
  xo=0
  mx=192
  my=192
  mr=144
  poly_degree=6
  nxbl=1
  nybl=1
  nrbl=1
  ntbl=3
  ! For cpu run set below for more parallelism
  !nxbl=16
  !nybl=16
  !nrbl=16
  !ntbl=48
/
&exset_input
  field_type='real'
  init_type='bessel_cosine_circ'
  nx=1.
  ny=1.
  bessel_order=1
  istep0=0
  nqty=1
/
&parallel_input
  decompflag=0
  single_stream=F
/
&laplace_input
  nstep=5
  dt=0.1
  diff=1.
  dirichlet_boundary_condition=T
  dump_file='dump.00000.h5'
  theta=0.5
  int_formula='gaussian'
  ngr=2
  static_condensation=T
  ! For GPU runs
  on_gpu=T
  timer_directive_wait=T ! For kernel timings, faster overall if false
  ! For CPU runs
  !on_gpu=F
  !timer_directive_wait=F
  compute_integrand_alpha=T
  reset_timers_at_step2=T ! Don't consider initialization costs
  maxit=300
  tol=1.e-8
  precon_type='jacobi'
/
/
