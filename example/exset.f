!-------------------------------------------------------------------------------
!! setup an initial condition for the Laplace test
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* setup an initial condition for the Laplace test
!-------------------------------------------------------------------------------
PROGRAM exset
  USE dump_mod
  USE exset_input
  USE fourier_input_mod
  USE gblock_mod
  USE grid_input_mod
  USE grid_utils_mod
  USE h1_rect_2D_mod
  USE init_function_mod
  USE io
  USE local
  USE nodal_mod
  USE pardata_mod
  USE seam_mod
  USE timer_mod
  IMPLICIT NONE

  CHARACTER(128) :: input_file='example.in'
  INTEGER(i4) :: nargs,im,ibl,imode
  TYPE(block_storage), ALLOCATABLE :: bl(:)
  TYPE(seam_type) :: seam
  TYPE(rfem_storage), ALLOCATABLE, TARGET :: rfield(:)
  TYPE(cfem_storage), ALLOCATABLE, TARGET :: cfield(:)
  TYPE(init_function_real) :: init_func_real
  TYPE(init_function_comp) :: init_func_comp
  TYPE(field_list_pointer) :: io_field_list(1)
  REAL(r8) :: bessel_root(2),bessel_val(2),kfac
  REAL(r8), PARAMETER :: sqrt2=SQRT(2._r8)
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

  ofname='exset.out'
  CALL par%init
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('exset','exset',iftn,idepth)
!-------------------------------------------------------------------------------
! determine configuration from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs==1) THEN
    CALL get_command_argument(1,input_file)
  ENDIF
  IF (input_file=='-h'.OR.nargs>1) THEN
    CALL print_usage
  ENDIF
!-------------------------------------------------------------------------------
! read input
!-------------------------------------------------------------------------------
  CALL par%nim_write('Reading input.')
  CALL exset_read_namelist(input_file)
!-------------------------------------------------------------------------------
! check input
!-------------------------------------------------------------------------------
  IF (field_type/='comp'.AND.fourier_in%nmodes_total>1)                         &
    CALL par%nim_stop('nmodes>1 requires field_type=comp',clean_shutdown=.TRUE.)
!-------------------------------------------------------------------------------
! initialize the wave number arrays
!-------------------------------------------------------------------------------
  ALLOCATE(par%keff_total(fourier_in%nmodes_total))
  ALLOCATE(par%nindex_total(fourier_in%nmodes_total))
  DO im=1,fourier_in%nmodes_total
    par%nindex_total(im)=(im-1)*grid_in%zperiod
  ENDDO
  IF (fourier_in%lin_nmax>0)                                                    &
    par%nindex_total=par%nindex_total                                           &
                     +fourier_in%lin_nmax-fourier_in%nmodes_total-1
  IF (ANY(par%nindex_total<0))                                                  &
    CALL par%nim_stop('Negative Fourier mode, check input!')
  par%keff_total=par%nindex_total*twopi
  IF (.NOT.grid_in%torgeom) par%keff_total=par%keff_total/grid_in%per_length
!-------------------------------------------------------------------------------
! initialize pardata
!-------------------------------------------------------------------------------
  CALL par%nim_write('Setting up the mesh.')
  CALL par%set_decomp(fourier_in%nmodes_total,grid_in%nbl_total)
!-------------------------------------------------------------------------------
! set the perturbation parameters
!-------------------------------------------------------------------------------
  init_func_real%init_type=init_type
  init_func_real%kx=nx*twopi
  init_func_real%ky=ny*twopi
  init_func_real%bessel_order=bessel_order
  init_func_comp%init_type=init_type
  ALLOCATE(init_func_comp%kx(par%nmodes))
  init_func_comp%kx=nx*twopi
  init_func_comp%ky=ny*twopi
  init_func_comp%bessel_order=bessel_order
!-------------------------------------------------------------------------------
! find the zeros of the bessel function if using bessel init type.  Use this to
! set kx appropriately.  Hijack the keff array to be the total k**2 of the
! perturbation. The laplace example resets the array.
!-------------------------------------------------------------------------------
  bessel_root(1)=0._r8
  bessel_root(2)=1._r8
  bessel_val=0._r8
  IF (init_type(1:6)=="bessel") THEN
    CALL par%nim_write('Finding Bessel function cutoff radii for grid.')
    DO imode=1,par%nmodes
      IF (init_type=="bessel_cosine_rect".AND.field_type=="comp")               &
        init_func_real%bessel_order=par%nindex_total(imode)
      ! Use 4th Bessel root, varies by order
      SELECT CASE(init_func_real%bessel_order)
      CASE(0) ! J0, n=0
        bessel_root(2)=11.7915_r8
      CASE(1) ! J1, n=1
        bessel_root(2)=13.3237_r8
      CASE(2) ! J2, n=1
        bessel_root(2)=14.7960_r8
      CASE(3) ! J3, n=3
        bessel_root(2)=16.2235_r8
      CASE(4) ! J4, n=4
        bessel_root(2)=17.6160_r8
      CASE(5) ! J5, n=5
        bessel_root(2)=18.980_r8
      CASE DEFAULT
        bessel_root(2)=10._r8
      END SELECT
      bessel_root(2)=bessel_root(2)/init_func_real%kx
      CALL init_func_real%find_root(bessel_root(2:2),bessel_val(2:2))
      kfac=bessel_root(2)/grid_in%xmax
      IF (init_type=="bessel_cosine_rect") THEN
        ! Fourier variation linked to Bessel ftn, ky independent
        par%keff_total(imode)=(init_func_real%kx*kfac)**2+(ny*twopi)**2
      ELSE ! bessel_cosine_circ
        ! Fourier variation independent
        par%keff_total(imode)=                                                  &
          (init_func_real%kx*kfac)**2+par%keff_total(imode)**2
      ENDIF
      init_func_comp%kx(imode)=init_func_real%kx*kfac
      IF (grid_in%annulus) THEN
        init_func_real%kx=init_func_real%kx*kfac
        ! 1st zero crossing for J0
        bessel_root(1)=2.4048_r8/init_func_real%kx
        CALL init_func_real%find_root(bessel_root(1:1),bessel_val(1:1))
        grid_in%xmin=bessel_root(1) ! set interior to match zero
      ELSE IF (field_type=="real") THEN
        init_func_real%kx=init_func_real%kx*kfac
      ENDIF
    ENDDO
  ELSE ! wave initializations
    DO imode=1,par%nmodes
      par%keff_total(imode)=(nx*twopi)**2+(ny*twopi)**2+par%keff_total(imode)**2
    ENDDO
  ENDIF
!-------------------------------------------------------------------------------
! setup the grid
!-------------------------------------------------------------------------------
  CALL setup_bl_mesh(bl,seam)
!-------------------------------------------------------------------------------
! set fields
!-------------------------------------------------------------------------------
  CALL par%nim_write('Initializing fields.')
  ALLOCATE(rfield(grid_in%nbl_total),cfield(grid_in%nbl_total))
  DO ibl=1,grid_in%nbl_total
    SELECT TYPE(mesh => bl(ibl)%b%mesh)
    CLASS IS (h1_rect_2D_real)
      SELECT CASE(field_type)
      CASE("real")
        ALLOCATE(h1_rect_2D_real::rfield(ibl)%f)
        SELECT TYPE(fld => rfield(ibl)%f)
        TYPE IS (h1_rect_2D_real)
          CALL fld%alloc(mesh%mx,mesh%my,nqty,grid_in%poly_degree,ibl,"field")
        END SELECT
        CALL rfield(ibl)%f%collocation(init_func_real,bl(ibl)%b%mesh)
      CASE("comp")
        ALLOCATE(h1_rect_2D_comp::cfield(ibl)%f)
        SELECT TYPE(fld => cfield(ibl)%f)
        TYPE IS (h1_rect_2D_comp)
          CALL fld%alloc(mesh%mx,mesh%my,nqty,fourier_in%nmodes_total,          &
                         grid_in%poly_degree,ibl,"field")
        END SELECT
        CALL cfield(ibl)%f%collocation(init_func_comp,bl(ibl)%b%mesh)
      END SELECT
    END SELECT
  ENDDO
!-------------------------------------------------------------------------------
! write dump file
!-------------------------------------------------------------------------------
  CALL par%nim_write('Writing initial dump file.')
  io_field_list(1)%name="field"
  SELECT CASE(field_type)
  CASE("real")
    io_field_list(1)%p=>rfield
  CASE("comp")
    io_field_list(1)%p=>cfield
  END SELECT
  CALL dump_write(0._r8,istep0,bl,seam,io_field_list)
!-------------------------------------------------------------------------------
! deallocate and finalize
!-------------------------------------------------------------------------------
  CALL seam%dealloc
  DO ibl=1,grid_in%nbl_total
    IF (ALLOCATED(rfield(ibl)%f)) CALL rfield(ibl)%f%dealloc
    IF (ALLOCATED(cfield(ibl)%f)) CALL cfield(ibl)%f%dealloc
    CALL bl(ibl)%b%dealloc
  ENDDO
  DEALLOCATE(bl,rfield,cfield,init_func_comp%kx)
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Initialize a field f with a given grid decomposition')
    CALL par%nim_write('Usage: ./exset <input namelist file>')
    CALL par%nim_write(                                                         &
      'Default input namelist file name is example.in if not specified')
    CALL par%nim_stop('check input',clean_shutdown=.TRUE.)
  END SUBROUTINE print_usage

END PROGRAM exset
