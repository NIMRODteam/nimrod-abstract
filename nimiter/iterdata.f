!-------------------------------------------------------------------------------
!! data structures for iter routine input/output
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* data structures for iter routine input/output
!-------------------------------------------------------------------------------
MODULE iterdata_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: iterdata

!-------------------------------------------------------------------------------
!> Type that defines a container for data for iter routine input/output
!-------------------------------------------------------------------------------
  TYPE :: iterdata
!-------------------------------------------------------------------------------
!   Input
!-------------------------------------------------------------------------------
    !* solver tolerance
    REAL(r8) :: tol
    !* maximum number of iterations
    INTEGER(i4) :: maxit
    !* report iteration information
    LOGICAL :: report=.TRUE.
!-------------------------------------------------------------------------------
!   Diagnostic output
!-------------------------------------------------------------------------------
    !* number iterations needed
    INTEGER(i4) :: its
    !* rhs norm
    REAL(r8) :: rhs_norm
    !* solution error
    REAL(r8) :: err
    !* initial guess used
    CHARACTER(8) :: seed
    !* initial solution error
    REAL(r8) :: er0
    !* if converged on last solve
    LOGICAL :: converged
  END TYPE iterdata

END MODULE iterdata_mod
