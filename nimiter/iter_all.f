!-------------------------------------------------------------------------------
!! loads modules associated with all iterative solvers to aid dynamic swapping
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* loads modules associated with all iterative solvers to aid dynamic swapping
!-------------------------------------------------------------------------------
MODULE iter_all_mod
  USE iter_cg_real_mod
  USE iter_cg_cv1m_mod
  USE iter_cg_comp_mod
  IMPLICIT NONE

END MODULE iter_all_mod
