!-------------------------------------------------------------------------------
!! loads modules associated with all preconditioners to aid dynamic swapping
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* loads modules associated with all preconditioners to aid dynamic swapping
!-------------------------------------------------------------------------------
MODULE precon_all_mod
  USE precon_none_real_mod
  USE precon_none_cv1m_mod
  USE precon_none_comp_mod
  USE precon_jacobi_real_mod
  USE precon_jacobi_cv1m_mod
  USE precon_jacobi_comp_mod
  IMPLICIT NONE

END MODULE precon_all_mod
